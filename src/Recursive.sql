Common table expression

WITH LINK(objectid, NAME, parent, LEVEL) AS (
    SELECT objectid, name, parent, 0 FROM jmenu WHERE PARENT IS NULL
    UNION ALL
    SELECT jmenu.objectid, jmenu.name, jmenu.parent, LEVEL + 1
    FROM LINK INNER JOIN jmenu ON LINK.objectid = jmenu.PARENT
)
SELECT objectid, NAME, parent, level FROM LINK WHERE NAME IS NOT NULL ORDER BY name;



WITH TMPMENU(objectid, NAME, parent, accessrule, LEVEL) AS (
    SELECT objectid, name, parent, accessrule, 0 FROM jmenu WHERE PARENT IS NULL and ACCESSRULE != 'PRIVATE'
    UNION ALL
    SELECT jmenu.objectid, jmenu.name, jmenu.parent, jmenu.accessrule, LEVEL + 1
    FROM TMPMENU INNER JOIN jmenu ON TMPMENU.objectid = jmenu.PARENT 
    WHERE jmenu.ACCESSRULE != 'PRIVATE'
)
SELECT objectid FROM TMPMENU WHERE NAME IS NOT NULL ORDER BY objectId;


JMENU
@jclass(cid=JMetaKeys.JMENU, did=JDomainDef.SYSTEM_ID, name=JMenu.NAME,
summaryFilter="if(!juser.administrator){ "+
		"\"objectId in ( WITH RECURSIVE TMPMENU(objectId, NAME, parent, accessrule, LEVEL) AS ( "+
		"	    SELECT objectId, name, parent, accessrule, 0 FROM jmenu WHERE PARENT IS NULL and ACCESSRULE != 'PRIVATE' "+
		"	    UNION ALL "+
		"	    SELECT jmenu.objectId, jmenu.name, jmenu.parent, jmenu.accessrule, LEVEL + 1 "+
		"	    FROM TMPMENU INNER JOIN jmenu ON TMPMENU.objectId = jmenu.PARENT  "+
		"	    WHERE jmenu.ACCESSRULE != 'PRIVATE' "+
		"	) SELECT objectId FROM TMPMENU WHERE NAME IS NOT NULL ORDER BY objectId) \" }")
