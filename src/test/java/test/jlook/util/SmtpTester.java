package test.jlook.util;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-persistence.xml"
})
public class SmtpTester {
	private static Log logger = Log.getLog(SmtpTester.class);
	
	@Resource(name="mailSender")
	private JavaMailSenderImpl sender;
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	@Test
	public void send() throws MessagingException {
		sender.setHost("smtp.gmail.com");
		sender.setPort(587);
		sender.setUsername("jlooktech@gmail.com");
		sender.setPassword("javalook");
		
		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", "true");
		sender.setJavaMailProperties(props);
		
		MimeMessage message = sender.createMimeMessage(); 
		MimeMessageHelper helper = new MimeMessageHelper(message); 
		helper.setTo("jlooktech@gmail.com");
		helper.setSubject("Testing");
		helper.setText("Thank you for ordering!");
		sender.send(message);
	}
//	@Test
//	public void sendEmail() throws IOException {
//		final String username = "jlooktech@gmail.com";
//		final String password = "javalook";
// 
//		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.host", "smtp.gmail.com");
//		props.put("mail.smtp.port", "587");
// 
//		Session session = Session.getInstance(props,
//		  new javax.mail.Authenticator() {
//			protected PasswordAuthentication getPasswordAuthentication() {
//				return new PasswordAuthentication(username, password);
//			}
//		  });
// 
//		try {
// 
//			Message message = new MimeMessage(session);
//			message.setFrom(new InternetAddress("jlooktech@gmail.com"));
//			message.setRecipients(Message.RecipientType.TO,
//				InternetAddress.parse("jlooktech@gmail.com"));
//			message.setSubject("Testing Subject");
//			message.setText("Dear Mail Crawler,"
//				+ "\n\n No spam to my email, please!");
// 
//			Transport.send(message);
// 
//			System.out.println("Done");
// 
//		} catch (MessagingException e) {
//			throw new RuntimeException(e);
//		}
//	}
}
