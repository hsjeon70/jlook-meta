package test.jlook.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;

import jlook.framework.infrastructure.security.JGrantedAuthority;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.security.core.GrantedAuthority;

/**
 * ROLE('User') name=$objectId
 * @author hsjeon70
 *
 */
public class SpElTester {
	private static Log logger = Log.getLog(SpElTester.class);
	
	@Test
	public void test1() {
		ExpressionParser parser = new SpelExpressionParser();
		Expression exp = parser.parseExpression("'Hello World'.concat('!')"); 
		String message = (String) exp.getValue();
		if(logger.isDebugEnabled()) {
			logger.debug("RESULT --> "+message);
		}
	}
	
	@Test
	public void test2() {
		GregorianCalendar c = new GregorianCalendar(); 
		c.set(1856, 7, 9);
		//Inventor tesla = new Inventor("Nikola Tesla", c.getTime(), "Serbian");
		ExpressionParser parser = new SpelExpressionParser();
		Expression exp = parser.parseExpression("name"); 
		//EvaluationContext context = new StandardEvaluationContext(tesla);
		//String name = (String) exp.getValue(context);
	}
	
	@Test
	public void test3() {
		ExpressionParser parser = new SpelExpressionParser();
		Expression exp = parser.parseExpression("objectId is 10 and username is not 'admin'");
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new JGrantedAuthority("User", 1L));
		JUserEntity entity = new JUserEntity(10L, "hsjeon@gmail.com", "1234", authorities);
		entity.setObjectId(10L);
		
		EvaluationContext context = new StandardEvaluationContext(entity);
//		context.setVariable("user", entity);
		
		String message = exp.getValue(context, String.class);
		logger.info(message);
	}
}


