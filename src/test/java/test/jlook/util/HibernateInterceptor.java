package test.jlook.util;

import java.io.Serializable;

import jlook.framework.infrastructure.util.Log;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.EntityMode;

public class HibernateInterceptor extends EmptyInterceptor {
	private static Log logger = Log.getLog(HibernateInterceptor.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object getEntity(String entityName, Serializable id) throws CallbackException {
		if(logger.isDebugEnabled()) {
			logger.debug("###############1");
		}
		return null;
	}
	
	public Object instantiate(String entityName, EntityMode entityMode, Serializable id) throws CallbackException {
		if(logger.isDebugEnabled()) {
			logger.debug("###############2");
		}
		return null;
	}
}
