package test.jlook.util.mvel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.security.JGrantedAuthority;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.tools.JStringTemplate;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.mvel2.MVEL;
import org.mvel2.Macro;
import org.mvel2.MacroProcessor;
import org.mvel2.ParserConfiguration;
import org.mvel2.ParserContext;
/**
 UPDATE JCLASS SET SUMMARY_FILTER = 'if( juser.admin ) { "type!=''ABSTRACT''" } else { "category!=''jlook.framework.domain.metadata''" }' WHERE NAME='JClass'
 
 UPDATE JCLASS SET JOBJECT_RULE = 'jobject.description = jobject.description + "(Description)"' WHERE NAME='JClass'
 
 * @author hsjeon70
 *
 */
public class MvelTester extends JServiceTester {
	private static Log logger = Log.getLog(MvelTester.class);
	
	@Resource(name=JStringTemplate.ID)
	private JStringTemplate template;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Test
	public void test1() {
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new JGrantedAuthority("User", 1L));
		authorities.add(new JGrantedAuthority("Administrator", 2L));
		JUserEntity entity = new JUserEntity(10L, "hsjeon@gmail.com", "1234", authorities);
		Object result = MVEL.eval("objectId", entity);
		logger.info("RESULT--> "+result);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("jUser", entity);
		map.put("name", "HongSeong");
		//result = MVEL.evalToString("Hello @{name}", map);
		try {
			logger.info("-->"+MVEL.eval("jUser.admin",map));
			result = MVEL.eval("jUser.admin ? \"Yes\" : 'World' ", map);
			logger.info("RESULT--> "+result);
			result = MVEL.eval("if(jUser.admin) {  \"Yes\" } else { 'World' }", map);
			logger.info("RESULT--> "+result);
		
			String exp = "if(jUser.administrator) { } else if(jUser.admin) { \"type='DOMAIN' or type='USER'\" } else { \"type='USER'\" }";
//			String exp = "if( jUser.admin ) { \"type!='ABSTRACT'\" } else { \"category!='look.framework.domain.metadata'\" }";
			logger.info(exp);
			result = template.eval(exp, map);
			logger.info("1RESULT**--> "+result);
		
			JClass jClass = jClassService.getJClass(JMetaKeys.JCLASS);
			String filter = jClass.getSummaryFilter();
			logger.info(filter);
			result = template.eval(filter, map);
			logger.info("2RESULT**--> "+result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ParserContext ctx = new ParserContext();
		
		
	}
	
	@Test
	public void test2() {
		JClass jClass = new JClass();
		jClass.setClassId(10L);
		jClass.setName("name");
		jClass.setDescription("Hello");
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("jobject", jClass);
		String exp = "jobject.description = jobject.description + \"(World)\"";
		Object result = MVEL.eval(exp, map);
		logger.info("--> "+result);
		logger.info("--> "+jClass.getDescription());
	}
	
	@Test
	public void macro() {
		Map<String, Macro> map = new HashMap<String, Macro>();
		MyMacro macro = new MyMacro();
		// Add modifyMacro to the Map
		map.put("modify", macro);
		MacroProcessor macroProcessor = new MacroProcessor();
		macroProcessor.setMacros(map);
		
		StringBuffer sb = new StringBuffer();
//		sb.append("add(10, 20)").append("\n");
//		sb.append("def add(a, b) { a+b };").append("\n");
		sb.append("modify --> ").append("\n");
		// Now we pre-parse our expression
		String parsedExpression = macroProcessor.parse(sb.toString());
		logger.info(parsedExpression);
		
//		Object result = MVEL.eval(sb.toString(), map);
//		logger.info("--> "+result);
	}
	
	@Test
	public void javaObject() {
//		ParserConfiguration pconf = new ParserConfiguration();
		ParserContext pctx = new ParserContext( );
		pctx.addImport( "Executor", test.jlook.util.mvel.Executor.class );
		pctx.addPackageImport("java.util");
		Map<String,Object> vars = new HashMap<String,Object>();
		vars.put("exe", new Executor());
		MVEL.eval("exe.doAction()",vars);
		Serializable s = MVEL.compileExpression("exe.doAction()", pctx);
		logger.info("---> "+s);
		Object ans = MVEL.executeExpression(s);
        System.out.println(ans.toString());
	}
	
	@Test
	public void test3() {
		 ParserContext ctx = new ParserContext();
	        try {
	            ctx.addImport("time", System.class.getMethod("currentTimeMillis"));
	        }
	        catch (NoSuchMethodException e) {
	            // handle exception here.
	        }

	        Serializable s = MVEL.compileExpression("time();", ctx);
	        Object ans = MVEL.executeExpression(s);
	        System.out.println(ans.toString());
	}
}
