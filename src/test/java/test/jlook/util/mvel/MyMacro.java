package test.jlook.util.mvel;

import jlook.framework.infrastructure.util.Log;

import org.mvel2.Macro;

public class MyMacro implements Macro {
	private static Log logger = Log.getLog(MyMacro.class);
	
	@Override
	public String doMacro() {
		logger.info("doMacro ------->");
		return "Hello World";
	}

}
