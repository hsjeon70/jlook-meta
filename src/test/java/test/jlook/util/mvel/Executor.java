package test.jlook.util.mvel;

import jlook.framework.infrastructure.util.Log;

public class Executor {
	private static Log logger = Log.getLog(Executor.class);
	
	public static final String NAME = "JEON";
	
	public Executor() {
		logger.info("##################");
	}
	public void doAction(String msg) {
		logger.info("--------> "+msg);
	}
}
