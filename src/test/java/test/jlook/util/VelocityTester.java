package test.jlook.util;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;

import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.junit.client.ClientTester;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.tools.JTemplate;
import jlook.framework.infrastructure.tools.JTemplateEngine;
import jlook.framework.infrastructure.tools.JTemplateException;
import jlook.framework.infrastructure.tools.JTemplateType;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JServiceException;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;

public class VelocityTester extends JServiceTester {
	private static Log logger = Log.getLog(VelocityTester.class);
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JTemplateEngine.ID)
	private JTemplateEngine engine;
	
	@Test
	public void hibernateConfig() throws Exception {
		ClassLoader cloader = ClientTester.class.getClassLoader();
		String filePath = "velocity/velocity-"+System.getProperty(GlobalEnv.MODE.getName())+".properties";
		InputStream in = cloader.getResourceAsStream(filePath);
		if(in==null) {
			throw new Exception("Cannot load environment properties. - "+filePath);
		}
		
		Properties props = new Properties();
		props.load(in);
		in.close();
		
	    VelocityEngine engine = new VelocityEngine();
	    
	    ExtendedProperties eprops = ExtendedProperties.convertProperties(props);
	    engine.setExtendedProperties(eprops);
//	    engine.setProperty( RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
//	    	      "org.apache.velocity.runtime.log.Log4JLogChute" );
//	    engine.setProperty("runtime.log.logsystem.log4j.logger","velocity");
	    engine.init();
	    
	    JDomain jDomain = jDomainService.getJDomain(3L);
	    JClass jClass = jClassService.findByName("USERINFO");
	    
	    VelocityContext context = new VelocityContext();
		context.put( "domain", jDomain );
		context.put( "entity", jClass );
		
		Template  template = engine.getTemplate("template/hibernate-config.vm");
		StringWriter sw = new StringWriter();

		template.merge( context, sw );
		if(logger.isInfoEnabled()) {
			logger.info("RESULT-->\n"+sw.toString());
		}
	}
	
	@Test
	public void templateEngineHibernate() throws JTemplateException, JServiceException {
		JDomain jDomain = jDomainService.getJDomain(3L);
	    JClass jClass = jClassService.findByName("USERINFO");
	    
	    JTemplate template = this.engine.newJTemplate(JTemplateType.hibernate_config);
		template.put("entity", jClass);
	    template.put("domain", jDomain);
	    
		String result  = template.execute();
		if(logger.isInfoEnabled()) {
			logger.info("RESULT-->\n"+result);
		}
	}

	@Test
	public void templateEngineJavaSource() throws JTemplateException, JServiceException {
		JDomain jDomain = jDomainService.getJDomain(3L);
	    JClass jClass = jClassService.findByName("USERINFO");
	    
	    JTemplate template = this.engine.newJTemplate(JTemplateType.domain_javasource);
		template.put("entity", jClass);
	    template.put("domain", jDomain);
	    
		String result  = template.execute();
		if(logger.isInfoEnabled()) {
			logger.info("RESULT-->\n"+result);
		}
	}

}
