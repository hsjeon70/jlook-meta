package test.jlook.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.junit.Test;

public class JavaParserTester {

	@Test
	public void test() throws IOException {
		
		
		File[] files1 = new File[]{};
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		
		Iterable<? extends JavaFileObject> compilationUnits1 =
		           fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files1));
		boolean result = compiler.getTask(null, fileManager, null, null, null, compilationUnits1).call();
		
		fileManager.close();
	}

	
}
