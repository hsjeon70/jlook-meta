package test.jlook.util;

import java.io.File;
import java.io.FileNotFoundException;

import jlook.framework.domain.docs.Header;
import jlook.framework.domain.docs.OpenAPI;
import jlook.framework.domain.docs.OpenAPIService;
import jlook.framework.domain.docs.Parameter;

import org.junit.Test;

import com.thoughtworks.qdox.JavaDocBuilder;
import com.thoughtworks.qdox.model.Annotation;
import com.thoughtworks.qdox.model.DocletTag;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaParameter;
import com.thoughtworks.qdox.model.JavaSource;

public class QDoxTester {

	@Test
	public void test() throws FileNotFoundException {
		File dir = new File("src/main/java/jlook/framework/interfaces");
		JavaDocBuilder builder = new JavaDocBuilder();
		builder.addSourceTree(dir);
		
		JavaSource[] sources = builder.getSources();
		for(JavaSource source : sources) {
			JavaClass[] javaClasses = source.getClasses();
			for(JavaClass javaClass : javaClasses) {
				String name = javaClass.getName();
				if(!name.endsWith("Controller")) {
					continue;
				}
				String comment = javaClass.getComment();
				
				DocletTag[] tags = javaClass.getTags();
				DocletTag tag = getOpenAPI(tags);
				if(tag==null) {
					continue;
				}
				
				OpenAPIService service = new OpenAPIService();
				service.setName(name);
				service.setCategory(javaClass.getPackageName());
				service.setDescription(javaClass.getComment());
				
				System.out.println(name+" ---> ##################### : "+tag.getValue());
				System.out.println(comment);
				
				JavaMethod[] javaMethods = javaClass.getMethods();
				for(JavaMethod javaMethod : javaMethods) {
					tags = javaMethod.getTags();
					tag = getOpenAPI(tags);
					if(tag==null) {
						continue;
					}
					
					System.out.println(javaMethod.getName()+ "Method ================================");
					System.out.println(javaMethod.getComment());
					
					OpenAPI api = new OpenAPI();
					api.setName(tag.getValue());
					api.setDescription(javaMethod.getComment());
					
					Annotation[] anns = javaMethod.getAnnotations();
					for(Annotation ann : anns) {
						if(ann.getType().getValue().endsWith("RequestMapping")) {
							String url = (String)ann.getNamedParameter("value");
							api.setUri(url);
							String method = (String)ann.getNamedParameter("method");
							api.setMethod(method==null?"GET":"POST");
						}
					}
					
					if(api.getUri()==null) {
						throw new RuntimeException("Cannot fine url. -------> ");
					}
					for(DocletTag t : tags) {
						String paramValue = t.getValue();
						if(t.getName().equals("return")) {
							api.setResponse(paramValue);
							continue;
						}
						if(t.getName().equals("publish")) {
							api.setName(paramValue);
							continue;
						}
						
						if(t.getName().equals("throws")) {
							api.setException(t.getValue());
							continue;
						}
						String[] params = t.getParameters();
						JavaParameter parameter = javaMethod.getParameterByName(params[0]);
						if(parameter==null) {
							throw new RuntimeException("Cannot fine parameter. -------> "+params[0]);
						}
						anns = parameter.getAnnotations();
						for(Annotation ann : anns) {
							if(ann.getType().getValue().endsWith("RequestHeader")) {
								Header header = new Header();
								header.setName((String)ann.getNamedParameter("value"));
								String[] ps = t.getParameters();
								String desc = "";
								for(int i=1;i<ps.length;i++) {
									desc += ps[i]+" ";
								}
								header.setDescription(desc.trim());
								header.setType(parameter.getType().getValue());
								header.setRequired((String)ann.getNamedParameter("required"));
								api.addHeader(header);
								System.out.println("\t"+header);
							} else if(ann.getType().getValue().endsWith("RequestParam")) {
								Parameter p = new Parameter();
								p.setName(name);
								String[] ps = t.getParameters();
								String desc = "";
								for(int i=1;i<ps.length;i++) {
									desc += ps[i]+" ";
								}
								p.setDescription(desc.trim());
								p.setRequired((String)ann.getNamedParameter("required"));
								p.setType(parameter.getType().getValue());
								api.addParameter(p);
								System.out.println("\t"+p);
							}
						}
					}
					
					service.addOpenAPI(api);
				}				
			}
		}
		
		System.out.println("----"+new File(".").getAbsolutePath());
	}
	
	public DocletTag getOpenAPI(DocletTag[] tags) {
		for(DocletTag tag : tags) {
			if(tag.getName().equals("publish")) {
				return tag;
			}
		}
		
		return null;
	}

}
