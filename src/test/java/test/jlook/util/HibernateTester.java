package test.jlook.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.util.Log;

public class HibernateTester extends JServiceTester implements EntityResolver {
	private static Log logger = Log.getLog(HibernateTester.class);
	
	protected HibernateTemplate template;
	
	@Resource(name="mySessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.template = new HibernateTemplate(sessionFactory);
	}
	@Test
	public void dynamicModel() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("##################################");
		}
		
		HibernateInterceptor hi = new HibernateInterceptor();
		SessionFactory factory = template.getSessionFactory();
		
		LocalSessionFactoryBean bean = (LocalSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		Configuration config = bean.getConfiguration();
		config.setInterceptor(hi);
		
		String xml = loadXml();
		if(logger.isDebugEnabled()) {
			logger.debug("-->\n"+xml);
		}
		config.setEntityResolver(this);
		
	}
	
	private String loadXml() throws IOException {
		ClassLoader cloader = this.getClass().getClassLoader();
		InputStream in = cloader.getResourceAsStream("examples/Customer.hbm.xml");
		if(in == null) {
			throw new IOException("Cannot find file.");
		}
		byte[] buff = new byte[1024];
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		while(true) {
			int len = in.read(buff);
			if(len==-1) break;
			bos.write(buff,0,len);
		}
		in.close();
		return bos.toString();
	}
	@Override
	public InputSource resolveEntity(String arg0, String arg1)
			throws SAXException, IOException {
		System.out.println(arg0+"----------"+arg1);
		return null;
	}
}
