package test.jlook.util;

import java.lang.reflect.Method;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.Modifier;
import jlook.framework.domain.JObject;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;


public class JavassistTester {
	private static Log logger = Log.getLog(JavassistTester.class);
	
	@Test
	public void test() throws Exception {
		ClassPool pool = ClassPool.getDefault();
//		System.out.println(pool.);
		CtClass ctClass = pool.makeClass("test.MyClass");
		CtClass jObjClass = pool.get("jlook.framework.domain.security.JUser");
		ctClass.setSuperclass(jObjClass);
		ctClass.setModifiers(Modifier.PUBLIC);
		
		CtField f = new CtField(pool.get("java.lang.String"), "name", ctClass);
		f.setModifiers(Modifier.PRIVATE);
		ctClass.addField(f);
		
		CtMethod mSet = CtNewMethod.getter("getName", f);
		mSet.insertAfter("System.out.println(\"Hello World-->\"+this.name);");
		ctClass.addMethod(mSet);
		
		CtMethod mGet = CtNewMethod.setter("setName", f);
		ctClass.addMethod(mGet);

//		ctClass.writeFile();
		
		Class<?> claxx = ctClass.toClass();
		if(logger.isDebugEnabled()) {
			logger.debug("--> "+claxx.getName());
		}
		
		JObject myObj = (JObject)claxx.newInstance();
		myObj.setObjectId(1L);
		if(logger.isDebugEnabled()) {
			logger.debug("--> "+myObj.getObjectId()+" : "+myObj);
		}
		
		Method mdSet = claxx.getMethod("setName", java.lang.String.class);
		mdSet.invoke(myObj, "HongSeong");
		
		Method mdGet = claxx.getMethod("getName", new Class[]{});
		Object result = mdGet.invoke(myObj, new Object[]{});
		System.out.println("result-->"+result);
		
		
	}
	
}
