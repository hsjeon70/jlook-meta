package test.jlook.repository;

import static org.junit.Assert.*;

import java.util.List;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

@Transactional(readOnly = true)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-test.xml",  
		"classpath:spring/context-persistence.xml",
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-infrastructure.xml"
})
public class FilterTester extends HibernateRepository {
	private static Log logger = Log.getLog(FilterTester.class);
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	
	@Test
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void test() {
		//Filter filter = this.template.enableFilter("myTest");
		Session session = this.template.getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria("jlook.framework.domain.metadata.JClass");
		Criterion filter = Restrictions.sqlRestriction("upper(name) like '%CLASS%'");
		criteria.add(filter);
		List<?> result = criteria.list();
		for(Object obj : result) {
			if(logger.isInfoEnabled()) {
				logger.info("\tRESULT --> "+obj);
			}
		}
		
	}

}
