package test.jlook.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.service.JMenuService;
import jlook.framework.service.JServiceException;

public class JMenuTester extends JAuthServiceTester {
	
	@Resource(name=JMenuService.ID)
	private JMenuService jMenuService;
	
	@Test
	public void getMyMenuList() {
		try {
			JResultModel rmodel = this.jMenuService.getMyMenuList();
			assertEquals("success", rmodel.getResult());
		} catch (JServiceException e) {
			fail("Fail to get My Menu list. - "+e.getMessage());
		}
	}
}
