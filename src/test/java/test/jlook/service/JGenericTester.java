package test.jlook.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;
import junit.framework.Assert;

public class JGenericTester extends JAuthServiceTester {
	private static Log logger = Log.getLog(JGenericTester.class);
	
	private static Long classId	= 2L;
	private static Long objectId 	= 1L;
	private static Long parentCid 	= null;
	private static Long parentOid	= null;

	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	@Test
	public void getTitleData() {
		try {
			JResultModel rmodel = this.jGenericService.getTitleData(classId, objectId);
			if(logger.isInfoEnabled()) {
				logger.info("## RESULT --> "+rmodel.getConfig());
				logger.info("## RESULT --> "+rmodel.getData());
			}
			Assert.assertEquals("success", rmodel.getResult());
		} catch (JServiceException e) {
			fail("Fail to get detail view. - "+e.getMessage());
		}
	}
	
	@Test
	public void getDetailView() {
		try {
			JResultModel rmodel = this.jGenericService.getDetailData(JViewType.detail, classId, objectId, parentCid, parentOid);
			if(logger.isInfoEnabled()) {
				logger.info("## RESULT --> "+rmodel.getConfig());
				logger.info("## RESULT --> "+rmodel.getData());
			}
			Assert.assertEquals("success", rmodel.getResult());
		} catch (JServiceException e) {
			fail("Fail to get detail view. - "+e.getMessage());
		}
	}
	
	@Test
	public void getSummaryView() {
		PageUtil page = new PageUtil();
		page.setPageNumber(1);
		page.setPageSize(15);
		
		try {
			JSummaryModel jsview = this.jGenericService.getSummaryData(JViewType.summary, classId, page, null);
			if(logger.isInfoEnabled()) {
				logger.info("## RESULT --> "+jsview.getConfig());
				logger.info("## RESULT --> "+jsview.getData());
				logger.info("## RESULT --> "+jsview.getColumnModel());
				logger.info("## RESULT --> "+jsview.getPage());
			}
			Assert.assertEquals("success", jsview.getResult());
		} catch (JServiceException e) {
			fail("Fail to get sumamry view. - "+e.getMessage());
		}
	}
	
	@Test
	public void testCRUD() {
		JDomain jDomain = new JDomain();
		jDomain.setClassId(JMetaKeys.JDOMAIN);
		jDomain.setName("MyDomain");
		jDomain.setDescription("Test domain");
		try {
			this.jGenericService.create(jDomain);
		} catch (JServiceException e) {
			fail("Fail to create JDomain. - "+e.getMessage());
		}
		
		Long objectId = jDomain.getObjectId();
		if(logger.isDebugEnabled()) {
			logger.debug("RESULT --> objectId="+objectId);
		}
		JDomain jd = null;
		try {
			JObject jObject = this.jGenericService.getJObject(JMetaKeys.JDOMAIN, objectId);
			assertNotNull(jObject);
			assertTrue(jObject instanceof JDomain);
			jd = (JDomain)jObject;
			if(logger.isDebugEnabled()) {
				logger.debug("RESULT --> "+jd.getName()+" - "+jd.getDescription());
			}
		} catch (JServiceException e) {
			fail("Fail to get JDomain. - "+e.getMessage());
		}
		
		jd.setDescription("Test -----");
		try {
			this.jGenericService.update(jd);
		} catch (JServiceException e) {
			e.printStackTrace();
			fail("Fail to update JDomain. - "+e.getMessage());
		}
		
		try {
			JObject jObject = this.jGenericService.getJObject(JMetaKeys.JDOMAIN, objectId);
			assertNotNull(jObject);
			assertTrue(jObject instanceof JDomain);
			jd = (JDomain)jObject;
			if(logger.isDebugEnabled()) {
				logger.debug("RESULT --> "+jd.getName()+" - "+jd.getDescription());
			}
		} catch (JServiceException e) {
			fail("Fail to get JDomain. - "+e.getMessage());
		}
		
		try {
			this.jGenericService.remove(JMetaKeys.JDOMAIN, objectId);
		} catch (JServiceException e) {
			fail("Fail to remove JDomain. - "+e.getMessage());
		}
	}
}
