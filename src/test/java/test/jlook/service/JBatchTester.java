package test.jlook.service;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamName;
import jlook.framework.domain.batch.JBatchParamType;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.infrastructure.batch.JClassBatchTask;
import jlook.framework.infrastructure.batch.JFileBatchTask;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.service.JBatchDefinitionService;
import jlook.framework.service.JBatchInstanceService;
import jlook.framework.service.JServiceException;

public class JBatchTester extends JAuthServiceTester {
	@Resource(name=JBatchDefinitionService.ID)
	private JBatchDefinitionService jBatchDefinitionService;
	
	@Resource(name=JBatchInstanceService.ID)
	private JBatchInstanceService jBatchInstanceService;
	
	@Test
	public void addEchoTaskBatch() {
		JBatchDefinition jbatch = new JBatchDefinition();
		jbatch.setName("Echo Task");
		jbatch.setDescription("Sample task batch");
		jbatch.setTaskClass("jlook.framework.infrastructure.batch.EchoBatch");
		
		try {
			this.jBatchDefinitionService.create(jbatch);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchInstance inst = new JBatchInstance();
		inst.setName("Echo Task Instance");
		inst.setDescription("Echo Task Instance");
		inst.setJBatchDefinition(jbatch);
		inst.setFixedDelay(new Long(1000*10));
		inst.setFixedRate(new Long(1000*300));
		inst.setCronExpression(null);
		inst.setServer(null);
		inst.setActive(true);
		
		try {
			this.jBatchInstanceService.create(inst);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void addJClassTaskBatch() {
		JBatchDefinition jbatch = new JBatchDefinition();
		jbatch.setName("JClass Batch Task");
		jbatch.setDescription("Table to file batch for JClass");
		jbatch.setTaskClass("jlook.framework.infrastructure.batch.JClassBatchTask");
		
		try {
			this.jBatchDefinitionService.create(jbatch);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchParamName name1 = new JBatchParamName();
		name1.setJBatchDefinition(jbatch);
		name1.setName(JClassBatchTask.PARAM_SOURCE_CID);
		name1.setType(JBatchParamType.LONG.name());
		try {
			this.jBatchDefinitionService.create(name1);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamName name2 = new JBatchParamName();
		name2.setJBatchDefinition(jbatch);
		name2.setName(JClassBatchTask.PARAM_UNIT_COUNT);
		name2.setType(JBatchParamType.INTEGER.name());
		try {
			this.jBatchDefinitionService.create(name2);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchInstance inst = new JBatchInstance();
		inst.setName("JClass Batch Task");
		inst.setDescription("Table to file batch for JClass - JAttribute");
		inst.setJBatchDefinition(jbatch);
		inst.setFixedDelay(new Long(1000*60));
		inst.setFixedRate(new Long(1000*600));
		inst.setCronExpression(null);
		inst.setServer(null);
		inst.setActive(true);
		
		try {
			this.jBatchInstanceService.create(inst);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchParamValue value1 = new JBatchParamValue();
		value1.setJBatchInstance(inst);
		value1.setJBatchParamName(name1);
		value1.setValue(Long.toString(JMetaKeys.JCLASS));
		try {
			this.jBatchInstanceService.create(value1);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamValue value2 = new JBatchParamValue();
		value2.setJBatchInstance(inst);
		value2.setJBatchParamName(name2);
		value2.setValue("15");
		try {
			this.jBatchInstanceService.create(value2);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void addJFileTaskBatch() {
		JBatchDefinition jbatch = new JBatchDefinition();
		jbatch.setName("JFile Batch Task");
		jbatch.setDescription("file batch task");
		jbatch.setTaskClass("jlook.framework.infrastructure.batch.JFileBatchTask");
		
		try {
			this.jBatchDefinitionService.create(jbatch);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchParamName name1 = new JBatchParamName();
		name1.setJBatchDefinition(jbatch);
		name1.setName(JFileBatchTask.PARAM_SOURCE_DIR);
		name1.setType(JBatchParamType.STRING.name());
		try {
			this.jBatchDefinitionService.create(name1);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamName name2 = new JBatchParamName();
		name2.setJBatchDefinition(jbatch);
		name2.setName(JFileBatchTask.PARAM_SOURCE_TMP);
		name2.setType(JBatchParamType.STRING.name());
		try {
			this.jBatchDefinitionService.create(name2);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamName name3 = new JBatchParamName();
		name3.setJBatchDefinition(jbatch);
		name3.setName(JFileBatchTask.PARAM_DELETE_FILE);
		name3.setType(JBatchParamType.BOOLEAN.name());
		try {
			this.jBatchDefinitionService.create(name3);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchInstance inst = new JBatchInstance();
		inst.setName("File Batch Task");
		inst.setDescription("File batch task from source directory");
		inst.setJBatchDefinition(jbatch);
		inst.setFixedDelay(new Long(1000*90));
		inst.setFixedRate(new Long(1000*60));
		inst.setCronExpression(null);
		inst.setServer(null);
		inst.setActive(true);
		
		try {
			this.jBatchInstanceService.create(inst);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		
		JBatchParamValue value1 = new JBatchParamValue();
		value1.setJBatchInstance(inst);
		value1.setJBatchParamName(name1);
		value1.setValue("batchDir");
		try {
			this.jBatchInstanceService.create(value1);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamValue value2 = new JBatchParamValue();
		value2.setJBatchInstance(inst);
		value2.setJBatchParamName(name2);
		value2.setValue("batchTmp");
		try {
			this.jBatchInstanceService.create(value2);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
		JBatchParamValue value3 = new JBatchParamValue();
		value3.setJBatchInstance(inst);
		value3.setJBatchParamName(name3);
		value3.setValue("true");
		try {
			this.jBatchInstanceService.create(value3);
		} catch (JServiceException e) {
			throw new RuntimeException(e);
		}
	}
}
