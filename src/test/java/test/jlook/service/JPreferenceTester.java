package test.jlook.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.common.JPreference;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

public class JPreferenceTester extends JAuthServiceTester {
	private static Log logger = Log.getLog(JPreferenceTester.class);
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	@Test
	public void create() {
		JPreference jpref = null;
		try {
			jpref = (JPreference)jGenericService.getJObject(JMetaKeys.JPREFERENCE, 6L);
			if(logger.isInfoEnabled()) {
				logger.info("--> "+jpref.getName());
			}
		} catch (JServiceException e) {
			fail("Fail to get JPreference. - "+e.getMessage());
		}
		
	}
}
