package test.jlook.service;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Mappings;
import org.hibernate.cfg.Settings;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.Mapping;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.xml.sax.EntityResolver;


public class JMetaTester extends JServiceTester {
	private static Log logger = Log.getLog(JMetaTester.class);
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	private String ENAME = "JEnum";
	
	protected HibernateTemplate template;
	
	@Resource(name="mySessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.template = new HibernateTemplate(sessionFactory);
	}
	
	@Test
	public void getAllAttributes() {
		try {
			JClass jClass = jClassService.findByName(ENAME);
			Map<String,JAttribute> jattMap = jMetaService.getAllAttributes(jClass);
			for(JAttribute jatt : jattMap.values()) {
				if(logger.isInfoEnabled()) {
					logger.info("\t--> "+jatt);
				}
			}
		} catch (JServiceException e) {
			logger.error(e.getMessage(), e);
			fail("Fail to get all attributes. - "+e.getMessage());
		}
		
	}

	//@Test
	public void createMapping() {
		LocalSessionFactoryBean bean = (LocalSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		Properties props = bean.getHibernateProperties();
		
		Session session = null;
		ClassLoader cloader = this.getClass().getClassLoader();
		InputStream in = cloader.getResourceAsStream("examples/Event.hbm.xml");
		try {
			String xml = load(in);
			
			Configuration config = new Configuration()
				.addXML(xml)
				.addProperties(props);
			
			Settings settings = config.buildSettings(props);
			Dialect dialect = settings.getDialect();
			
//			Mapping mapping = config.buildMapping();
			Mappings mappings = config.createMappings();
			String[] sqlList = config.generateSchemaCreationScript(dialect);
			for(String sql : sqlList) {
				if(logger.isInfoEnabled()) {
					logger.info("\t--> "+sql);
				}
			}
			
			SessionFactory sfactory = config.buildSessionFactory();
			
		} catch(IOException e) {
			logger.error(e.getMessage(), e);
			fail("Fail to load xml file. - "+e.getMessage());
		} finally {
			if(session!=null) session.close();
		}
	}
	
	private String load(InputStream in) throws IOException {
		ByteArrayOutputStream bao = null;
		try {
			bao = new ByteArrayOutputStream();
			byte[] buff = new byte[1024];
			while(true) {
				int len = in.read(buff);
				if(len==-1) break;
				bao.write(buff,0,len);
			}
			
			return new String(bao.toByteArray());
		} finally {
			if(bao!=null) bao.close();
			if(in!=null) in.close();
		}
	}
}
