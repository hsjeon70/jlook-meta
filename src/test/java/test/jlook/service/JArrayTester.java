package test.jlook.service;

import static org.junit.Assert.*;

import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JArrayService;
import jlook.framework.service.JServiceException;

import org.junit.Test;


public class JArrayTester extends JServiceTester {
	private static Log logger = Log.getLog(JArrayTester.class);
	
	@Resource(name=JArrayService.ID)
	private JArrayService service;
	
	private String name="Test01";
	private String description = "Test01";
	private static Long objectId=1L;
	
	@Test
	public void testCRD() {
		try {
			objectId = service.create(name, description, new String[]{"1", "2", "3"});
			if(logger.isInfoEnabled()) {
				logger.info("\t-->"+objectId);
			}
		} catch (JServiceException e) {
			fail("Fail to create JArray. - "+e.getMessage());
		}
	
		JArray jArray;
		try {
			jArray = service.getJArray(objectId);
		} catch (JServiceException e) {
			fail("Fail to get JArray. - "+e.getMessage());
			return;
		}
		
		Set<JArrayDetail> details = jArray.getValues();
		for(JArrayDetail detail : details) {
			if(logger.isInfoEnabled()) {
				logger.info("\t--->"+detail.getValue());
			}
		}
		
		try {
			this.service.remove(jArray);
		} catch (JServiceException e) {
			fail("Fail to remove JArray. - "+e.getMessage());
		}
	}
}
