package test.jlook.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import jlook.framework.domain.account.JUser;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;

import org.junit.Test;


public class JUserTester extends JServiceTester {

	@Resource(name=JUserService.ID)
	private JUserService juser;
	
	@Test
	public void register() {
		String email = "test77@jlook.com";
		JUser jUser = new JUser();
		jUser.setEmail(email);
		jUser.setPassword("1111");
		jUser.setNickname("test77");
		
		try {
			JUser tmp = juser.findByEmail(email);
		} catch (JObjectNotFoundException e) {
			return;
		} catch (JServiceException e) {
			fail("Fail to get JUser. - "+e.getMessage());
		}
		
		try {
			juser.create(jUser);
		} catch (JServiceException e) {
			fail("Fail to create JUser. - "+e.getMessage());
		}
	}
	
	//@Test
	public void remove() {
		try {
			juser.remove("test77@jlook.com");
		} catch (JServiceException e) {
			fail("Fail to remove JUser. - "+e.getMessage());
		}
	}

}
