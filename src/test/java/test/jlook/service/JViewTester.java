package test.jlook.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.metadata.JView;
import jlook.framework.infrastructure.junit.service.JServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JViewService;

public class JViewTester extends JServiceTester {
	private static Log logger = Log.getLog(JViewTester.class);
	
	@Resource(name=JViewService.ID)
	private JViewService jViewService;
	
	@Test
	public void getJView() {
		try {
			JView jView = this.jViewService.getJView(2L);
			if(logger.isInfoEnabled()) {
				logger.info("## RESULT --> "+jView);
			}
		} catch (JServiceException e) {
			fail("Fail to get JView. - "+e.getMessage());
		}
	}
}
