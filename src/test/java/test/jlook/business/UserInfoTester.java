package test.jlook.business;

import javax.annotation.Resource;

import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.metadata.InheritanceType;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JTypeService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.junit.Test;


public class UserInfoTester extends JAuthServiceTester  {
	private static Log logger = Log.getLog(UserInfoTester.class);
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JTypeService.ID)
	private JTypeService jDataTypeService;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	private String ENAME = "USERINFO";
	
	@Test
	public void createUSERINFO() throws JServiceException {
		JClass jUserClass = jClassService.findByName(JUser.NAME);
		
		JClass jClass = new JClass();
		jClass.setClassId(JMetaKeys.JCLASS);
		jClass.setName(ENAME);
		jClass.setDescription("BUSPIA USER INFORMATION");
		jClass.setCategory("domain");
		jClass.setInheritanceJClass(jUserClass);
		jClass.setInheritanceType(InheritanceType.EXTENDS_TYPE);
		jClass.setType(JClassType.CONCRETE_TYPE);
		//jClass.setStatus(JClassStatus.CREATED.name());
		jGenericService.create(jClass);
		
		JAttribute jatt = null;
		jatt = create("address",JPrimitive.STRING_TYPE,100);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("hobby",JMetaKeys.JArray.getName(),15);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		if(logger.isDebugEnabled()) {
			logger.debug("### --> "+jClass.getJAttributes());
		}
		
		
		
	}
	
	private JAttribute create(String name, String type, Integer length) throws JServiceException {
		JAttribute jatt = new JAttribute();
		jatt.setClassId(JMetaKeys.JATTRIBUTE);
		jatt.setName(name);
		jatt.setLabel(name);
		JType jdt = jDataTypeService.findByName(type);
		jatt.setType(jdt);
		jatt.setLength(length);
		return jatt;
	}

	@Test
	public void removeUSERINFO() throws JServiceException {
		jClassService.remove(ENAME);
	}
}
