package test.jlook.tools.genkey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import jlook.framework.domain.License;
import jlook.framework.infrastructure.util.DateUtil;

import org.junit.Test;

import sun.misc.*;

public class LicenseManager {
	private final static String KEY = "jlook-core platform by hsjeon70@gmail.com";
	
	
	@Test
	public void genTempLicense() throws Exception{
		String key = encrypt(KEY);
		
		Calendar today = Calendar.getInstance();
		today.add(Calendar.MONTH, 1);
		String expireDate = DateUtil.formatDate(today.getTime());
		String ip = "none";
		
		License lic = new License();
		lic.setType(LicenseType.development.name());
		lic.setKey(key);
		lic.setIp(ip);
		lic.setExpiredDate(expireDate);
		
		String myKey = toString(lic);
		System.out.println(myKey);
		UUID uuid = UUID.nameUUIDFromBytes(myKey.getBytes());
		String id = uuid.toString();
		System.out.println("id --> "+id);
		lic.setId(id);
		
		JAXBContext jaxbContext = JAXBContext.newInstance(License.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		File f = new File("src/main/resource","jlook-core-license.xml");
		FileOutputStream fo = new FileOutputStream(f);
		marshaller.marshal(lic, fo);
	    fo.close();
	}
	
	@Test
	public void validateLicense() throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(License.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		File f = new File("src/main/resource","jlook-core-license.xml");
		License lic = (License)unmarshaller.unmarshal(f);
		
		String myKey = toString(lic);
		System.out.println("--> "+decrypt(lic.getKey()));
		UUID uuid = UUID.nameUUIDFromBytes(myKey.getBytes());
		if(uuid.toString().equals(lic.getId())) {
			System.out.println("VALID LICENSE");
		} else {
			System.out.println("INVALID LICENSE");
		}
	}// XTl0UNhWkEeyU7BZyzrpge2XMTIJyuqod9ZYaUz7ZvWAvwfgg3wSDUutgxe5fqCp05/08/2012none
	
	@Test
	public void createSecretKey() throws Exception {
		KeyGenerator keygenerator = KeyGenerator.getInstance("Blowfish");
		SecretKey secretkey = keygenerator.generateKey();
		byte[] key = secretkey.getEncoded();
		File f =new File("src/main/resource/domain","secret.key");
		if(f.exists()) {
			return;
		}
		System.out.println(f.getAbsolutePath());
		FileOutputStream fo = new FileOutputStream(f);
		fo.write(key,0,key.length);
		fo.flush();
		fo.close();
	}
	
	private SecretKey getSecretKey() throws Exception {
		File f =new File("src/main/resource/domain","secret.key");
		FileInputStream fi = new FileInputStream(f);
		byte[] buff = new byte[1024];
		int length = fi.read(buff);
		byte[] key = new byte[length];
		System.arraycopy(buff, 0, key, 0, key.length);
		SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
		return keySpec;
	}
	
	@Test
	public void test() throws Exception {
		String enString = encrypt(KEY);
		System.out.println(enString);
		String deString = decrypt(enString);
		System.out.println(deString);
	}
	
	public String encrypt(String text) throws Exception {
		SecretKey secretkey = getSecretKey();
	    // create a cipher based upon Blowfish
	    Cipher cipher = Cipher.getInstance("Blowfish");

	    // initialise cipher to with secret key
	    cipher.init(Cipher.ENCRYPT_MODE, secretkey);

	   		    // encrypt message
	    byte[] encrypted = cipher.doFinal(text.getBytes());
	    BASE64Encoder encoder  = new BASE64Encoder();
	    String enString = encoder.encode(encrypted);
	    
		return enString;
	}
	
	public String decrypt(String text) throws Exception {
		BASE64Decoder decoder  = new BASE64Decoder();
	    byte[] deBytes = decoder.decodeBuffer(text);
		
	    SecretKey secretkey = getSecretKey();
	    // create a cipher based upon Blowfish
	    Cipher cipher = Cipher.getInstance("Blowfish");
	    // re-initialise the cipher to be in decrypt mode
	    cipher.init(Cipher.DECRYPT_MODE, secretkey);

	    // decrypt message
	    byte[] decrypted = cipher.doFinal(deBytes);

	    return new String(decrypted);
	}
	
	private String toString(License lic) {
		return lic.getKey().trim()+lic.getType().trim()+lic.getExpiredDate().trim()+lic.getIp().trim();
	}
}
