package test.jlook.tools;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import jlook.framework.infrastructure.JClassLoader;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-persistence.xml"
})
public class JClassLoaderTester {
	private static Log logger = Log.getLog(JClassLoaderTester.class);
	
	@Resource(name=JClassLoader.ID)
	private JClassLoader jClassLoader;
	
	static {
		System.setProperty("app.mode", "local");
		System.setProperty("app.home","/Users/hsjeon70/Applications/jlook");
	}
	@Test
	public void test() throws ClassNotFoundException {
		logger.debug("#### "+jClassLoader);
		Thread.currentThread().setContextClassLoader(this.jClassLoader);
		
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		String name = "d4.com.shopping.ORDERITEM";
		Class clazz = null;
		if ( contextClassLoader != null ) {
			clazz = contextClassLoader.loadClass(name);
		}
		logger.debug("#### "+clazz+", "+clazz.getClassLoader());
		Method[] methods = clazz.getDeclaredMethods();
		for(Method method : methods) {
			logger.warn("	###---> "+method.getName());
		}
	}

}
