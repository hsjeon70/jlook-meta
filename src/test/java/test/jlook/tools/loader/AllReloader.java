package test.jlook.tools.loader;

import javax.annotation.Resource;

import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.hibernate.JSessionFactoryBean;
import jlook.framework.infrastructure.junit.BaseLoader;

import org.junit.Test;


public class AllReloader extends BaseLoader {
	
	@Resource(name=jlook.framework.infrastructure.loader.InitDataLoader.ID)
	private jlook.framework.infrastructure.loader.InitDataLoader fwDataLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.MetadataLoader.ID)
	private jlook.framework.infrastructure.loader.MetadataLoader fwMetadataLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.EnumLoader.ID)
	private jlook.framework.infrastructure.loader.EnumLoader fwEnumLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.ViewLoader.ID)
	private jlook.framework.infrastructure.loader.ViewLoader fwViewLoader;
	
	@Resource(name=jlook.integration.infrastructure.loader.EnumLoader.ID)
	private jlook.integration.infrastructure.loader.EnumLoader intEnumLoader;
	
	@Resource(name=jlook.integration.infrastructure.loader.MetadataLoader.ID)
	private jlook.integration.infrastructure.loader.MetadataLoader intMetadataLoader;
	
	@Test
	@Override
	public void doInit() throws Exception {
		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		bean.dropDatabaseSchema();
		bean.createDatabaseSchema();
		
		this.fwEnumLoader.setDataLoadEnabled(true);
		this.fwEnumLoader.doInit();
		
		this.fwMetadataLoader.setDataLoadEnabled(true);
		this.fwMetadataLoader.doInit();
		
		this.fwDataLoader.setDataLoadEnabled(true);
		this.fwDataLoader.doInit();
		
		this.intEnumLoader.setDataLoadEnabled(true);
		this.intEnumLoader.doInit();
		
		this.intMetadataLoader.setDataLoadEnabled(true);
		this.intMetadataLoader.doInit();
		
		this.fwViewLoader.setDataLoadEnabled(true);
		this.fwViewLoader.doInit();
	}

}
