package test.jlook.tools.loader.framework;


import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.BaseLoader;

import org.junit.Test;



public class InitDataLoader extends BaseLoader {
	
	@Resource(name=jlook.framework.infrastructure.loader.InitDataLoader.ID)
	private jlook.framework.infrastructure.loader.InitDataLoader dataLoader;
	
	@Test
	public void doInit() throws Exception {
		this.dataLoader.setDataLoadEnabled(true);
		this.dataLoader.doInit();
	}
}
