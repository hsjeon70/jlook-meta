package test.jlook.tools.loader.framework;


import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.BaseLoader;

import org.junit.Test;



public class ViewLoader extends BaseLoader {
	
	@Resource(name=jlook.framework.infrastructure.loader.ViewLoader.ID)
	private jlook.framework.infrastructure.loader.ViewLoader viewLoader;
	
	@Test
	public void doInit() throws Exception {
		this.viewLoader.setDataLoadEnabled(true);
		this.viewLoader.doInit();
	}
}
