package test.jlook.tools.loader.framework;


import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.BaseLoader;

import org.junit.Test;



public class MetadataLoader extends BaseLoader {

	@Resource(name=jlook.framework.infrastructure.loader.MetadataLoader.ID)
	private jlook.framework.infrastructure.loader.MetadataLoader metadataLoader;
	
	@Test
	public void doInit() throws Exception {
		this.metadataLoader.setDataLoadEnabled(true);
		this.metadataLoader.doInit();
	}
}
