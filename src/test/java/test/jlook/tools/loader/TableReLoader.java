package test.jlook.tools.loader;

import javax.annotation.Resource;

import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.hibernate.JSessionFactoryBean;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JServiceException;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateTemplate;


public class TableReLoader extends BaseLoader {
	private static Log logger = Log.getLog(TableReLoader.class);
	
	private boolean loaded;
	protected HibernateTemplate template;
	
	@Resource(name="mySessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		if(logger.isDebugEnabled()) {
			logger.debug(sessionFactory.getClass());
		}
		this.template = new HibernateTemplate(sessionFactory);
	}
	
	@Test
	public void doInit() throws JServiceException {
		if(this.loaded) {
			return;
		}
		
		try {
			initialize();

	        this.loaded = true;
		} catch (JContextException e) {
			e.printStackTrace();
			throw new JServiceException(e.getMessage(), e);
		}
	}
	
	private void initialize() throws JServiceException, JContextException {

		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		bean.dropDatabaseSchema();
		bean.createDatabaseSchema();
			
	}

}
