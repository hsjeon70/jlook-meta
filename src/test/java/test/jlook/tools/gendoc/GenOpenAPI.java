package test.jlook.tools.gendoc;


import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.tools.OpenAPIGenerator;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;

public class GenOpenAPI extends BaseLoader{
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(GenOpenAPI.class);
	
	@Resource(name=OpenAPIGenerator.ID)
	private OpenAPIGenerator openApiGen;

	@Test
	@Override
	public void doInit() throws Exception {
		openApiGen.generate();
	}
}
