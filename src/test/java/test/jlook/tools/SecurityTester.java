package test.jlook.tools;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-persistence.xml"
})
public class SecurityTester {
	
	@Resource(name="passwordEncoder")
	private ShaPasswordEncoder encoder;
	
	@Test
	public void keyGen() {
		encoder.encodePassword("javalook", null);
	}
}
