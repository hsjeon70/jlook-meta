package test.jlook;

import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.task.JTaskExecutor;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-persistence.xml"
})
public class JTaskTester {
	private static Log logger = Log.getLog(JTaskTester.class);
	
	@Resource(name=JTaskExecutor.ID)
	private JTaskExecutor jTaskExecutor;
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	@Test
	public void doTask() throws Exception {
		jTaskExecutor.run("jlook.framework.infrastructure.task.EchoTask", 3);
		
		
	}
}
