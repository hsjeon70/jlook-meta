package test.jlook.demo;

import javax.annotation.Resource;

import org.junit.Test;

import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.junit.service.JAuthServiceTester;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JTypeService;

/**
 * @author hsjeon70
 *
 */
public class DBLoader extends JAuthServiceTester {
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	@Resource(name=JTypeService.ID)
	private JTypeService jDataTypeService;
	
	@Test
	public void loadMetadataDept() throws JServiceException {
		JClass dept = new JClass();
		dept.setClassId(JMetaKeys.JCLASS);
		dept.setName("DEPT");
		dept.setDescription("DEPT Entity");
		dept.setCategory("domain");
		dept.setType(JClassType.CONCRETE_TYPE);
		
		JAttribute jatt = null;
		jatt = create("DEPTNO",JPrimitive.INTEGER_TYPE,2,true);
		jatt.setJClass(dept);
		dept.addJAttribute(jatt);
		
		jatt = create("DNAME",JPrimitive.STRING_TYPE,14,false);
		jatt.setJClass(dept);
		dept.addJAttribute(jatt);
		
		jatt = create("LOC",JPrimitive.STRING_TYPE,13,false);
		jatt.setJClass(dept);
		dept.addJAttribute(jatt);
		
		jGenericService.create(dept);
	}
	
	@Test
	public void loadMetadataEmp() throws JServiceException {
		JClass emp = new JClass();
		emp.setClassId(JMetaKeys.JCLASS);
		emp.setName("EMP");
		emp.setDescription("EMP Entity");
		emp.setCategory("domain");
		emp.setType(JClassType.CONCRETE_TYPE);
		
		JAttribute jatt = null;
		jatt = create("EMPNO",JPrimitive.INTEGER_TYPE,4,true);
		jatt.setJClass(emp);
		emp.addJAttribute(jatt);
		
		jatt = create("ENAME",JPrimitive.STRING_TYPE,10,false);
		jatt.setJClass(emp);
		emp.addJAttribute(jatt);
		
		jatt = create("JOB",JPrimitive.STRING_TYPE,9,false);
		jatt.setJClass(emp);
		emp.addJAttribute(jatt);
		
		jatt = create("MGR","EMP",15,false);
		jatt.setJClass(emp);
		emp.addJAttribute(jatt);
		
		jatt = create("HIREDATE",JPrimitive.DATE_TYPE,null,false);
		jatt.setJClass(emp);
		emp.addJAttribute(jatt);
	}
	
	private JAttribute create(String name, String type, Integer length, boolean primary) throws JServiceException {
		JAttribute jatt = new JAttribute();
		jatt.setName(name);
		JType jdt = jDataTypeService.findByName(type);
		jatt.setType(jdt);
		jatt.setLength(length);
		jatt.setPrimary(primary);
		
		return jatt;
	}
}
