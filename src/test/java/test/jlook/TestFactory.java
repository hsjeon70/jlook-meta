package test.jlook;

import org.springframework.beans.factory.FactoryBean;

public class TestFactory implements FactoryBean<Test> {

	@Override
	public Test getObject() throws Exception {
		return new Test();
	}

	@Override
	public Class<Test> getObjectType() {
		return Test.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
	
	public static void main(String[] args) throws Exception {
		TestFactory factory = new TestFactory();
		for(int i=0;i<10;i++) {
			Test t  = factory.getObject();
			System.out.println(t.getId());
		}
	}
}
