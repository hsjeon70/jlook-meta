package test.jlook.client;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.common.JPreferenceDetail;
import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.client.AuthClientTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.ParamKeys;

public class JPreferenceClientTester extends AuthClientTester {
	private static Log logger = Log.getLog(JPreferenceClientTester.class);
	
	
	@Test
	public void createDetail() {
		Long prefId = 6L;
		Long domainId = 3L;
		super.newHttpRequest(HttpMethod.POST, "/service/generic/create");
		try {
			request.setParameter(ParamKeys.CLASS_ID, Long.toString(JMetaKeys.JPREFERENCEDETAIL));
			request.setParameter(JPreferenceDetail.A_JPREFERENCE, Long.toString(prefId));
			request.setParameter(JPreferenceDetail.A_TARGET, Long.toString(domainId));
			request.setParameter(JPreferenceDetail.A_VALUE, "/service/test/openapi");
			
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}
}
