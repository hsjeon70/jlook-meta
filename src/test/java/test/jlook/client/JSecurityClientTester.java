package test.jlook.client;

import static org.junit.Assert.*;

import java.io.IOException;

import jlook.framework.GlobalKeys;
import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.client.ClientTester;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;


public class JSecurityClientTester extends ClientTester {
	private static Log logger = Log.getLog(JSecurityClientTester.class);
	
	public static final String REGISTER_URI = "/service/security/register";
	
	private String username = "buspi";
	private String email = "test01@jlook.com";
	private String password = "1111";
	private String picture;
	
	@Test
	public void signIn() {
		super.newHttpRequest(HttpMethod.POST, GlobalKeys.SIGNIN_URI);
		request.setParameter("j_username", "buspia@gmail.com");
		request.setParameter("j_password", "111");
		
		try {
			super.execute(request);
			String status = response.getStatusLine();
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+status);
			}
			
			assertTrue("Invalid StatusCode - "+response.getStatusCode(), response.getStatusCode()==302);
			if(response.getStatusCode()==302) {
				String location = response.getHeader("Location");
				response.release();
				
				super.newHttpRequest(HttpMethod.GET, location);
				
				super.execute(request);
				status = response.getStatusLine();
				if(logger.isDebugEnabled()) {
					logger.debug("--> "+status);
				}
			}
			
			String content = response.getContent();
			assertNotNull("Response is null.",content);
			if(logger.isInfoEnabled()) {
				logger.info("Content --> "+content);
			}
			response.release();
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			super.shutdown();
		}
	}
	
	@Test
	public void signOut() {
		super.newHttpRequest(HttpMethod.GET, GlobalKeys.SIGNOUT_URI);
		try {
			super.execute(request);
			assertTrue("Fail to Http request - "+response.getStatusCode(), response.getStatusCode()==200);
			String content = response.getContent();
			if(logger.isDebugEnabled()) {
				logger.debug("\t"+content);
			}
		} catch(IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			response.release();
		}
	}
	
	@Test
	public void register() {
		super.newHttpRequest(HttpMethod.POST, REGISTER_URI);
		try {
			request.setParameter("username", username);
			request.setParameter("email", email);
			request.setParameter("password", password);
			request.setParameter("picture", picture);
			
			super.execute(request);
			String content = response.getContent();
			if(logger.isDebugEnabled()) {
				logger.debug("\t"+content);
			}
		} catch(IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			response.release();
		}
	}
	
	

}
