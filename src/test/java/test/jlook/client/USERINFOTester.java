package test.jlook.client;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import jlook.framework.domain.account.JUser;
import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.client.AuthClientTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.ParamKeys;

import org.junit.Test;


public class USERINFOTester extends AuthClientTester {
	private static Log logger = Log.getLog(USERINFOTester.class);
	
	private Long classId = 23L;
	private Long objectId = 4L;
	private String email	="test11@gmail.com";
	private String nickname	="test00";
	private String password	="1111";
	private String address	="Incheon";
	private String JGroup	= null;
	private String hobby	="baseball";
	
	@Test
	public void create() {
		super.newHttpRequest(HttpMethod.POST, "/service/generic/create");
		try {
			request.setParameter(ParamKeys.CLASS_ID, this.classId.toString());
			request.setParameter(JUser.A_EMAIL, email);
			request.setParameter(JUser.A_NICKNAME, nickname);
			request.setParameter(JUser.A_PASSWORD, password);
			request.setParameter("address", address);
			request.setParameter("hobby", hobby);
			request.setParameter("billiards", hobby);
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}
	
	@Test
	public void save() {
		super.newHttpRequest(HttpMethod.POST, "/service/generic/save");
		try {
			request.setParameter(ParamKeys.CLASS_ID, this.classId.toString());
			request.setParameter(ParamKeys.OBJECT_ID, this.objectId.toString());
			request.setParameter(JUser.A_EMAIL, email);
			request.setParameter(JUser.A_NICKNAME, nickname);
			request.setParameter(JUser.A_PASSWORD, password);
			request.setParameter("address", address);
			request.setParameter("hobby", hobby);
			request.setParameter("hobby", "billiards");
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}
	
	@Test
	public void getDetail() {
		super.newHttpRequest(HttpMethod.GET, "/service/generic/detail");
		try {
			request.setParameter(ParamKeys.CLASS_ID, this.classId.toString());
			request.setParameter(ParamKeys.OBJECT_ID, this.objectId.toString());
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}
	
	
	@Test
	public void remove() {
		super.newHttpRequest(HttpMethod.POST, "/service/generic/remove");
		try {
			request.setParameter(ParamKeys.CLASS_ID, this.classId.toString());
			request.setParameter(ParamKeys.OBJECT_ID, this.objectId.toString());
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}
}
