package test.jlook.client;

import static org.junit.Assert.*;

import java.io.IOException;

import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.client.AuthClientTester;
import jlook.framework.infrastructure.util.Log;

import org.junit.Test;


public class JObjectRatingClientTester extends AuthClientTester {
	private static Log logger = Log.getLog(JObjectRatingClientTester.class);
	
	private String name = "FoodStore";
	private Long target = 1L;
	
	@Test
	public void getObjectRatingDetailList() {
		super.newHttpRequest(HttpMethod.GET, "/service/objectrating/"+name+"/"+target);
		try {
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch(IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			if(response!=null) response.release();
		}
	}

}
