package test.jlook.client;

import static org.junit.Assert.*;

import java.io.IOException;

import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.client.AuthClientTester;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.ParamKeys;

import org.junit.Test;


public class JGenericClientTester extends AuthClientTester {
	private static Log logger = Log.getLog(JGenericClientTester.class);
	
	private Long classId = 2L;
	
	@Test
	public void getSummaryData() {
		super.newHttpRequest(HttpMethod.POST, "/service/generic/summary");
		try {
			request.setParameter(ParamKeys.CLASS_ID, this.classId.toString());
			super.execute(request);
			int code = response.getStatusCode();
			assertTrue("Fail to Http Request - "+code, code==200);
			String content = response.getContent();
			assertNotNull("Responsed content is null.",content);
			if(logger.isDebugEnabled()) {
				logger.debug("--> "+content);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		}
	}

}
