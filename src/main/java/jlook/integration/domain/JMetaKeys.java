package jlook.integration.domain;

import jlook.framework.domain.JObject;

public enum JMetaKeys {
	JAdapterDefinition(JMetaKeys.JADAPTERDEFINITION, 			jlook.integration.domain.adapter.JAdapterDefinition.class),
	JAdapterParamDefinition(JMetaKeys.JADAPTERPARAMDEFINITION, 	jlook.integration.domain.adapter.JAdapterParamDefinition.class),
	JAdapterInstance(JMetaKeys.JADAPTERINSTANCE, 				jlook.integration.domain.adapter.JAdapterInstance.class),
	JAdapterParamInstance(JMetaKeys.JADAPTERPARAMINSTANCE, 		jlook.integration.domain.adapter.JAdapterParamInstance.class)
	;
	
	JMetaKeys(long classId, Class<? extends JObject> clazz) {
		this.classId = classId;
		this.clazz = clazz;
		this.name = this.clazz.getSimpleName();
	}
	
	private long classId;
	private String name;
	private Class<? extends JObject> clazz;
	
	public Long getClassId() {
		return this.classId;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Class<? extends JObject> getJClass() {
		return this.clazz;
	}
	
	public static final long JADAPTERDEFINITION 		= 38L;
	public static final long JADAPTERPARAMDEFINITION 	= 39L;
	public static final long JADAPTERINSTANCE 			= 40L;
	public static final long JADAPTERPARAMINSTANCE		= 41L;
	
}
