package jlook.integration.domain;

public enum MessageType {
	bytes,
	xml,
	object,
	json,
	map;
}
