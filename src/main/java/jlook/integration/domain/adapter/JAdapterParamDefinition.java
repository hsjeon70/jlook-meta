package jlook.integration.domain.adapter;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.integration.domain.JMetaKeys;

@jclass(cid=JMetaKeys.JADAPTERPARAMDEFINITION, did=JDomainDef.SYSTEM_ID, name=JAdapterParamDefinition.NAME)
public class JAdapterParamDefinition extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAdapterParamDefinition";
	
	public static final String A_JADAPTERDEFINITION = "JAdapterDefinition";
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_TYPE 			= "type";
	public static final String A_DEFAULT_VALUE	= "defaultValue";
	
	@jattr(name=A_JADAPTERDEFINITION, label="JAdapterDefinition", type=JAdapterDefinition.NAME, length=15, description="AdapterDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAdapterDefinition jAdapterDefinition;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Adapter Param Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Adapter Param Description", 
			required=false, unique=false, searchable=false, derived=false) 			private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="Adapter Param Type", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="STRING", validation="JAdapterParamType") 	private String type;
	@jattr(name=A_DEFAULT_VALUE, label="Default Value", type=JPrimitive.STRING_TYPE, length=100, description="Adapter Param Default Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String defaultValue;
	
	public JAdapterDefinition getJAdapterDefinition() {
		return jAdapterDefinition;
	}
	public void setJAdapterDefinition(JAdapterDefinition jAdapterDefinition) {
		this.jAdapterDefinition = jAdapterDefinition;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((jAdapterDefinition == null) ? 0 : jAdapterDefinition
						.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAdapterParamDefinition other = (JAdapterParamDefinition) obj;
		if (jAdapterDefinition == null) {
			if (other.jAdapterDefinition != null)
				return false;
		} else if (!jAdapterDefinition.equals(other.jAdapterDefinition))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
