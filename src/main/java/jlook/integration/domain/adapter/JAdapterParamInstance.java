package jlook.integration.domain.adapter;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.integration.domain.JMetaKeys;

@jclass(cid=JMetaKeys.JADAPTERPARAMINSTANCE, did=JDomainDef.SYSTEM_ID, name=JAdapterParamInstance.NAME)
public class JAdapterParamInstance extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAdapterParamInstance";
	
	public static final String A_JADAPTERINSTANCE 			= "JAdapterInstance";
	public static final String A_JADAPTERPARAMDEFINITION 	= "JAdapterParamDefinition";
	public static final String A_VALUE						= "value";
	
	@jattr(name=A_JADAPTERINSTANCE, label="JAdapterInstance", type=JAdapterInstance.NAME, length=15, description="Adapter Instance", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAdapterInstance jAdapterInstance;
	@jattr(name=A_JADAPTERPARAMDEFINITION, label="JAdapterParamDefinition", type=JAdapterParamDefinition.NAME, length=15, description="Adapter Param Definition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAdapterParamDefinition jAdapterParamDefinition;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.STRING_TYPE, length=100, description="Adapter Param Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String value;
	
	public JAdapterInstance getJAdapterInstance() {
		return jAdapterInstance;
	}
	
	public void setJAdapterInstance(JAdapterInstance jAdapterInstance) {
		this.jAdapterInstance = jAdapterInstance;
	}
	public JAdapterParamDefinition getJAdapterParamDefinition() {
		return jAdapterParamDefinition;
	}
	public void setJAdapterParamDefinition(
			JAdapterParamDefinition jAdapterParamDefinition) {
		this.jAdapterParamDefinition = jAdapterParamDefinition;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((jAdapterInstance == null) ? 0 : jAdapterInstance.hashCode());
		result = prime
				* result
				+ ((jAdapterParamDefinition == null) ? 0
						: jAdapterParamDefinition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAdapterParamInstance other = (JAdapterParamInstance) obj;
		if (jAdapterInstance == null) {
			if (other.jAdapterInstance != null)
				return false;
		} else if (!jAdapterInstance.equals(other.jAdapterInstance))
			return false;
		if (jAdapterParamDefinition == null) {
			if (other.jAdapterParamDefinition != null)
				return false;
		} else if (!jAdapterParamDefinition
				.equals(other.jAdapterParamDefinition))
			return false;
		return true;
	}
	
}
