package jlook.integration.domain.adapter;

import java.util.Map;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.integration.domain.JMetaKeys;

@jclass(cid=JMetaKeys.JADAPTERDEFINITION, did=JDomainDef.SYSTEM_ID, name=JAdapterDefinition.NAME)
public class JAdapterDefinition extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAdapterDefinition";
	
	public static final String A_NAME 			= "name";
	public static final String A_TYPE			= "type";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_ADAPTER_CLASS 	= "adapterClass";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="AdapterDefinition Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="AdapterDefinition Name", 
			required=true, unique=true, searchable=true, derived=false, defaultValue=JAdapterType.OUTBOUND_TYPE, validation="JAdapterType") 		private String type;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="AdapterDefinition Description", 
			required=false, unique=false, searchable=false, derived=false) 			private String description;
	@jattr(name=A_ADAPTER_CLASS, label="Adapter Class", type=JPrimitive.STRING_TYPE, length=500, description="Class Name for AdapterDefinition", 
			required=true, unique=false, searchable=false, derived=false) 			private String adapterClass;
	
	private Map<String,JAdapterParamDefinition> jAdapterParamDefinitions;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdapterClass() {
		return adapterClass;
	}
	public void setAdapterClass(String adapterClass) {
		this.adapterClass = adapterClass;
	}
	public Map<String, JAdapterParamDefinition> getJAdapterParamDefinitions() {
		return jAdapterParamDefinitions;
	}
	public void setJAdapterParamDefinitions(
			Map<String, JAdapterParamDefinition> jAdapterParamDefinitions) {
		this.jAdapterParamDefinitions = jAdapterParamDefinitions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAdapterDefinition other = (JAdapterDefinition) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
