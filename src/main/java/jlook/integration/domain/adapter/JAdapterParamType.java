package jlook.integration.domain.adapter;

public enum JAdapterParamType {
	BOOLEAN,
	INTEGER,
	LONG,
	FLOAT,
	DOUBLE,
	STRING,
	DATE,
	TIME;
}
