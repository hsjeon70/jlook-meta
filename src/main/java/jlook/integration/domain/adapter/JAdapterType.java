package jlook.integration.domain.adapter;

public enum JAdapterType {
	inbound,
	outbound,
	all;
	
	public static final String INBOUND_TYPE = "inbound";
	public static final String OUTBOUND_TYPE = "outbound";
	public static final String ALL_TYPE = "all";
}
