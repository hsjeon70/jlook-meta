package jlook.integration.domain.adapter;

import java.util.Set;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.integration.domain.JMetaKeys;

@jclass(cid=JMetaKeys.JADAPTERINSTANCE, did=JDomainDef.SYSTEM_ID, name=JAdapterInstance.NAME)
public class JAdapterInstance extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAdapterInstance";
	
	public static final String A_JADAPTERDEFINITION = "JAdapterDefinition";
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_CHANNEL 		= "channel";
	
	@jattr(name=A_JADAPTERDEFINITION, label="JAdapterDefinition", type=JAdapterDefinition.NAME, length=15, description="AdapterDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAdapterDefinition jAdapterDefinition;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="AdapterInstance Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="AdapterInstance Description", 
			required=false, unique=false, searchable=false, derived=false) 			private String description;
	@jattr(name=A_CHANNEL, label="Channel", type=JPrimitive.STRING_TYPE, length=100, description="Channel for AdapterInstance", 
			required=true, 	unique=true, searchable=true, derived=false) 		private String channel;
	
	private Set<JAdapterParamInstance> jAdapterParamInstances;
	
	public JAdapterDefinition getJAdapterDefinition() {
		return jAdapterDefinition;
	}
	public void setJAdapterDefinition(JAdapterDefinition jAdapterDefinition) {
		this.jAdapterDefinition = jAdapterDefinition;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public Set<JAdapterParamInstance> getJAdapterParamInstances() {
		return jAdapterParamInstances;
	}
	public void setJAdapterParamInstances(
			Set<JAdapterParamInstance> jAdapterParamInstances) {
		this.jAdapterParamInstances = jAdapterParamInstances;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((jAdapterDefinition == null) ? 0 : jAdapterDefinition
						.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAdapterInstance other = (JAdapterInstance) obj;
		if (jAdapterDefinition == null) {
			if (other.jAdapterDefinition != null)
				return false;
		} else if (!jAdapterDefinition.equals(other.jAdapterDefinition))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
