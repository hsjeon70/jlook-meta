package jlook.integration.infrastructure.loader;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.domain.metadata.JEnum;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.loader.util.JEnumUtil;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationService;
import jlook.integration.domain.adapter.JAdapterParamType;
import jlook.integration.domain.adapter.JAdapterType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component(EnumLoader.ID)
public class EnumLoader extends BaseLoader {
	private static Log logger = Log.getLog(EnumLoader.class);
	
	public static final String ID = "intEnumLoader";
	
	private boolean loaded;
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	@Resource(name=JValidationService.ID)
	private JValidationService jValidationService;
	
	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}
	
	
	 @PostConstruct
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}
			
		}
    }
	 
	 private void initialize() throws JServiceException, JContextException {
		 Class<?>[] enums = new Class<?>[]{
				 JAdapterType.class, JAdapterParamType.class
				 };
		 for(Class<?> enumCls : enums) {
			JEnum jEnum = JEnumUtil.getJEnum(enumCls);
			jValidationService.create(jEnum);
		 }
	 }

}
