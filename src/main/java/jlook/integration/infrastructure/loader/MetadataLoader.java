package jlook.integration.infrastructure.loader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.reflect.Field;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.integration.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.metadata.JValidation;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JAttributeService;
import jlook.framework.service.JClassService;
import jlook.framework.service.JTypeService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Component(MetadataLoader.ID)
public class MetadataLoader extends BaseLoader {
	public static final String ID = "intMetadataLoader";
	
	private static Log logger = Log.getLog(MetadataLoader.class);
	
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	private boolean loaded;

	@Resource(name=JClassService.ID)
	private JClassService jClassSerivce;
	
	@Resource(name=JAttributeService.ID)
	private JAttributeService jAttributeService;
	
	@Resource(name=JTypeService.ID)
	private JTypeService jDataTypeService;
	
	@Resource(name=JValidationService.ID)
	private JValidationService jValidationService;
	
	public boolean isDataLoadEnabled() {
		return dataLoadEnabled;
	}

	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	 @PostConstruct
	 @Override
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}	
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	private void initialize() throws JServiceException, JContextException {	
		
		Map<Long, Class<? extends JObject>> map = new HashMap<Long, Class<? extends JObject>>();
		JMetaKeys[] metas = JMetaKeys.values();
		if(logger.isDebugEnabled()) {
			logger.debug("METADATA CLASS COUNT --> "+metas.length);
		}
		for(JMetaKeys meta : metas) {
			Class<? extends JObject> jclass = meta.getJClass();
			Long classId = meta.getClassId();
			map.put(classId, jclass);
		}
		
		List<Long>  list = new ArrayList<Long>(map.keySet());
		Collections.sort(list);
		
		if(logger.isDebugEnabled()) {
			logger.debug("METADATA CLASS LIST --> "+list);
		}
		for(Long classId : list) {
			Class<? extends JObject> jclass = map.get(classId);
			loadMetadata(jclass);
		}
	}
	
	public void loadMetadata(Class<? extends JObject> jclass) throws JServiceException {
		JClass jClass = handleJClass(jclass);
		if(jClass==null) {
			return;
		}
		Field[] fields = jclass.getDeclaredFields();
		for(Field field : fields) {
			// load JAttribute
			handleJAttr(jClass, field);
		}
	}
	
	private JClass handleJClass(Class<? extends JObject> jclass) throws JServiceException {
		if(logger.isDebugEnabled()) {
			logger.debug(jclass.getSimpleName()+" is creating. --------->");
		}
		jclass jcls = jclass.getAnnotation(jclass.class);
		if(jcls==null) {
			if(logger.isWarnEnabled()) {
				logger.warn("## Cannot find jclass annotiation - "+jclass);
			}
			return null;
		}
		JClass jClass = new JClass();
		
		long domainId = jcls.did();
		if(domainId>0) {
			super.changeDomainId(domainId);
		}
		
		String name = jcls.name();
		jClass.setName(name==null ? jclass.getSimpleName() : name);
		jClass.setLabel(jClass.getName());
		jClass.setType(jcls.type());
		
		String desc = jcls.description();
		jClass.setDescription("".equals(desc) ? jclass.getName() : desc);
		
		Package pckg = jclass.getPackage();
		jClass.setCategory(pckg.getName());
		
		String inheritanceJClass = jcls.inheritanceJClass();
		if(inheritanceJClass!=null && !inheritanceJClass.equals("")) {
			JClass extendsJClass = this.jClassSerivce.findByName(inheritanceJClass);
			jClass.setInheritanceJClass(extendsJClass);
		}
		jClass.setInheritanceType(jcls.inheritanceType());
		jClass.setSummaryFilter(jcls.summaryFilter()==null || jcls.summaryFilter().equals("") ? null : jcls.summaryFilter());
		jClass.setJObjectRule(jcls.JObjectFilter()==null || jcls.JObjectFilter().equals("") ? null : jcls.JObjectFilter());
		jClass.setActionClass(jcls.actionClass()==null || jcls.actionClass().equals("") ? null : jcls.actionClass());
		jClass.setAccessRule(jcls.accessRule());
		jClass.setStatus(jcls.status());
		
		this.jClassSerivce.create(jClass);
		return jClass;
	}
	
	private void handleJAttr(JClass jClass, Field field) throws JServiceException {
		jattr jtt = field.getAnnotation(jattr.class);
		if(jtt==null) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("\t"+field.getName()+" is creating. --------->");
		}
		
		JAttribute jattr = new JAttribute();
		
		jattr.setJClass(jClass);
		jattr.setName(jtt.name());
		jattr.setDescription(jtt.description());
		jattr.setLabel(jtt.label());
		
		String type = jtt.type();
		JType jDataType = this.jDataTypeService.findByName(type);
		if(logger.isDebugEnabled()) {
			logger.debug("\t JDataType("+type+") = "+jDataType);
		}
		jattr.setType(jDataType);
		int length = jtt.length();
		if(length>0) {
			jattr.setLength(length);
		}
		
		jattr.setDefaultValue("".equals(jtt.defaultValue()) ? null : jtt.defaultValue());
		
		jattr.setRequired(jtt.required());
		jattr.setPrimary(jtt.primary());
		jattr.setUnique(jtt.unique());
		jattr.setSearchable(jtt.searchable());
		jattr.setVirtual(jtt.virtual());
		jattr.setReadOnly(jtt.readOnly());
		jattr.setDerived(jtt.derived());
		jattr.setEncrypt(jtt.encrypt().equals("") ? null : jtt.encrypt());
		String validation = jtt.validation();
		if(validation!=null && !validation.equals("")) {
			JValidation jValidation = this.jValidationService.findByName(validation);
			jattr.setJValidation(jValidation);
		}
		jattr.setStatus(jtt.status());
		this.jAttributeService.create(jattr);
	}
}
