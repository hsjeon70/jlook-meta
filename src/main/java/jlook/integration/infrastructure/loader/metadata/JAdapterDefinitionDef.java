package jlook.integration.infrastructure.loader.metadata;

import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.integration.domain.adapter.JAdapterDefinition;
import jlook.integration.domain.adapter.JAdapterType;

public enum JAdapterDefinitionDef {
	FtpClientAdapter("FtpClientAdapter",JAdapterType.OUTBOUND_TYPE,"FTP Client Adapter","org.springframework.integration.file.FileReadingMessageSource");
	
	JAdapterDefinitionDef(String name, String type, String description, String adapterClass) {
		this.instance = new JAdapterDefinition();
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setName(name);
		this.instance.setType(type);
		this.instance.setDescription(description);
		this.instance.setAdapterClass(adapterClass);
	}
	
	public JAdapterDefinition instance;
}
