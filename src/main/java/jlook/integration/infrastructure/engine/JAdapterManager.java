package jlook.integration.infrastructure.engine;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import jlook.framework.infrastructure.util.Log;

import org.springframework.stereotype.Component;

@Component(JAdapterManager.ID)
public class JAdapterManager {
	public static final String ID = "jAdapterManager";
	private static Log logger = Log.getLog(JAdapterManager.class);
	
	@PostConstruct
	public void doInit() {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> ");
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started. ---------> ");
		}
	}
	
	@PreDestroy
	public void destroy() {
		if(logger.isDebugEnabled()) {
			logger.debug("It is destorying. ---------> ");
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is destroyed. ---------> ");
		}
	}
}
