package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.common.JPreferenceDetail;


public interface JPreferenceRepository {
	public static final String ID = "jPreferenceRepository";
	
	public JPreference select(Long objectId) throws JRepositoryException;
	public JPreference select(String name, String type) throws JRepositoryException;
	public List<JPreference> select(String type) throws JRepositoryException;
	public JPreferenceDetail select(JPreference jPreference, Long target) throws JRepositoryException;
	public void update(JPreferenceDetail detail) throws JRepositoryException;
	public void insert(JPreference jpref) throws JRepositoryException;
	public void insert(JPreferenceDetail detail) throws JRepositoryException;
	public void delete(List<JPreferenceDetail> details) throws JRepositoryException;
	public List<JPreferenceDetail> select(JPreferenceType type, Long target) throws JRepositoryException;
}
