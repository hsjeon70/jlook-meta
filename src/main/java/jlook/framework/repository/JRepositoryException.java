package jlook.framework.repository;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.annotation.Message;

@Message(text=JRepositoryException.TEXT)
public class JRepositoryException extends JException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Data Error - {0}";
	
	public JRepositoryException(String message, Throwable cause, Object... parameter) {
		super(message, cause, parameter);
	}

	public JRepositoryException(String message, Object[] parameter) {
		super(message, null, parameter);
	}

	public JRepositoryException(Throwable cause, Object... parameter) {
		super(TEXT, cause, parameter);
	}
	
	public JRepositoryException(Object[] parameters) {
		this(TEXT, parameters);
	}
	
}
