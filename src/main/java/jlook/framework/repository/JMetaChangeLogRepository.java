package jlook.framework.repository;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JMetaChangeLog;

public interface JMetaChangeLogRepository {
	public static final String ID = "jMetaChangeLogRepository";
	
	public void insert(JMetaChangeLog log) throws JRepositoryException;
	
	public void delete(JAttribute jAttribute) throws JRepositoryException;
	public void delete(JClass jClass) throws JRepositoryException;
	
}
