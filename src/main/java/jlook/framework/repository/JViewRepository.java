package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;

public interface JViewRepository {
	public static final String ID = "jViewRepository";
	
	public void insert(JView jView) throws JRepositoryException;
	public void update(JView jView) throws JRepositoryException;
	public void insert(JViewDetail detail) throws JRepositoryException;
	
	public JView select(Long jViewId) throws JRepositoryException;
	public JView select(JClass jClass, String name) throws JRepositoryException;
	
	public void delete(JView jView) throws JRepositoryException;
	public void delete(JViewDetail detail) throws JRepositoryException;
	
	public List<JViewDetail> select(JAttribute jAttribute) throws JRepositoryException;
	public List<JView> select(JClass jClass) throws JRepositoryException;
	
}	
