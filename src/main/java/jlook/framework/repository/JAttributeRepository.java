package jlook.framework.repository;


import jlook.framework.domain.metadata.JAttribute;

public interface JAttributeRepository {
	public static final String ID = "jAttributeRepository";
	
	public void insert(JAttribute jattr) throws JRepositoryException;
	public void delete(JAttribute jattr) throws JRepositoryException;
	public void update(JAttribute jattr) throws JRepositoryException;
	
}
