package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.metadata.JClass;


public interface JObjectRatingRepository {
	public static final String ID = "jObjectRatingRepository";
	
	public void insert(JObjectRating jObjectRating) throws JRepositoryException;
	
	public JObjectRating selectByTargetClass(JClass targetClass) throws JRepositoryException;
	
	public void insert(JObjectRatingDetail detail) throws JRepositoryException;
	public List<JObjectRatingDetail> select(JObjectRating jObjectRating, Long target) throws JRepositoryException;
}
