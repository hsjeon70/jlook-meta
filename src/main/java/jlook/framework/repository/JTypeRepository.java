package jlook.framework.repository;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;

public interface JTypeRepository {
	public static final String ID = "jDataTypeRepository";
	
	public void insert(JType jDataType) throws JRepositoryException;
	public JType selectByName(String name) throws JRepositoryException;
	public void update(JType jDataType) throws JRepositoryException;
	public JType selectByJClass(JClass jClass) throws JRepositoryException;
	
	public void delete(JType jDataType) throws JRepositoryException;
}
