package jlook.framework.repository;

import java.util.List;
import java.util.Map;

import jlook.framework.domain.JObject;
import jlook.framework.infrastructure.util.PageUtil;


public interface JGenericRepository {
	public static final String ID = "jGenericRepository";
	
	public Map<String,Object> selectSummaryData(
			String entityName, String filter, PageUtil page, Map<String,String[]> keywordAttrMap, 
			Map<String,String[]> searchAttrMap, List<String> parentAttrList, JObject parentJObject) throws JRepositoryException;
	
	public void insert(JObject jObject) throws JRepositoryException;
	public void update(JObject jObject) throws JRepositoryException;
	public JObject select(String entityName, Long objectId) throws JRepositoryException;
	public void delete(JObject jObject) throws JRepositoryException;
	public boolean existsPrimaryKey(Long objectId, String entityName, Map<String, Object> map) throws JRepositoryException;
}
