package jlook.framework.repository.impl;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.batch.JBatchHistory;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JBatchHistoryRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JBatchHistoryRepository.ID)
public class JBatchHistoryRepositoryImpl extends HibernateRepository implements
		JBatchHistoryRepository {

	@Override
	public void insert(JBatchHistory jBatchHisotry) throws JRepositoryException {
		try {
			this.template.save(jBatchHisotry);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JBatchHistory", e);
		}
	}

	@Override
	public void update(JBatchHistory jBatchHisotry) throws JRepositoryException {
		try {
			this.template.update(jBatchHisotry);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JBatchHistory", e);
		}
	}

	

}
