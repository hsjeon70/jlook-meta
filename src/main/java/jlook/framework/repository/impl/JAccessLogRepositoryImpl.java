package jlook.framework.repository.impl;

import jlook.framework.domain.management.JAccessLog;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JAccessLogRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JAccessLogRepository.ID)
public class JAccessLogRepositoryImpl extends HibernateRepository implements
		JAccessLogRepository {

	@Override
	public void insert(JAccessLog jAccessLog) throws JRepositoryException {
		try {
			this.template.save(jAccessLog);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JAccessLog.",e);
		}
	}

}
