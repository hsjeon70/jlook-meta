package jlook.framework.repository.impl;

import java.util.ArrayList;
import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JDataAccessRuleRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JDataAccessRuleRepository.ID)
public class JDataAccessRuleRepositoryImpl extends HibernateRepository
		implements JDataAccessRuleRepository {
	private static Log logger = Log.getLog(JDataAccessRuleRepositoryImpl.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JDataAccessRule> select(JClass jClass, List<String> roleList)
			throws JRepositoryException {
		if(roleList==null || roleList.size()==0) {
			return new ArrayList<JDataAccessRule>();
		}
		
		StringBuffer sql = new StringBuffer();
		sql.append("select dar from JDataAccessRule as dar").append("\n");
		sql.append(" inner join dar.JRole as role ").append("\n");
		sql.append(" where dar.JClass=? ").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (dar.domainId=? or dar.domainId=?)").append("\n");
		}
		sql.append(" and (");
		for(int i=0;i<roleList.size();i++) {
			String role = roleList.get(i);
			sql.append(" role.name='").append(role).append("'");
			if(i<roleList.size()-1) {
				sql.append(" or");
			}
		}
		sql.append(")");
		if(logger.isDebugEnabled()) {
			logger.debug("--> sql=\n"+sql);
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jClass,  super.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jClass};
		}
		try {
			List<?> result = this.template.find(sql.toString(), params);
			return (List<JDataAccessRule>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JDataAccessRule list.",e);
		}
	}

	@Override
	public void insert(JDataAccessRule jDataAccessRule)
			throws JRepositoryException {
		try {
			this.template.save(jDataAccessRule);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JDataAccessRule.",e);
		}
	}


}
