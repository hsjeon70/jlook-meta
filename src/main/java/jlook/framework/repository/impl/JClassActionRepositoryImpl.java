package jlook.framework.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JClassActionRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JClassActionRepository.ID)
public class JClassActionRepositoryImpl extends HibernateRepository implements
		JClassActionRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<JClassAction> selectByJClass(JClass jClass, JClassActionType type)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JClassAction jca where jca.JClass=? and (jca.type=? or jca.type=?)").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jca.domainId=? or jca.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jClass, type.name(), JClassActionType.ALL_TYPE, super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jClass, type.name(), JClassActionType.ALL_TYPE};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
			return (List<JClassAction>)result;
		} catch(Exception e) {
			e.printStackTrace();
			throw new JRepositoryException("Fail to select JClassAction. - "+jClass,e);
		}
	}

	@Override
	public void insert(JClassAction jClassAction) throws JRepositoryException {
		try {
			this.template.save(jClassAction);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JClassAction. - "+e.getMessage(), e);
		}
	}

}
