package jlook.framework.repository.impl;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.security.JMenuAccessRule;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JMenuAccessRuleRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JMenuAccessRuleRepository.ID)
public class JMenuAccessRuleRepositoryImpl extends HibernateRepository
		implements JMenuAccessRuleRepository {

	@Override
	public void insert(JMenuAccessRule entity) throws JRepositoryException {
		try {
			this.template.save(entity);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JMenuAccessRule.",e);
		}
 	}

}
