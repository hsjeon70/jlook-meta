package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JViewRepository;

import org.springframework.stereotype.Repository;


@Repository(JViewRepository.ID)
public class JViewRepositoryImpl extends HibernateRepository implements
		JViewRepository {
	
	@Override
	public void insert(JView jView) throws JRepositoryException {
		try {
			this.template.save(jView);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JView.", e);
		}
	}

	@Override
	public void insert(JViewDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JViewDetail.", e);
		}
	}

	@Override
	public JView select(Long jViewId) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JView jv where jv.objectId=?").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jv.domainId=? or jv.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jViewId, super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jViewId};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			e.printStackTrace();
			throw new JRepositoryException("Fail to select JView. - "+jViewId,e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JView)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JView.", new Object[]{jViewId});
		}
	}

	@Override
	public JView select(JClass jClass, String name)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JView jv where jv.JClass=? and jv.name=? ").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jv.domainId=? or jv.domainId=?)");
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jClass, name, this.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jClass, name};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JView. - "+jClass.getName()+", view="+name,e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JView)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JView.", new Object[]{jClass.getName()+", view="+name});
		}
	}

	@Override
	public void delete(JView jView) throws JRepositoryException {
		try {
			this.template.delete(jView);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JView."+jView, e);
		}
	}

	@Override
	public void delete(JViewDetail detail) throws JRepositoryException {
		try {
			this.template.delete(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JViewDetail."+detail, e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JViewDetail> select(JAttribute jAttribute)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JViewDetail jvd where jvd.JAttribute=?").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jvd.domainId=? or jvd.domainId=?)");
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jAttribute, this.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jAttribute};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
			return (List<JViewDetail>) result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JViewDetail list. - "+jAttribute,e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JView> select(JClass jClass) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JView jv where jv.JClass=?").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jvd.domainId=? or jvd.domainId=?)");
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jClass, this.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jClass};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
			return (List<JView>) result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JView list. - "+jClass,e);
		}
	}

	@Override
	public void update(JView jView) throws JRepositoryException {
		try {
			this.template.save(jView);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JView.", e);
		}
	}
}
