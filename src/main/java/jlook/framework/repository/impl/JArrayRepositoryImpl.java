package jlook.framework.repository.impl;

import java.util.List;
import java.util.Set;

import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JArrayRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JArrayRepository.ID)
public class JArrayRepositoryImpl extends HibernateRepository implements
		JArrayRepository {

	@Override
	public void insert(JArray jArray) throws JRepositoryException {
		try {
			this.template.save(jArray);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JArray.", e);
		}
	}

	@Override
	public void delete(JArray jArray) throws JRepositoryException {
		try {
			this.template.delete(jArray);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JArray.", e);
		}
	}

	@Override
	public JArray select(Long objectId) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JArray ja where ja.objectId=?");
		if(!isAdministrator()) {
			sql.append(" and (ja.domainId=? or ja.domainId=?)");
		}
		List<?> result = null;
		try {
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JArray("+objectId+")",e);
		}
		if(result!=null && result.size()==1) {
			return (JArray)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JArray", new Object[]{objectId});
		}
	}

	@Override
	public JArray selectByName(String name) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JArray ja where ja.name=?");
		if(!isAdministrator()) {
			sql.append(" and (ja.domainId=? or ja.domainId=?)");
		}
		List<?> result = null;
		try {
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{name, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{name};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JArray("+name+")",e);
		}
		if(result!=null && result.size()==1) {
			return (JArray)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JArray", new Object[]{name});
		}
	}

	@Override
	public void update(JArray jArray) throws JRepositoryException {
		try {
			this.template.save(jArray);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JArray.", e);
		}
	}

	@Override
	public void deleteAll(Set<JArrayDetail> values) throws JRepositoryException {
		try {
			this.template.deleteAll(values);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JArrayDetail.", e);
		}
	}

	@Override
	public void insert(JArrayDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JArrayDetail.", e);
		}
	}

	@Override
	public void delete(JArrayDetail detail) throws JRepositoryException {
		try {
			this.template.delete(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JArrayDetail.", e);
		}
	}

	@Override
	public void update(JArrayDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JArrayDetail.", e);
		}
	}

}
