package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JObjectRatingRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JObjectRatingRepository.ID)
public class JObjectRatingRepositoryImpl extends HibernateRepository implements
		JObjectRatingRepository {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JObjectRatingRepositoryImpl.class);

	@Override
	public void insert(JObjectRating jObjectRating) throws JRepositoryException {
		try {
			this.template.save(jObjectRating);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JObjectRating. - "+jObjectRating, e);
		}
	}

	@Override
	public JObjectRating selectByTargetClass(JClass targetClass) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JObjectRating rt where rt.targetClass=?");
			if(!isAdministrator()) {				
				sql.append(" and (rt.domainId=? or rt.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{targetClass,super.getDomainId(),JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{targetClass};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JObjectRating by targetClass("+targetClass+"). - ", e);
		}
		if(result!=null && result.size()==1) {
			return (JObjectRating)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JObjectRating.",new Object[]{targetClass});
		}
	}

	@Override
	public void insert(JObjectRatingDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JObjectRatingDetail. - "+detail, e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JObjectRatingDetail> select(JObjectRating jObjectRating,
			Long target) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JObjectRatingDetail ord where ord.JObjectRating=? and ord.target=?");
			if(!isAdministrator()) {
				sql.append(" and (ord.domainId=? or ord.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{jObjectRating, target, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{jObjectRating, target};
			}
			result = this.template.find(sql.toString(), params);
			return (List<JObjectRatingDetail>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JObjectRatingDetail list. - "+jObjectRating+", target="+target, e);
		}
	}
	
	
	
}
