package jlook.framework.repository.impl;

import jlook.framework.domain.common.JAnnounceMessage;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JAnnounceRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JAnnounceRepository.ID)
public class JAnnounceRepositoryImpl extends HibernateRepository implements
		JAnnounceRepository {

	@Override
	public void insert(JAnnounceMessage message) throws JRepositoryException {
		try {
			this.template.save(message);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JAnnounceMessage.",e);
		}
	}
	
	
}
