package jlook.framework.repository.impl;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchParamName;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JBatchDefinitionRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JBatchDefinitionRepository.ID)
public class JBatchDefinitionRepositoryImpl extends HibernateRepository implements
		JBatchDefinitionRepository {

	@Override
	public void insert(JBatchDefinition jBatchDefinition) throws JRepositoryException {
		try {
			this.template.save(jBatchDefinition);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JBatchDefinition.", e);
		}
	}

	@Override
	public void update(JBatchDefinition jBatchDefinition) throws JRepositoryException {
		try {
			this.template.update(jBatchDefinition);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JBatchDefinition.", e);
		}
	}

	@Override
	public void insert(JBatchParamName jBatchParamName)
			throws JRepositoryException {
		try {
			this.template.save(jBatchParamName);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JBatchParamName.", e);
		}
	}

}
