package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.metadata.JValidation;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JValidationRepository;

import org.springframework.stereotype.Repository;


@Repository(JValidationRepository.ID)
public class JValidationRepositoryImpl extends HibernateRepository implements
		JValidationRepository {

	@Override
	public void insert(JValidation jValidation) throws JRepositoryException {
		try {
			this.template.save(jValidation);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JValidation.", e);
		}
	}

	@Override
	public void insert(JEnumDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JEnumDetail.", e);
		}
	}

	@Override
	public JValidation selectByName(String name) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JValidation jv where jv.name=?");
		if(!isAdministrator()) {
			sql.append(" and (jv.domainId=? or jv.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{name,super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{name};
		}
		List<?> result = this.template.find(sql.toString(), params);
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JValidation)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JValidation.", new Object[]{name});
		}
	}

	@Override
	public void delete(JValidation jValidation) throws JRepositoryException {
		try {
			this.template.delete(jValidation);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JValidation.", e);
		}
	}

}
