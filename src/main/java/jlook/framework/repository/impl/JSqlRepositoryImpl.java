package jlook.framework.repository.impl;

import java.util.List;
import java.util.Map;

import jlook.framework.infrastructure.jdbc.SpringJdbcRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JSqlRepository;

import org.springframework.stereotype.Repository;


/**
 * Only for administrator
 * @author hsjeon70
 *
 */
@Repository(JSqlRepository.ID)
public class JSqlRepositoryImpl extends SpringJdbcRepository implements JSqlRepository {

	@Override
	public void run(String sql) throws JRepositoryException {
		this.jdbcTemplate.update(sql);
	}

	@Override
	public int count(String table) throws JRepositoryException {
		try {
			return this.jdbcTemplate.queryForInt("select count(*) from "+table);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to query for int. - "+table, e);
		}
	}

	@Override
	public List<Map<String, Object>> query(String sql)
			throws JRepositoryException {
		try {
			return this.jdbcTemplate.queryForList(sql);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to query for list. - "+sql, e);
		}
	}

	@Override
	public int count(String table, Long objectId) throws JRepositoryException {
		try {
			return this.jdbcTemplate.queryForInt("select count(*) from "+table+" where objectId="+objectId);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to count. - "+table+", objectId="+objectId, e);
		}
	}

}
