package jlook.framework.repository.impl;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JAttributeRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JAttributeRepository.ID)
public class JAttributeRepositoryImpl extends HibernateRepository implements
		JAttributeRepository {

	@Override
	public void insert(JAttribute jattr) throws JRepositoryException {
		try {
			this.template.save(jattr);
		} catch(Exception e) {
			throw new JRepositoryException("Fail insert JAttribute. - "+jattr,e);
		}
	}

	@Override
	public void delete(JAttribute jattr) throws JRepositoryException {
		try {
			this.template.delete(jattr);
		} catch(Exception e) {
			throw new JRepositoryException("Fail delete JAttribute. - "+jattr,e);
		}
	}

	@Override
	public void update(JAttribute jattr) throws JRepositoryException {
		try {
			this.template.update(jattr);
		} catch(Exception e) {
			throw new JRepositoryException("Fail update JAttribute. - "+jattr,e);
		}
	}

}
