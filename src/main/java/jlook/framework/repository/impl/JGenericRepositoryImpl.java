package jlook.framework.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jlook.framework.Constants;
import jlook.framework.domain.JObject;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.repository.JGenericRepository;
import jlook.framework.repository.JRepositoryException;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.stereotype.Repository;


@Repository(JGenericRepository.ID)
public class JGenericRepositoryImpl extends HibernateRepository implements JGenericRepository {
	private static Log logger = Log.getLog(JGenericRepositoryImpl.class);
	
	@Override
	public Map<String, Object> selectSummaryData(String entityName, String filter, PageUtil page, Map<String,String[]> keywordAttrMap,
			Map<String,String[]> searchAttrMap, List<String> parentAttrList, JObject parentJObject) throws JRepositoryException {
		Session session = this.template.getSessionFactory().getCurrentSession();
		Junction domainFilter = null;
		if(!isAdministrator()) {
			domainFilter = Restrictions.conjunction().add(
				Restrictions.or(Restrictions.eq(JObject.A_DID,this.getDomainId()), Restrictions.eq(JObject.A_DID,JDomainDef.SYSTEM_ID) ));
		}
		
		/**
		 * Search Filter : and
		 */
		Junction keywordFilter = null;
		if(keywordAttrMap!=null && keywordAttrMap.size()>0) {
			LogicalExpression lExp = null;
			Criterion beforeExp = null, current = null;
			for(String attrName : keywordAttrMap.keySet()) {
				String[] attrValues = keywordAttrMap.get(attrName);
				if(attrValues==null || attrValues.length==0) {
					continue;
				}
				// TODO OTHER DATA TYPE.
				current = getCriterion(attrName, attrValues);
				if(current==null) {
					continue;
				}
				if(beforeExp==null) {
					beforeExp = current;
					continue;
				}
				if(lExp==null) {
					lExp = Restrictions.or(beforeExp,current);
				} else {
					lExp = Restrictions.or(lExp, current);
				}
				beforeExp = current;
			}
			
			// TODO : admin can search by objectId
			if(super.isAdministrator()) {
				if(lExp==null) {
					
				} else {
					
				}
			}
			if(beforeExp!=null) {
				keywordFilter = Restrictions.conjunction().add(lExp==null? beforeExp : lExp);
			}
		}
		
		Junction searchFilter = null;
		if(searchAttrMap!=null && searchAttrMap.size()>0) {
			LogicalExpression lExp = null;
			Criterion beforeExp = null, current = null;
			for(String attrName : searchAttrMap.keySet()) {
				String[] attrValues = searchAttrMap.get(attrName);
				if(attrValues==null || attrValues.length==0) {
					continue;
				}
				// TODO OTHER DATA TYPE.
				current = getCriterion(attrName, attrValues);
				if(current==null) {
					continue;
				}
				if(beforeExp==null) {
					beforeExp = current;
					continue;
				}
				if(lExp==null) {
					lExp = Restrictions.and(beforeExp,current);
				} else {
					lExp = Restrictions.and(lExp, current);
				}
				beforeExp = current;
			}
			
			if(beforeExp!=null) {
				searchFilter = Restrictions.conjunction().add(lExp==null? beforeExp : lExp);
			}
		}
		
		/**
		 * parent reference handling
		 */
		Junction parentFilter = null;
		if(parentAttrList!=null && parentAttrList.size()>0 && parentJObject!=null) {
			LogicalExpression lExp = null;
			Criterion beforeExp = null, current = null;
			for(String attrName : parentAttrList) {
				if(current==null) {
					current = Restrictions.eq(attrName, parentJObject);
					continue;
				}
				
				if(beforeExp==null) {
					beforeExp = current;
					current = Restrictions.eq(attrName, parentJObject);
					lExp = Restrictions.or(beforeExp,current);
				} else {
					beforeExp = current;
					current = Restrictions.eq(attrName, parentJObject);
					lExp = Restrictions.or(lExp, current);
				}
			}
			if(current!=null) {
				parentFilter = Restrictions.disjunction().add(lExp==null? current : lExp);
			}
		}
		
		Criteria criteria = session.createCriteria(entityName);
		if(parentFilter!=null) {
			criteria.add(parentFilter);
		}
		if(domainFilter!=null) {
			criteria.add(domainFilter);
		}
		if(keywordFilter!=null) {
			criteria.add(keywordFilter);
		}
		if(searchFilter!=null) {
			criteria.add(searchFilter);
		}
		if(filter!=null && !filter.equals("")) {
			criteria.add(Restrictions.sqlRestriction(filter));
		}
		
		if(page.getPageSize()>0) {
			int firstResult = page.getPageSize() * (page.getPageNumber() - 1);
			int maxResults  = firstResult+page.getPageSize();
			if(logger.isDebugEnabled()) {
				logger.debug("select --> "+entityName+" : "+firstResult+"~"+maxResults);
			}
			
			criteria.setFirstResult(firstResult);
			criteria.setMaxResults(maxResults);
		}
		
		List<String> orderBy = page.getOrderBy();
		if(orderBy!=null) {
			for(String item : orderBy) {
				int idx = item.indexOf(" ");
				if(idx==-1) {
					continue;
				}
				if(item.endsWith("asc")) {
					criteria.addOrder(Order.asc(item.substring(0,idx)));
				} else if(item.endsWith("desc")) {
					criteria.addOrder(Order.desc(item.substring(0,idx)));
				}
			}
		} else {
			criteria.addOrder(Order.desc(JObject.A_OID));
		}
		
		List<?> result;
		try {
			result = criteria.list();
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select summary data. - "+entityName,e);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("--> RESULT COUNT="+result.size());
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(Constants.DATA, result);
		 
		try {
			criteria = session.createCriteria(entityName);
			if(parentFilter!=null) {
				criteria.add(parentFilter);
			}
			if(domainFilter!=null) {
				criteria.add(domainFilter);
			}
			if(keywordFilter!=null) {
				criteria.add(keywordFilter);
			}
			if(searchFilter!=null) {
				criteria.add(searchFilter);
			}
			if(filter!=null && !filter.equals("")) {
				criteria.add(Restrictions.sqlRestriction(filter));
			}
			
			criteria.setProjection(Projections.rowCount());
			Long totalCount = (Long)criteria.uniqueResult();
			if(logger.isDebugEnabled()) {
				logger.debug("Total Count="+totalCount);
			}
			map.put(Constants.COUNT, totalCount);
			return map;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select total count.",e);
		}		
	}

	@Override
	public void insert(JObject jObject) throws JRepositoryException {
		try {
			this.template.save(jObject);
		} catch(Exception e) {
			throw new JRepositoryException("Cannot insert JObject.",e);
		}
	}

	@Override
	public JObject select(String entityName, Long objectId)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from ").append(entityName).append(" jobj where jobj.objectId=?");
		if(!isAdministrator()) {
			sql.append(" and (jobj.domainId=? or jobj.domainId=?)");
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Sql--> "+sql);
		}
		List<?> result = null;
		try {
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId, this.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JObject("+entityName+", "+objectId+").",e);
		}
		
		if(result!=null && result.size()==1) {
			return (JObject)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JObject {0} - {1}", new Object[]{entityName, objectId});
		}
	}

	@Override
	public void delete(JObject jObject) throws JRepositoryException {
		try {
			this.template.delete(jObject);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JObject - "+jObject,e);
		}
	}
	
	/**
	 * TODO : save or update method Error - a different object with the same identifier value was already associated with the session:
	 */
	@Override
	public void update(JObject jObject) throws JRepositoryException {
		try {
			this.template.merge(jObject);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JObject - "+jObject,e);
		}
	}

	@Override
	public boolean existsPrimaryKey(Long objectId, String entityName, Map<String, Object> map)
			throws JRepositoryException {
		if(map!=null && map.size()==0) {
			return true;
		}
		Session session = this.template.getSessionFactory().getCurrentSession();
		if(logger.isDebugEnabled()) {
			logger.debug("select --> "+entityName+" : primary keys - "+map);
		}
		
		Junction filter = Restrictions.conjunction().add(
				Restrictions.or(Restrictions.eq(JObject.A_DID,this.getDomainId()), Restrictions.eq(JObject.A_DID,JDomainDef.SYSTEM_ID) )); 
		
		for(String name : map.keySet()) {
			Object value = map.get(name);
			SimpleExpression expr = Restrictions.eq(name, value);
			filter.add(expr);
		}
		
		if(objectId!=null) {
			filter.add(Restrictions.ne(JObject.A_OID, objectId));
		}
		Criteria criteria = session.createCriteria(entityName);
		criteria.add(filter);
		
		List<?> result = criteria.list();
		if(result.size()==0) {
			return false;
		} else if(result.size()==1) {
			return true;
		}
		throw new JRepositoryException("Duplicated key data. {0} - {1}", new Object[]{entityName, map});
	}
	
	private Criterion getCriterion(String attrName, String[] values) {
//		current = Restrictions.sqlRestriction("lower("+attrName+") like lower(?)", "%"+attrValue+"%", Hibernate.STRING);
		Criterion current = null;
		for(String value : values) {
			if(value==null || value.equals("")) {
				continue;
			}
			@SuppressWarnings("deprecation")
			Criterion c = Restrictions.sqlRestriction("lower("+attrName+") like lower(?)", "%"+value+"%", Hibernate.STRING);
			if(current==null) {
				current = c;
			} else {
				current = Restrictions.or(current, c);
			}
		}
		
		return current;
	}

}
