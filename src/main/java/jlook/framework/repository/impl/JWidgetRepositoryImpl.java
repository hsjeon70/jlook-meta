package jlook.framework.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamName;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JWidgetRepository;

@Repository(JWidgetRepository.ID)
public class JWidgetRepositoryImpl extends HibernateRepository implements
		JWidgetRepository {

	@Override
	public void insert(JWidgetDefinition jWidgetDefinition)
			throws JRepositoryException {
		try {
			this.template.save(jWidgetDefinition);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JWidgetDefinition. - "+e.getMessage(),e);
		}
	}

	@Override
	public void insert(JWidgetParamName jWidgetParamName)
			throws JRepositoryException {
		try {
			this.template.save(jWidgetParamName);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JWidgetParamName. - "+e.getMessage(),e);
		}
	}

	@Override
	public JWidgetDefinition selectDefinition(Long objectId)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JWidgetDefinition jw where jw.objectId=?").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jw.domainId=? or jw.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{objectId, super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{objectId};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			e.printStackTrace();
			throw new JRepositoryException("Fail to select JWidgetDefinition. - "+objectId,e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JWidgetDefinition)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JWidgetDefinition.", new Object[]{objectId});
		}
	}

	@Override
	public JWidgetInstance selectInstance(Long objectId)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JWidgetInstance jw where jw.objectId=?").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jw.domainId=? or jw.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{objectId, super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{objectId};
		}
		
		List<?> result = null;
		try {
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			e.printStackTrace();
			throw new JRepositoryException("Fail to select JWidgetInstance. - "+objectId,e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JWidgetInstance)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JWidgetInstance.", new Object[]{objectId});
		}
	}

}
