package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserDomain;
import jlook.framework.domain.account.JUserRole;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JUserRepository;

import org.springframework.stereotype.Repository;


@Repository(JUserRepository.ID)
public class JUserRepositoryImpl extends HibernateRepository implements
		JUserRepository {
	private static Log logger = Log.getLog(JUserRepositoryImpl.class);
	
	@Override
	public JUser selectByEmail(String email)  throws JRepositoryException {
		Long domainId = super.getDomainId();
		if(logger.isDebugEnabled()) {
			logger.debug("selectByEmail - email="+email+", domainId="+domainId);
		}
		
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JUser ju where ju.email=?");
			if(!isAdministrator()) {
				sql.append(" and (ju.domainId=? or ju.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{email,domainId,JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{email};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JUser by email("+email+").", e);
		}
		if(result!=null && result.size()==1) {
			return (JUser)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JUser",new Object[]{email});
		}
	}

	@Override
	public void insert(JUser jUser) throws JRepositoryException {
		if(logger.isDebugEnabled()) {
			logger.debug(jUser.getEmail()+" is creating. - "+jUser.getJRoles());
		}
		try {
			this.template.save(jUser);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JUser. - "+jUser, e);
		}
	}

	@Override
	public void delete(JUser jUser) throws JRepositoryException {
		try {
			this.template.delete(jUser);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JUser. - "+jUser, e);
		}
	}

	@Override
	public void insert(JUserRole jUserRole) throws JRepositoryException {
		try {
			this.template.save(jUserRole);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JUserRole. - "+jUserRole, e);
		}
	}

	@Override
	public void insert(JUserDomain jUserDomain) throws JRepositoryException {
		try {
			this.template.save(jUserDomain);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JUserDomain. - "+jUserDomain, e);
		}
	} 
}
