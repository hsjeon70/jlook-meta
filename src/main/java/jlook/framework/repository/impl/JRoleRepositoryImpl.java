package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JRoleRepository;

import org.springframework.stereotype.Repository;


@Repository(JRoleRepository.ID)
public class JRoleRepositoryImpl extends HibernateRepository implements
		JRoleRepository {

	@Override
	public void insert(JRole role) throws JRepositoryException {
		this.template.save(role);
	}

	@Override
	public JRole selectByName(String name) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JRole jr where jr.name=?");
		if(!isAdministrator()) {
			sql.append(" and (jr.domainId=? or jr.domainId=?)");
		}
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{name,super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{name};
		}
		List<?> result = this.template.find(sql.toString(), params);
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JRole)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JRole.",new Object[]{name});
		}
	}

}
