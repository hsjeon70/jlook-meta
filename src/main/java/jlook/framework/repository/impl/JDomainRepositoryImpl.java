package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.account.JDomain;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JDomainRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JDomainRepository.ID)
public class JDomainRepositoryImpl extends HibernateRepository implements
		JDomainRepository {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JDomainRepositoryImpl.class);
	
	@Override
	public void insert(JDomain jDomain) throws JRepositoryException {
		try {
			this.template.save(jDomain);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JDomain. - "+jDomain, e);
		}
	}

	@Override
	public JDomain selectByName(String name) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JDomain jr where jr.name=?");
		if(!isAdministrator()) {
			sql.append(" and (jr.domainId=? or jr.domainId=?)");
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{name,super.getDomainId(),JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{name};
		}
		List<?> result = this.template.find(sql.toString(), params);
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JDomain)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JDomain.", new Object[]{name});
		}
	}

	@Override
	public JDomain select(Long id) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("from JDomain jr where jr.objectId=?");
		
		List<?> result = this.template.find(sql.toString(), new Object[]{id});
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JDomain)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JDomain.",new Object[]{id});
		}
	}

	@Override
	public void update(JDomain jDomain) throws JRepositoryException {
		try {
			this.template.update(jDomain);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JDomain. - "+jDomain,e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JDomain> selectAllList() throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JDomain jd").append(" ");
			if(!this.isAdministrator()) {
				sql.append("where jd.active=TRUE");
			}
			result = this.template.find(sql.toString());
			return (List<JDomain>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JDomain list.",e);
		}
	}

}
