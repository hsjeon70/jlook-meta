package jlook.framework.repository.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JMetaChangeLog;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JMetaChangeLogRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JMetaChangeLogRepository.ID)
public class JMetaChangeLogRepositoryImpl extends HibernateRepository implements
		JMetaChangeLogRepository {

	@Override
	public void insert(JMetaChangeLog log) throws JRepositoryException {
		try {
			this.template.save(log);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JMetaChangeLog. - "+e.getMessage(), e);
		}
	}

	@Override
	public void delete(JAttribute jAttribute) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from JMetaChangeLog mcl where mcl.JAttribute=?");
		Session session = this.template.getSessionFactory().getCurrentSession();
		Query query = session.createQuery(sql.toString());
		query.setParameter(0, jAttribute);
		try {
			query.executeUpdate();
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JMetaChangeLog. - "+jAttribute, e);
		}
	}

	@Override
	public void delete(JClass jClass) throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from JMetaChangeLog mcl where mcl.JClass=?");
		Session session = this.template.getSessionFactory().getCurrentSession();
		Query query = session.createQuery(sql.toString());
		query.setParameter(0, jClass);
		try {
			query.executeUpdate();
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JMetaChangeLog. - "+jClass, e);
		}
	}

}
