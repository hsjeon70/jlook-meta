package jlook.framework.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JBatchInstanceRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JBatchInstanceRepository.ID)
public class JBatchInstanceRepositoryImpl extends HibernateRepository implements
		JBatchInstanceRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<JBatchInstance> selectAll() throws JRepositoryException {
		try {
			return (List<JBatchInstance>)this.template.find("from JBatchInstance jbi where jbi.active=true");
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JBatchInstances.", e);
		}
	}

	@Override
	public void insert(JBatchInstance jBatchInstance)
			throws JRepositoryException {
		try {
			this.template.save(jBatchInstance);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JBatchInstance.", e);
		}
	}

	@Override
	public void update(JBatchInstance jBatchInstance)
			throws JRepositoryException {
		try {
			this.template.update(jBatchInstance);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JBatchInstance.", e);
		}
	}

	@Override
	public void insert(JBatchParamValue jBatchParamValue)
			throws JRepositoryException {
		try {
			this.template.save(jBatchParamValue);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JBatchParamValue.", e);
		}
	}

}
