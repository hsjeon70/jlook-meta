package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JAttachmentRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JAttachmentRepository.ID)
public class JAttachmentRepositoryImpl extends HibernateRepository implements JAttachmentRepository {

	@Override
	public void insert(JAttachment attachment) throws JRepositoryException {
		try {
			this.template.save(attachment);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JAttachment. - "+attachment,e);
		}
	}
	
	@Override
	public void update(JAttachment attachment) throws JRepositoryException {
		try {
			this.template.save(attachment);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JAttachment. - "+attachment,e);
		}
	}

	@Override
	public JAttachment select(Long objectId) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JAttachment jatt where jatt.objectId=?");
			if(!isAdministrator()) {
				sql.append(" and (jatt.domainId=? or jatt.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail select JAttachment("+objectId+").",e);
		}
		
		if(result!=null && result.size()==1) {
			return (JAttachment)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JAttachment", new Object[]{objectId});
		}
	}

	@Override
	public JAttachment selectByName(String name) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JAttachment jatt where jatt.name=?");
			if(!isAdministrator()) {
				sql.append(" and (jatt.domainId=? or jatt.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{name, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{name};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail select JAttachment("+name+").",e);
		}
		
		if(result!=null && result.size()==1) {
			return (JAttachment)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JAttachment", new Object[]{name});
		}
	}

	@Override
	public void delete(JAttachment attachment) throws JRepositoryException {
		try {
			this.template.delete(attachment);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JAttachment. - "+attachment,e);
		}
	}

	@Override
	public JAttachmentDetail selectDetail(Long objectId) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JAttachmentDetail jatt where jatt.objectId=?");
			if(!isAdministrator()) {
				sql.append(" and (jatt.domainId=? or jatt.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail select JAttachmentDetail("+objectId+").",e);
		}
		
		if(result!=null && result.size()==1) {
			return (JAttachmentDetail)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JAttachmentDetail", new Object[]{objectId});
		}
	}

	@Override
	public void insert(JAttachmentDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JAttachmentDetail. - "+detail,e);
		}
	}

	@Override
	public void delete(JAttachmentDetail detail) throws JRepositoryException {
		try {
			this.template.delete(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JAttachmentDetail. - "+detail,e);
		}
	}

	@Override
	public void update(JAttachmentDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JAttachment. - "+detail,e);
		}
	}

}
