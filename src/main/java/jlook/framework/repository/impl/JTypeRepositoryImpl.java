package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JTypeRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JTypeRepository.ID)
public class JTypeRepositoryImpl extends HibernateRepository implements
		JTypeRepository {

	@Override
	public void insert(JType jDataType) throws JRepositoryException {
		try {
			this.template.save(jDataType);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JDataType. - "+jDataType,e);
		}
	}

	@Override
	public JType selectByName(String name) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JType jdt where jdt.name=?");
			if(!isAdministrator()) {
				sql.append(" and (jdt.domainId=? or jdt.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{name,super.getDomainId(),JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{name};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JDataType by name("+name+").",e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JType)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JDataType.", new Object[]{name});
		}
	}

	@Override
	public JType selectByJClass(JClass jClass) throws JRepositoryException{
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JType jdt where jdt.JClass=?");
			if(!isAdministrator()) {
				sql.append(" and (jdt.domainId=? or jdt.domainId=?)");
			}
			
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{jClass,super.getDomainId(),JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{jClass};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JDataType by JClass("+jClass+").",e);
		}
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JType)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JDataType.", new Object[]{jClass});
		}
	}

	@Override
	public void delete(JType jDataType) throws JRepositoryException {
		try {
			this.template.delete(jDataType);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JDataType. - "+jDataType,e);
		}
	}

	@Override
	public void update(JType jDataType) throws JRepositoryException {
		try {
			this.template.save(jDataType);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JDataType. - "+jDataType,e);
		}
	}

}
