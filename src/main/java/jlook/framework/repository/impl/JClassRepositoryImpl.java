package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JClassRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JClassRepository.ID)
public class JClassRepositoryImpl extends HibernateRepository implements
		JClassRepository {

	@Override
	public void insert(JClass jclass) throws JRepositoryException {
		try {
			this.template.save(jclass);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JClass. - "+jclass,e);
		}
	}

	@Override
	public JClass selectByName(String name) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JClass jcls where jcls.name=?");
			if(!isAdministrator()) {
				sql.append(" and (jcls.domainId=? or jcls.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{name, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{name};
			}
			result = this.template.find(sql.toString(),params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JClass by name("+name+").", e);
		}
		
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JClass)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JType.", new Object[]{name});
		}
	}

	@Override
	public JClass select(Long objectId) throws JRepositoryException {
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JClass jcls where jcls.objectId=?");
			if(!isAdministrator()) {
				sql.append(" and (jcls.domainId=? or jcls.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId, super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JClass("+objectId+").",e);
		}
		
		if(result==null || result.size()==0) {
			return null;
		} else if(result.size()==1) {
			return (JClass)result.get(0);
		} else {
			throw new JRepositoryException("Duplicated JClass.",new Object[]{objectId});
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JClass> selectList() throws JRepositoryException {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JClass jcls");
			if(!isAdministrator()) {
				sql.append(" where (jcls.domainId=? or jcls.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{};
			}
			return this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JClasses.", e);
		}
	}

	@Override
	public void delete(JClass jClass) throws JRepositoryException {
		try {
			this.template.delete(jClass);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JClass. - "+jClass,e);
		}
	}

	@Override
	public void update(JClass jClass) throws JRepositoryException {
		try {
			this.template.save(jClass);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to save JClass. - "+jClass,e);
		}
	}
	/**
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<JClass> selectReferences(JClass jClass)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("select jc ").append("\n");
		sql.append("from JClass jc, JAttribute ja, JType jd ").append("\n");
		sql.append("where ja.type=jd.objectId and jd.JClass=? and jc.objectId=ja.JClass ").append("\n");
		if(!isAdministrator()) {
			sql.append(" and (jc.domainId=? or jc.domainId=?)").append("\n");
		}
		sql.append("order by jc.label asc");
		
//		sql.append("select jc from JClass jc").append("\n");
//		sql.append("left outer join jlook.framework.domain.security.JDataAccessRule jda").append("\n");
//		sql.append("where jc.objectId in (").append("\n");
//		sql.append("select ja.JClass from JType jt").append("\n");
//		sql.append("inner join jlook.framework.domain.metadata.JAttribute ja").append("\n");
//		sql.append("where jt.JClass=?)").append("\n");
//		sql.append("and ((jc.accessRule='PRIVATE') ").append("\n");
//		sql.append("or (jc.accessRule='PROTECTED' and (jda.canread or jda.cancreate))").append("\n");
//		sql.append("or (jc.accessRule='PUBLIC' and (jda.canread is null or jda.canread or jda.cancreate) ))").append("\n");
//		if(!isAdministrator()) {
//			sql.append("and (jc.domainId=? or jc.domainId=?)").append("\n");
//		}
//		sql.append("order by jc.label asc");
		
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{jClass, this.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{jClass};
		}
		try {
			List<?> result = this.template.find(sql.toString(), params);
			return (List<JClass>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select incoming references - "+jClass,e);
		}
	}

}
