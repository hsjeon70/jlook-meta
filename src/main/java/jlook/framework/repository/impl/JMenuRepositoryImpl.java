package jlook.framework.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JMenuRepository;
import jlook.framework.repository.JRepositoryException;

@Repository(JMenuRepository.ID)
public class JMenuRepositoryImpl extends HibernateRepository implements
		JMenuRepository {
	private static Log logger = Log.getLog(JMenuRepositoryImpl.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JMenu> select(List<JRole> roleList) throws JRepositoryException {
		String str = "";
		for(int i=0;i<roleList.size();i++) {
			str+="?,";
		}
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct jm").append("\n");
		sql.append("from JMenu as jm, JMenuAccessRule as jr").append("\n");
		sql.append("	where jm.accessRule!='PRIVATE' and jm.objectId=jr.JMenu").append("\n");
		sql.append("	and jr.enabled=true").append("\n");
		if(!str.equals("")) {
			sql.append("	and jr.JRole in (").append(str.substring(0, str.length()-1)).append(")").append("\n");
		}
		sql.append(" order by jm.parent, jm.sequence");
		if(logger.isDebugEnabled()) {
			logger.debug("Sql --> \n"+sql);
		}
		List<?> result = this.template.find(sql.toString(), roleList.toArray());
		return (List<JMenu>)result;
	}

	@Override
	public void insert(JMenu jMenu) throws JRepositoryException {
		try {
			this.template.save(jMenu);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JMenu.",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JMenu> selectByPublic(List<JRole> roleList)
			throws JRepositoryException {
		String str = "";
		for(int i=0;i<roleList.size();i++) {
			str+="?,";
		}
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct jm").append("\n");
		sql.append("from JMenu as jm, JMenuAccessRule as jr").append("\n");
		sql.append("	where jm.accessRule='PUBLIC' and jm.objectId=jr.JMenu").append("\n");
		sql.append("	and jr.enabled=true").append("\n");
		if(!str.equals("")) {
			sql.append("	and jr.JRole in (").append(str.substring(0, str.length()-1)).append(")").append("\n");
		}
		sql.append(" order by jm.parent, jm.sequence");
		if(logger.isDebugEnabled()) {
			logger.debug("Sql --> \n"+sql);
		}
		List<?> result = this.template.find(sql.toString(), roleList.toArray());
		return (List<JMenu>)result;
	}

}
