package jlook.framework.repository.impl;

import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.repository.JMetaRepository;

import org.springframework.stereotype.Repository;


@Repository(JMetaRepository.ID)
public class JMetaRepositoryImpl extends HibernateRepository implements
		JMetaRepository {
	
}
