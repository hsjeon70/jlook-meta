package jlook.framework.repository.impl;

import java.util.List;

import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.common.JPreferenceDetail;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.repository.JPreferenceRepository;
import jlook.framework.repository.JRepositoryException;

import org.springframework.stereotype.Repository;


@Repository(JPreferenceRepository.ID)
public class JPreferenceRepositoryImpl extends HibernateRepository implements
		JPreferenceRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<JPreference> select(String type)
			throws JRepositoryException {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JPreference jp where jp.type=?");
			if(!isAdministrator()) {
			sql.append(" and (jp.domainId=? or jp.domainId=?)");
			}
			
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{type,super.getDomainId(), JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{type};
			}
			List<?> result = this.template.find(sql.toString(), params);
			return (List<JPreference>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JPreferences("+type+").",e);
		}
	}

	@Override
	public JPreferenceDetail select(JPreference jPreference, Long target)
			throws JRepositoryException {
		Long userDomainId = super.getDomainId();
		List<?> result;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JPreferenceDetail jpd where jpd.JPreference=? and jpd.target=?");
			if(!isAdministrator()) {
				sql.append(" and (jpd.domainId=? or jpd.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{jPreference, target, userDomainId, JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{jPreference, target};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JPreferenceDetail.",e);
		}
		if(result!=null && result.size()==1) {
			return (JPreferenceDetail)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JPreferenceDetail.",new Object[]{jPreference.getName()+", "+target});
		}
	}

	@Override
	public void insert(JPreference jpref) throws JRepositoryException {
		try {
			this.template.save(jpref);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JPreference.", e);
		}
	}

	@Override
	public void insert(JPreferenceDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to insert JPreferenceDetail.", e);
		}
	}

	@Override
	public JPreference select(Long objectId) throws JRepositoryException {
		List<?> result = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JPreference jp where jp.objectId=?");
			if(!isAdministrator()) {
				sql.append(" and (jp.domainId=? or jp.domainId=?)");
			}
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{objectId,super.getDomainId(),JDomainDef.SYSTEM};
			} else {
				params = new Object[]{objectId};
			}
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JPreference("+objectId+").",e);
		}
		if(result!=null && result.size()==1) {
			return (JPreference)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JPreference.",new Object[]{objectId});
		}
	}

	@Override
	public void update(JPreferenceDetail detail) throws JRepositoryException {
		try {
			this.template.save(detail);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to update JPreferenceDetail.", e);
		}
	}

	@Override
	public JPreference select(String name, String type)
			throws JRepositoryException {
		List<?> result = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from JPreference jp where jp.name=? and jp.type=?");
			if(!isAdministrator()) {
				sql.append(" and (jp.domainId=? or jp.domainId=?)");
			}
			
			Object[] params = null;
			if(!isAdministrator()) {
				params = new Object[]{name,type,super.getDomainId(),JDomainDef.SYSTEM_ID};
			} else {
				params = new Object[]{name, type};
			}
			
			result = this.template.find(sql.toString(), params);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JPreference("+name+", "+type+").",e);
		}
		if(result!=null && result.size()==1) {
			return (JPreference)result.get(0);
		} else if(result==null || result.size()==0) {
			return null;
		} else {
			throw new JRepositoryException("Duplicated JPreference.",new Object[]{name+", "+type});
		}
	}

	@Override
	public void delete(List<JPreferenceDetail> details)
			throws JRepositoryException {
		try {
			this.template.deleteAll(details);
		} catch(Exception e) {
			throw new JRepositoryException("Fail to delete JPreferneceDetail list.",e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JPreferenceDetail> select(JPreferenceType type, Long target)
			throws JRepositoryException {
		StringBuffer sql = new StringBuffer();
		sql.append("select jpd ").append("\n");
		sql.append("from JPreferenceDetail jpd, JPreference jp ").append("\n");
		sql.append("where jpd.JPreference=jp.objectId and jp.type=? and jpd.target=?");
		if(!isAdministrator()) {
			sql.append(" and (jpd.domainId=? or jpd.domainId=?)").append("\n");
		}
		Object[] params = null;
		if(!isAdministrator()) {
			params = new Object[]{type.name(), target, this.getDomainId(), JDomainDef.SYSTEM_ID};
		} else {
			params = new Object[]{type.name(), target};
		}
		try {
			List<?> result = this.template.find(sql.toString(), params);
			return (List<JPreferenceDetail>)result;
		} catch(Exception e) {
			throw new JRepositoryException("Fail to select JPreferenceDetail list. - type="+type+", target="+target,e);
		}
	}
}
