package jlook.framework.repository;

import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchParamName;

public interface JBatchDefinitionRepository {
	public static final String ID ="jBatchRepository";
	
	public void insert(JBatchDefinition jBatchDefinition) throws JRepositoryException;
	public void update(JBatchDefinition jBatchDefinition) throws JRepositoryException;
	
	public void insert(JBatchParamName jBatchParamName) throws JRepositoryException;
}
