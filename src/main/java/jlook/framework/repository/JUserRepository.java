package jlook.framework.repository;

import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserDomain;
import jlook.framework.domain.account.JUserRole;

public interface JUserRepository {
	public static final String ID = "securityRepository";
	
	public JUser selectByEmail(String email) throws JRepositoryException;
	
	public void insert(JUser jUser) throws JRepositoryException;
	public void delete(JUser jUser) throws JRepositoryException;
	public void insert(JUserRole jUserRole) throws JRepositoryException;
	public void insert(JUserDomain jUserDomain) throws JRepositoryException;
}
