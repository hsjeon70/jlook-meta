package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;

public interface JClassActionRepository {
	public static final String ID = "jClassActionRepository";
	
	public void insert(JClassAction jClassAction) throws JRepositoryException;
	public List<JClassAction> selectByJClass(JClass jClass, JClassActionType type) throws JRepositoryException;
}
