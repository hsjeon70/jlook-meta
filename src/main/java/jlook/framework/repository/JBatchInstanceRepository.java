package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamValue;

public interface JBatchInstanceRepository {
	public static final String ID = "jBatchInstanceRepository";
	
	public List<JBatchInstance> selectAll() throws JRepositoryException;
	
	public void insert(JBatchInstance jBatchInstance) throws JRepositoryException;
	public void update(JBatchInstance jBatchInstance) throws JRepositoryException;
	
	public void insert(JBatchParamValue jBatchParamValue) throws JRepositoryException;
}
