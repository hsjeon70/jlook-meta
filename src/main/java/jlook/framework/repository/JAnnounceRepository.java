package jlook.framework.repository;

import jlook.framework.domain.common.JAnnounceMessage;

public interface JAnnounceRepository {
	public static final String ID = "jAnnounceRepository";
	
	public void insert(JAnnounceMessage message) throws JRepositoryException;
	
}
