package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.account.JDomain;


public interface JDomainRepository {
	public static final String ID = "jDomainRepository";
	
	public void insert(JDomain jDomain) throws JRepositoryException;
	public void update(JDomain jDomain) throws JRepositoryException;
	
	public JDomain selectByName(String name) throws JRepositoryException;
	public JDomain select(Long id) throws JRepositoryException;
	public List<JDomain> selectAllList() throws JRepositoryException;
}
