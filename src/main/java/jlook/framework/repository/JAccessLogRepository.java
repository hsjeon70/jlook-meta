package jlook.framework.repository;

import jlook.framework.domain.management.JAccessLog;

public interface JAccessLogRepository {
	public static final String ID = "jAccessLogRepository";
	
	public void insert(JAccessLog jAccessLog) throws JRepositoryException;
	
}
