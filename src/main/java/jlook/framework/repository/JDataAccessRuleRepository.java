package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;


public interface JDataAccessRuleRepository {
	public static final String ID = "jDataAccessRuleRepository";
	
	public void insert(JDataAccessRule jDataAccessRule) throws JRepositoryException;
	public List<JDataAccessRule> select(JClass jClass, List<String> roleList) throws JRepositoryException;
}
