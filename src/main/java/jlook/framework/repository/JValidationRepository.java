package jlook.framework.repository;

import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.metadata.JValidation;

public interface JValidationRepository {
	public static final String ID = "jValidationRepository";
	
	public void insert(JValidation jValidation) throws JRepositoryException;
	public void insert(JEnumDetail detail) throws JRepositoryException;
	
	public JValidation selectByName(String name) throws JRepositoryException;
	
	public void delete(JValidation jValidation) throws JRepositoryException;
}
