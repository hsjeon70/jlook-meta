package jlook.framework.repository;

import jlook.framework.domain.security.JRole;

public interface JRoleRepository {
	public static final String ID = "jRoleRepository";
	
	public void insert(JRole role) throws JRepositoryException;
	public JRole selectByName(String name) throws JRepositoryException;
	
}
