package jlook.framework.repository;

import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;

public interface JAttachmentRepository {
	public static final String ID = "jAttachmentRepository";
	
	public void insert(JAttachment attachment) throws JRepositoryException;
	public void insert(JAttachmentDetail detail) throws JRepositoryException;
	public void update(JAttachment attachment) throws JRepositoryException;
	public void update(JAttachmentDetail detail) throws JRepositoryException;
	public JAttachment select(Long objectId) throws JRepositoryException;
	public JAttachmentDetail selectDetail(Long objectId) throws JRepositoryException;
	public JAttachment selectByName(String name) throws JRepositoryException;
	public void delete(JAttachment attachment) throws JRepositoryException;
	public void delete(JAttachmentDetail detail) throws JRepositoryException;
	
}
