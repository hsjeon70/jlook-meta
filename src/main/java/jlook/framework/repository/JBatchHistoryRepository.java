package jlook.framework.repository;

import jlook.framework.domain.batch.JBatchHistory;

public interface JBatchHistoryRepository {
	public static final String ID = "jBatchHistoryRepository";
	
	public void insert(JBatchHistory jBatchHisotry) throws JRepositoryException;
	public void update(JBatchHistory jBatchHisotry) throws JRepositoryException;
	
}
