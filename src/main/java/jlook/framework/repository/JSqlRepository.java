package jlook.framework.repository;

import java.util.List;
import java.util.Map;

public interface JSqlRepository {
	public static final String ID = "jSqlRepository";
	
	public void run(String sql) throws JRepositoryException;
	public int count(String table) throws JRepositoryException;
	public int count(String table, Long objectId) throws JRepositoryException;
	public List<Map<String,Object>> query(String sql) throws JRepositoryException;
}
