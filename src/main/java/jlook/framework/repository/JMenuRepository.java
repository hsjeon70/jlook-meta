package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.security.JRole;


public interface JMenuRepository {
	public static final String ID = "jMenuRepository";
	
	public List<JMenu> select(List<JRole> roleList) throws JRepositoryException;
	public List<JMenu> selectByPublic(List<JRole> roleList) throws JRepositoryException;
	public void insert(JMenu jMenu) throws JRepositoryException;
}
