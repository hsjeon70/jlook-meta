package jlook.framework.repository;

import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamName;

public interface JWidgetRepository {
	public static final String ID = "jWidgetRepository";
	
	public void insert(JWidgetDefinition jWidgetDefinition) throws JRepositoryException;
	public void insert(JWidgetParamName jWidgetParamName) throws JRepositoryException;
	
	public JWidgetDefinition selectDefinition(Long objectId) throws JRepositoryException;
	public JWidgetInstance selectInstance(Long objectId) throws JRepositoryException;
	
}
