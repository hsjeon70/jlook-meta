package jlook.framework.repository;

import java.util.Set;

import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;


public interface JArrayRepository {
	public static final String ID = "jArrayRepository";
	
	public void insert(JArray jArray) throws JRepositoryException;
	public void insert(JArrayDetail detail) throws JRepositoryException;
	
	public void delete(JArray jArray) throws JRepositoryException;
	public void delete(JArrayDetail detail) throws JRepositoryException;
	
	public void update(JArray jArray) throws JRepositoryException;
	public void update(JArrayDetail detail) throws JRepositoryException;
	
	public void deleteAll(Set<JArrayDetail> values) throws JRepositoryException;
	
	public JArray select(Long objectId) throws JRepositoryException;
	public JArray selectByName(String name) throws JRepositoryException;
	
}
