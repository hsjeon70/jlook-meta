package jlook.framework.repository;

import java.util.List;

import jlook.framework.domain.metadata.JClass;


public interface JClassRepository {
	public static final String ID = "jClassRepository";
	
	public void insert(JClass jclass) throws JRepositoryException;
	public JClass select(Long objectId) throws JRepositoryException;
	public JClass selectByName(String name) throws JRepositoryException;
	// TODO : return JClass list for domain specific data.
	public List<JClass> selectList() throws JRepositoryException;
	
	public void delete(JClass jClass) throws JRepositoryException;
	public void update(JClass jClass) throws JRepositoryException;
	
	public List<JClass> selectReferences(JClass jClass) throws JRepositoryException;
}
