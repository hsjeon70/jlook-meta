package jlook.framework.repository;

import jlook.framework.domain.security.JMenuAccessRule;

public interface JMenuAccessRuleRepository {
	public static final String ID = "jMenuAccessRuleRepository";
	
	public void insert(JMenuAccessRule entity) throws JRepositoryException;
}
