package jlook.framework;

public enum ClientType {
	Android,
	iPhone,
	Web,
	Loader,
	Batch;
}
