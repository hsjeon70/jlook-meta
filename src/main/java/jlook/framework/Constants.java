package jlook.framework;

public interface Constants {
	
	public static final String DATA = "DATA";
	public static final String COUNT = "COUNT";
	
	public static final String DEFAULT_CHARSET = "utf-8";
	public static final String HTTP_CHARSET = "charset="+DEFAULT_CHARSET;
	
	// JFileView
	public static final String FILE_NAME = "FILE_NAME";
	
	//--------
	public static final String OPENAPI_URI_PREFIX = "/openapi/";
	public static final String OPENAPI_VIEWNAME = "openapi/result";
	
	public static final String JSTLVIEW_RESOLVER_ID= "jstlViewResolver";
	
	public static final String READONLY = "readOnly";
}
