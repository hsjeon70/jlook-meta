package jlook.framework.domain;

import java.sql.Timestamp;

import jlook.framework.service.JServiceException;

public interface JProxy {
	public void set(String attributeName, Object attributeValue) throws JServiceException;
	public Object get(String attributeName) throws JServiceException;
	
	public void setObjectId(Long objectId);
	public Long getObjectId();
	public void setClassId(Long classId);
	public Long getClassId();
	public void setDomainId(Long domainId);
	public Long getDomainId();
	public void setCreatedBy(Long createdBy);
	public Long getCreatedBy();
	public void setCreatedOn(Timestamp createdOn);
	public Timestamp getCreatedOn();
	public void setUpdatedBy(Long updatedBy);
	public Long getUpdatedBy();
	public void setUpdatedOn(Timestamp updatedOn);
	public Timestamp getUpdatedOn();
}
