package jlook.framework.domain.common;

public enum JAnnounceTargetType {
	DOMAIN,
	GROUP,
	USER;
}
