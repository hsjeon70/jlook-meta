package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JOBJECTRATINGDETAIL, did=JDomainDef.SYSTEM_ID, name=JObjectRatingDetail.NAME)
public class JObjectRatingDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JObjectRatingDetail";
	
	public static final String A_JOBJECTRATING 	= "JObjectRating";
	public static final String A_TARGET 	= "target";
	public static final String A_VALUE 			= "value";
	public static final String A_COMMENT 		= "comment";
	public static final String A_WRITER 		= "writer";
		
	@jattr(name=A_JOBJECTRATING, label="ObjectRating", type=JObjectRating.NAME, length=15, description="ObjectRating", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JObjectRating jObjectRating;
	@jattr(name=A_TARGET, label="Target Object", type=JPrimitive.LONG_TYPE, length=15, description="Target Object", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private Long target;
	@jattr(name=A_WRITER, label="Writer", type=JPrimitive.STRING_TYPE, length=20, description="ObjectRating Nickname", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 		private String writer;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.DOUBLE_TYPE, length=5, description="ObjectRating Value", 
			required=false, unique=false, searchable=false, derived=false) 		private Double value;
	@jattr(name=A_COMMENT, label="Comment", type=JPrimitive.STRING_TYPE, length=500, description="ObjectRating Comment", 
			required=false, unique=false, searchable=false, derived=false) 		private String comment;
	
	public JObjectRating getJObjectRating() {
		return jObjectRating;
	}
	public void setJObjectRating(JObjectRating jObjectRating) {
		this.jObjectRating = jObjectRating;
	}
	public Long getTarget() {
		return this.target;
	}
	public void setTarget(Long targetObject) {
		this.target = targetObject;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	
}
