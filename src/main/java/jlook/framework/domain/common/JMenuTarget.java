package jlook.framework.domain.common;

public enum JMenuTarget {
	main,
	document,
	overlay;
	
	public static final String MAIN = "main";
	public static final String DOCUMENT = "document";
	public static final String OVERLAY = "overlay";
}
