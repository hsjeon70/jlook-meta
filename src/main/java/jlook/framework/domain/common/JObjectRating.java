package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JOBJECTRATING, did=JDomainDef.SYSTEM_ID, name=JObjectRating.NAME)
public class JObjectRating extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JObjectRating";
	
	public static final String A_NAME = "name";
	public static final String A_TARGET_CLASS	= "targetClass";
	public static final String A_DESCRIPTION 	= "description";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=50, description="ObjectRating Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_TARGET_CLASS, label="Target Class", type=JClass.NAME, length=15, description="Target Table Name for ObjectRating", 
			required=true, primary=false, unique=true, searchable=true, derived=false) 		private JClass targetClass;// table name
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="ObjectRating Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public JClass getTargetClass() {
		return targetClass;
	}
	public void setTargetClass(JClass targetClass) {
		this.targetClass = targetClass;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
