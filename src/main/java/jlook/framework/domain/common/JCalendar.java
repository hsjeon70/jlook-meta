package jlook.framework.domain.common;

import java.sql.Date;
import java.sql.Time;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JCALENDAR, did=JDomainDef.SYSTEM_ID, name=JCalendar.NAME)
public class JCalendar extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JCalendar";
	
	public static final String A_TITLE 			= "title";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_LOCATION 		= "location";
	
	public static final String A_ALL_DAY 	= "allDay";
	public static final String A_START_DATE = "startDate";
	public static final String A_START_TIME = "startTime";
	public static final String A_END_DATE 	= "endDate";
	public static final String A_END_TIME 	= "endTime";
	
	public static final String A_TYPE 		= "type";
	
	public static final String A_ALERT_ENABLED  = "alertEnabled";
	public static final String A_ALERT_TYPE 	= "alertType";
	public static final String A_ALERT_DATE 	= "alertDate";
	public static final String A_ALERT_TIME 	= "alertTime";
	
	@jattr(name=A_TITLE, label="Title", type=JPrimitive.STRING_TYPE, length=50, description="Schedule Title", 
			required=true, primary=false, unique=true, searchable=true, derived=false) 		private String title;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=1000, description="Schedule Description", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private String description;
	@jattr(name=A_LOCATION, label="Location", type=JPrimitive.STRING_TYPE, length=300, description="Schedule Location", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private String location;
	
	@jattr(name=A_ALL_DAY, label="All Day", type=JPrimitive.BOOLEAN_TYPE, length=1, description="All Day", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean allDay;
	@jattr(name=A_START_DATE, label="Start Date", type=JPrimitive.DATE_TYPE, description="Schedule Start Date", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Date startDate;
	@jattr(name=A_START_TIME, label="Start Time", type=JPrimitive.TIME_TYPE, description="Schedule Start Time", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Time startTime;
	@jattr(name=A_END_DATE, label="End Date", type=JPrimitive.DATE_TYPE, description="Schedule End Date", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Date endDate;
	@jattr(name=A_END_TIME, label="End Time", type=JPrimitive.TIME_TYPE, description="Schedule End Time", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Time endTime;
	
	private String type;	// calendar type
	
	@jattr(name=A_ALERT_ENABLED, label="Alert Enabled", type=JPrimitive.BOOLEAN_TYPE, description="Alert Enabled", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean alertEnabled;
	private String alertType;
	@jattr(name=A_ALERT_DATE, label="Alert Date", type=JPrimitive.DATE_TYPE, description="Schedule Alert Date", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Date alertDate;
	@jattr(name=A_ALERT_TIME, label="Alert Time", type=JPrimitive.TIME_TYPE, description="Schedule Alert Time", 
			required=false, primary=false, unique=false, searchable=true, derived=false) 		private Time alertTime;
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the allDay
	 */
	public boolean isAllDay() {
		return allDay;
	}
	/**
	 * @param allDay the allDay to set
	 */
	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the startTime
	 */
	public Time getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the endTime
	 */
	public Time getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the alertEnabled
	 */
	public boolean isAlertEnabled() {
		return alertEnabled;
	}
	/**
	 * @param alertEnabled the alertEnabled to set
	 */
	public void setAlertEnabled(boolean alertEnabled) {
		this.alertEnabled = alertEnabled;
	}
	/**
	 * @return the alertType
	 */
	public String getAlertType() {
		return alertType;
	}
	/**
	 * @param alertType the alertType to set
	 */
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	/**
	 * @return the alertDate
	 */
	public Date getAlertDate() {
		return alertDate;
	}
	/**
	 * @param alertDate the alertDate to set
	 */
	public void setAlertDate(Date alertDate) {
		this.alertDate = alertDate;
	}
	/**
	 * @return the alertTime
	 */
	public Time getAlertTime() {
		return alertTime;
	}
	/**
	 * @param alertTime the alertTime to set
	 */
	public void setAlertTime(Time alertTime) {
		this.alertTime = alertTime;
	}
	
	
}
