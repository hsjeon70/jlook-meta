package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JPREFERENCEDETAIL, did=JDomainDef.SYSTEM_ID, name=JPreferenceDetail.NAME, 
	accessRule=JAccessRule.PROTECTED_TYPE,
	summaryFilter="if(juser.administrator){ \n"
	+"\"JPreference in (select objectId from JPreference where (type='SYSTEM') or (type='DOMAIN' and target=\"+juser.domainId+\") or (type='USER' and target=\"+juser.objectId+\"))\" \n"
	+"} else if(juser.admin) { \n"
	+"\"JPreference in (select objectId from JPreference where (type='DOMAIN' and target=\"+juser.domainId+\") or (type='USER' and target=\"+juser.objectId+\"))\" \n"
	+"} else {\n"
	+"\"JPreference in (select objectId from JPreference where (type='USER' and target=\"+juser.objectId+\"))\" \n"
	+"}")
public class JPreferenceDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JPreferenceDetail";
	
	public static final String A_JPREFERENCE = "JPreference";
	public static final String A_TARGET = "target";
	public static final String A_VALUE = "value";
	
	@jattr(name=A_JPREFERENCE, label="Preference", type=JPreference.NAME, length=15, description="Preference", 
			required=true, primary=true, unique=false, searchable=true, derived=false, readOnly=true) 		private JPreference jPreference;
	@jattr(name=A_TARGET, label="Target", type=JPrimitive.LONG_TYPE, length=15, description="Target JObject", 
			required=false, primary=true, unique=false, searchable=false, derived=false, readOnly=true) 		private Long target;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.STRING_TYPE, length=2000, description="Preference value", 
			required=false, unique=false, searchable=true, derived=false) 	private String value;
	public JPreference getJPreference() {
		return jPreference;
	}
	public void setJPreference(JPreference jPreference) {
		this.jPreference = jPreference;
	}
	public Long getTarget() {
		return target;
	}
	public void setTarget(Long target) {
		this.target = target;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
