package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JPREFERENCE, did=JDomainDef.SYSTEM_ID, name=JPreference.NAME, accessRule=JAccessRule.PROTECTED_TYPE, 
		summaryFilter="if(juser.administrator) { } else if(juser.admin) { \"type='DOMAIN' or type='USER'\" } else { \"type='USER'\" }")
public class JPreference extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JPreference";
	
	public static final String A_NAME = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TYPE = "type";
	public static final String A_DEFAULT_VALUE = "defaultValue";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=50, description="Preference Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Preference Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="Preference Type", 
			required=true, primary=true, unique=false, searchable=true, derived=false, validation="JPreferenceType") 	private String type;
	@jattr(name=A_DEFAULT_VALUE, label="Default Value", type=JPrimitive.STRING_TYPE, length=2000, description="Preference Default Value", 
			required=false, unique=false, searchable=false, derived=false) 	private String defaultValue;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
}
