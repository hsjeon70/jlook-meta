package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JGroup;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JANNOUNCEDETAIL, did=JDomainDef.SYSTEM_ID, name=JAnnounceDetail.NAME, accessRule=JAccessRule.PROTECTED_TYPE)
public class JAnnounceDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAnnounceDetail";
	
	public static final String A_JANNOUNCEMESSAGE = "JAnnounceMessage";
	public static final String A_JDOMAIN = "JDomain";
	public static final String A_JGROUP  = "JGroup";
	public static final String A_JUSER	 = "JUser";
	
	@jattr(name=A_JANNOUNCEMESSAGE, label="Announce Message", type=JAnnounceMessage.NAME, length=15, description="Announce Message", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAnnounceMessage jAnnounceMessage;
	@jattr(name=A_JDOMAIN, label="JDomain", type=JDomain.NAME, length=15, description="JDomain", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 		private JDomain jDomain;
	@jattr(name=A_JGROUP, label="JGroup", type=JGroup.NAME, length=15, description="JGroup", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 		private JGroup jGroup;
	@jattr(name=A_JUSER, label="JUser", type=JUser.NAME, length=15, description="JUser", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 		private JUser jUser;
	
	public JAnnounceMessage getJAnnounceMessage() {
		return jAnnounceMessage;
	}
	public void setJAnnounceMessage(JAnnounceMessage jAnnounceMessage) {
		this.jAnnounceMessage = jAnnounceMessage;
	}
	public JDomain getJDomain() {
		return jDomain;
	}
	public void setJDomain(JDomain jDomain) {
		this.jDomain = jDomain;
	}
	public JGroup getJGroup() {
		return jGroup;
	}
	public void setJGroup(JGroup jGroup) {
		this.jGroup = jGroup;
	}
	public JUser getJUser() {
		return jUser;
	}
	public void setJUser(JUser jUser) {
		this.jUser = jUser;
	}
		
	
}
