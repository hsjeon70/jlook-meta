package jlook.framework.domain.common;

import java.util.Date;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JANNOUNCEMESSAGE, did=JDomainDef.SYSTEM_ID, name=JAnnounceMessage.NAME, accessRule=JAccessRule.PROTECTED_TYPE,
		summaryFilter="if(!juser.administrator && !juser.admin){ \n"+
						"\"(active=true and current_date()>=fromDate and current_date()<=toDate) "+
						"and (objectId not in (select objectId from jannouncedetail) or "+
						"objectId in (select objectId from jannouncedetail where JDomain=\"+juser.domainId+\" or JGroup=\"+juser.JGroupId+\" or JUser=\"+juser.objectId+\"))\" \n} ")
public class JAnnounceMessage extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAnnounceMessage";
	public static final String A_TITLE = "title";
	public static final String A_MESSAGE = "message";
	public static final String A_FROMDATE = "fromDate";
	public static final String A_TODATE = "toDate";
	public static final String A_ACTIVE = "active";
	
	@jattr(name=A_TITLE, label="Title", type=JPrimitive.STRING_TYPE, length=100, description="Announcement title", 
			required=true, primary=false, unique=false, searchable=true, derived=false) 		private String title;
	@jattr(name=A_MESSAGE, label="Message", type=JPrimitive.STRING_TYPE, length=4000, description="Announcement message", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private String message;
	@jattr(name=A_FROMDATE, label="From Date", type=JPrimitive.DATE_TYPE, description="From Date", 
			required=true, unique=false, searchable=true, derived=false) 		private Date fromDate;
	@jattr(name=A_TODATE, label="To Date", type=JPrimitive.DATE_TYPE, description="To Date", 
			required=true, unique=false, searchable=true, derived=false) 		private Date toDate;
	@jattr(name=A_ACTIVE, label="Active", type=JPrimitive.BOOLEAN_TYPE, description="Active", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="false", validation="JBoolean") 		private boolean active;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
