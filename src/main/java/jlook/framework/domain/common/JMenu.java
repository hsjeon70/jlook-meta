package jlook.framework.domain.common;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JMENU, did=JDomainDef.SYSTEM_ID, name=JMenu.NAME,
summaryFilter="if(!juser.administrator){ "+
		"\"accessRule!='PRIVATE'\" }")
public class JMenu extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JMenu";
	
	public static final String A_NAME 			= "name";
	public static final String A_LABEL 			= "label";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_ACTIVIE		= "active";
	public static final String A_ICON_IMAGE		= "iconImage";
	public static final String A_PARENT			= "parent";
	public static final String A_ACTION_URL 	= "actionUrl";
	public static final String A_TARGET	 		= "target";
	public static final String A_WIDGET 		= "widget";
	public static final String A_SEQUENCE		= "sequence";
	public static final String A_ACCESS_RULE	= "accessRule";
	
	@jattr(name=A_PARENT, label="Parent", type=JMenu.NAME, length=15, description="Parent Menu", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 	private JMenu parent;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Menu Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=20, description="Menu Label", 
			required=true, primary=false, unique=false, searchable=true, derived=false) 		private String label;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=4000, description="Menu Description", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private String description;
	@jattr(name=A_ACTIVIE, label="Active", type=JPrimitive.BOOLEAN_TYPE, description="Menu Active", 
			required=true, primary=false, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 		private boolean active;
	
	@jattr(name=A_ICON_IMAGE, label="Icon Image", type=JPrimitive.STRING_TYPE, length=500, description="Icon Image", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private String iconImage;
	@jattr(name=A_ACTION_URL, label="Action URL", type=JPrimitive.STRING_TYPE, length=500, description="Action URL", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private String actionUrl;
	@jattr(name=A_TARGET, label="Target", type=JPrimitive.STRING_TYPE, length=10, description="Action Target", 
			required=false, primary=false, unique=false, searchable=false, derived=false, validation="JMenuTarget") 		private String target;
	@jattr(name=A_WIDGET, label="Widget", type=JPrimitive.STRING_TYPE, length=20, description="Target Widget Type", 
			required=false, primary=false, unique=false, searchable=false, derived=false, validation="JWidgetType") 		private String widget;
	@jattr(name=A_SEQUENCE, label="Sequence", type=JPrimitive.INTEGER_TYPE, length=2, description="Menu Sequence", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 		private Integer sequence;
	@jattr(name=A_ACCESS_RULE, label="Access Rule", type=JPrimitive.STRING_TYPE, length=9, description="Access Rule", 
			required=false, unique=false, searchable=true, derived=false, hidden=true, defaultValue="PROTECTED", validation="JAccessRule") 	private String accessRule;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the parent
	 */
	public JMenu getParent() {
		return parent;
	}
	/**
	 * @param parent the parent to set
	 */
	public void setParent(JMenu parent) {
		this.parent = parent;
	}
	/**
	 * @return the actionUrl
	 */
	public String getActionUrl() {
		return actionUrl;
	}
	/**
	 * @param actionUrl the actionUrl to set
	 */
	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}
	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}
	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getIconImage() {
		return iconImage;
	}
	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
	/**
	 * @return the widget
	 */
	public String getWidget() {
		return widget;
	}
	/**
	 * @param widget the widget to set
	 */
	public void setWidget(String widget) {
		this.widget = widget;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getAccessRule() {
		return accessRule;
	}
	public void setAccessRule(String accessRule) {
		this.accessRule = accessRule;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMenu other = (JMenu) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JMenu [objectId=" + objectId+", name=" + name + ", parent=" + parent + ", label="
				+ label + ", description=" + description + ", active=" + active
				+ ", actionUrl=" + actionUrl + ", target=" + target
				+ ", widget=" + widget + "]";
	}
	
	
}
