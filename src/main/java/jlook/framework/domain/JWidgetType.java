package jlook.framework.domain;

public enum JWidgetType {
	grid,
	object,
	html,
	none;
	
	public static final String GRID = "grid";
	public static final String OBJECT = "object";
	public static final String HTML = "html";
	public static final String NONE = "none";
	
}
