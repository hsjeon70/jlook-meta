package jlook.framework.domain.util;

import java.util.HashSet;
import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JARRAY, did=JDomainDef.SYSTEM_ID, name=JArray.NAME)
public class JArray extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JArray";
	
	public static final String A_NAME = "name";
	public static final String A_DESCRIPTION = "description";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Array Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Array Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	
	private Set<JArrayDetail> values = new HashSet<JArrayDetail>();
	
	public Set<JArrayDetail> getValues() {
		return values;
	}
	public void setValues(Set<JArrayDetail> values) {
		this.values = values;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Override
	public String toString() {
		return "JArray [name=" + name + ", values=" + values
				+ ", objectId=" + objectId + ", classId=" + classId
				+ ", domainId=" + domainId + "]";
	}
	
	
}
