package jlook.framework.domain.util;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JARRAYDETAIL, did=JDomainDef.SYSTEM_ID, name=JArrayDetail.NAME)
public class JArrayDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JArrayDetail";
	
	public static final String A_JARRAY = "JArray";
	public static final String A_VALUE = "value";
	
	@jattr(name=A_JARRAY, label="Array", type=JArray.NAME, length=15, description="Array", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JArray jArray;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.STRING_TYPE, length=200, description="Array value", 
			required=false, primary=true, unique=false, searchable=false, derived=false) 	private String value;
	
	public JArray getJArray() {
		return jArray;
	}
	public void setJArray(JArray jArray) {
		this.jArray = jArray;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jArray == null) ? 0 : jArray.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JArrayDetail other = (JArrayDetail) obj;
		if (jArray == null) {
			if (other.jArray != null)
				return false;
		} else if (!jArray.equals(other.jArray))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JArrayDetail [jArray=" + jArray==null?"":jArray.getObjectId() + ", value=" + value
				+ ", objectId=" + objectId + ", classId=" + classId
				+ ", domainId=" + domainId + "]";
	}
	
	
}
