package jlook.framework.domain.util;

import java.util.HashSet;
import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JATTACHMENT, did=JDomainDef.SYSTEM_ID, name=JAttachment.NAME)
public class JAttachment extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAttachment";
	
	public static final String A_NAME 			= "name";
	public static final String A_SUBJECT 	= "subject";
	public static final String A_CONTENT 	= "content";
	
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Attachment Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_CONTENT, label="Subject", type=JPrimitive.STRING_TYPE, length=200, description="Attachment Subject", 
			required=false, unique=false, searchable=false, derived=false) 	private String subject;
	@jattr(name=A_CONTENT, label="Content", type=JPrimitive.STRING_TYPE, length=200, description="Attachment Content", 
			required=false, unique=false, searchable=false, derived=false) 	private String content;
	
	private Set<JAttachmentDetail> jAttachmentDetails=new HashSet<JAttachmentDetail>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Set<JAttachmentDetail> getJAttachmentDetails() {
		return jAttachmentDetails;
	}
	public void setJAttachmentDetails(Set<JAttachmentDetail> jAttachmentDetails) {
		this.jAttachmentDetails = jAttachmentDetails;
	}
	
	public void addJAttachmentDetail(JAttachmentDetail detail) {
		if(this.jAttachmentDetails==null) {
			this.jAttachmentDetails = new HashSet<JAttachmentDetail>();
		}
		if(detail.getJAttachment()==null) {
			detail.setJAttachment(this);
		}
		this.jAttachmentDetails.add(detail);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAttachment other = (JAttachment) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
