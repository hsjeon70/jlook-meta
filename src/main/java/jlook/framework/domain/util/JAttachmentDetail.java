package jlook.framework.domain.util;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JATTACHMENTDETAIL, did=JDomainDef.SYSTEM_ID, name=JAttachmentDetail.NAME)
public class JAttachmentDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JAttachmentDetail";
	
	public static final String A_JATTACHMENT 	= "JAttachment";
	public static final String A_NAME 			= "name";
	public static final String A_COMMENTS 		= "commnents";
	public static final String A_REFERER 		= "referer";
	public static final String A_TYPE 			= "type";
	public static final String A_PATH 			= "path";
	public static final String A_ABSOLUTE_PATH  = "absolutePath";
	public static final String A_LENGTH 		= "length";
	public static final String A_SAVEDNAME 		= "savedName";
	public static final String A_DEFAULT_YN		= "defaultYn";
	
	@jattr(name=A_JATTACHMENT, label="Attachment", type=JAttachment.NAME, length=15, description="Attachment", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JAttachment jAttachment;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="File Name", 
			required=true, unique=false, searchable=true, derived=false) 		private String name;
	@jattr(name=A_COMMENTS, label="Description", type=JPrimitive.STRING_TYPE, length=2000, description="File Comments", 
			required=false, unique=false, searchable=false, derived=false) 		private String comments;
	@jattr(name=A_REFERER, label="Referer", type=JPrimitive.STRING_TYPE, length=200, description="HTTP Header Referer", 
			required=false, unique=false, searchable=true, derived=true) 		private String referer;
	@jattr(name=A_SAVEDNAME, label="Saved Name", type=JPrimitive.STRING_TYPE, length=500, description="Saved File Name", 
			required=true, primary=true, unique=true, searchable=false, derived=false) 		private String savedName;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=100, description="File Type", 
			required=true, unique=false, searchable=false, derived=false) 		private String type;
	@jattr(name=A_PATH, label="Path", type=JPrimitive.STRING_TYPE, length=1000, description="File Path", 
			required=true, unique=false, searchable=false, derived=false) 		private String path;
	@jattr(name=A_ABSOLUTE_PATH, label="Absulute Path", type=JPrimitive.STRING_TYPE, length=1000, description="File Path", 
			required=true, unique=false, searchable=false, derived=false) 		private String absolutePath;
	@jattr(name=A_LENGTH, label="Length", type=JPrimitive.INTEGER_TYPE, length=10, description="File Length", 
			required=true, unique=false, searchable=false, derived=false) 		private Long length;
	@jattr(name=A_DEFAULT_YN, label="Default YN", type=JPrimitive.BOOLEAN_TYPE, length=1, description="Default File YN", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false") 		private String defaultYn;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReferer() {
		return referer;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	public String getSavedName() {
		return savedName;
	}
	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Long getLength() {
		return length;
	}
	public void setLength(Long length) {
		this.length = length;
	}
	public JAttachment getJAttachment() {
		return jAttachment;
	}
	public void setJAttachment(JAttachment jAttachment) {
		this.jAttachment = jAttachment;
	}
	public String getDefaultYn() {
		return defaultYn;
	}
	public void setDefaultYn(String defaultYn) {
		this.defaultYn = defaultYn;
	}
	public String getAbsolutePath() {
		return absolutePath;
	}
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((jAttachment == null) ? 0 : jAttachment.hashCode());
		result = prime * result
				+ ((savedName == null) ? 0 : savedName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAttachmentDetail other = (JAttachmentDetail) obj;
		if (jAttachment == null) {
			if (other.jAttachment != null)
				return false;
		} else if (!jAttachment.equals(other.jAttachment))
			return false;
		if (savedName == null) {
			if (other.savedName != null)
				return false;
		} else if (!savedName.equals(other.savedName))
			return false;
		return true;
	}
	
	
}
