package jlook.framework.domain.batch;


import java.util.Map;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JBATCHDEFINITION, did=JDomainDef.SYSTEM_ID, name=JBatchDefinition.NAME)
public class JBatchDefinition  extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JBatchDefinition";
	
	public static final String A_NAME		 = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TASK_CLASS	 = "taskClass";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JBatch Name", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 	private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JBatch Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TASK_CLASS, label="Task Class", type=JPrimitive.STRING_TYPE, length=300, description="JBatch Task Class", 
			required=true, unique=false, searchable=true, derived=false) 	private String taskClass;
	
	private Map<String, JBatchParamName> jBatchParamNames;
	
	public Map<String, JBatchParamName> getJBatchParamNames() {
		return jBatchParamNames;
	}

	public void setJBatchParamNames(Map<String, JBatchParamName> jBatchParamNames) {
		this.jBatchParamNames = jBatchParamNames;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTaskClass() {
		return taskClass;
	}

	public void setTaskClass(String taskClass) {
		this.taskClass = taskClass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JBatchDefinition other = (JBatchDefinition) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JBatchDefinition [name=" + name + ", description="
				+ description + ", taskClass=" + taskClass + "]";
	}

	
}
