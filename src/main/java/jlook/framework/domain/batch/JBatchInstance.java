package jlook.framework.domain.batch;

import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.batch.JBatchInstanceAction;

@jclass(cid=JMetaKeys.JBATCHINSTANCE, did=JDomainDef.SYSTEM_ID, name=JBatchInstance.NAME, actionClass=JBatchInstanceAction.CLASS_NAME)
public class JBatchInstance  extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JBatchInstance";
	
	public static final String A_JBATCHDEFINITION  = "JBatchDefinition";
	public static final String A_NAME		 = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_FIXED_DELAY = "fixedDelay";
	public static final String A_FIXED_RATE	 = "fixedRate";
	public static final String A_CRON_EXPRESSION = "cronExpression";
	public static final String A_SERVER		 = "server";
	public static final String A_ACTIVE		 = "active";
	
	@jattr(name=A_JBATCHDEFINITION, label="JBatchDefinition", type=JBatchDefinition.NAME, length=15, description="JBatchDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JBatchDefinition jBatchDefinition;
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JBatch Name", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 	private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JBatch Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_FIXED_DELAY, label="Fixed Delay", type=JPrimitive.LONG_TYPE, length=10, description="JBatch Fixed Delay", 
			required=false, unique=false, searchable=true, derived=false) 	private Long fixedDelay;
	@jattr(name=A_FIXED_RATE, label="Fixed Rate", type=JPrimitive.LONG_TYPE, length=10, description="JBatch Fixed Rate", 
			required=false, unique=false, searchable=true, derived=false) 	private Long fixedRate;
	@jattr(name=A_CRON_EXPRESSION, label="Cron Expression", type=JPrimitive.STRING_TYPE, length=100, description="JBatch Cron Expression", 
			required=false, unique=false, searchable=true, derived=false) 	private String cronExpression;
	
	@jattr(name=A_SERVER, label="Batch Server", type=JPrimitive.STRING_TYPE, length=20, description="JBatch Server", 
			required=false, unique=false, searchable=false, derived=false) 	private String server;
	@jattr(name=A_ACTIVE, label="Active", type=JPrimitive.BOOLEAN_TYPE, description="JBatch Active", 
			required=false, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean active;
	
	private Set<JBatchParamValue> jBatchParamValues;
	
	public Set<JBatchParamValue> getJBatchParamValues() {
		return jBatchParamValues;
	}

	public void setJBatchParamValues(Set<JBatchParamValue> jBatchParamValues) {
		this.jBatchParamValues = jBatchParamValues;
	}

	public Long getFixedDelay() {
		return fixedDelay;
	}

	public void setFixedDelay(Long fixedDelay) {
		this.fixedDelay = fixedDelay;
	}

	public Long getFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(Long fixedRate) {
		this.fixedRate = fixedRate;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JBatchDefinition getJBatchDefinition() {
		return jBatchDefinition;
	}

	public void setJBatchDefinition(JBatchDefinition jBatchDefinition) {
		this.jBatchDefinition = jBatchDefinition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((jBatchDefinition == null) ? 0 : jBatchDefinition.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JBatchInstance other = (JBatchInstance) obj;
		if (jBatchDefinition == null) {
			if (other.jBatchDefinition != null)
				return false;
		} else if (!jBatchDefinition.equals(other.jBatchDefinition))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JBatchInstance [jBatchDefinition=" + jBatchDefinition
				+ ", name=" + name + ", fixedDelay=" + fixedDelay
				+ ", fixedRate=" + fixedRate + ", cronExpression="
				+ cronExpression + ", server=" + server + ", active=" + active
				+ "]";
	}

	

	
	
}
