package jlook.framework.domain.batch;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JBATCHPARAMNAME, did=JDomainDef.SYSTEM_ID, name=JBatchParamName.NAME)
public class JBatchParamName extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JBatchParamName";
	
	public static final String A_JBATCHDEFINITION 	 = "JBatchDefinition";
	public static final String A_NAME 		 = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TYPE 		 = "type";
	public static final String A_DEFAULT_VALUE 		 = "defaultValue";
	
	@jattr(name=A_JBATCHDEFINITION, label="JBatchDefinition", type=JBatchDefinition.NAME, length=15, description="JBatchDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JBatchDefinition jBatchDefinition;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JBatch Param Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JBatch Param Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="JBatch Param Type", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="STRING", validation="JBatchParamType") 	private String type;
	@jattr(name=A_DEFAULT_VALUE, label="Default Value", type=JPrimitive.STRING_TYPE, length=100, description="JBatch Default Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String defaultValue;
	
	public JBatchDefinition getJBatchDefinition() {
		return jBatchDefinition;
	}
	public void setJBatchDefinition(JBatchDefinition jBatchDefinition) {
		this.jBatchDefinition = jBatchDefinition;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
}
