package jlook.framework.domain.batch;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.batch.JBatchParamValueAction;

@jclass(cid=JMetaKeys.JBATCHPARAMVALUE, did=JDomainDef.SYSTEM_ID, name=JBatchParamValue.NAME, actionClass=JBatchParamValueAction.CLASS_NAME)
public class JBatchParamValue extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JBatchParamValue";
	
	public static final String A_JBATCHINSTANCE = "JBatchInstance";
	public static final String A_JBATCHPARAMNAME = "JBatchParamName";
	public static final String A_VALUE = "value";
	
	@jattr(name=A_JBATCHINSTANCE, label="JBatchInstance", type=JBatchInstance.NAME, length=15, description="JBatchInstance", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JBatchInstance jBatchInstance;
	@jattr(name=A_JBATCHPARAMNAME, label="JBatchParamName", type=JBatchParamName.NAME, length=15, description="JBatchParamName", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JBatchParamName jBatchParamName;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.STRING_TYPE, length=100, description="JBatch Param Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String value;
	
	public JBatchInstance getJBatchInstance() {
		return jBatchInstance;
	}
	public void setJBatchInstance(JBatchInstance jBatchInstance) {
		this.jBatchInstance = jBatchInstance;
	}
	public JBatchParamName getJBatchParamName() {
		return jBatchParamName;
	}
	public void setJBatchParamName(JBatchParamName jBatchParamName) {
		this.jBatchParamName = jBatchParamName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((jBatchInstance == null) ? 0 : jBatchInstance.hashCode());
		result = prime * result
				+ ((jBatchParamName == null) ? 0 : jBatchParamName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JBatchParamValue other = (JBatchParamValue) obj;
		if (jBatchInstance == null) {
			if (other.jBatchInstance != null)
				return false;
		} else if (!jBatchInstance.equals(other.jBatchInstance))
			return false;
		if (jBatchParamName == null) {
			if (other.jBatchParamName != null)
				return false;
		} else if (!jBatchParamName.equals(other.jBatchParamName))
			return false;
		return true;
	}
	
	
}
