package jlook.framework.domain.batch;

import java.sql.Timestamp;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JBATCHHISTORY, did=JDomainDef.SYSTEM_ID, name=JBatchHistory.NAME, accessRule=JAccessRule.PRIVATE_TYPE)
public class JBatchHistory extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JBatchHistory";
	
	public static final String A_NAME			= "name";
	public static final String A_RESULT			= "result";
	public static final String A_JBATCHDEFINITION 	= "JBatchDefinition";
	public static final String A_JBATCHINSTANCE 	= "JBatchInstance";
	public static final String A_START_TIME 	= "startTime";
	public static final String A_END_TIME 		= "endTime";
	public static final String A_ELAPSE_TIME 	= "elapseTime";
	public static final String A_SERVER 		= "server";
	public static final String A_ERROR_MESSAGE 	= "errorMessage";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private String name;
	@jattr(name=A_RESULT, label="Result", type=JPrimitive.STRING_TYPE, length=10, description="Result", 
			required=false, unique=false, searchable=true, derived=false, defaultValue="success", validation="JBatchResult") 	private String result;
	@jattr(name=A_JBATCHDEFINITION, label="JBatchDefinition", type=JBatchDefinition.NAME, length=15, description="JBatch", 
			required=true, primary=true, unique=false, searchable=true, derived=false) private JBatchDefinition jBatchDefinition;
	@jattr(name=A_JBATCHINSTANCE, label="JBatchInstance", type=JBatchInstance.NAME, length=15, description="JBatchInstance", 
			required=true, primary=true, unique=false, searchable=true, derived=false) private JBatchInstance jBatchInstance;
	@jattr(name=A_START_TIME, label="Start Time", type=JPrimitive.TIMESTAMP_TYPE, description="Start Time", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private Timestamp startTime;
	@jattr(name=A_END_TIME, label="End Time", type=JPrimitive.TIMESTAMP_TYPE, description="End Time", 
			required=false, unique=false, searchable=true, derived=false) 		private Timestamp endTime;
	@jattr(name=A_ELAPSE_TIME, label="Elapse Time", type=JPrimitive.STRING_TYPE, length=200, description="Elapse Time", 
			required=false, unique=false, searchable=false, derived=false) 	private String elapseTime;
	@jattr(name=A_SERVER, label="Server", type=JPrimitive.STRING_TYPE, length=20, description="Server", 
			required=false, unique=false, searchable=false, derived=false) 	private String server;
	@jattr(name=A_ERROR_MESSAGE, label="Error Message", type=JPrimitive.STRING_TYPE, length=4000, description="Error Message", 
			required=false, unique=false, searchable=false, derived=false) 	private String errorMessage;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public JBatchDefinition getJBatchDefinition() {
		return jBatchDefinition;
	}
	public void setJBatchDefinition(JBatchDefinition jBatchDefinition) {
		this.jBatchDefinition = jBatchDefinition;
	}
	public JBatchInstance getJBatchInstance() {
		return jBatchInstance;
	}
	public void setJBatchInstance(JBatchInstance jBatchInstance) {
		this.jBatchInstance = jBatchInstance;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public String getElapseTime() {
		return elapseTime;
	}
	public void setElapseTime(String elapseTime) {
		this.elapseTime = elapseTime;
	}
	
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		return "JBatchHistory [name=" + name + ", result=" + result
				+ ", startTime=" + startTime + ", endTime=" + endTime
				+ ", server=" + server + ", errorMessage=" + errorMessage + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JBatchHistory other = (JBatchHistory) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}
	
	
}
