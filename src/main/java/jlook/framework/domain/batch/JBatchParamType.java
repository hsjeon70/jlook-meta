package jlook.framework.domain.batch;

public enum JBatchParamType {
	BOOLEAN,
	INTEGER,
	LONG,
	FLOAT,
	DOUBLE,
	STRING,
	DATE,
	TIME;
}
