package jlook.framework.domain.security;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.common.JMenu;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;

@jclass(cid=JMetaKeys.JMENUACCESSRULE, did=JDomainDef.SYSTEM_ID, name=JMenuAccessRule.NAME,
summaryFilter="if(!juser.administrator){ "+
		" \"(JRole!="+JRoleDef.ADMINISTRATOR_ID+") and JMenu in (select objectId from JMenu jm where jm.accessRule!='PRIVATE')\" }")
public class JMenuAccessRule extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JMenuAccessRule";
	
	public static final String A_JROLE = "JRole";
	public static final String A_JMENU = "JMenu";
	public static final String A_ENABLED = "enabled";
	
	@jattr(name=A_JROLE, label="Role", type=JRole.NAME, length=15, description="Role", 
			primary=true, required=true, unique=false, searchable=true, derived=false) 		private JRole jRole;
	@jattr(name=A_JMENU, label="Menu", type=JMenu.NAME, length=15, description="Menu", 
			primary=true, required=true, unique=false, searchable=true, derived=false) 		private JMenu jMenu;
	@jattr(name=A_ENABLED, label="Enabled", type=JPrimitive.BOOLEAN_TYPE, description="Enabled", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="false", validation="JBoolean") 		private boolean enabled;
	
	public JRole getJRole() {
		return jRole;
	}
	public void setJRole(JRole jRole) {
		this.jRole = jRole;
	}
	public JMenu getJMenu() {
		return jMenu;
	}
	public void setJMenu(JMenu jMenu) {
		this.jMenu = jMenu;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jMenu == null) ? 0 : jMenu.hashCode());
		result = prime * result + ((jRole == null) ? 0 : jRole.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMenuAccessRule other = (JMenuAccessRule) obj;
		if (jMenu == null) {
			if (other.jMenu != null)
				return false;
		} else if (!jMenu.equals(other.jMenu))
			return false;
		if (jRole == null) {
			if (other.jRole != null)
				return false;
		} else if (!jRole.equals(other.jRole))
			return false;
		return true;
	}
	
	
}
