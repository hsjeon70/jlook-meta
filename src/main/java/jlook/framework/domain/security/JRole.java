package jlook.framework.domain.security;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;

@jclass(cid=JMetaKeys.JROLE, did=JDomainDef.SYSTEM_ID, name=JRole.NAME,
summaryFilter="if(!juser.administrator){ "+
		" \"(objectId!="+JRoleDef.ADMINISTRATOR_ID+")\" }")
public class JRole extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JRole";
	
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION	= "description";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Role Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false, readOnly=true) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Role Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JRole other = (JRole) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
