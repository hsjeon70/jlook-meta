package jlook.framework.domain.security;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;

@jclass(cid=JMetaKeys.JWIDGETACCESSRULE, did=JDomainDef.SYSTEM_ID, name=JWidgetAccessRule.NAME,
summaryFilter="if(!juser.administrator){ "+
		" \"(JRole!="+JRoleDef.ADMINISTRATOR_ID+")\" }")
public class JWidgetAccessRule extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JWidgetAccessRule";
	
	public static final String A_JROLE 		= "JRole";
	public static final String A_JWIDGET 	= "JWidget";
	public static final String A_CANCREATE 	= "canCreate";
	public static final String A_CANREAD 	= "canRead";
	public static final String A_CANUPDATE 	= "canUpdate";
	public static final String A_CANDELETE 	= "canDelete";
	
	@jattr(name=A_JROLE, label="Role", type=JRole.NAME, length=15, description="Role", 
			primary=true, required=true, unique=false, searchable=true, derived=false) 		private JRole jRole;
	@jattr(name=A_JWIDGET, label="JWidgetInstance", type=JWidgetInstance.NAME, length=15, description="JWidget Instance", 
			primary=true, required=true, unique=false, searchable=true, derived=false) 		private JWidgetInstance jWidgetInstance;
	@jattr(name=A_CANCREATE, label="Can Create", type=JPrimitive.BOOLEAN_TYPE, description="Can Create", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean canCreate;
	@jattr(name=A_CANREAD, label="Can Read", type=JPrimitive.BOOLEAN_TYPE, description="Can Read", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="true",	validation="JBoolean") 	private boolean canRead;
	@jattr(name=A_CANUPDATE, label="Can Update", type=JPrimitive.BOOLEAN_TYPE, description="Can Update", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean canUpdate;
	@jattr(name=A_CANDELETE, label="Can Delete", type=JPrimitive.BOOLEAN_TYPE, description="Can Delete", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean canDelete;
	
	public JRole getJRole() {
		return jRole;
	}
	public void setJRole(JRole jRole) {
		this.jRole = jRole;
	}
	public JWidgetInstance getJWidgetInstance() {
		return jWidgetInstance;
	}
	public void setJWidgetInstance(JWidgetInstance jWidgetInstance) {
		this.jWidgetInstance = jWidgetInstance;
	}
	public boolean isCanCreate() {
		return canCreate;
	}
	public void setCanCreate(boolean canCreate) {
		this.canCreate = canCreate;
	}
	public boolean isCanRead() {
		return canRead;
	}
	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}
	public boolean isCanUpdate() {
		return canUpdate;
	}
	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}
	public boolean isCanDelete() {
		return canDelete;
	}
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jWidgetInstance == null) ? 0 : jWidgetInstance.hashCode());
		result = prime * result + ((jRole == null) ? 0 : jRole.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JWidgetAccessRule other = (JWidgetAccessRule) obj;
		if (jWidgetInstance == null) {
			if (other.jWidgetInstance != null)
				return false;
		} else if (!jWidgetInstance.equals(other.jWidgetInstance))
			return false;
		if (jRole == null) {
			if (other.jRole != null)
				return false;
		} else if (!jRole.equals(other.jRole))
			return false;
		return true;
	}
	
	
	
	
}
