package jlook.framework.domain.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.JValidationType;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.metadata.JPattern;
import jlook.framework.domain.metadata.JRange;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.service.JServiceException;


@XmlRootElement(name="columnModel")
public class ColumnModel implements Comparable<ColumnModel>{
	protected String name;
	protected String label;
	protected String description;
	protected String type;
	protected String UIType;
	protected Integer length;
	protected Integer displayLength;
	protected Long typeId;
	protected Integer sequence;
	protected boolean readOnly;
	protected boolean required;
	protected boolean derived;
	protected String encrypt;
	protected String align;
	protected ValidationModel validation;
	
	public ColumnModel() {};
	public ColumnModel(JAttribute jattr) throws JServiceException {
		this.name = jattr.getName();
		if(jattr.getLabel()!=null) {
			this.label = jattr.getLabel();
		} else {
			this.label = this.name;
		}
		this.description = jattr.getDescription();
		this.required = jattr.isRequired();
		this.derived = jattr.isDerived();
		this.encrypt = jattr.getEncrypt();
		this.readOnly = jattr.isReadOnly();
		
		if(JObject.A_OID.equals(name)||JObject.A_CID.equals(name)||JObject.A_DID.equals(name)) {
			this.sequence = jattr.getObjectId().intValue();
		} else if (JObject.A_CREATED_BY.equals(name)||JObject.A_CREATED_ON.equals(name)||JObject.A_UPDATED_BY.equals(name)||JObject.A_UPDATED_ON.equals(name)) {
			this.sequence = 90+jattr.getObjectId().intValue();
		} else {
			this.sequence = jattr.getObjectId().intValue();
		}
		
		JType jdt = jattr.getType();
		this.type = jdt.getName();
		if(!jdt.isPrimitive()) {
			JClass ref = jdt.getJClass();
			this.typeId = ref.getObjectId();
		}
		this.length = jattr.getLength();
		this.displayLength = this.length;
		Object jvalid = jattr.getJValidation();
		if(jvalid!=null) {
			if(jvalid instanceof JEnum) {
				JEnum jEnum = (JEnum)jvalid;
				EnumModel em = new EnumModel();
				em.setName(jEnum.getName());
				em.setDescription(jEnum.getDescription());
				em.setUIType(jEnum.getType());
				Set<JEnumDetail> details = jEnum.getValues();
				List<JEnumDetail> list = new ArrayList<JEnumDetail>(details);
				Collections.sort(list);
				for(JEnumDetail detail : list) {
					em.addValue(detail.getValue(), detail.getLabel());
				}
				em.setType(JValidationType.enumeration.name());
				this.validation = em;
			} else if(jvalid instanceof JPattern) {
				JPattern jPattern = (JPattern)jvalid;
				PatternModel pm = new PatternModel();
				pm.setName(jPattern.getName());
				pm.setDescription(jPattern.getDescription());
				pm.setRegExpression(jPattern.getRegExpression());
				pm.setType(JValidationType.pattern.name());
				this.validation = pm;
			} else if(jvalid instanceof JRange) {
				JRange jRange = (JRange)jvalid;
				RangeModel rm = new RangeModel();
				rm.setName(jRange.getName());
				rm.setDescription(jRange.getName());
				rm.setMinValue(jRange.getMinValue());
				rm.setMinInclusive(jRange.isMinInclusive());
				rm.setMaxValue(jRange.getMaxValue());
				rm.setMaxInclusive(jRange.isMaxInclusive());
				rm.setType(JValidationType.range.name());
				this.validation = rm;
			} else {
				throw new JServiceException("Invalid JValidation Type - "+jvalid);
			}
		}
		
		if((this.validation!=null) && (this.validation instanceof EnumModel)) {
			EnumModel enumModel = (EnumModel)this.validation;
			this.UIType = enumModel.getUIType();
			this.align = Align.CENTER;
			return;
		}
		try {
			JPrimitive jPrimitive = JPrimitive.getPrimitive(type);
			switch(jPrimitive) {
			case STRING : 	this.UIType = "TEXT";		this.align = Align.LEFT;	break;
			case INTEGER : 	this.UIType = "TEXT"; 		this.align = Align.RIGHT; 	break;
			case LONG : 	this.UIType = "TEXT"; 		this.align = Align.RIGHT; 	break;
			case FLOAT : 	this.UIType = "TEXT"; 		this.align = Align.RIGHT; 	break;
			case DOUBLE : 	this.UIType = "TEXT"; 		this.align = Align.RIGHT; 	break;
			case CHAR : 	this.UIType = "TEXT"; 		this.align = Align.LEFT; 	break;
			case DATETIME : this.UIType = "TEXT"; 		this.align = Align.CENTER; 	break;
			case TIMESTAMP :this.UIType = "TEXT"; 		this.align = Align.CENTER; 	break;
			case CURRENCY : this.UIType = "TEXT"; 		this.align = Align.RIGHT; 	break;
			case TIMEZONE :	this.UIType = "TEXT"; 		this.align = Align.CENTER; 	break;
			case BOOLEAN : 	this.UIType = "TEXT"; 		this.align = Align.CENTER; 	break;
			case TIME : 	this.UIType = "TIME"; 		this.align = Align.CENTER; 	break;
			case DATE : 	this.UIType = "DATE"; 		this.align = Align.CENTER; 	break;
			default : 		this.UIType = "TEXT";		this.align = Align.LEFT; 	break;
			}
		} catch(Exception e) {
			if(JAttachment.NAME.equals(type)) {
				this.UIType = "ATTACHMENT";
			} else if(JArray.NAME.equals(type)) {
				this.UIType = "TEXT";
			} else {
				this.UIType = "REFERENCE";
			}
		}
	}
	
	public ColumnModel(JViewDetail detail)  throws JServiceException {
		this(detail.getJAttribute());
		this.setLabel(detail.getLabel());
		this.setLength(detail.getLength());
		this.setSequence(detail.getSequence());
		this.setReadOnly(detail.isReadOnly());
	}
	
	public Long getTypeId() {
		return this.typeId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}

	public ValidationModel getValidation() {
		return validation;
	}

	public void setValidation(ValidationModel validation) {
		this.validation = validation;
	}
	
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public boolean isReadOnly() {
		return readOnly;
	}
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isDerived() {
		return derived;
	}
	public void setDerived(boolean derived) {
		this.derived = derived;
	}
	public void setUIType(String UIType) {
		this.UIType = UIType;
	}
	public String getUIType() {
		return this.UIType;
	}
	
	public String getEncrypt() {
		return encrypt;
	}
	public void setEncrypt(String encrypt) {
		this.encrypt = encrypt;
	}
	public Integer getDisplayLength() {
		return displayLength;
	}
	public void setDisplayLength(Integer displayLength) {
		this.displayLength = displayLength;
	}
	public String getAlign() {
		return align;
	}
	public void setAlign(String align) {
		this.align = align;
	}
	@Override
	public String toString() {
		return "ColumnModel [name=" + name + ", label=" + label
				+ ", description=" + description + ", type=" + type
				+ ", length=" + length + ", typeId=" + typeId + ", validation="
				+ validation + "]";
	}
	
	@Override
	public int compareTo(ColumnModel o) {
		if(o==null||o.getSequence()==null) {
			return 1;
		}
		if(this.sequence==null) {
			return -1;
		}
		
		return this.sequence - o.getSequence();
	}
	
	
}
