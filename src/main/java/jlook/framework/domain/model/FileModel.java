package jlook.framework.domain.model;

import jlook.framework.domain.util.JAttachmentDetail;

public class FileModel {
	private Long objectId;
	private String name;
	private String comments;
	private String savedName;
	private int length;
	private String type;
	private boolean defaultYn;
	
	public FileModel(){}
	public FileModel(JAttachmentDetail detail) {
		this.objectId = detail.getObjectId();
		this.name = detail.getName();
		this.type = detail.getType();
		this.comments = detail.getComments();
		this.length = detail.getLength().intValue();
		this.defaultYn = "Y".equals(detail.getDefaultYn());
		this.savedName = detail.getSavedName();
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public String getName() {
		return name;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSavedName() {
		return savedName;
	}
	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isDefaultYn() {
		return defaultYn;
	}
	public void setDefaultYn(boolean defaultYn) {
		this.defaultYn = defaultYn;
	}
	@Override
	public String toString() {
		return "FileModel [objectId=" + objectId + ", name=" + name
				+ ", savedName=" + savedName + ", length=" + length + ", type="
				+ type + ", defaultYn=" + defaultYn + "]";
	}
	
	
}
