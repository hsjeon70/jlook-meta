package jlook.framework.domain.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Preference")
public class PreferenceModel {
	
	private String name;
	private String type;
	private String description;
	private String value;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "PreferenceModel [name=" + name + ", type=" + type
				+ ", description=" + description + ", value=" + value + "]";
	}
	
	
}
