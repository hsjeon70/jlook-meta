package jlook.framework.domain.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import jlook.framework.infrastructure.util.PageUtil;


@XmlRootElement(name="Result")
public class JSummaryModel extends JResultModel {
	private PageUtil page;
	private List<ColumnModel> columnModel;
	
	public void setPage(PageUtil page){
		this.page = page;
	}
	public PageUtil getPage() {
		return this.page;
	}
	public List<ColumnModel> getColumnModel() {
		return columnModel;
	}
	public void setColumnModel(List<ColumnModel> columnModel) {
		this.columnModel = columnModel;
	}
	
	@Override
	public String toString() {
		return "JSummaryModel [page=" + page + ", columnModel=" + columnModel
				+ ", config=" + config + ", result=" + result + ", message="
				+ message + ", data=" + data + "]";
	}
	
	
}
