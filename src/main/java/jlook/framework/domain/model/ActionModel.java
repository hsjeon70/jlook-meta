package jlook.framework.domain.model;

import jlook.framework.domain.metadata.JClassAction;

public class ActionModel {
	private String name;
	private String label;
	private String description;
	private String targetUrl;
	private String method;
	private String target;
	private String widget;
	
	public ActionModel() {}
	public ActionModel(JClassAction jClassAction) {
		this.name = jClassAction.getName();
		this.label = jClassAction.getLabel();
		this.description = jClassAction.getDescription();
		this.method = jClassAction.getTargetMethod();
		this.targetUrl = jClassAction.getTargetUrl();
		this.target = jClassAction.getTarget();
		this.widget = jClassAction.getWidget();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getWidget() {
		return widget;
	}
	public void setWidget(String widget) {
		this.widget = widget;
	}
	
	
}
