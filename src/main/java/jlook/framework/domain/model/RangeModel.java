package jlook.framework.domain.model;

public class RangeModel extends ValidationModel {
	private Double minValue;
	private boolean minInclusive;
	private Double maxValue;
	private boolean maxInclusive;
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	public boolean isMinInclusive() {
		return minInclusive;
	}
	public void setMinInclusive(boolean minInclusive) {
		this.minInclusive = minInclusive;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
	public boolean isMaxInclusive() {
		return maxInclusive;
	}
	public void setMaxInclusive(boolean maxInclusive) {
		this.maxInclusive = maxInclusive;
	}
	
}
