package jlook.framework.domain.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="validation")
public class ValidationModel {
	protected String name;
	protected String description;
	protected String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "ValidationModel [name=" + name + ", description=" + description
				+ ", type=" + type + "]";
	}
	
}
