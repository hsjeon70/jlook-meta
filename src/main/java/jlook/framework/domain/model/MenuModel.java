package jlook.framework.domain.model;

import java.util.ArrayList;
import java.util.List;

import jlook.framework.domain.common.JMenu;

public class MenuModel implements Comparable<MenuModel> {
	private Long objectId;
	private String name;
	private String label;
	private String description;
	private String actionUrl;
	private String target;
	private String widget;
	private Integer sequence;
	
	public List<MenuModel> children = new ArrayList<MenuModel>();

	public MenuModel() {}
	public MenuModel(JMenu jMenu) {
		this.setObjectId(jMenu.getObjectId());
		this.setName(jMenu.getName());
		this.setLabel(jMenu.getLabel());
		this.setDescription(jMenu.getDescription());
		this.setActionUrl(jMenu.getActionUrl());
		this.setTarget(jMenu.getTarget());
		this.setWidget(jMenu.getWidget());
		this.setSequence(jMenu.getSequence());
	}
	
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getActionUrl() {
		return actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<MenuModel> getChildren() {
		return children;
	}

	public void setChildren(List<MenuModel> children) {
		this.children.addAll(children);
	}
	
	public void addChild(MenuModel model) {
		this.children.add(model);
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getWidget() {
		return widget;
	}

	public void setWidget(String widget) {
		this.widget = widget;
	}

	@Override
	public String toString() {
		return "MenuModel [objectId=" + objectId + ", name=" + name + "]";
	}

	@Override
	public int compareTo(MenuModel o) {
		if(this.sequence==null) {
			this.sequence = 0;
		}
		if(o.getSequence()==null) {
			o.setSequence(0);
		}
		return this.sequence-o.getSequence();
	}
}
