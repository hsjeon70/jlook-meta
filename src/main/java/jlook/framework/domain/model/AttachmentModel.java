package jlook.framework.domain.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;

public class AttachmentModel {
	private Long objectId;
	private String subject;
	private String content;
	
	private List<FileModel> files;
	
	public AttachmentModel() {}
	public AttachmentModel(JAttachment attach) {
		this.objectId = attach.getObjectId();
		this.subject = attach.getSubject();
		this.content = attach.getContent();
		Set<JAttachmentDetail> details = attach.getJAttachmentDetails();
		for(JAttachmentDetail detail : details) {
			FileModel fmodel = new FileModel(detail);
			this.addFile(fmodel);
		}
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<FileModel> getFiles() {
		return files;
	}
	public void setFiles(List<FileModel> files) {
		this.files = files;
	}
	public void addFile(FileModel model) {
		if(this.files==null) {
			this.files = new ArrayList<FileModel>();
		}
		
		this.files.add(model);
	}
	@Override
	public String toString() {
		return "AttachmentModel [objectId=" + objectId + ", files=" + files
				+ "]";
	}
	
	
}
