package jlook.framework.domain.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Result")
public class JResultModel {
	public static final String DATA_KEY = "resultData";
	public static final String CONFIG_KEY = "configMap";
	public static final String LOGIN_URL_KEY = "loginUrl";
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	
	protected Map<String,Object> config;
	protected List<String> standards;
	protected List<ActionModel> actions;
	protected String result;
	protected String message;
	protected Object data;
	
	public JResultModel() {
		this.result = SUCCESS;
		this.config = new HashMap<String,Object>();
	}
	public Map<String, Object> getConfig() {
		return config;
	}
	public void setConfig(Map<String, Object> config) {
		if(this.config==null) {
			this.config = new HashMap<String,Object>();
		}
		this.config.putAll(config);
	}
	
	public Object getConfig(String name) {
		if(this.config==null) {
			return null;
		}
		return this.config.get(name);
	}
	
	public void addConfig(Map<String,?> config) {
		this.config.putAll(config);
	}
	public void addConfig(String name, Object value) {
		this.config.put(name,value);
	}
	public void putConfigAll(Map<String, Object> config) {
		this.config.putAll(config);
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public List<String> getStandards() {
		return standards;
	}
	public void setStandards(List<String> standards) {
		this.standards = standards;
	}
	public List<ActionModel> getActions() {
		return actions;
	}
	public void setActions(List<ActionModel> actions) {
		this.actions = actions;
	}
	@Override
	public String toString() {
		return "JResultModel [config=" + config + ", result=" + result
				+ ", message=" + message + ", data=" + data + "]";
	}
}
