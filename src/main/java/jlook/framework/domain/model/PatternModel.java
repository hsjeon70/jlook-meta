package jlook.framework.domain.model;

public class PatternModel extends ValidationModel {
	private String regExpression;

	public String getRegExpression() {
		return regExpression;
	}

	public void setRegExpression(String regExpression) {
		this.regExpression = regExpression;
	}
	
	
}
