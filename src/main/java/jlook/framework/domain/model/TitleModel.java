package jlook.framework.domain.model;

public class TitleModel {
	private Long classId;
	private Long objectId;
	private String value;
	
	
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "TitleModel [classId="+classId+", objectId=" + objectId + ", value=" + value + "]";
	}
}
