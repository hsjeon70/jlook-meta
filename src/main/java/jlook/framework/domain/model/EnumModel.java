package jlook.framework.domain.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnumModel extends ValidationModel {
	private String UIType;
	private List<Map<String,String>> values = new ArrayList<Map<String,String>>();
	
	public String getUIType() {
		return UIType;
	}

	public void setUIType(String UIType) {
		this.UIType = UIType;
	}

	public List<Map<String,String>> getValues() {
		return this.values;
	}
	
	public void addValue(String value, String label) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("value", value);
		map.put("label", label);
		this.values.add(map);
	}

	@Override
	public String toString() {
		return "EnumModel [UIType=" + UIType + ", values=" + values + ", name="
				+ name + ", description=" + description + ", type=" + type
				+ "]";
	}
	
	
}
