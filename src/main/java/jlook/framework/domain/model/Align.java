package jlook.framework.domain.model;

public enum Align {
	center,
	left,
	right;
	
	public static final String CENTER = "center";
	public static final String LEFT = "left";
	public static final String RIGHT = "right";
	
}
