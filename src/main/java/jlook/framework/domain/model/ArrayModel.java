package jlook.framework.domain.model;

import java.util.ArrayList;
import java.util.List;

public class ArrayModel {
	private Long objectId;
	
	private List<String> values;
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	
	public void addValue(String value) {
		if(this.values==null) {
			this.values = new ArrayList<String>();
		}
		
		this.values.add(value);
	}
	@Override
	public String toString() {
		return "ArrayModel [objectId=" + objectId + ", values=" + values + "]";
	}	
}
