package jlook.framework.domain.model;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.service.JServiceException;

public class AttributeModel extends ColumnModel {
	private Object value;
	
	public AttributeModel() {};
	public AttributeModel(JAttribute jattr) throws JServiceException {
		super(jattr);
	}
	public AttributeModel(JViewDetail detail) throws JServiceException {
		super(detail);
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "AttributeModel [name=" + name + ", label=" + label + ", description="
				+ description + ", required=" + required + ", value=" + value
				+ ", type=" + type + ", length=" + length
				+ ", typeId=" + typeId + ", validation=" + validation + "]";
	}
	
}
