package jlook.framework.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JOBJECT, did=JDomainDef.SYSTEM_ID, name=JObject.NAME, type=JClassType.INTERFACE_TYPE, status=JMetaStatus.NONE_TYPE)
public class JObject implements Serializable, Comparable<JObject> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JObject";
	
	public static final String A_OID = "objectId";
	public static final String A_CID = "classId";
	public static final String A_DID = "domainId";
	public static final String A_CREATED_BY = "createdBy";
	public static final String A_CREATED_ON = "createdOn";
	public static final String A_UPDATED_BY = "updatedBy";
	public static final String A_UPDATED_ON = "updatedOn";
	
	
	@jattr(name=A_OID, label="Object Id", type=JPrimitive.LONG_TYPE, length=15, description="Object Id", 
			required=true, unique=true, searchable=true, derived=true) 		protected Long objectId;
	@jattr(name=A_CID, label="Class Id", type=JPrimitive.LONG_TYPE, length=15, description="Class Id", 
			required=true, unique=false, searchable=true, derived=true) 		protected Long classId;
	@jattr(name=A_DID, label="Domain Id", type=JPrimitive.LONG_TYPE, length=15, description="Domain Id", 
			required=true, primary=true, unique=false, searchable=true, derived=true) 		protected Long domainId;
	@jattr(name=A_CREATED_BY, label="CreatedBy", type=JPrimitive.LONG_TYPE, length=15, description="CreatedBy", 
			required=true, unique=false, searchable=true, derived=true) 		protected Long createdBy;
	@jattr(name=A_CREATED_ON, label="CreatedOn", type=JPrimitive.TIMESTAMP_TYPE, description="CreatedOn", 
			required=true, unique=false, searchable=true, derived=true) 		protected Timestamp createdOn;
	@jattr(name=A_UPDATED_BY, label="UpdatedBy", type=JPrimitive.LONG_TYPE, length=15, description="UpdatedBy", 
			required=false, unique=false, searchable=true, derived=true) 		protected Long updatedBy;
	@jattr(name=A_UPDATED_ON, label="UpdatedOn", type=JPrimitive.TIMESTAMP_TYPE, description="UpdatedOn", 
			required=false, unique=false, searchable=true, derived=true) 		protected Timestamp updatedOn;
	
	
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	
	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((domainId == null) ? 0 : domainId.hashCode());
		result = prime * result
				+ ((objectId == null) ? 0 : objectId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JObject other = (JObject) obj;
		if (domainId == null) {
			if (other.domainId != null)
				return false;
		} else if (!domainId.equals(other.domainId))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		return true;
	}

	@Override
	public int compareTo(JObject target) {
		if(target==null) {
			return 1;
		}
		if(this.objectId==null) {
			return -1;
		}
		return new Long(this.objectId-target.getObjectId()).intValue();
	}
	
	
}
