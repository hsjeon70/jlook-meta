package jlook.framework.domain.account;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JUSERDOMAIN, did=JDomainDef.SYSTEM_ID, name=JUserDomain.NAME,
summaryFilter="if(!juser.administrator){ \n"+
		" \"(JUser in (select objectId from JUser where domainId=\"+juser.domainId+\"))\" \n}")
public class JUserDomain extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JUserDomain";
	public static final String A_JUSER = "JUser";
	public static final String A_JDOMAIN = "JDomain";
	
	@jattr(name=A_JUSER, label="User", type=JUser.NAME, length=15, description="User", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JUser jUser;
	@jattr(name=A_JDOMAIN, label="Domain", type=JDomain.NAME, length=15, description="Domain", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JDomain jDomain;
	
	public JUser getJUser() {
		return jUser;
	}
	public void setJUser(JUser jUser) {
		this.jUser = jUser;
	}
	public JDomain getJDomain() {
		return jDomain;
	}
	public void setJDomain(JDomain jDomain) {
		this.jDomain = jDomain;
	}
	
}
