package jlook.framework.domain.account;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JUSERROLE, did=JDomainDef.SYSTEM_ID, name=JUserRole.NAME,
summaryFilter="if(!juser.administrator){ \n"+
		" \"(JUser in (select objectId from JUser where domainId=\"+juser.domainId+\"))\" \n}")
public class JUserRole  extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JUserRole";
	
	public static final String A_JUSER = "JUser";
	public static final String A_JROLE = "JRole";
	
	@jattr(name=A_JUSER, label="User", type=JUser.NAME, length=15, description="User", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JUser jUser;
	@jattr(name=A_JROLE, label="Role", type=JRole.NAME, length=15, description="Role", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JRole jRole;
	
	public JUser getJUser() {
		return jUser;
	}
	public void setJUser(JUser jUser) {
		this.jUser = jUser;
	}
	public JRole getJRole() {
		return jRole;
	}
	public void setJRole(JRole jRole) {
		this.jRole = jRole;
	}
	
	
}
