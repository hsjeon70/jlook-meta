package jlook.framework.domain.account;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.account.JDomainAction;

/**
 * Domain value object
 * 
 * @author hsjeon70
 *
 */
@jclass(cid=JMetaKeys.JDOMAIN, did=JDomainDef.SYSTEM_ID, name=JDomain.NAME, 
	accessRule=JAccessRule.PROTECTED_TYPE,
 	actionClass=JDomainAction.CLASS_NAME,
	summaryFilter="if(!juser.administrator){ \n"
			+" \"(objectId=\"+juser.domainId+\")\" \n"
			+"}")
public class JDomain extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JDomain";
	
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION	= "description";
	public static final String A_ACTIVE			= "active";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Domain Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false, readOnly=true) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Domain Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_ACTIVE, label="Active", type=JPrimitive.BOOLEAN_TYPE,  description="Domain Active", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean active;
	
	public JDomain() {}
	
	public JDomain(Long objectId) {
		this.setObjectId(objectId);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JDomain other = (JDomain) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
