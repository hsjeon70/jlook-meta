package jlook.framework.domain.account;

import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;


@jclass(cid=JMetaKeys.JGROUP, did=JDomainDef.SYSTEM_ID, name=JGroup.NAME,
summaryFilter="if(!juser.administrator){ \n"+
		" \"(domainId=\"+juser.domainId+\")\" \n}")
public class JGroup extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JGroup";
	
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_PARENT 		= "parent";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Group Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Group Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_PARENT, label="Parent", type=JGroup.NAME, length=15, description="Parent Group", 
			required=false, unique=false, searchable=false, derived=false) 	private JGroup parent;
	
	private Set<JRole> roles;
	
	public JGroup() {}
	
	public JGroup(Long objectId) {
		this.objectId = objectId;
	}
	
	public void setJRoles(Set<JRole> roles) {
		this.roles = roles;
	}
	public Set<JRole> getJRoles() {
		return this.roles;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JGroup getParent() {
		return parent;
	}

	public void setParent(JGroup parent) {
		this.parent = parent;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JGroup other = (JGroup) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
