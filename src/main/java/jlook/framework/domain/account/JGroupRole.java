package jlook.framework.domain.account;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JGROUPROLE, did=JDomainDef.SYSTEM_ID, name=JGroupRole.NAME,
summaryFilter="if(!juser.administrator){ \n"+
		" \"(JGroup in (select objectId from JGroup where domainId=\"+juser.domainId+\"))\" \n}")
public class JGroupRole extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JGroupRole";
	public static final String A_JGROUP = "JGroup";
	public static final String A_JROLE  = "JRole";
	
	@jattr(name=A_JGROUP, label="Group", type=JGroup.NAME, length=15, description="Group", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JGroup jGroup;
	@jattr(name=A_JROLE, label="Role", type=JRole.NAME, length=15, description="Role", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private JRole jRole;
	
	public JGroup getJGroup() {
		return jGroup;
	}
	public void setJGroup(JGroup jGroup) {
		this.jGroup = jGroup;
	}
	public JRole getJRole() {
		return jRole;
	}
	public void setJRole(JRole jRole) {
		this.jRole = jRole;
	}
	
	
}
