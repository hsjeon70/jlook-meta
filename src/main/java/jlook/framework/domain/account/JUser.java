package jlook.framework.domain.account;

import java.util.HashSet;
import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.EncryptType;
import jlook.framework.domain.security.JRole;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.infrastructure.annotation.GsonExclude;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.account.JUserAction;


@jclass(cid=JMetaKeys.JUSER, did=JDomainDef.SYSTEM_ID, name=JUser.NAME,
	actionClass=JUserAction.CLASS_NAME,
	summaryFilter="if(!juser.administrator){ \n"+
			" \"(domainId=\"+juser.domainId+\")\" \n}")
public class JUser extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JUser";
	
	public static final String A_NICKNAME 	= "nickname";
	public static final String A_EMAIL 		= "email";
	public static final String A_PASSWORD 	= "password";
	public static final String A_MOBILE		= "mobile";
	public static final String A_JGROUP 	= "JGroup";
	public static final String A_PICTURE	= "picture";
	
	@jattr(name=A_NICKNAME, label="Nickname", type=JPrimitive.STRING_TYPE, length=20, description="User Nickname", 
			required=true, unique=false, searchable=true, derived=false) 		private String nickname;
	@jattr(name=A_EMAIL, label="Email", type=JPrimitive.STRING_TYPE, length=100, description="User Email address", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 			private String email;
	@jattr(name=A_PASSWORD, label="Password", type=JPrimitive.STRING_TYPE, length=15, description="User Password", 
			required=true, unique=false, searchable=false, derived=false, encrypt=EncryptType.PASSWORD) 		private String password;
	@jattr(name=A_MOBILE, label="Mobile Phone", type=JPrimitive.STRING_TYPE, length=15, description="User Mobile Phone", 
			required=false, unique=false, searchable=false, derived=false) 		private String mobile;
	@jattr(name=A_JGROUP, label="Group", type=JGroup.NAME, length=15, description="User Group", 
			required=false, unique=false, searchable=false, derived=false) 		private JGroup jGroup;
	
	@GsonExclude
	@jattr(name=A_PICTURE, label="Picture", type=JAttachment.NAME, length=15, description="User Picture", 
	required=false, unique=false, searchable=false, derived=false) 		private JAttachment picture;
	
	@GsonExclude
	private Set<JRole> roles;
	@GsonExclude
	private Set<JDomain> domains;
	
	
	public JUser() {
		
	}
	
	public JUser(Long objectId) {
		this.objectId = objectId;
	}
	
	public void addJRole(JRole jRole) {
		if(this.roles==null) {
			this.roles = new HashSet<JRole>();
		}
		
		this.roles.add(jRole);
	}
	public void setJRoles(Set<JRole> roles) {
		this.roles = roles;
	}
	public Set<JRole> getJRoles() {
		return this.roles;
	}
	
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public JGroup getJGroup() {
		return this.jGroup;
	}
	
	public void setJGroup(JGroup jGroup) {
		this.jGroup = jGroup;
	}
	public JAttachment getPicture() {
		return picture;
	}
	public void setPicture(JAttachment picture) {
		this.picture = picture;
	}

	public Set<JDomain> getJDomains() {
		return domains;
	}

	public void setJDomains(Set<JDomain> domains) {
		this.domains = domains;
	}
	
	public void addJDomain(JDomain jDomain) {
		if(this.domains==null) {
			this.domains = new HashSet<JDomain>();
		}
		
		this.domains.add(jDomain);
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JUser other = (JUser) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
}
