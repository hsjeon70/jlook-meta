package jlook.framework.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface jattr {
	String name();
	String description()	default "";
	String label()			;
	String type()			;
	int length()			default 0;
	String defaultValue()	default	"";
	boolean required()		default false;
	boolean primary()		default false;
	boolean unique()		default false;
	boolean searchable()	default false;
	boolean derived()		default false;
	String validation()		default "";
	boolean readOnly()		default false;
	boolean virtual()		default false;
	boolean hidden()		default false;
	String encrypt()		default "";
	String target()			default "";
	String status()			default "CREATED";
}
