package jlook.framework.domain.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JClassType;
import jlook.framework.domain.metadata.JAccessRule;


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface jclass {
	long cid();
	long did();
	String name();
	String label() default "";
	String description() default "";
	String type() default JClassType.CONCRETE_TYPE;
	String inheritanceJClass() default "";
	String inheritanceType() default "";
	String summaryFilter() default "";
	String JObjectFilter() default "";
	String actionClass() default "";
	String accessRule() default JAccessRule.PUBLIC_TYPE;
	String status() default JMetaStatus.CREATED_TYPE;
}
