package jlook.framework.domain;

import jlook.framework.infrastructure.JException;

public class JErrorInfo {
	private Throwable exception;
	private String message;
	
	private Throwable cause;
	private String reason;
	
	private Throwable rootCause;
	private String rootMessage;
	
	public JErrorInfo(Throwable th) {
		this.exception = th;
		this.message = th.getMessage();
		
		Throwable cause = getRootCause(th);
		if(cause instanceof JException) {
			this.cause = (JException)cause;
			this.reason = cause.getMessage();
			
			this.rootCause = cause.getCause();
			this.rootMessage = this.rootCause==null ? null : this.rootCause.getMessage();
		} else {
			this.cause = cause.getCause();
			this.reason = this.cause==null ? null: this.cause.getMessage();
		}
	}
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

	public String getRootMessage() {
		return rootMessage;
	}

	public void setRootMessage(String rootMessage) {
		this.rootMessage = rootMessage;
	}

	public Throwable getRootCause() {
		return rootCause;
	}

	public void setRootCause(Throwable rootCause) {
		this.rootCause = rootCause;
	}

	@Override
	public String toString() {
		return "JErrorInfo [message=" + message + ", exception=" + exception
				+ ", reason=" + reason + ", cause=" + cause
				+ ", rootMessage=" + rootMessage + ", rootCause=" + rootCause
				+ "]";
	}
	
	private Throwable getRootCause(Throwable th) {
		Throwable cause = th.getCause();
		if(cause==null) {
			return th;
		} else if(cause instanceof JException) {
			return getRootCause(cause);
		} else {
			return th;
		}
	}
}
