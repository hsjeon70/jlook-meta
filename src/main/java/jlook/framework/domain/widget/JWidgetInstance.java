package jlook.framework.domain.widget;

import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JWIDGETINSTANCE, did=JDomainDef.SYSTEM_ID, name=JWidgetInstance.NAME)
public class JWidgetInstance extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JWidgetInstance";
	public static final String A_TITLE = "title";
	public static final String A_JWIDGETDEFINITION = "JWidgetDefinition";
	public static final String A_JVIEW = "JView";
	
	@jattr(name=A_TITLE, label="Title", type=JPrimitive.STRING_TYPE, length=30, description="JWidget Title", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private String title;
	@jattr(name=A_JWIDGETDEFINITION, label="JWidgetDefinition", type=JWidgetDefinition.NAME, length=15, description="JWidgetDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JWidgetDefinition jWidgetDefinition;
	@jattr(name=A_JVIEW, label="JView", type=JView.NAME, length=15, description="JView", 
			required=true, primary=false, unique=false, searchable=true, derived=false) 	private JView jView;
	
	private Set<JWidgetParamValue> jWidgetParamValues;
	
	public Set<JWidgetParamValue> getJWidgetParamValues() {
		return jWidgetParamValues;
	}
	public void setJWidgetParamValues(Set<JWidgetParamValue> jWidgetParamValues) {
		this.jWidgetParamValues = jWidgetParamValues;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public JWidgetDefinition getJWidgetDefinition() {
		return jWidgetDefinition;
	}
	public void setJWidgetDefinition(JWidgetDefinition jWidgetDefinition) {
		this.jWidgetDefinition = jWidgetDefinition;
	}
	public JView getJView() {
		return jView;
	}
	public void setJView(JView jView) {
		this.jView = jView;
	}
}
