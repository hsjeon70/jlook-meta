package jlook.framework.domain.widget;

import java.util.Map;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JWIDGETDEFINITION, did=JDomainDef.SYSTEM_ID, name=JWidgetDefinition.NAME)
public class JWidgetDefinition extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JWidgetDefinition";
	public static final String A_NAME = "name";
	public static final String A_DESCRIPTION = "description";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JWidget Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JWidget Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	
	private Map<String, JWidgetParamName> jWidgetParamNames;
	
	public Map<String, JWidgetParamName> getJWidgetParamNames() {
		return jWidgetParamNames;
	}
	public void setJWidgetParamNames(Map<String, JWidgetParamName> jWidgetParamNames) {
		this.jWidgetParamNames = jWidgetParamNames;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
