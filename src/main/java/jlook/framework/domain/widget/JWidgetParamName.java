package jlook.framework.domain.widget;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JWIDGETPARAMNAME, did=JDomainDef.SYSTEM_ID, name=JWidgetParamName.NAME)
public class JWidgetParamName extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JWidgetParamName";
	
	public static final String A_JWIDGETDEFINITION = "JWidgetDefinition";
	public static final String A_NAME = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TYPE = "type";
	public static final String A_DEFAULT_VALUE = "defaultValue";
	
	@jattr(name=A_JWIDGETDEFINITION, label="JWidgetDefinition", type=JWidgetDefinition.NAME, length=15, description="JWidgetDefinition", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JWidgetDefinition jWidgetDefinition;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JWidget Param Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JWidget Param Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="JWidget Param Type", 
			required=true, unique=false, searchable=true, derived=false, defaultValue="CLIENT", validation="JWidgetParamType") 	private String type;
	@jattr(name=A_DEFAULT_VALUE, label="Default Value", type=JPrimitive.STRING_TYPE, length=100, description="JWidget Default Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String defaultValue;
	
	public JWidgetDefinition getJWidgetDefinition() {
		return jWidgetDefinition;
	}
	public void setJWidgetDefinition(JWidgetDefinition jWidgetDefinition) {
		this.jWidgetDefinition = jWidgetDefinition;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
}
