package jlook.framework.domain.widget;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JWIDGETPARAMVALUE, did=JDomainDef.SYSTEM_ID, name=JWidgetParamValue.NAME)
public class JWidgetParamValue extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JWidgetParamValue";
	public static final String A_JWIDGETINSTANCE = "JWidgetInstance";
	public static final String A_JWIDGETPARAMNAME = "JWidgetParamName";
	public static final String A_VALUE = "value";
	
	@jattr(name=A_JWIDGETINSTANCE, label="JWidgetInstance", type=JWidgetInstance.NAME, length=15, description="JWidgetInstance", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JWidgetInstance jWidgetInstance;
	@jattr(name=A_JWIDGETPARAMNAME, label="JWidgetParamName", type=JWidgetParamName.NAME, length=15, description="JWidgetParamName", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JWidgetParamName jWidgetParamName;
	@jattr(name=A_VALUE, label="Param Value", type=JPrimitive.STRING_TYPE, length=100, description="JWidget Param Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String value;
	
	public JWidgetInstance getJWidgetInstance() {
		return jWidgetInstance;
	}
	public void setJWidgetInstance(JWidgetInstance jWidgetInstance) {
		this.jWidgetInstance = jWidgetInstance;
	}
	public JWidgetParamName getJWidgetParamName() {
		return jWidgetParamName;
	}
	public void setJWidgetParamName(JWidgetParamName jWidgetParamName) {
		this.jWidgetParamName = jWidgetParamName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
