package jlook.framework.domain;

import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JGroup;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserRole;
import jlook.framework.domain.account.JUserDomain;
import jlook.framework.domain.account.JGroupRole;
import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchHistory;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamName;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.domain.common.JAnnounceDetail;
import jlook.framework.domain.common.JAnnounceMessage;
import jlook.framework.domain.common.JCalendar;
import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.common.JPreferenceDetail;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.metadata.JPattern;
import jlook.framework.domain.metadata.JRange;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.metadata.JValidation;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.metadata.JMetaChangeLog;
import jlook.framework.domain.management.JAccessLog;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.domain.security.JMenuAccessRule;
import jlook.framework.domain.security.JRole;
import jlook.framework.domain.security.JWidgetAccessRule;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetParamName;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamValue;

public enum JMetaKeys {
	JObject(JMetaKeys.JOBJECT, JObject.class),
	JDomain(JMetaKeys.JDOMAIN, JDomain.class),
	JGroup(JMetaKeys.JGROUP, JGroup.class),
	JUser(JMetaKeys.JUSER, JUser.class),
	JRole(JMetaKeys.JROLE, JRole.class),
	JUserRole(JMetaKeys.JUSERROLE, JUserRole.class),
	JUserDomain(JMetaKeys.JUSERDOMAIN, JUserDomain.class),
	JGroupRole(JMetaKeys.JGROUPROLE, JGroupRole.class),
	JValidation(JMetaKeys.JVALIDATION, JValidation.class),
	JEnum(JMetaKeys.JENUM, JEnum.class),
	JEnumDetail(JMetaKeys.JENUMDETAIL, JEnumDetail.class),
	JPattern(JMetaKeys.JPATTERN, JPattern.class),
	JRange(JMetaKeys.JRANGE, JRange.class),
	JClass(JMetaKeys.JCLASS, JClass.class),
	JType(JMetaKeys.JTYPE, JType.class),
	JAttribute(JMetaKeys.JATTRIBUTE, JAttribute.class),
	JView(JMetaKeys.JVIEW, JView.class),
	JViewDetail(JMetaKeys.JVIEWDETAIL, JViewDetail.class),
	JDataAccessRule(JMetaKeys.JDATAACCESSRULE, JDataAccessRule.class),
	JClassAction(JMetaKeys.JCLASSACTION, JClassAction.class),
	JMetaChangeLog(JMetaKeys.JMETACHANGELOG, JMetaChangeLog.class),
	JPreference(JMetaKeys.JPREFERENCE, JPreference.class),
	JPreferenceDetail(JMetaKeys.JPREFERENCEDETAIL, JPreferenceDetail.class),
	JArray(JMetaKeys.JARRAY, JArray.class),
	JArrayDetail(JMetaKeys.JARRAYDETAIL, JArrayDetail.class),
	JAttachment(JMetaKeys.JATTACHMENT, JAttachment.class),
	JAttachmentDetail(JMetaKeys.JATTACHMENTDETAIL, JAttachmentDetail.class),
	JObjectRating(JMetaKeys.JOBJECTRATING, JObjectRating.class),
	JObjectRatingDetail(JMetaKeys.JOBJECTRATINGDETAIL, JObjectRatingDetail.class),
	JAnnounceMessage(JMetaKeys.JANNOUNCEMESSAGE, JAnnounceMessage.class),
	JAnnounceDetail(JMetaKeys.JANNOUNCEDETAIL, JAnnounceDetail.class),
	JMenu(JMetaKeys.JMENU, JMenu.class),
	JMenuAccessRule(JMetaKeys.JMENUACCESSRULE, JMenuAccessRule.class),
	JAccessLog(JMetaKeys.JACCESSLOG, JAccessLog.class),
	JBatchDefinition(JMetaKeys.JBATCHDEFINITION, JBatchDefinition.class),
	JBatchParamName(JMetaKeys.JBATCHPARAMNAME, JBatchParamName.class),
	JBatchInstance(JMetaKeys.JBATCHINSTANCE, JBatchInstance.class),
	JBatchParamValue(JMetaKeys.JBATCHPARAMVALUE, JBatchParamValue.class),
	JBatchHistory(JMetaKeys.JBATCHHISTORY, JBatchHistory.class),
	JWidgetDefinition(JMetaKeys.JWIDGETDEFINITION, JWidgetDefinition.class),
	JWidgetParamName(JMetaKeys.JWIDGETPARAMNAME, JWidgetParamName.class),
	JWidgetInstance(JMetaKeys.JWIDGETINSTANCE, JWidgetInstance.class),
	JWidgetParamValue(JMetaKeys.JWIDGETPARAMVALUE, JWidgetParamValue.class),
	JWidgetAccessRule(JMetaKeys.JWIDGETACCESSRULE, JWidgetAccessRule.class),
	JCalendar(JMetaKeys.JCALENDAR, JCalendar.class);
	
	JMetaKeys(long classId, Class<? extends JObject> clazz) {
		this.classId = classId;
		this.clazz = clazz;
		this.name = this.clazz.getSimpleName();
	}
	
	private long classId;
	private String name;
	private Class<? extends JObject> clazz;
	
	public Long getClassId() {
		return this.classId;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Class<? extends JObject> getJClass() {
		return this.clazz;
	}
	
	public static final long JOBJECT			= 1L;
	public static final long JDOMAIN 			= 2L;
	public static final long JARRAY				= 3L;
	public static final long JARRAYDETAIL		= 4L;
	public static final long JATTACHMENT 		= 5L;
	public static final long JATTACHMENTDETAIL 	= 6L;
	public static final long JGROUP 			= 7L;
	public static final long JUSER 				= 8L;
	public static final long JROLE 				= 9L;
	public static final long JUSERROLE			= 10L;
	public static final long JUSERDOMAIN		= 11L;
	public static final long JGROUPROLE			= 12L;
	public static final long JVALIDATION		= 13L;
	public static final long JENUM				= 14L;
	public static final long JENUMDETAIL		= 15L;
	public static final long JPATTERN			= 16L;
	public static final long JRANGE				= 17L;
	public static final long JCLASS 			= 18L;
	public static final long JTYPE 				= 19L;
	public static final long JATTRIBUTE 		= 20L;
	public static final long JVIEW				= 21L;
	public static final long JVIEWDETAIL		= 22L;
	public static final long JDATAACCESSRULE	= 23L;
	public static final long JMENU 				= 24L;
	public static final long JMENUACCESSRULE	= 25L;
	public static final long JCLASSACTION		= 26L;
	public static final long JMETACHANGELOG		= 27L;
	public static final long JPREFERENCE 		= 28L;
	public static final long JPREFERENCEDETAIL 	= 29L;
	public static final long JACCESSLOG			= 30L;
	public static final long JOBJECTRATING 		= 31L;
	public static final long JOBJECTRATINGDETAIL= 32L;
	public static final long JANNOUNCEMESSAGE	= 33L;
	public static final long JANNOUNCEDETAIL	= 34L;
	public static final long JBATCHDEFINITION	= 35L;
	public static final long JBATCHPARAMNAME	= 36L;
	public static final long JBATCHINSTANCE		= 37L;
	public static final long JBATCHPARAMVALUE	= 38L;
	public static final long JBATCHHISTORY		= 39L;
	public static final long JWIDGETDEFINITION  = 40L;
	public static final long JWIDGETPARAMNAME	= 41L;
	public static final long JWIDGETINSTANCE	= 42L;
	public static final long JWIDGETPARAMVALUE 	= 43L;
	public static final long JWIDGETACCESSRULE 	= 44L;
	public static final long JCALENDAR			= 45L;
	
//	public static final long JCATEGORY 			= 5L;;
}
