package jlook.framework.domain;

public enum JPrimitive {
	STRING(JPrimitive.STRING_TYPE, 		java.lang.String.class),
	BOOLEAN(JPrimitive.BOOLEAN_TYPE, 	boolean.class),
	INTEGER(JPrimitive.INTEGER_TYPE, 	java.lang.Integer.class),
	LONG(JPrimitive.LONG_TYPE, 			java.lang.Long.class),
	FLOAT(JPrimitive.FLOAT_TYPE, 		java.lang.Float.class),
	DOUBLE(JPrimitive.DOUBLE_TYPE, 		java.lang.Double.class),
	CHAR(JPrimitive.CHAR_TYPE,			java.lang.Character.class),
	DATE(JPrimitive.DATE_TYPE,			java.util.Date.class),
	TIME(JPrimitive.TIME_TYPE,			java.sql.Time.class),
	DATETIME(JPrimitive.DATETIME_TYPE, 	java.sql.Timestamp.class),
	TIMESTAMP(JPrimitive.TIMESTAMP_TYPE,java.sql.Timestamp.class),
	TIMEZONE(JPrimitive.TIMEZONE_TYPE,	java.util.TimeZone.class),
	CURRENCY(JPrimitive.CURRENCY_TYPE,	java.util.Currency.class);
	
	public static final String STRING_TYPE 		= "string";
	public static final String INTEGER_TYPE 	= "integer";
	public static final String LONG_TYPE 		= "long";
	public static final String FLOAT_TYPE 		= "float";
	public static final String DOUBLE_TYPE 		= "double";
	public static final String CHAR_TYPE 		= "character";
	public static final String BOOLEAN_TYPE 	= "boolean";
	public static final String DATE_TYPE 		= "date";
	public static final String TIME_TYPE 		= "time";
	public static final String DATETIME_TYPE 	= "datetime";
	public static final String TIMESTAMP_TYPE 	= "timestamp";
	public static final String TIMEZONE_TYPE 	= "timezone";
	public static final String CURRENCY_TYPE 	= "currency";
	
	JPrimitive(String name, Class<?> clazz) {
		this.name = name;
		this.clazz = clazz;
	}
	private String name;
	private Class<?> clazz;
	
	public String getName() {
		return this.name;
	}
	
	public Class<?> getJavaClass() {
		return this.clazz;
	}
	
	public static JPrimitive getPrimitive(String name) {
		JPrimitive ptype = JPrimitive.valueOf(name.toUpperCase());
		return ptype;
	}
	
	public boolean equals(String name) {
		if(name==null) return false;
		return name.equals(this.name);
	}
}
