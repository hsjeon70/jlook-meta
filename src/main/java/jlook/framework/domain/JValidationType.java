package jlook.framework.domain;

public enum JValidationType {
	enumeration,
	pattern,
	range;
}
