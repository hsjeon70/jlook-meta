package jlook.framework.domain;

public enum JBoolean {
	TRUE(Boolean.TRUE),
	FALSE(Boolean.FALSE);
	
	private JBoolean(Boolean value) {
		this.value = value;
	}
	
	private Boolean value;
	
	public Boolean getValue() {
		return this.value;
	}
	
	public String toString() {
		return this.name().toLowerCase();
	}
}
