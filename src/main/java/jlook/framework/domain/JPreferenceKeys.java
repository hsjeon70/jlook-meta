package jlook.framework.domain;

public interface JPreferenceKeys {
	public static final String APPLICATION_NAME 			= "application.name";
	public static final String APPLICATION_VERSION 			= "application.version";
	public static final String APPLICATION_ADMINISTRATOR 	= "application.administrator";
	
	public static final String DOMAIN_VERSION 				= "domain.version";
	public static final String DOMAIN_ADMINISTRATOR			= "domain.administrator";
	public static final String DOMAIN_SIGNIN_REDIRECT_URL 	= "domain.signIn.redirectUrl";
	
	public static final String USER_CONTACT_INFO		= "user.contact.info";
	public static final String USER_SIGNIN_REDIRECT_URL = "user.signIn.redirectUrl";
}
