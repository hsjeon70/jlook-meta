package jlook.framework.domain.docs;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="header")
public class Header {
	private String name;
	private String type;
	private String description;
	private String required;
	
	
	public String getRequired() {
		return required;
	}
	public void setRequired(String required) {
		this.required = required;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Header [name=" + name + ", type=" + type + ", description="
				+ description + ", required=" + required + "]";
	}
	
	
}
