package jlook.framework.domain.docs;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="OpenAPI-Service")
public class OpenAPIService {
	private String category;
	private String name;
	private String description;
	
	private List<OpenAPI> openAPIList;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="OpenAPI")
	public List<OpenAPI> getOpenAPIList() {
		return openAPIList;
	}

	public void setOpenAPIList(List<OpenAPI> openAPIList) {
		this.openAPIList = openAPIList;
	}
	
	public void addOpenAPI(OpenAPI api) {
		if(this.openAPIList==null) {
			this.openAPIList = new ArrayList<OpenAPI>();
		}
		this.openAPIList.add(api);
	}
	
	
}
