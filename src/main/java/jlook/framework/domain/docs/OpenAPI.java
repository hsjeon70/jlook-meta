package jlook.framework.domain.docs;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="OpenAPI")
public class OpenAPI {
	private String name;
	private String uri;
	private String method;
	private String description;
	private String response;
	private String exception;
	
	private List<Header> headers;
	private List<Parameter> parameters;
	
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name="header")
	public List<Header> getHeaders() {
		return headers;
	}
	public void setHeaders(List<Header> headers) {
		this.headers = headers;
	}
	@XmlElement(name="parameter")
	public List<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}
	
	public void addHeader(Header header) {
		if(this.headers==null) {
			this.headers = new ArrayList<Header>();
		}
		
		this.headers.add(header);
	}
	
	public void addParameter(Parameter parameter) {
		if(this.parameters==null) {
			this.parameters = new ArrayList<Parameter>();
		}
		
		this.parameters.add(parameter);
	}
	
}
