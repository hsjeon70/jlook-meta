package jlook.framework.domain.management;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JACCESSLOG, did=JDomainDef.SYSTEM_ID, name=JAccessLog.NAME, accessRule=JAccessRule.PRIVATE_TYPE)
public class JAccessLog extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAccessLog";
	
	public static final String A_REQUEST_URI 	= "requestUri";
	public static final String A_USER_AGENT 	= "userAgent";
	public static final String A_REFERER 		= "referer";
	public static final String A_CLIENT_TYPE 	= "clientType";
	public static final String A_ELAPSE_TIME 	= "elapseTime";
	public static final String A_RESULT 		= "result";
	public static final String A_MESSAGE 		= "message";
	
	@jattr(name=A_REQUEST_URI, label="Request URI", type=JPrimitive.STRING_TYPE, length=500, description="Request URI", 
			required=true, unique=false, searchable=true, derived=false) 		private String requestUri;
	@jattr(name=A_USER_AGENT, label="User Agent", type=JPrimitive.STRING_TYPE, length=100, description="User Agent", 
			required=false, unique=false, searchable=false, derived=false) 		private String userAgent;
	@jattr(name=A_REFERER, label="Referer", type=JPrimitive.STRING_TYPE, length=500, description="Referer", 
			required=false, unique=false, searchable=false, derived=false) 		private String referer;
	@jattr(name=A_CLIENT_TYPE, label="Client Type", type=JPrimitive.STRING_TYPE, length=10, description="Client Type", 
			required=true, unique=false, searchable=true, derived=false) 		private String clientType;
	@jattr(name=A_ELAPSE_TIME, label="Elapse Time", type=JPrimitive.STRING_TYPE, length=10, description="Elapse Time", 
			required=true, unique=false, searchable=true, derived=false) 		private String elapseTime;
	@jattr(name=A_RESULT, label="Result", type=JPrimitive.STRING_TYPE, length=10, description="Result", 
			required=true, unique=false, searchable=true, derived=false) 		private String result;
	@jattr(name=A_MESSAGE, label="Message", type=JPrimitive.STRING_TYPE, length=4000, description="Message", 
			required=false, unique=false, searchable=false, derived=false) 		private String message;
	
	public String getRequestUri() {
		return requestUri;
	}
	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public String getElapseTime() {
		return elapseTime;
	}
	public void setElapseTime(String elapseTime) {
		this.elapseTime = elapseTime;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
