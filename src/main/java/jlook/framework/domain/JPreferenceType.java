package jlook.framework.domain;

public enum JPreferenceType {
	SYSTEM,
	DOMAIN,
	USER;
}
