package jlook.framework.domain;

public enum JMetaStatus {
	NONE,
	CREATED,
	CHANGED,
	UNMAPPED,
	REMAPPED,
	MAPERROR,
	BUILDERROR;
	
	public static final String NONE_TYPE 	= "NONE";
	public static final String CREATED_TYPE = "CREATED";
	public static final String CHANGED_TYPE = "CHANGED";
	
	public static final String UNMAPPED_TYPE 	= "UNMAPPED";
	public static final String REMAPPED_TYPE 	= "REMAPPED";
	public static final String MAPERROR_TYPE 	= "MAPERROR";
	public static final String BUILDERROR_TYPE 	= "BUILDERROR";
	
}
