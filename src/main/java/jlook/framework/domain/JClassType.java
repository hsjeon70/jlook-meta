package jlook.framework.domain;

public enum JClassType {
	CONCRETE,
	ABSTRACT,
	INTERFACE;
	
	public static final String CONCRETE_TYPE  = "CONCRETE";
	public static final String ABSTRACT_TYPE  = "ABSTRACT";
	public static final String INTERFACE_TYPE = "INTERFACE";
}
