package jlook.framework.domain;

public enum HttpMethod {
	get,
	post,
	delete,
	put;
	
	public static final String GET 	= "get";
	public static final String POST = "post";
	public static final String DELETE = "delete";
	public static final String PUT = "put";
	
}
