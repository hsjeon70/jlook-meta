package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JENUMDETAIL, did=JDomainDef.SYSTEM_ID, name=JEnumDetail.NAME)
public class JEnumDetail extends JObject implements Comparable<JObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JEnumDetail";
	
	public static final String A_JENUM = "JEnum";
	public static final String A_VALUE = "value";
	public static final String A_LABEL = "label";
	public static final String A_SEQUENCE = "sequence";

	@jattr(name=A_JENUM, label="JEnum", type=JEnum.NAME, length=15, description="JEnum", 
			required=true, primary=true, unique=false, searchable=false, derived=false)  		private JEnum jEnum;
	@jattr(name=A_VALUE, label="Value", type=JPrimitive.STRING_TYPE, length=50, description="Enumeration Value", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private String value;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=50, description="Enumeration Label", 
			required=true, primary=false, unique=false, searchable=true, derived=false) 		private String label;
	@jattr(name=A_SEQUENCE, label="Sequence", type=JPrimitive.INTEGER_TYPE, length=5, description="Enumeration Sequence", 
			required=true, primary=false, unique=false, searchable=true, derived=false) 		private Integer sequence;
	public JEnum getJEnum() {
		return jEnum;
	}
	public void setJEnum(JEnum jEnum) {
		this.jEnum = jEnum;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
	@Override
	public int compareTo(JObject jObject) {
		if(jObject instanceof JEnumDetail) {
			JEnumDetail detail = (JEnumDetail)jObject;
			return this.sequence-detail.getSequence();
		} else {
			return super.compareTo(jObject);
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jEnum == null) ? 0 : jEnum.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JEnumDetail other = (JEnumDetail) obj;
		if (jEnum == null) {
			if (other.jEnum != null)
				return false;
		} else if (!jEnum.equals(other.jEnum))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JEnumDetail [value=" + value + ", label=" + label + "]";
	}
	
	
}
