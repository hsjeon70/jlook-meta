package jlook.framework.domain.metadata;

public enum JViewType {
	title,
	summary,
	detail;
	
	public static final String TITLE = "title";
	public static final String SUMMARY = "summary";
	public static final String DETAIL = "detail";
	
	public String getSystemViewName(JClass jClass) {
		return jClass.getName()+" "+Character.toUpperCase(name().charAt(0))+name().substring(1)+"View";
	}
}
