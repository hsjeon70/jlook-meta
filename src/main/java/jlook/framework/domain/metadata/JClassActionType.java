package jlook.framework.domain.metadata;

public enum JClassActionType {
	ALL,
	CLASS,
	OBJECT;
	
	public static final String ALL_TYPE = "ALL";
	public static final String CLASS_TYPE  = "CLASS";
	public static final String OBJECT_TYPE = "OBJECT";
}
