package jlook.framework.domain.metadata;

import java.util.HashMap;
import java.util.Map;

import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.metadata.JClassAction;


@jclass(cid=JMetaKeys.JCLASS, did=JDomainDef.SYSTEM_ID, name=JClass.NAME, actionClass=JClassAction.CLASS_NAME)
public class JClass extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JClass";
	
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_CATEGORY 		= "category";
	public static final String A_LABEL			= "label";
	public static final String A_TYPE			= "type";
	public static final String A_INHERITANCE_JCLASS = "inheritanceJClass";
	public static final String A_INHERITANCE_TYPE  	= "inheritanceType";
	public static final String A_SUMMARY_FILTER		= "summaryFilter";
	public static final String A_JOBJECT_RULE		= "JObjectRule";
	public static final String A_ACTION_CLASS		= "actionClass";
	public static final String A_ACCESS_RULE		= "accessRule";
	public static final String A_STATUS				= "status";
	public static final String A_MAP_ACTION			= "mapAction";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JClass Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false, readOnly=true) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JClass Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_CATEGORY, label="Category", type=JPrimitive.STRING_TYPE, length=200, description="JClass Category", 
			required=false, unique=false, searchable=false, derived=false, readOnly=true) 	private String category;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=30, description="JClass Label", 
			required=false, unique=false, searchable=false, derived=false) 		private String label;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=10, description="JClass Type", 
			required=true, unique=false, searchable=true, derived=false, readOnly=true, defaultValue=JClassType.CONCRETE_TYPE, validation="JClassType") 	private String type;
	@jattr(name=A_INHERITANCE_JCLASS, label="Inheritance JClass", type=JClass.NAME, length=15, description="Inheritance JClass", 
			required=false, unique=false, searchable=false, derived=false, readOnly=true) 	private JClass inheritanceJClass;
	@jattr(name=A_INHERITANCE_TYPE, label="Inheritance Type", type=JPrimitive.STRING_TYPE, length=10, description="Inheritance Type", 
			required=false, unique=false, searchable=false, derived=false, readOnly=true, validation="InheritanceType") 	private String inheritanceType;
	@jattr(name=A_ACTION_CLASS, label="Action Class", type=JPrimitive.STRING_TYPE, length=200, description="Action Class", 
			required=false, unique=false, searchable=false, derived=false) 	private String actionClass;
	@jattr(name=A_SUMMARY_FILTER, label="Summary Filter", type=JPrimitive.STRING_TYPE, length=4000, description="Summary View filtering", 
			required=false, unique=false, searchable=false, derived=false) 	private String summaryFilter;
	@jattr(name=A_JOBJECT_RULE, label="JObject Rule", type=JPrimitive.STRING_TYPE, length=4000, description="JObject Rule", 
			required=false, unique=false, searchable=false, derived=false) 	private String jObjectRule;
	@jattr(name=A_ACCESS_RULE, label="Access Rule", type=JPrimitive.STRING_TYPE, length=9, description="Access Rule", 
			required=false, unique=false, searchable=true, derived=false, defaultValue="PUBLIC", validation="JAccessRule") 	private String accessRule;
	@jattr(name=A_STATUS, label="Status", type=JPrimitive.STRING_TYPE, length=6, description="JClass Status", 
			required=true, unique=false, searchable=true, derived=true, defaultValue="NONE", validation="JMetaStatus") 	private String status;
	@jattr(name=A_MAP_ACTION, label="Map Action", type=JPrimitive.STRING_TYPE, length=9, description="JClass Map Action", 
			required=false, unique=false, searchable=false, derived=false, virtual=true, validation="JClassMapType") 	private String mapAction;
	
	private Map<String,JAttribute> jAttributes = new HashMap<String,JAttribute>();
	
	public JClass() {}
	public JClass(Long objectId) {
		this.objectId = objectId;
	}
	
	public boolean typeOf(JClass jClass) {
		if(this.equals(jClass)) {
			return true;
		}
		JClass parent = this.getInheritanceJClass();
		if(parent==null) {
			return false;
		}
		return parent.typeOf(jClass);
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Map<String,JAttribute> getJAttributes() {
		return this.jAttributes;
	}
	
	public void setJAttributes(Map<String,JAttribute> jAttributes) {
		this.jAttributes = jAttributes;
	}
	
	public void addJAttribute(JAttribute jAttribute) {
		if(this.jAttributes==null) {
			this.jAttributes = new HashMap<String,JAttribute>();
		}
		
		this.jAttributes.put(jAttribute.getName(), jAttribute);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public JClass getInheritanceJClass() {
		return inheritanceJClass;
	}

	public void setInheritanceJClass(JClass inheritanceJClass) {
		this.inheritanceJClass = inheritanceJClass;
	}

	public String getInheritanceType() {
		return inheritanceType;
	}

	public void setInheritanceType(String inheritanceType) {
		this.inheritanceType = inheritanceType;
	}

	
	public String getActionClass() {
		return actionClass;
	}
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	public String getSummaryFilter() {
		return summaryFilter;
	}

	public void setSummaryFilter(String summaryFilter) {
		this.summaryFilter = summaryFilter;
	}

	public String getJObjectRule() {
		return jObjectRule;
	}

	public void setJObjectRule(String jObjectRule) {
		this.jObjectRule = jObjectRule;
	}

	
	public String getAccessRule() {
		return accessRule;
	}
	public void setAccessRule(String accessRule) {
		this.accessRule = accessRule;
	}
	public String getMapAction() {
		return mapAction;
	}
	public void setMapAction(String mapAction) {
		this.mapAction = mapAction;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JClass other = (JClass) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JClass [name=" + name + ", status=" + status + ", objectId="
				+ objectId + ", classId=" + classId + ", domainId=" + domainId
				+ "]";
	}
	
}
