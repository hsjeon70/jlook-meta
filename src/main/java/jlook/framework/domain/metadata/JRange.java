package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.JValidationException;

@jclass(cid=JMetaKeys.JRANGE, did=JDomainDef.SYSTEM_ID, name=JRange.NAME, inheritanceJClass="JValidation", inheritanceType=InheritanceType.EXTENDS_TYPE)
public class JRange extends JValidation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JRange";
	
	public static final String A_MINVALUE 		= "minValue";
	public static final String A_MININCLUSIVE 	= "minInclusive";
	public static final String A_MAXVALUE 		= "maxValue";
	public static final String A_MAXINCLUSIVE 	= "maxInclusive";
	
	@jattr(name=A_MINVALUE, label="MinValue", type=JPrimitive.DOUBLE_TYPE, length=10, description="Minimum value", 
			required=false, unique=false, searchable=false, derived=false) 		private Double minValue;
	@jattr(name=A_MININCLUSIVE, label="MinInclusive", type=JPrimitive.BOOLEAN_TYPE, description="Minimum Inclusive", 
			required=false, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 		private boolean minInclusive;
	@jattr(name=A_MAXVALUE, label="MaxValue", type=JPrimitive.DOUBLE_TYPE, length=10, description="Maximum value", 
			required=false, unique=false, searchable=false, derived=false) 		private Double maxValue;
	@jattr(name=A_MAXINCLUSIVE, label="MaxInclusive", type=JPrimitive.BOOLEAN_TYPE, description="Maximum Inclusive", 
			required=false, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 		private boolean maxInclusive;
	
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	public boolean isMinInclusive() {
		return minInclusive;
	}
	public void setMinInclusive(boolean minInclusive) {
		this.minInclusive = minInclusive;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
	public boolean isMaxInclusive() {
		return maxInclusive;
	}
	public void setMaxInclusive(boolean maxInclusive) {
		this.maxInclusive = maxInclusive;
	}
	@Override
	public void validate(String[] values) throws JValidationException {
		if(values==null) {
			return;
		}
		Double doubleValue = null;
		for(String value : values) {
			if(value==null) {
				continue;
			}
			try {
				doubleValue = new Double(value);
			} catch(Exception e) {
				throw new JValidationException("Range validation is failed. {0} - {1}", new Object[]{this.toString(), value});
			}
			
			boolean result = checkMinimum(doubleValue, this.minInclusive) && checkMaximum(doubleValue,maxInclusive);
			if(!result) {
				throw new JValidationException("Range validation is failed. {0} - {1}", new Object[]{this.toString(), value});
			}
		}
	}

	@Override
	public void validate(Object value) throws JValidationException {
		if(value==null) {
			return;
		}
		
		Double doubleValue = null;
		if(value instanceof String) {
			try {
				doubleValue = new Double((String)value);
			} catch(Exception e) {
				throw new JValidationException("Range validation is failed. {0} - {1}", new Object[]{this.toString(), value});
			}
		} else if (value instanceof Double) {
			doubleValue = (Double)value;
		} else if (value instanceof Integer) {
			doubleValue = ((Integer)value).doubleValue();
		} else if (value instanceof Float) {
			doubleValue = ((Float)value).doubleValue();
		} else if (value instanceof Short) {
			doubleValue = ((Short)value).doubleValue();
		} else {
			throw new JValidationException("Invalid input type. It is not a number.  {0} - {1}", new Object[]{this.toString(), value});
		}
		
		boolean result = checkMinimum(doubleValue, this.minInclusive) && checkMaximum(doubleValue,maxInclusive);
		if(!result) {
			throw new JValidationException("Range validation is failed. {0} - {1}", new Object[]{this.toString(), value});
		}
	}
	
	private boolean checkMinimum(Double value, boolean inclusive) {
		boolean isValid = false;
		
		if(this.minValue==null) {
			return true;
		} else if(value==null) {
			return false;
		} else {
			int result = value.compareTo(minValue);
			
			if(inclusive) {
				if(result>=0) isValid = true;
			} else {
				if(result>0) isValid = true;
			}
		}
		return isValid;
	}
	
	private boolean checkMaximum(Double value, boolean inclusive) {
		boolean isValid = false;
		
		if(this.maxValue==null) {
			return true;
		} else if(value==null) {
			return false;
		} else {
			int result = value.compareTo(maxValue);
			
			if(inclusive) {
				if(result <= 0) isValid = true;
			} else {
				if(result < 0) isValid = true;
			}
		}
		return isValid;
	}
	
	@Override
	public String toString() {
		return "JRange [minValue=" + minValue + ", minInclusive="
				+ minInclusive + ", maxValue=" + maxValue + ", maxInclusive="
				+ maxInclusive + "]";
	}
	
}
