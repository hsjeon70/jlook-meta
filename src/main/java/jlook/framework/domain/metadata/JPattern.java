package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.JValidationException;

@jclass(cid=JMetaKeys.JPATTERN, did=JDomainDef.SYSTEM_ID, name=JPattern.NAME, inheritanceJClass="JValidation", inheritanceType=InheritanceType.EXTENDS_TYPE)
public class JPattern extends JValidation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JPattern";
	
	public static final String A_REGEXPRESSION = "regExpression";
	
	@jattr(name=A_REGEXPRESSION, label="RegExpression", type=JPrimitive.STRING_TYPE, length=500, description="Pattern regular expression", 
			required=true, unique=false, searchable=false, derived=false) 		private String regExpression;

	public String getRegExpression() {
		return regExpression;
	}

	public void setRegExpression(String regExpression) {
		this.regExpression = regExpression;
	}

	@Override
	public void validate(String[] values) throws JValidationException {
		if(values==null) {
			return;
		}
		
		boolean valid = false;
		for(String value : values) {
			if(value==null) {
				continue;
			}
			valid = value.matches(regExpression);
			if(!valid) {
				throw new JValidationException("Pattern validation is failed.{0} - {1}", new Object[]{regExpression, value});
			}
		}
	}

	@Override
	public void validate(Object value) throws JValidationException {
		if(value instanceof String) {
			validate(new String[]{(String)value});
		} else {
			throw new JValidationException("Input value is not a string. - {0}", new Object[]{value});
		}
	}
	
	
	
}
