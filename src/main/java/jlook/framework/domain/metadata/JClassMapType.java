package jlook.framework.domain.metadata;

public enum JClassMapType {
	mapping,
	unmapping, 
	remapping;
	
	public static final String MAPPING   = "mapping";
	public static final String UNMAPPING = "unmapping";
	public static final String REMAPPING = "remapping";
}
