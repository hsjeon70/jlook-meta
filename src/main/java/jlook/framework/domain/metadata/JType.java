package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JTYPE, did=JDomainDef.SYSTEM_ID, name=JType.NAME, accessRule=JAccessRule.PRIVATE_TYPE)
public class JType extends JObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JType";
	
	public static final String A_NAME 			= "name";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_JCLASS 		= "JClass";
	
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="Type Name", 
			required=true, primary=true, unique=true, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="Type Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_JCLASS, label="JClass", type=JClass.NAME, length=15, description="Referencing JClass", 
			required=false, unique=false, searchable=true, derived=false) 	private JClass jClass;
	
	public boolean isPrimitive() {
		return this.jClass==null;
	}
	
	public boolean isJArray() {
		if(this.jClass==null) {
			return false;
		}
		
		return this.jClass.getName().equals(JArray.NAME);
	}
	
	public boolean isJAttachment() {
		if(this.jClass==null) {
			return false;
		}
		
		return this.jClass.getName().equals(JAttachment.NAME);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public JClass getJClass() {
		return jClass;
	}
	public void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JType other = (JType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JDataType [name=" + name + ", objectId=" + objectId
				+ ", classId=" + classId + ", domainId=" + domainId + "]";
	}
	
}
