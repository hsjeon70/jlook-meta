package jlook.framework.domain.metadata;

public enum JActionTarget {
	tab,
	main,
	document,
	overlay;
	
	public static final String TAB = "tab";
	public static final String MAIN = "main";
	public static final String DOCUMENT = "document";
	public static final String OVERLAY = "overlay";
}
