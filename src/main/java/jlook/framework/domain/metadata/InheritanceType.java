package jlook.framework.domain.metadata;

public enum InheritanceType {
	EXTENDS,
	IMPLEMENTS;
	
	public static final String EXTENDS_TYPE 	= "EXTENDS";
	public static final String IMPLEMENTS_TYPE 	= "IMPLEMENTS";
	
}
