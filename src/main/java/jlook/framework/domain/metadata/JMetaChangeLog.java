package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JMETACHANGELOG, did=JDomainDef.SYSTEM_ID, name=JMetaChangeLog.NAME, accessRule=JAccessRule.PRIVATE_TYPE)
public class JMetaChangeLog extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JMetaChangeLog";
	public static final String A_JCLASS  		= "JClass";
	public static final String A_JATTRIBUTE 	= "JAttribute";
	public static final String A_ACTION			= "action";
	public static final String A_TARGET			= "target";
	public static final String A_BEFORE_VALUE 	= "beforeValue";
	public static final String A_AFTER_VALUE 	= "afterValue";
	
	@jattr(name=A_JCLASS, label="JClass", type=JClass.NAME, length=15, description="JClass ", 
			required=true, unique=false, searchable=true, derived=false) 	private JClass jClass;
	@jattr(name=A_JATTRIBUTE, label="JAttribute", type=JAttribute.NAME, length=15, description="JAttribute ", 
			required=false, unique=false, searchable=true, derived=false) 	private JAttribute jAttribute;
	@jattr(name=A_ACTION, label="Action", type=JPrimitive.STRING_TYPE, length=6, description="Action", 
			required=true, unique=false, searchable=true, derived=false, validation="JActionType") 	private String action;
	@jattr(name=A_TARGET, label="Target", type=JAttribute.NAME, length=15, description="Target ", 
			required=false, unique=false, searchable=true, derived=false) 	private JAttribute target;
	@jattr(name=A_BEFORE_VALUE, label="Before Value", type=JPrimitive.STRING_TYPE, length=4000, description="Before value ", 
			required=false, unique=false, searchable=true, derived=false) 	private String beforeValue;
	@jattr(name=A_AFTER_VALUE, label="After Value", type=JPrimitive.STRING_TYPE, length=4000, description="After value ", 
			required=false, unique=false, searchable=true, derived=false) 	private String afterValue;
	
	public JClass getJClass() {
		return jClass;
	}
	public void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	public JAttribute getTarget() {
		return target;
	}
	public void setTarget(JAttribute target) {
		this.target = target;
	}
	public JAttribute getJAttribute() {
		return jAttribute;
	}
	public void setJAttribute(JAttribute jAttribute) {
		this.jAttribute = jAttribute;
	}
	public String getBeforeValue() {
		return beforeValue;
	}
	public void setBeforeValue(String beforeValue) {
		this.beforeValue = beforeValue;
	}
	public String getAfterValue() {
		return afterValue;
	}
	public void setAfterValue(String afterValue) {
		this.afterValue = afterValue;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
}
