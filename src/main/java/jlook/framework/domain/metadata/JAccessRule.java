package jlook.framework.domain.metadata;

public enum JAccessRule {
	PUBLIC,
	PROTECTED,
	PRIVATE;
	
	public static final String PUBLIC_TYPE = "PUBLIC";
	public static final String PROTECTED_TYPE = "PROTECTED";
	public static final String PRIVATE_TYPE = "PRIVATE";
	
}
