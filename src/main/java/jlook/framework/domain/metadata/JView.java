package jlook.framework.domain.metadata;

import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JVIEW, did=JDomainDef.SYSTEM_ID, name=JView.NAME)
public class JView extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JView";
	public static final String A_JCLASS = "JClass";
	public static final String A_NAME = "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TYPE = "type";
	
	@jattr(name=A_JCLASS, label="JClass", type=JClass.NAME, length=15, description="JView JClass ", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 	private JClass jClass;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=40, description="JView Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JView Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=7, description="JView Type", 
			required=true, unique=false, searchable=false, derived=false, defaultValue=JViewType.DETAIL, validation="JViewType") 	private String type;
	
	private Set<JViewDetail> details;
	
	/**
	 * @return the jClass
	 */
	public JClass getJClass() {
		return jClass;
	}
	/**
	 * @param jClass the jClass to set
	 */
	public void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	public Set<JViewDetail> getDetails() {
		return details;
	}
	public void setDetails(Set<JViewDetail> details) {
		this.details = details;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jClass == null) ? 0 : jClass.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JView other = (JView) obj;
		if (jClass == null) {
			if (other.jClass != null)
				return false;
		} else if (!jClass.equals(other.jClass))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
