package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JCLASSACTION, did=JDomainDef.SYSTEM_ID, name=JClassAction.NAME)
public class JClassAction extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JClassAction";
	
	public static final String A_JCLASS 	= "JClass";
	public static final String A_NAME 		= "name";
	public static final String A_DESCRIPTION = "description";
	public static final String A_TYPE 		= "type";
	public static final String A_LABEL 		= "label";
	public static final String A_TARGET_METHOD = "targetMethod";
	public static final String A_TARGET_URL 	= "targetUrl";
	public static final String A_TARGET			= "target";
	public static final String A_WIDGET			= "widget";
	
	@jattr(name=A_JCLASS, label="JClass", type=JClass.NAME, length=15, description="JClassAction JClass ", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 	private JClass jClass;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=20, description="JClassAction Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private String name;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JClassAction Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=6, description="JClassAction Type", 
			required=false, unique=false, searchable=true, derived=false, defaultValue="CLASS", validation="JClassActionType") 		private String type;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=20, description="JClassAction Label", 
			required=false, unique=false, searchable=false, derived=false) 		private String label;
	@jattr(name=A_TARGET_METHOD, label="Target Method", type=JPrimitive.STRING_TYPE, length=500, description="JClassAction target http method", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="get", validation="HttpMethod") 	private String targetMethod;
	@jattr(name=A_TARGET_URL, label="Target URL", type=JPrimitive.STRING_TYPE, length=500, description="JClassAction target url", 
			required=true, unique=false, searchable=false, derived=false) 	private String targetUrl;
	@jattr(name=A_TARGET, label="Target", type=JPrimitive.STRING_TYPE, length=10, description="Action Target", 
			required=false, primary=false, unique=false, searchable=false, derived=false, validation="JActionTarget") 		private String target;
	@jattr(name=A_WIDGET, label="Widget", type=JPrimitive.STRING_TYPE, length=20, description="Target Widget Type", 
			required=false, primary=false, unique=false, searchable=false, derived=false, validation="JWidgetType") 		private String widget;
	
	public JClass getJClass() {
		return jClass;
	}
	public void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	public String getWidget() {
		return widget;
	}
	public void setWidget(String widget) {
		this.widget = widget;
	}
	public String getTargetMethod() {
		return targetMethod;
	}
	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	
}
