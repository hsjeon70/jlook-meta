package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.action.metadata.JAttributeAction;

@jclass(cid=JMetaKeys.JATTRIBUTE, did=JDomainDef.SYSTEM_ID, name=JAttribute.NAME, actionClass=JAttributeAction.CLASS_NAME)
public class JAttribute extends JObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JAttribute";
	
	public static final String A_JCLASS 		= "JClass";
	public static final String A_NAME 			= "name";
	public static final String A_LABEL 			= "label";
	public static final String A_DESCRIPTION 	= "description";
	public static final String A_TYPE			= "type";
	public static final String A_LENGTH 		= "length";
	public static final String A_DEFAULT_VALUE 	= "defaultValue";
	public static final String A_REQUIRED		= "required";
	public static final String A_PRIMARY		= "primary";
	public static final String A_UNIQUE			= "unique";
	public static final String A_SEARCHABLE		= "searchable";
	public static final String A_VIRTUAL		= "virtual";
	public static final String A_HIDDEN			= "hidden";
	public static final String A_READONLY		= "readOnly";
	public static final String A_DERIVED		= "derived";
	public static final String A_JVALIDATION  	= "JValidation";
	public static final String A_ENCRYPT		= "encrypt";
	public static final String A_TARGET			= "target";
	public static final String A_STATUS			= "status";
	
	@jattr(name=A_JCLASS, label="JClass", type=JClass.NAME, length=15, description="JAttribute JClass ", 
			required=true, primary=true, unique=false, searchable=true, derived=false, readOnly=true) 	private JClass jClass;
	@jattr(name=A_NAME, label="Name", type=JPrimitive.STRING_TYPE, length=30, description="JAttribute Name", 
			required=true, primary=true, unique=false, searchable=true, derived=false, readOnly=true) 		private String name;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=30, description="JAttribute label", 
			required=false, unique=false, searchable=false, derived=false) 	private String label;
	@jattr(name=A_DESCRIPTION, label="Description", type=JPrimitive.STRING_TYPE, length=200, description="JAttribute Description", 
			required=false, unique=false, searchable=false, derived=false) 	private String description;
	@jattr(name=A_TYPE, label="Type", type=JType.NAME, length=15, description="JAttribute Type", 
			required=true, unique=false, searchable=true, derived=false, readOnly=true) 	private JType type;
	@jattr(name=A_LENGTH, label="Length", type=JPrimitive.INTEGER_TYPE, length=10, description="JAttribute Length", 
			required=false, unique=false, searchable=false, derived=false) 	private Integer length;
	
	@jattr(name=A_DEFAULT_VALUE, label="Default Value", type=JPrimitive.STRING_TYPE, length=100, description="JAttribute Default Value", 
			required=false, unique=false, searchable=true, derived=false) 	private String defaultValue;
	
	@jattr(name=A_REQUIRED, label="Required", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Required", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean required;
	@jattr(name=A_PRIMARY, label="Primary", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Primary Key", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean primary;
	@jattr(name=A_UNIQUE, label="Unique", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Unique", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean unique;
	@jattr(name=A_SEARCHABLE, label="Searchable", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Searchable", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean searchable;
	@jattr(name=A_VIRTUAL, label="Virtual", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Virtual", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean virtual;
	@jattr(name=A_HIDDEN, label="Hidden", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Hidden", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean hidden;
	@jattr(name=A_READONLY, label="ReadOnly", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute ReadOnly", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean readOnly;
	@jattr(name=A_DERIVED, label="Derived", type=JPrimitive.BOOLEAN_TYPE, description="JAttribute Derived", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="false",	validation="JBoolean") 	private boolean derived;
	
	@jattr(name=A_ENCRYPT, label="Encrypt", type=JPrimitive.STRING_TYPE, length=10, description="JAttribute Encrypt", 
			required=false, unique=false, searchable=false, derived=false, validation="EncryptType") 	private String encrypt;
	
	@jattr(name=A_JVALIDATION, label="JValidation", type=JValidation.NAME, length=15, description="Attribute Validation ", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 	private JValidation jValidation;
	@jattr(name=A_TARGET, label="Target", type=JAttribute.NAME, length=15, description="Target Class ", 
			required=false, primary=false, unique=false, searchable=false, derived=false) 	private JAttribute target;
	@jattr(name=A_STATUS, label="Status", type=JPrimitive.STRING_TYPE, length=7, description="JAttribute Status", 
			required=true, unique=false, searchable=true, derived=true, defaultValue=JMetaStatus.NONE_TYPE, validation="JMetaStatus") 	private String status;
	
	public JClass getJClass() {
		return jClass;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	public void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public JType getType() {
		return type;
	}
	public void setType(JType type) {
		this.type = type;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public boolean isUnique() {
		return unique;
	}
	public void setUnique(boolean unique) {
		this.unique = unique;
	}
	public boolean isSearchable() {
		return searchable;
	}
	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}
	public boolean isDerived() {
		return derived;
	}
	public void setDerived(boolean derived) {
		this.derived = derived;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public JValidation getJValidation() {
		return jValidation;
	}
	public void setJValidation(JValidation jValidation) {
		this.jValidation = jValidation;
	}
	public boolean isVirtual() {
		return virtual;
	}
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}
	public String getEncrypt() {
		return encrypt;
	}
	public void setEncrypt(String encrypt) {
		this.encrypt = encrypt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isReadOnly() {
		return readOnly;
	}
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	
	public JAttribute getTarget() {
		return target;
	}
	public void setTarget(JAttribute target) {
		this.target = target;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((jClass == null) ? 0 : jClass.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JAttribute other = (JAttribute) obj;
		if (jClass == null) {
			if (other.jClass != null)
				return false;
		} else if (!jClass.equals(other.jClass))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "JAttribute [name=" + name
				+ ", objectId=" + objectId + ", classId=" + classId
				+ ", domainId=" + domainId + ", jClass="+jClass+"]";
	}
}
