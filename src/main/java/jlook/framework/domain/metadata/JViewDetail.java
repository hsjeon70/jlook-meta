package jlook.framework.domain.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;

@jclass(cid=JMetaKeys.JVIEWDETAIL, did=JDomainDef.SYSTEM_ID, name=JViewDetail.NAME)
public class JViewDetail extends JObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "JViewDetail";
	public static final String A_JVIEW = "JView";
	public static final String A_JATTRIBUTE = "JAttribute";
	public static final String A_SEQUENCE = "sequence";
	public static final String A_LENGTH = "length";
	public static final String A_LABEL = "label";
	public static final String A_READONLY = "readOnly";
	
	@jattr(name=A_JVIEW, label="JView", type=JView.NAME, length=15, description="JView", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 	private JView jView;
	@jattr(name=A_JATTRIBUTE, label="JAttribute", type=JAttribute.NAME, length=15, description="JAttribute", 
			required=true, primary=true, unique=false, searchable=false, derived=false) 	private JAttribute jAttribute;
	@jattr(name=A_SEQUENCE, label="Sequence", type=JPrimitive.INTEGER_TYPE, length=3, description="Display Sequence", 
			required=true, primary=true, unique=false, searchable=true, derived=false) 		private Integer sequence;
	@jattr(name=A_LENGTH, label="Length", type=JPrimitive.INTEGER_TYPE, length=3, description="Length", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 		private Integer length;
	@jattr(name=A_LABEL, label="Label", type=JPrimitive.STRING_TYPE, length=30, description="Label", 
			required=false, primary=true, unique=false, searchable=true, derived=false) 		private String label;
	@jattr(name=A_READONLY, label="ReadOnly", type=JPrimitive.BOOLEAN_TYPE, description="ReadOnly", 
			required=true, primary=true, unique=false, searchable=true, derived=false, defaultValue="false", validation="JBoolean") 		private boolean readOnly;
	/**
	 * @return the jView
	 */
	public JView getJView() {
		return jView;
	}
	/**
	 * @param jView the jView to set
	 */
	public void setJView(JView jView) {
		this.jView = jView;
	}
	/**
	 * @return the jAttribute
	 */
	public JAttribute getJAttribute() {
		return jAttribute;
	}
	/**
	 * @param jAttribute the jAttribute to set
	 */
	public void setJAttribute(JAttribute jAttribute) {
		this.jAttribute = jAttribute;
	}
	/**
	 * @return the sequence
	 */
	public Integer getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	/**
	 * @return the length
	 */
	public Integer getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(Integer length) {
		this.length = length;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}
	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((jAttribute == null) ? 0 : jAttribute.hashCode());
		result = prime * result + ((jView == null) ? 0 : jView.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JViewDetail other = (JViewDetail) obj;
		if (jAttribute == null) {
			if (other.jAttribute != null)
				return false;
		} else if (!jAttribute.equals(other.jAttribute))
			return false;
		if (jView == null) {
			if (other.jView != null)
				return false;
		} else if (!jView.equals(other.jView))
			return false;
		return true;
	}
	
	
}
