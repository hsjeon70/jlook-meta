package jlook.framework.domain.metadata;

import java.util.Set;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.annotation.jattr;
import jlook.framework.domain.annotation.jclass;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.service.JValidationException;


@jclass(cid=JMetaKeys.JENUM, did=JDomainDef.SYSTEM_ID, name=JEnum.NAME, inheritanceJClass="JValidation", inheritanceType=InheritanceType.EXTENDS_TYPE)
public class JEnum extends JValidation {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String NAME = "JEnum";
	
	public static final String A_TYPE = "type";
	public static final String A_DYNAMIC_VALUE_SQL = "dynamicValueSql";
	
	@jattr(name=A_TYPE, label="Type", type=JPrimitive.STRING_TYPE, length=20, description="Enum UI Type", 
			required=true, unique=false, searchable=false, derived=false, defaultValue="SELECT", validation="JEnumType") 		private String type;
	@jattr(name=A_DYNAMIC_VALUE_SQL, label="Dynamic Value Sql", type=JPrimitive.STRING_TYPE, length=4000, description="Dynamic Value Sql", 
			required=false, unique=false, searchable=false, derived=false) 		private String dynamicValueSql;
	
	private Set<JEnumDetail> values;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDynamicValueSql() {
		return dynamicValueSql;
	}

	public void setDynamicValueSql(String dynamicValueSql) {
		this.dynamicValueSql = dynamicValueSql;
	}

	public Set<JEnumDetail> getValues() {
		return values;
	}

	public void setValues(Set<JEnumDetail> values) {
		this.values = values;
	}

	@Override
	public void validate(String[] values) throws JValidationException {
		if(values==null) {
			return;
		}
		
		for(String value : values) {
			if(value==null) {
				continue;
			}
			boolean findIt = false;
			for(JEnumDetail detail : this.values) {
				if(detail.getValue().equals(value)) {
					findIt = true;
					break;
				}
			}
			
			if(findIt) {
				continue;
			}
			
			throw new JValidationException("Enumeration validation is failed. {0} - {1}", new Object[]{this.values, value});
		}
	}

	@Override
	public void validate(Object value) throws JValidationException {
		if(value instanceof String) {
			validate(new String[]{(String)value});
		} else if(value instanceof Boolean) {
			validate(new String[]{value.toString()});
		} else if(value instanceof JArray) {
			JArray jArray = (JArray)value;
			Set<JArrayDetail> details = jArray.getValues();
			String[] strArr = new String[details.size()];
			int i=0;
			for(JArrayDetail detail : details) {
				strArr[i++] = detail.getValue();
			}
			
			validate(strArr);
		} else {
			throw new JValidationException("Input value is invalid type. - {0}", new Object[]{value});
		}
	}
	
	
}
