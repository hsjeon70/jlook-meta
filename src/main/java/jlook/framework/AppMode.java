package jlook.framework;

public enum AppMode {
	local,
	dev,
	prod,
	test;
}
