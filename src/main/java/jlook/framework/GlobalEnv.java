package jlook.framework;

public enum GlobalEnv {
	H2DB(GlobalEnv.H2DB_HOME),
	HOME(GlobalEnv.APP_HOME),
	MODE(GlobalEnv.APP_MODE),
	SERVER(GlobalEnv.APP_SERVER);
	
	GlobalEnv(String name) {
		this.name = name;
	}
	
	public static final String H2DB_HOME = "h2.baseDir";
	public static final String APP_HOME = "app.home";
	public static final String APP_MODE = "app.mode";
	public static final String APP_SERVER = "app.server";
	
	private String name;
	
	public String getName() {
		return this.name;
	}
	
}
