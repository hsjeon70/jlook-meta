package jlook.framework.service;

import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JValidation;

public interface JValidationService {
	public static final String ID = "jValidationService";
	
	public void create(JValidation jValidation) throws JServiceException;
	public void create(JEnum jEnum) throws JServiceException;
	
	public JValidation findByName(String name) throws JServiceException;
	public void remove(JValidation jValidation) throws JServiceException;
}
