package jlook.framework.service;

import java.util.Set;

import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;


public interface JArrayService {
	
	public static final String ID = "jArrayService";
	
	public Long create(String name, String description, String[] values) throws JServiceException;
	public Long create(JArray jArray) throws JServiceException;
	public void remove(JArray jArray) throws JServiceException;
	public void remove(Long objectId) throws JServiceException;
	public JArray getJArray(Long objectId) throws JServiceException;
	public void update(JArray jArray) throws JServiceException;
	public void removeAll(Set<JArrayDetail> values) throws JServiceException;
 }
