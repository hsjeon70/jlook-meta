package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.model.PreferenceModel;


public interface JPreferenceService {
	public static final String ID = "jPreferenceService";
	
	public List<PreferenceModel> getDomainPreferenceModels(Long domainId) throws JServiceException;
	
	public List<PreferenceModel> getPreferenceModels(JPreferenceType preferenceType) throws JServiceException;
	public List<JPreference> getJPreferences(JPreferenceType preferenceType) throws JServiceException;
	
	public String getPreferenceValue(JPreferenceType preferenceType, String preferenceName) throws JServiceException;
	
	public void create(JPreference jPreference) throws JServiceException;
	/**
	 * Creating API for Preference 
	 * @param name
	 * @param description
	 * @param type
	 * @param defaultValue
	 * @throws JServiceException
	 */
	public void create(String name, String description, JPreferenceType type, String defaultValue) throws JServiceException;
	/**
	 * Create or Update API for PreferenceDetail
	 * @param preferenceId
	 * @param target
	 * @param value
	 * @throws JServiceException
	 */
	public void savePreperfence(Long preferenceId, Long target, String value) throws JServiceException;
	/**
	 * Create or Update API for PreferenceDetail
	 * @param name
	 * @param type
	 * @param target
	 * @param value
	 * @throws JServiceException
	 */
	public void savePreperfence(String name, JPreferenceType type, Long target, String value) throws JServiceException;
	
	public void create(JPreference jPreference, Long target, String value) throws JServiceException;
	
	public void remove(JPreferenceType type, Long objectId) throws JServiceException;
}
