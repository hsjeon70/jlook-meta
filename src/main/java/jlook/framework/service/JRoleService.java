package jlook.framework.service;

import jlook.framework.domain.security.JRole;

public interface JRoleService {
	public static final String ID = "jRoleService";
	
	public void create(JRole role) throws JServiceException;
	public JRole findByName(String name) throws JServiceException;
	
}
