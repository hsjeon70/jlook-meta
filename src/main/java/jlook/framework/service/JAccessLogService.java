package jlook.framework.service;

import jlook.framework.domain.management.JAccessLog;

public interface JAccessLogService {
	public static final String ID = "jAccessLogService";
	
	public void create(JAccessLog jAccessLog) throws JServiceException;
	
}
