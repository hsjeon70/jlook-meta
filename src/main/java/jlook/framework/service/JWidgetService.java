package jlook.framework.service;

import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamName;

public interface JWidgetService {
	public static final String ID = "jWidget";
	
	public void create(JWidgetDefinition jWidgetDefinition) throws JServiceException;
	public void create(JWidgetParamName jWidgetParamName) throws JServiceException;
	
	public JWidgetInstance getWidgetInstance(Long objectId) throws JServiceException;
}
