package jlook.framework.service;

import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JUser;

public interface JAdminService {
	public static final String ID = "jAdminService";
	
	public void register(JDomain jDomain, JUser jUser) throws JServiceException;
	
}
