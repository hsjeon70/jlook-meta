package jlook.framework.service;

import jlook.framework.domain.security.JMenuAccessRule;

public interface JMenuAccessRuleService {
	public static final String ID = "jMenuAccessRuleService";
	
	public void create(JMenuAccessRule jMenuAccessRule) throws JServiceException;
}
