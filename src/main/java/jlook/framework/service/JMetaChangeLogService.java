package jlook.framework.service;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.service.action.JActionType;

public interface JMetaChangeLogService {
	public static final String ID = "jMetaChangeLogService";
	
	public boolean create(JActionType action, JClass before, JClass after) throws JServiceException;
	public boolean create(JActionType action, JAttribute before, JAttribute after) throws JServiceException;
	
	public void remove(JAttribute jAttribute) throws JServiceException;
	public void remove(JClass jClass) throws JServiceException;
}
