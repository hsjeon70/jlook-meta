package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;


public interface JDataAccessRuleService {
	public static final String ID = "jDataAccessRuleService";
	
	public void create(JDataAccessRule jDataAccessRule) throws JServiceException;
	
	public List<JDataAccessRule> getJDataAccessRules(JClass jClass) throws JServiceException;
	
//	public List<JDataAccessRule> getJDataAccessRules(Long jUserId, Long jClassId) throws JServiceException;
}
