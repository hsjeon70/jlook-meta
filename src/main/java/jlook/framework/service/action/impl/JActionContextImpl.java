package jlook.framework.service.action.impl;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JProxy;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.JClassLoader;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.service.JClassService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationException;
import jlook.framework.service.action.JActionContext;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.util.JObjectUtil;

public class JActionContextImpl implements JActionContext {
	protected JUserEntity userEntity;
	protected JClass jClass;
	protected JObject beforeImage;
	protected JGenericService jGenericService;
	protected JClassLoader jClassLoader;
	
	protected JActionContextImpl(JGenericService jGenericService) throws JActionException {
		this.jGenericService = jGenericService;
	}
	
	public void setUserEntity(JUserEntity userEntity) {
		this.userEntity = userEntity;
	}
	
	public void setJClassLoader(JClassLoader jClassLoader) {
		this.jClassLoader = jClassLoader;
	}
	
	protected void setBeforeImage(JObject jObject) throws JActionException {
		if(jObject==null) {
			return; 
		}
		try {
			beforeImage = JObjectUtil.copy(jObject);
		} catch (JValidationException e) {
			throw new JActionException("Fail to copy before image for JObject.", e);
		}
	}
	
	protected void setJClass(JClass jClass) {
		this.jClass = jClass;
	}
	
	@Override
	public JUserEntity getUserEntity() {
		return this.userEntity;
	}
	
	@Override
	public JObject getBeforeImage() {
		return this.beforeImage;
	}

	@Override
	public JClass getJClass() {
		return this.jClass;
	}

	@Override
	public JGenericService getJGenericService() {
		return this.jGenericService;
	}

	@Override
	public Object getBean(String beanName) {
		return ApplicationContextHolder.getBean(beanName);
	}

	@Override
	public JObject newJObject(String className) throws JActionException {
		Class<?> clazz = null;
		try {
			clazz = jClassLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new JActionException("Cannot find the class. - "+className);
		}
		Object obj = null;
		try {
			obj = clazz.newInstance();
		} catch (Exception e) {
			throw new JActionException("Cannot create an object for the class. - "+className);
		}
		if(obj instanceof JObject) {
			return (JObject)obj;
		} else {
			throw new JActionException("Invalid Object. It is not type of JObject. - "+className);
		}
	}

	@Override
	public JProxy toProxy(JObject jObject) throws JActionException {
		JClassService jClassService = (JClassService)this.getBean(JClassService.ID);
		JClass jClass = null;
		try {
			jClass = jClassService.getJClass(jObject.getClassId());
		} catch (JServiceException e) {
			throw new JActionException("Cannot find JClass for this JObject. - "+jObject.getClassId());
		}
		
		JProxy proxy = new JProxyImpl(this, jClass, jObject);
		return proxy;
	}

}
