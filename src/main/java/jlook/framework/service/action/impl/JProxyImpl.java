package jlook.framework.service.action.impl;

import java.sql.Timestamp;
import java.util.Map;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JProxy;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.JException;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.action.JActionContext;
import jlook.framework.service.util.JObjectUtil;

public class JProxyImpl implements JProxy {
	private JObject jObject;
	private JClass jClass;
	private JMetaService jMetaService;
	
	protected JProxyImpl(JActionContext context, JClass jClass, JObject jObject) {
		this.jObject = jObject;
		this.jClass = jClass;
		this.jMetaService = (JMetaService)context.getBean(JMetaService.ID);
	}
	
	@Override
	public void set(String attributeName, Object attributeValue)
			throws JServiceException {
		Map<String,JAttribute> attMap = this.jClass.getJAttributes();
		if(!attMap.containsKey(attributeName)) {
			throw new JServiceException("Wrong attribute name. Cannot find attribute. - "+attributeName);
		}
		JAttribute jattr = attMap.get(attributeName);
		JType type = jattr.getType();
		Class<?> attrClass = null;
		if(type.isPrimitive()) {
			attrClass = JObjectUtil.getPrimitiveClass(type);
		} else {
			JClass jClassForAttr = type.getJClass();
			attrClass = this.jMetaService.getEntityClass(jClassForAttr);
		}
		try {
			JObjectUtil.setValue(this.jObject, jattr, attrClass, attributeValue);
		} catch (JException e) {
			throw new JServiceException("Fail to set value at attribute. - "+e.getMessage(), e);
		}
	}

	@Override
	public Object get(String attributeName) throws JServiceException {
		Map<String,JAttribute> attMap = this.jClass.getJAttributes();
		if(!attMap.containsKey(attributeName)) {
			throw new JServiceException("Wrong attribute name. Cannot find attribute. - "+attributeName);
		}
		JAttribute jattr = attMap.get(attributeName);
		try {
			return JObjectUtil.getValue(this.jObject, jattr);
		} catch (JException e) {
			throw new JServiceException("Fail to get attribute value. - "+e.getMessage(), e);
		}
	}

	@Override
	public void setObjectId(Long objectId) {
		this.jObject.setObjectId(objectId);
	}

	@Override
	public Long getObjectId() {
		return this.jObject.getObjectId();
	}

	@Override
	public void setClassId(Long classId) {
		this.jObject.setClassId(classId);
	}

	@Override
	public Long getClassId() {
		return this.jObject.getClassId();
	}

	@Override
	public void setDomainId(Long domainId) {
		this.jObject.setDomainId(domainId);
	}

	@Override
	public Long getDomainId() {
		return this.jObject.getDomainId();
	}

	@Override
	public void setCreatedBy(Long createdBy) {
		this.jObject.setCreatedBy(createdBy);
	}

	@Override
	public Long getCreatedBy() {
		return this.jObject.getCreatedBy();
	}

	@Override
	public void setCreatedOn(Timestamp createdOn) {
		this.jObject.setCreatedOn(createdOn);
	}

	@Override
	public Timestamp getCreatedOn() {
		return this.jObject.getCreatedOn();
	}

	@Override
	public void setUpdatedBy(Long updatedBy) {
		this.jObject.setUpdatedBy(updatedBy);
	}

	@Override
	public Long getUpdatedBy() {
		return this.jObject.getUpdatedBy();
	}

	@Override
	public void setUpdatedOn(Timestamp updatedOn) {
		this.jObject.setUpdatedOn(updatedOn);
	}

	@Override
	public Timestamp getUpdatedOn() {
		return this.jObject.getUpdatedOn();
	}

}
