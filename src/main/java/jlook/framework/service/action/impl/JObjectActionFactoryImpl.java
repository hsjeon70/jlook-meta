package jlook.framework.service.action.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.JClassLoader;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JObjectAction;
import jlook.framework.service.action.JObjectActionFactory;

@Component(JObjectActionFactory.ID)
public class JObjectActionFactoryImpl implements JObjectActionFactory {
	private static Log logger = Log.getLog(JObjectActionFactoryImpl.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JClassLoader.ID)
	private JClassLoader jClassLoader;
	
	@Override
	public JObjectAction newJObjectAction(JGenericService jGenericService, JClass jClass, JObject beforeImage)
			throws JActionException {
		String postActionClass = jClass.getActionClass();
		if(postActionClass==null || postActionClass.equals("")) {
			return null;
		}
		Class<?> clazz = null;
		try {
			clazz = Class.forName(postActionClass);
		} catch (ClassNotFoundException e) {
			throw new JActionException("Cannot find Post Action Class. - "+postActionClass,e);
		}
		Object obj = null;
		try {
			obj = clazz.newInstance();
		} catch (Exception e) {
			throw new JActionException("Cannot create instance for PostActionClass. - "+postActionClass,e);
		}
		JUserEntity entity = null;
		try {
			entity = security.getUserEntity();
		} catch (JSecurityException e) {
			if(logger.isWarnEnabled()) {
				logger.warn("Fail to get JUserEntity. - "+e.getMessage(), e);
			}
		}
		if(obj instanceof JObjectAction) {
			JObjectAction action =  (JObjectAction)obj;
			JActionContextImpl context = new JActionContextImpl(jGenericService);
			context.setBeforeImage(beforeImage);
			context.setJClass(jClass);
			context.setUserEntity(entity);
			context.setJClassLoader(jClassLoader);
			action.setJActionContext(context);
			return action;
		}
		throw new JActionException("Invalid PostActionClass object. - "+obj);
	}

}
