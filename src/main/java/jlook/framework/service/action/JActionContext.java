package jlook.framework.service.action;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JProxy;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.service.JGenericService;

public interface JActionContext {
	
	public JUserEntity getUserEntity();
	public JObject getBeforeImage();
	public JClass getJClass();
	public JGenericService getJGenericService();
	public Object getBean(String beanName);
	public JProxy toProxy(JObject jObject)  throws JActionException;
	
	public JObject newJObject(String className) throws JActionException;
}
