package jlook.framework.service.action;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.annotation.Message;

@Message(text=JActionException.TEXT)
public class JActionException extends JException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Action Error";
	
	public JActionException(String message) {
		super(message);
	}
	public JActionException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JActionException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JActionException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JActionException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JActionException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
