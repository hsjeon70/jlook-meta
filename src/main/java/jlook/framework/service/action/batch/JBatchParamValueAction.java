package jlook.framework.service.action.batch;

import jlook.framework.domain.JObject;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.infrastructure.batch.impl.JBatchManager;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JObjectActionSupport;

public class JBatchParamValueAction extends JObjectActionSupport {
	public static final String CLASS_NAME = "jlook.framework.service.action.batch.JBatchParamValueAction";
	
	@Override
	public void doPostCreate(JObject jObject) throws JActionException {
		doAction(jObject);
	}

	@Override
	public void doPostUpdate(JObject jObject) throws JActionException {
		doAction(jObject);
	}

	@Override
	public void doPostDelete(JObject jObject) throws JActionException {
		doAction(jObject);
	}
	
	public void doAction(JObject jObject) throws JActionException {
		if(jObject==null) {
			return;
		}
		
		JBatchParamValue jBatchParamValue = (JBatchParamValue)jObject;
		JBatchInstance jBatch = jBatchParamValue.getJBatchInstance();
		JBatchManager manager = (JBatchManager)context.getBean(JBatchManager.ID);
		manager.updateJBatch(jBatch);
	}
}
