package jlook.framework.service.action.batch;

import jlook.framework.domain.JObject;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.infrastructure.batch.impl.JBatchManager;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JObjectActionSupport;

public class JBatchInstanceAction extends JObjectActionSupport {
	public static final String CLASS_NAME = "jlook.framework.service.action.batch.JBatchInstanceAction";
	
	@Override
	public void doPostCreate(JObject jObject) throws JActionException {
		if(jObject==null) {
			return;
		}
		JBatchInstance jBatch = (JBatchInstance)jObject;
		if(jBatch.isActive()) {
			JBatchManager manager = (JBatchManager)context.getBean(JBatchManager.ID);
			manager.addJBatch(jBatch);
		}
	}

	@Override
	public void doPostUpdate(JObject jObject) throws JActionException {
		JBatchManager manager = (JBatchManager)context.getBean(JBatchManager.ID);
		JBatchInstance jBatch = (JBatchInstance)jObject;
		JBatchInstance before = (JBatchInstance)context.getBeforeImage();
		if(before.isActive() != jBatch.isActive()) {
			if(jBatch.isActive()) {
				manager.addJBatch(jBatch);
			} else {
				manager.removeJBatch(jBatch);
			}
		} else if(jBatch.isActive()) {
			manager.updateJBatch(jBatch);
		}
	}

	@Override
	public void doPostDelete(JObject jObject) throws JActionException {
		if(jObject==null) {
			return;
		}
		JBatchInstance jBatch = (JBatchInstance)jObject;
		if(jBatch.isActive()) {
			JBatchManager manager = (JBatchManager)context.getBean(JBatchManager.ID);
			manager.removeJBatch(jBatch);
		}
	}
	
}
