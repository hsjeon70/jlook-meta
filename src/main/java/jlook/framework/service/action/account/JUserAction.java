package jlook.framework.service.action.account;

import java.util.List;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JObjectActionSupport;

public class JUserAction extends JObjectActionSupport {
	public static final String CLASS_NAME = "jlook.framework.service.action.account.JUserAction";
	
	@Override
	public void doPostCreate(JObject jObject) throws JActionException {
		JPreferenceService jpService = (JPreferenceService)this.context.getBean(JPreferenceService.ID);
		List<JPreference> preferences = null;
		try {
			preferences = jpService.getJPreferences(JPreferenceType.USER);
		} catch (JServiceException e) {
			throw new JActionException("Fail to get JPreference List for user.",e);
		}
		
		for(JPreference jpref : preferences) {
			try {
				jpService.create(jpref, jObject.getObjectId(), "");
			} catch (JServiceException e) {
				throw new JActionException("Fail to create Preference. - "+jpref,e);
			}
		}
		
	}
	
	/**
	 * Remove User PreferenceDetail.
	 */
	@Override
	public void doPreDelete(JObject jObject) throws JActionException {
		JPreferenceService jpService = (JPreferenceService)this.context.getBean(JPreferenceService.ID);
		try {
			jpService.remove(JPreferenceType.USER, jObject.getObjectId());
		} catch (JServiceException e) {
			throw new JActionException("Fail to remove domain preferences - "+jObject.getObjectId(), e);
		}
	}
}
