package jlook.framework.service.action;

import jlook.framework.domain.JObject;

public interface JObjectAction {
	
	public void setJActionContext(JActionContext context);
	public void doPreExecute(JActionType type, JObject jObject) throws JActionException;
	public void doPostExecute(JActionType type, JObject jObject) throws JActionException;
	
}
