package jlook.framework.service.action;

public enum JActionType {
	create,
	update,
	delete,
	select;
	
}
