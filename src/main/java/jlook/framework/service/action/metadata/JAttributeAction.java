package jlook.framework.service.action.metadata;

import java.util.Set;

import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.service.JAttributeService;
import jlook.framework.service.JClassService;
import jlook.framework.service.JMetaChangeLogService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JViewService;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JActionType;
import jlook.framework.service.action.JObjectActionSupport;

public class JAttributeAction extends JObjectActionSupport {
	public static final String CLASS_NAME = "jlook.framework.service.action.metadata.JAttributeAction";
	
	@Override
	public void doPostUpdate(JObject jObject) throws JActionException {
		JAttribute jatt = (JAttribute)jObject;
		JClass jClass = jatt.getJClass();
		String status = jClass.getStatus();
		if(JMetaStatus.NONE_TYPE.equals(status)||JMetaStatus.MAPERROR_TYPE.equals(status)) {
			return;
		}
		
		JAttribute before = (JAttribute)context.getBeforeImage();
		JAttribute after = (JAttribute)jObject;
		
		if(JMetaStatus.NONE_TYPE.equals(jatt.getStatus())||JMetaStatus.MAPERROR_TYPE.equals(jatt.getStatus())) {
			return;
		}
		
		/**
		 * if searchable is changed, add or remove attribute mapping at Summary ViewDetail
		 */
		if(before.isSearchable()!=after.isSearchable()) {
			updateSummaryView(jClass, after);
		}
		
		// change log
		boolean changed = false;
		if(before!=null) {
			JMetaChangeLogService logService = (JMetaChangeLogService)context.getBean(JMetaChangeLogService.ID);
			try {
				changed = logService.create(JActionType.update, before, after);
			} catch (JServiceException e) {
				throw new JActionException("Fail to create JMetaChangeLog. - "+e.getMessage(),e);
			}
		}
		
		if(changed) {
			updateJClassStatus(jClass, JMetaStatus.CHANGED);
			updateJAttributeStatus(after, JMetaStatus.CHANGED);
		}
	}
	
	@Override
	public void doPostCreate(JObject jObject) throws JActionException {
		JAttribute jatt = (JAttribute)jObject;
		JClass jClass = jatt.getJClass();
		String status = jClass.getStatus();
		if(JMetaStatus.NONE_TYPE.equals(status)||JMetaStatus.MAPERROR_TYPE.equals(status)) {
			return;
		}
		
		JMetaChangeLogService logService = (JMetaChangeLogService)context.getBean(JMetaChangeLogService.ID);
		boolean changed = false;
		try {
			changed = logService.create(JActionType.create, null, jatt);
		} catch (JServiceException e) {
			throw new JActionException("Fail to create JMetaChangeLog. - "+e.getMessage(),e);
		}
		
		if(changed && JMetaStatus.CREATED_TYPE.equals(status)) {
			updateJClassStatus(jClass, JMetaStatus.CHANGED);
//			updateJAttributeStatus(jatt, JMetaStatus.NONE);
		}
	}
	
	@Override
	public void doPreDelete(JObject jObject) throws JActionException {
		JAttribute jatt = (JAttribute)jObject;
		JViewService jViewService = (JViewService)context.getBean(JViewService.ID);
		try {
			jViewService.remove(jatt);
		} catch (JServiceException e) {
			throw new JActionException("Fail to remove JViewDetail - "+jatt, e);
		}
		
	}
	
	@Override
	public void doPostDelete(JObject jObject) throws JActionException {
		JAttribute jatt = (JAttribute)jObject;
		JClass jClass = jatt.getJClass();
		String status = jClass.getStatus();
		if(JMetaStatus.NONE_TYPE.equals(status)||JMetaStatus.MAPERROR_TYPE.equals(status)) {
			return;
		}
		
		JMetaChangeLogService logService = (JMetaChangeLogService)context.getBean(JMetaChangeLogService.ID);
		try {
			logService.remove(jatt);
		} catch (JServiceException e) {
			throw new JActionException("Fail to create JMetaChangeLog. - "+e.getMessage(),e);
		}
	}
	
	private void updateJClassStatus(JClass jClass, JMetaStatus status) throws JActionException {
		jClass.setStatus(status.name());
		JClassService jClassService = (JClassService)context.getBean(JClassService.ID);
		try {
			jClassService.update(jClass);
		} catch (JServiceException e) {
			throw new JActionException("Fail to update JClass.", e);
		}
	}
	
	private void updateJAttributeStatus(JAttribute jAttr, JMetaStatus status) throws JActionException {
		jAttr.setStatus(status.name());
		JAttributeService jAttrService = (JAttributeService)context.getBean(JAttributeService.ID);
		try {
			jAttrService.update(jAttr);
		} catch (JServiceException e) {
			throw new JActionException("Fail to update JClass.", e);
		}
	}
	
	private void updateSummaryView(JClass jClass, JAttribute jattr) throws JActionException {
		JViewService jViewService = (JViewService)context.getBean(JViewService.ID);
		JView jView = null;
		try {
			jView = jViewService.getSystemJView(jClass, JViewType.summary);
		} catch (JServiceException e) {
			throw new JActionException("Fail to get system JView. - "+jClass, e);
		}
		
		if(jattr.isSearchable()) {	// add JViewDetail
			JViewDetail detail = new JViewDetail();
			detail.setJView(jView);
			detail.setJAttribute(jattr);
			detail.setLabel(jattr.getLabel());
			detail.setLength(jattr.getLength());
			detail.setReadOnly(false);
			detail.setSequence(30);
			try {
				jViewService.create(detail);
			} catch (JServiceException e) {
				throw new JActionException("Fail to create JViewDetail - "+detail,e);
			}
		} else {	// remove JViewDetail
			Set<JViewDetail> details = jView.getDetails();
			for(JViewDetail detail : details) {
				JAttribute tmp = detail.getJAttribute();
				if(jattr.getObjectId().equals(tmp.getObjectId())) {
					try {
						jViewService.remove(detail);
					} catch (JServiceException e) {
						throw new JActionException("Fail to remove JViewDetail - "+detail,e);
					}
				}
			}
		}
	}
}
