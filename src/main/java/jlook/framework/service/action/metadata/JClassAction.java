package jlook.framework.service.action.metadata;

import java.util.List;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassMapType;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JMetaChangeLogService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JTypeService;
import jlook.framework.service.JViewService;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JActionType;
import jlook.framework.service.action.JObjectActionSupport;

public class JClassAction extends JObjectActionSupport {
	private static Log logger = Log.getLog(JClassAction.class);
	
	public static final String CLASS_NAME = "jlook.framework.service.action.metadata.JClassAction";
	
	@Override
	public void doPostCreate(JObject jObject) throws JActionException {
		JClass jClass = (JClass)jObject;
		JType jType = new JType();
		jType.setClassId(JMetaKeys.JTYPE);
		jType.setJClass(jClass);
		jType.setName(jClass.getName());
		jType.setDescription(jClass.getDescription());
		
		JTypeService jTypeService = (JTypeService)context.getBean(JTypeService.ID);
		try {
			if(!jTypeService.exists(jClass)) {
				jTypeService.create(jType);
			}
		} catch (JServiceException e) {
			throw new JActionException("Cannot create JType for JClass. - "+jClass.getName(), e);
		}
	}

	@Override
	public void doPostUpdate(JObject jObject) throws JActionException {
		JClass jClass = (JClass)jObject;
		
		if(logger.isDebugEnabled()) {
			logger.debug("MapAction --> "+jClass.getMapAction());
		}
		
		JMetaStatus status = null;
		try {
			status = JMetaStatus.valueOf(jClass.getStatus());
		} catch(Exception e) {
			throw new JActionException("Invalid meta status for JClass. - "+jClass.getStatus(),e);
		}
		
		// todo : authorization
		if(jClass.getDomainId().equals(JDomainDef.SYSTEM_ID) ) {
			
		}
				
		JMetaService jMetaService = (JMetaService)context.getBean(JMetaService.ID);
		JMetaChangeLogService logService = (JMetaChangeLogService)context.getBean(JMetaChangeLogService.ID);
		if( status.equals(JMetaStatus.NONE) || status.equals(JMetaStatus.MAPERROR) || status.equals(JMetaStatus.UNMAPPED)) {
			if(JClassMapType.MAPPING.equals(jClass.getMapAction())) {
				try {
					jMetaService.mapEntity(jClass.getObjectId());
				} catch (JServiceException e) {
					throw new JActionException("Fail to map entity("+jClass.getObjectId()+"). - "+e.getMessage(),e);
				} 
			}
			return;
		}
		
		JClass before = (JClass)context.getBeforeImage();
		boolean changed = false;
		try {
			if( status.equals(JMetaStatus.CREATED) || status.equals(JMetaStatus.CHANGED) ) {
				// change log
				changed = logService.create(JActionType.update, before, jClass);
			}
		} catch (JServiceException e) {
			throw new JActionException("Fail to create JMetaChangeLog. - "+e.getMessage(),e);
		}
		
		if(!jClass.getName().equals(before.getName())) {
			JTypeService jTypeService = (JTypeService)this.context.getBean(JTypeService.ID);
			try {
				JType jType = jTypeService.findByJClass(jClass);
				jType.setName(jClass.getName());
				jTypeService.update(jType);
			} catch (JServiceException e) {
				throw new JActionException("Fail to update JType. entity("+jClass.getObjectId()+")- "+e.getMessage(), e);
			}
			
			JViewService jViewService = (JViewService)this.context.getBean(JViewService.ID);
			try {
				List<JView> views = jViewService.getAllJViewList(jClass);
				for(JView jView : views) {
					String viewName = null;
					if(jView.getType().equals(JViewType.TITLE)) {
						viewName = JViewType.title.getSystemViewName(jClass);
					} else if(jView.getType().equals(JViewType.SUMMARY)) {
						viewName = JViewType.summary.getSystemViewName(jClass);
					} else if(jView.getType().equals(JViewType.DETAIL)) {
						viewName = JViewType.detail.getSystemViewName(jClass);
					}
					
					if(viewName==null) continue;
					
					jView.setName(viewName);
					jViewService.update(jView);
				}
			} catch (JServiceException e) {
				throw new JActionException("Fail to update JView. entity("+jClass.getObjectId()+")- "+e.getMessage(), e);
			}
			
		}
		
		if( status.equals(JMetaStatus.CREATED) || status.equals(JMetaStatus.CHANGED) ) {
			if(JClassMapType.UNMAPPING.equals(jClass.getMapAction())) {
				try {
					logService.remove(jClass);
					jMetaService.unmapEntity(jClass.getObjectId());
				} catch (JServiceException e) {
					throw new JActionException("Fail to unmap entity("+jClass.getObjectId()+"). - "+e.getMessage(),e);
				}
			} else if(changed){
				jClass.setStatus(JMetaStatus.CHANGED_TYPE);
			}
		} else if( status.equals(JMetaStatus.UNMAPPED) && JClassMapType.REMAPPING.equals(jClass.getMapAction()) ) {
			jClass.setStatus(JMetaStatus.REMAPPED_TYPE);
		}
	}

	@Override
	public void doPreDelete(JObject jObject) throws JActionException {
		JClass jClass = (JClass)jObject;
		JTypeService jTypeService = (JTypeService)context.getBean(JTypeService.ID);
		try {
			if(jTypeService.exists(jClass)) {
				JType jType = jTypeService.findByJClass(jClass);
				jTypeService.remove(jType);
			}
		} catch (JServiceException e) {
			throw new JActionException("Cannot create JType for JClass. - "+jClass.getName(), e);
		}
	}
	
	@Override
	public void doPostSelect(JObject jObject) throws JActionException {
		JClass jClass = (JClass)jObject;
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> "+jClass.getObjectId());
			logger.debug("\t--> "+jClass.getClassId());
			logger.debug("\t--> "+jClass.getDomainId());
		}
	}

}
