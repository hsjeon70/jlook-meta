package jlook.framework.service.action;

import jlook.framework.domain.JObject;

public abstract class JObjectActionSupport implements JObjectAction {
	protected JActionContext context;
	
	@Override
	public void setJActionContext(JActionContext context) {
		this.context = context;
	}
	
	@Override
	final public void doPreExecute(JActionType type, JObject jObject) throws JActionException {
		switch(type) {
			case create : doPreCreate(jObject);
				break;
			case delete : doPreDelete(jObject);
				break;
			case update : doPreUpdate(jObject);
				break;
		}
	}
	
	@Override
	final public void doPostExecute(JActionType type, JObject jObject) throws JActionException {
		switch(type) {
			case create : doPostCreate(jObject);
				break;
			case delete : doPostDelete(jObject);
				break;
			case update : doPostUpdate(jObject);
				break;
			case select : doPostSelect(jObject);
		}
	}
	
	public void doPreCreate(JObject jObject) throws JActionException {}
	public void doPreUpdate(JObject jObject) throws JActionException {}
	public void doPreDelete(JObject jObject) throws JActionException {}
	
	public void doPostCreate(JObject jObject) throws JActionException {}
	public void doPostUpdate(JObject jObject) throws JActionException {}
	public void doPostDelete(JObject jObject) throws JActionException {}
	public void doPostSelect(JObject jObject) throws JActionException {}
}
