package jlook.framework.service.action;

import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.service.JGenericService;

public interface JObjectActionFactory {
	public static final String ID = "jObjectPostActionFactory";
	
	public JObjectAction newJObjectAction(JGenericService jGenericService, JClass jClass, JObject beforeImage) throws JActionException;
}
