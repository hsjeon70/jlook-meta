package jlook.framework.service;

import jlook.framework.domain.batch.JBatchHistory;

public interface JBatchHistoryService {
	
	public static final String ID = "jBatchHistoryService";
	
	public void create(JBatchHistory jBatchHistory) throws JServiceException;
	public void update(JBatchHistory jBatchHistory) throws JServiceException;
}
