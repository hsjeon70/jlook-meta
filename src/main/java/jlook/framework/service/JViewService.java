package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.metadata.JViewType;


public interface JViewService {

	public static final String ID = "jViewService";
	
	public void create(JView jView) throws JServiceException;
	public void create(JViewDetail detail) throws JServiceException;
	public void create(String name, String description, JClass jClass, JViewType type, List<JAttribute> jattList) throws JServiceException;
	
	public void update(JView jView) throws JServiceException;
	
	public JView getSystemJView(JClass jClass, JViewType type) throws JServiceException;
	public JView getJView(JClass jClass, String viewName) throws JServiceException;
	public JView getJView(Long jViewId) throws JServiceException;
	
	public void remove(JView jView) throws JServiceException;
	public void remove(JViewDetail detail) throws JServiceException;
	public void remove(JAttribute jAttribute) throws JServiceException;
	
	public List<JView> getAllJViewList(JClass jClass) throws JServiceException;
	public List<JViewDetail> getJViewDetailList(JAttribute jAttribute) throws JServiceException;
}
