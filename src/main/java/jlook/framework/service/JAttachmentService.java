package jlook.framework.service;

import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;

public interface JAttachmentService {
	public static final String ID = "jAttachmentService";
	
	public JResultModel create(JAttachment attachment) throws JServiceException;
	public JResultModel update(JAttachment attachment) throws JServiceException;
	public JResultModel create(Long objectId, String subject, String content, JAttachmentDetail detail) throws JServiceException; 
	public JAttachment getJAttachment(Long objectId) throws JServiceException;
	public JAttachmentDetail getJAttachmentDetail(Long objectId) throws JServiceException;
	public void remove(JAttachment attachment) throws JServiceException;
	public void remove(Long attachmentId) throws JServiceException;
	public void remove(Long attachmentId, Long detailId) throws JServiceException;
	public void removeDetail(Long detailId) throws JServiceException;
	
}
