package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;

public interface JClassActionService {
	public static final String ID = "jClassActionService";
	
	public void create(JClassAction jClassAction) throws JServiceException;
	public List<JClassAction> findByJClass(JClass jClass, JClassActionType type) throws JServiceException;
}
