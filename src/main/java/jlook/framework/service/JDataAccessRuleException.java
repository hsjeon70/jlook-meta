package jlook.framework.service;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JDataAccessRuleException.TEXT)
public class JDataAccessRuleException extends JServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TEXT = "Invalid Data Access. {0} - {1}";
	
	public JDataAccessRuleException(Object[] parameters) {
		super(TEXT,parameters);
	}
	
	public JDataAccessRuleException(String msg, Object[] parameters) {
		super(msg, parameters);
	}
	
	public JDataAccessRuleException(Throwable cause, Object... parameters) {
		super(cause, parameters);
	}
	
	public JDataAccessRuleException(String msg, Throwable cause, Object[] parameters) {
		super(msg, cause, parameters);
	}
}
