package jlook.framework.service;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JObjectNotFoundException.TEXT)
public class JObjectNotFoundException extends JServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TEXT = "Cannot find {0}. - {1}";
	
	public JObjectNotFoundException(Object[] parameters) {
		super(TEXT,parameters);
	}
	
	public JObjectNotFoundException(String msg, Object[] parameters) {
		super(msg, parameters);
	}
	
	public JObjectNotFoundException(Throwable cause, Object... parameters) {
		super(cause, parameters);
	}
	
	public JObjectNotFoundException(String msg, Throwable cause, Object[] parameters) {
		super(msg, cause, parameters);
	}
}
