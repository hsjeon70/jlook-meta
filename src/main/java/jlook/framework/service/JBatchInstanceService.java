package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamValue;

public interface JBatchInstanceService {
	public static final String ID = "jBatchInstanceService";
	
	public List<JBatchInstance> getJBatchInstances() throws JServiceException;
	
	public void create(JBatchInstance jBatchInstance) throws JServiceException;
	public void update(JBatchInstance jBatchInstance) throws JServiceException;
	
	public void create(JBatchParamValue jBatchParamValue) throws JServiceException;
}
