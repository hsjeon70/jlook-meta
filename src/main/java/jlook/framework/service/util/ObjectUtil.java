package jlook.framework.service.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import jlook.framework.infrastructure.JException;

public class ObjectUtil {
	
	public static Object getValue(Object obj, String propName) throws JException {
		Class<?> clazz = obj.getClass();
		Method method = null;
		String methodName = "get"+propName.substring(0,1).toUpperCase()+propName.substring(1);
		try {
			method = clazz.getMethod(methodName, new Class[0]);
		} catch (SecurityException e) {
			throw new JException("Fail to get method. - "+methodName, e);
		} catch (NoSuchMethodException e) {
			methodName = "is"+propName.substring(0,1).toUpperCase()+propName.substring(1);
			try {
				method = clazz.getMethod(methodName, new Class[0]);
			} catch (SecurityException e1) {
				throw new JException("Fail to get method. - "+methodName, e);
			} catch (NoSuchMethodException e1) {
				throw new JException("Cannot find method name for the property. - "+propName);
			}
		}
		
		try {
			return method.invoke(obj, new Object[0]);
		} catch (Exception e) {
			throw new JException("Fail to invoke the method.",e);
		}
	}
}
