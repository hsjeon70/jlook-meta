package jlook.framework.service.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Currency;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.model.ArrayModel;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JValidationException;


public abstract class JObjectUtil {
	private static Log logger = Log.getLog(JObjectUtil.class);
	
	public static String getClassName(JClass jClass) {
		String className = null;
		String category = jClass.getCategory();
		String name 	= jClass.getName();
		Long domainId 	= jClass.getDomainId();
		if(JDomainDef.SYSTEM_ID == domainId.longValue()) {
			className = category==null ? name : category+"."+name;
		} else {
			className = category==null ? "d"+domainId+"."+name : "d"+domainId+"."+category+"."+name;
		}
		return className;
	}
	
	public static String getConfigPath(JClass jClass) {
		String name 	= jClass.getName();
		Long domainId = jClass.getDomainId();
		return "d"+domainId+"/"+name;
	}
	
	public static Object getValue(JObject jObject, JAttribute jattr) throws JException {
		Class<?> clazz = jObject.getClass();
		JType type = jattr.getType();
		
		String attrName = jattr.getName();
		try {
			Method method = null;
			if(type.isPrimitive() && JPrimitive.BOOLEAN.getName().equals(type.getName())) {
				method = clazz.getMethod("is"+Character.toUpperCase(attrName.charAt(0))+attrName.substring(1), new Class[]{});
			} else {
				method = clazz.getMethod("get"+Character.toUpperCase(attrName.charAt(0))+attrName.substring(1), new Class[]{});
			}
			return method.invoke(jObject, new Object[]{});
		} catch (SecurityException e) {
			throw new JException("Security Error - "+e.getMessage(),e);
		} catch (NoSuchMethodException e) {
			throw new JException("Not Found Method - "+e.getMessage(),e);
		} catch(Exception e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		}
	}
	
	/**
	 * Primitive type
	 * 
	 * @param jatt
	 * @param value
	 * @return
	 * @throws JException
	 */
	public static Object getValue(JAttribute jatt, String value) throws JException {
		if(value==null) {
			return null;
		}
		JType type = jatt.getType();
		Object data = getPrimitiveValue(type, value);
		
		return data;
	}
	
	/**
	 * Primitive type
	 * @param jObject
	 * @param jattr
	 * @param values
	 * @throws JException
	 */
	public static void setValue(JObject jObject, JAttribute jattr, Class<?> attrClass, Object value) throws JException {
		if(jObject==null) {
			return;
		}
		
		JType type = jattr.getType();
		if(!type.isPrimitive()) {
			if(value==null || value instanceof JObject) {
				JObjectUtil.setValue(jObject, jattr, attrClass, (JObject)value);
				return;
			} else {
				throw new JValidationException("Invalid Reference type. - {0}",new Object[]{value});
			}
		}
		Class<?> clazz = jObject.getClass();
		String attrName = jattr.getName();
		String methodName = "set"+Character.toUpperCase(attrName.charAt(0))+attrName.substring(1);
		try {
			Method method = clazz.getMethod(methodName, new Class[]{attrClass});
			method.invoke(jObject, new Object[]{value});
		} catch (SecurityException e) {
			throw new JException("Security Error - "+e.getMessage(),e);
		} catch (NoSuchMethodException e) {
			throw new JException("Not Found Method - "+e.getMessage(),e);
		} catch (IllegalArgumentException e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		} catch (IllegalAccessException e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		} catch (InvocationTargetException e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		} 
	}
	
	/**
	 * Reference and Primitive type
	 * @param jObject
	 * @param jattr
	 * @param objectId
	 * @throws JException
	 */
	private static void setValue(JObject jObject, JAttribute jattr, Class<?> attrClass, JObject value) throws JException {
		if(jObject==null) {
			return;
		}
		Class<?> clazz = jObject.getClass();
		String attrName = jattr.getName();
		String methodName = "set"+Character.toUpperCase(attrName.charAt(0))+attrName.substring(1);
		Method method = null;
		try {
			method = clazz.getMethod(methodName, new Class[]{attrClass});
			method.invoke(jObject, new Object[]{value});
		} catch (SecurityException e) {
			throw new JException("Security Error - "+e.getMessage(),e);
		} catch (NoSuchMethodException e) {
			throw new JException("Not Found Method("+methodName+"-"+attrClass.getName()+") - "+e.getMessage(),e);
		} catch (IllegalArgumentException e) {
			throw new JException("Fail to invoke Method("+methodName+"-"+attrClass.getName()+") - "+e.getMessage(),e);
		} catch (IllegalAccessException e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		} catch (InvocationTargetException e) {
			throw new JException("Fail to invoke Method - "+e.getMessage(),e);
		}
	}
	
	public static void checkLength(JAttribute jattr, Object value) throws JValidationException {
		if(value == null) return;
		Integer length = jattr.getLength();
		if(length==null) return;
		
		JType type = jattr.getType();
		if(type.isPrimitive()) {
			String strValue = value.toString();
			if(jattr.getEncrypt()==null && strValue.length() > length) {
				throw new JValidationException("Attribute({0}) length is too long.({1}) - {2}", new Object[]{jattr.getName(), length, strValue});
			}
		}
	}
	
	public static void checkLength(JAttribute jattr, String[] values) throws JValidationException {
		Integer length = jattr.getLength();
		if(length==null) return;
		if(values==null || values.length==0) return;
		
		for(String value : values) {
			if(value==null) {
				continue;
			}
			if(value.length() > length) {
				throw new JValidationException("Attribute({0}) length is too long.({1})", new Object[]{jattr.getName(), value.length()});
			}
		}
	}
	
	// primitive type
	public static Class<?> getPrimitiveClass(JType type) throws JValidationException {
		JPrimitive p = JPrimitive.getPrimitive(type.getName());
		return p.getJavaClass();
	}
	
	public static Object getPrimitiveValue(JType type, Object value) throws JValidationException {
		if(value==null) {
			return null;
		}
		try {
			JPrimitive p = JPrimitive.getPrimitive(type.getName());
			switch(p) {
				case DATE		: return (value instanceof Date) ? DateUtil.formatDate((Date)value) : value.toString();
				case DATETIME	: return (value instanceof Timestamp) ? DateUtil.formatDateTime((Timestamp)value) : value.toString();
				case TIMESTAMP	: return (value instanceof Timestamp) ? DateUtil.formatDateTime((Timestamp)value) : value.toString();
				case BOOLEAN 	: 
				case TIME		: 
				case STRING 	: 
				case INTEGER	:
				case LONG		:
				case FLOAT		:
				case DOUBLE		:
				case CHAR		:
				case TIMEZONE	:
				case CURRENCY	:
				default			: return value;
			}
		} catch(Exception e) {
			throw new JValidationException("Cannot convert vlaue to attribute value. {0} - {1}", e, new Object[]{type.getName(), value});
		}
	}
	
	// primitive type
	public static Object getPrimitiveValue(JType type, String value) throws JValidationException {
		if(value==null) {
			return null;
		}
		try {
			JPrimitive p = JPrimitive.getPrimitive(type.getName());
			switch(p) {
				case STRING 	: return value;
				case BOOLEAN 	: return value==null||value.equals("") ? (Boolean)null 		: new Boolean(value);
				case INTEGER	: return value==null||value.equals("") ? (Integer)null 		: new Integer(value);
				case LONG		: return value==null||value.equals("") ? (Long)null 		: new Long(value);
				case FLOAT		: return value==null||value.equals("") ? (Float)null 		: new Float(value);
				case DOUBLE		: return value==null||value.equals("") ? (Double)null 		: new Double(value);
				case CHAR		: return value==null||value.equals("") ? (Character)null 	: new Character(value.charAt(0));
				case DATE		: return value==null||value.equals("") ? (Date)null 		: DateUtil.parseDate(value);
				case TIME		: return value==null||value.equals("") ? (Time)null 		: DateUtil.parseTime(value);
				case DATETIME	: return value==null||value.equals("") ? (Timestamp)null 	: DateUtil.parseDateTime(value);
				case TIMESTAMP	: return value==null||value.equals("") ? (Timestamp)null 	: DateUtil.parseDateTime(value);
				case TIMEZONE	: return value==null||value.equals("") ? (TimeZone)null 	: TimeZone.getTimeZone(value);
				case CURRENCY	: return value==null||value.equals("") ? (Currency)null 	: Currency.getInstance(value);
				default			: return value;
			}
		} catch(Exception e) {
			throw new JValidationException("Cannot convert vlaue to attribute value. {0} - {1}", e, new Object[]{type.getName(), value});
		}
	}
	
	// reference type
	public static JObject getReferenceValue(Class<?> target, Long objectId) throws JValidationException {
		Object obj;
		try {
			obj = target.newInstance();
			if(obj instanceof JObject) {
				JObject result = (JObject)obj;
				result.setObjectId(objectId);
				return result;
			} else {
				throw new JValidationException("Invalid parameter type. - {0}", new Object[]{target.getName()});
			}
		} catch (InstantiationException e) {
			throw new JValidationException("Fail to convert value to attribute value. - {0}",e,new Object[]{target.getName()});
		} catch (IllegalAccessException e) {
			throw new JValidationException("Fail to convert value to attribute value. - {0}",e,new Object[]{target.getName()});
		}
	}
	
	public static ArrayModel getArrayModel(JArray jArray) {
		Set<JArrayDetail> details = jArray.getValues();
		ArrayModel model = new ArrayModel();
		model.setObjectId(jArray.getObjectId());
		for(JArrayDetail detail : details) {
			model.addValue(detail.getValue());
		}
		return model;
	}
	
	public static JObject copy(JObject source) throws JValidationException {
		Class<? extends JObject> srcClass = source.getClass();
		Object tgtObj;
		try {
			tgtObj = srcClass.newInstance();
		} catch (Exception e) {
			throw new JValidationException("Cannot create Object. - {0}",e, new Object[]{srcClass.getName()});
		}
		if(!(tgtObj instanceof JObject)) {
			throw new JValidationException("Invalid JObject type. - {0}", new Object[]{srcClass.getName()});
		}
		
		JObject target = (JObject)tgtObj;
		copy(source,target);
		return target;
	}
	
	public static void copy(JObject source, JObject target) throws JValidationException {
		Class<? extends JObject> srcClass = source.getClass();
		Class<? extends JObject> tgtClass = target.getClass();
		if(!srcClass.equals(tgtClass)) {
			throw new JValidationException("Invalid Operation.- Source object({0}) is not a type of Target object({1}).", new Object[]{source, target});
		}
		Method methods[] = srcClass.getDeclaredMethods();
		for(Method srcMethod : methods) {
			Class<?> returnType = srcMethod.getReturnType();
			if(returnType.equals(Void.TYPE) || (!srcMethod.getName().startsWith("is") && !srcMethod.getName().startsWith("get"))) {
				continue;
			}
			
			Class<?>[] paramTypes = srcMethod.getParameterTypes();
			if(paramTypes.length>0) {
				continue;
			}
			
			String name = srcMethod.getName();
			
			if(logger.isDebugEnabled()) {
				logger.debug(name+"\t####--> "+returnType.equals(Void.class)+"\t"+returnType.getName());
			}
			String methodName = null;
			if(returnType.equals(boolean.class)) {
				methodName = name.replaceFirst("is", "set");
			} else {
				methodName = name.replaceFirst("get", "set");
			}
			Method tgtMethod = null;
			try {
				tgtMethod = tgtClass.getMethod(methodName, new Class[]{returnType});
			} catch (Exception e) {
				logger.error("Fail to get target method. - "+methodName+", "+returnType+" - "+e.getMessage(), e);
				continue;
			}
			
			Object value = null;
			try {
				value = srcMethod.invoke(source, new Object[]{});
			} catch (Exception e) {
				logger.error("Fail to get value. - "+e.getMessage(), e);
				continue;
			}
			
			if(value!=null) {
				try {
					tgtMethod.invoke(target, new Object[]{value});
				} catch (Exception e) {
					logger.error("Fail to set a value. - "+e.getMessage(), e);
				}
			}
		}
	}
}
