package jlook.framework.service;

import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchParamName;

public interface JBatchDefinitionService {
	public static final String ID = "jBatchDefinitionService";
	
	public void create(JBatchDefinition jBatchDefinition) throws JServiceException;
	
	public void update(JBatchDefinition jBatchDefinition) throws JServiceException;
	
	public void create(JBatchParamName jBatchParamName) throws JServiceException;
}
