package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.common.JObjectRatingDetail;


public interface JObjectRatingService {
	public static final String ID = "jObjectRatingService";
	
	public void create(JObjectRating jObjectRating) throws JServiceException;
	public void saveObjectRating(Long targetClassId, Long target, Double value, String comment, String writer) throws JServiceException;
	public List<JObjectRatingDetail> getJObjectRatingDetailList(Long targetClassId, Long target) throws JServiceException;
	public String getAverage(List<JObjectRatingDetail> detailList);
}
