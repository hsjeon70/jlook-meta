package jlook.framework.service;

import java.util.Map;

import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;


public interface JMetaService {
	public static final String ID = "jMetaService";
	
	// basic method
	public void mapEntity(Long classId) throws JServiceException;
	public void unmapEntity(Long classId) throws JServiceException;
	public void updateDBSchema() throws JServiceException;
	
	public void buildMeta() throws JServiceException;
	
	// ----------------------------------
	public Map<String,JAttribute> getAllAttributes(JClass jClass) throws JServiceException;
	public Class<?> getEntityClass(JClass jClass) throws JServiceException;
	public JObject newInstance(JClass jClass) throws JServiceException;
	
	public void createJView(JClass jClass) throws JServiceException;
	public void createDetailView(JClass jClass) throws JServiceException;
	public void createSummaryView(JClass jClass) throws JServiceException;
	public void createTitleView(JClass jClass) throws JServiceException;
	
	public void recreateJView(JClass jClass) throws JServiceException;
	
	public void removeJView(JClass jClass) throws JServiceException;
	public void removeDetailView(JClass jClass) throws JServiceException;
	public void removeSummaryView(JClass jClass) throws JServiceException;
	public void removeTitleView(JClass jClass) throws JServiceException;
	
}
