package jlook.framework.service;

import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;

public interface JTypeService {
	public static final String ID = "jDataTypeService";
	
	public void create(JType jDataType) throws JServiceException;
	public JType findByName(String name) throws JServiceException;
	public JType findByJClass(JClass jClass) throws JServiceException;
	public boolean exists(JClass jClass) throws JServiceException;
	
	public void remove(JType jDataType) throws JServiceException;
	public void update(JType jDataType) throws JServiceException;
}
