package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.metadata.JPattern;
import jlook.framework.domain.metadata.JRange;
import jlook.framework.domain.metadata.JValidation;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JValidationRepository;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JValidationService.ID)
public class JValidationServiceImpl implements JValidationService {
	
	@Resource(name=JValidationRepository.ID)
	private JValidationRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JValidation jValidation) throws JServiceException {
		try {
			JValidation tmp = this.repository.selectByName(jValidation.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JValidation", jValidation.getName()});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to crate JValidation. - "+jValidation,e);
		}
		
		JUserEntity entity = security.getUserEntity();
		
		if(jValidation instanceof JEnum) {
			jValidation.setClassId(JMetaKeys.JENUM);
		} else if(jValidation instanceof JPattern) {
			jValidation.setClassId(JMetaKeys.JPATTERN);
		} else if(jValidation instanceof JRange) {
			jValidation.setClassId(JMetaKeys.JRANGE);
		}
		
		jValidation.setDomainId(security.getDomainId());
		jValidation.setCreatedBy(entity.getObjectId());
		jValidation.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jValidation);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JValidation - "+jValidation, e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JEnum jEnum) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		this.create((JValidation)jEnum);
		Set<JEnumDetail> values = jEnum.getValues();
		for(JEnumDetail detail : values) {
			detail.setClassId(JMetaKeys.JENUMDETAIL);
			detail.setDomainId(security.getDomainId());
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			try {
				this.repository.insert(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to create JEnumDetail. - "+detail, e);
			}
		}
	}

	@Override
	public JValidation findByName(String name) throws JServiceException {
		try {
			JValidation jValidation = this.repository.selectByName(name);
			if(jValidation==null) {
				throw new JObjectNotFoundException(new Object[]{"JValidation",name});
			}
			return jValidation;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JValidaiton by name("+name+").",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JValidation jValidation) throws JServiceException {
		try {
			this.repository.delete(jValidation);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JValidation.",e);
		}
	}

}
