package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JViewRepository;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JViewService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JViewService.ID)
public class JViewServiceImpl implements JViewService {

	@Resource(name=JViewRepository.ID)
	private JViewRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JView jView) throws JServiceException {
		if(JClassType.INTERFACE.name().equals(jView.getJClass().getType())) {
			throw new JServiceException("Cannot create JView for abstract JClass. - "+jView.getJClass());
		}
		JUserEntity entity = security.getUserEntity();
		try {
			jView.setClassId(JMetaKeys.JVIEW);
			jView.setDomainId(security.getDomainId());
			jView.setCreatedBy(entity.getObjectId());
			jView.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jView);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JView",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JViewDetail detail) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			detail.setClassId(JMetaKeys.JVIEWDETAIL);
			detail.setDomainId(security.getDomainId());
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(detail);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JViewDetail",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(String name, String description, JClass jClass,
			JViewType type, List<JAttribute> jattList) throws JServiceException {
		JView jView = new JView();
		jView.setName(name);
		jView.setDescription(description);
		jView.setJClass(jClass);
		jView.setType(type.name());
		this.create(jView);
		
		for(int i=0;i<jattList.size();i++) {
			JAttribute jatt = jattList.get(i);
			JViewDetail detail = new JViewDetail();
			detail.setJView(jView);
			detail.setJAttribute(jatt);
			detail.setLabel(jatt.getLabel());
			detail.setLength(jatt.getLength());
			detail.setSequence(10+i);
			detail.setReadOnly(false);
			this.create(detail);
		}
	}

	@Override
	public JView getSystemJView(JClass jClass, JViewType type)
			throws JServiceException {
		String viewName = type.getSystemViewName(jClass);
		try {
			JView jView = this.repository.select(jClass, viewName);
			if(jView==null) {
				throw new JObjectNotFoundException(new Object[]{"JView", viewName+", "+jClass});
			}
			
			return jView;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JView. - "+viewName+", "+jClass,e);
		}
	}

	@Override
	public JView getJView(Long jViewId) throws JServiceException {
		try {
			JView jView = this.repository.select(jViewId);
			if(jView==null) {
				throw new JObjectNotFoundException(new Object[]{"JView", jViewId});
			}
			
			return jView;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JView. - "+jViewId,e);
		}
	}

	@Override
	public JView getJView(JClass jClass, String viewName)
			throws JServiceException {
		try {
			JView jView = this.repository.select(jClass, viewName);
			if(jView==null) {
				throw new JObjectNotFoundException(new Object[]{"JView", viewName+", "+jClass});
			}
			
			return jView;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JView. - "+viewName+", "+jClass,e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JView jView) throws JServiceException {
		try {
			Set<JViewDetail> details = jView.getDetails();
			for(JViewDetail detail : details) {
				this.repository.delete(detail);
			}
			this.repository.delete(jView);
		} catch (JRepositoryException e) {
			throw new JServiceException("Cannot remove JView. - "+jView, e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JViewDetail detail) throws JServiceException {
		try {
			this.repository.delete(detail);
		} catch (JRepositoryException e) {
			throw new JServiceException("Cannot remove JViewDetail. - "+detail, e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JAttribute jAttribute) throws JServiceException {
		List<JViewDetail> details = this.getJViewDetailList(jAttribute);
		try {
			for(JViewDetail detail : details) {
				this.repository.delete(detail);
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Cannot remove JViewDetail. - "+jAttribute, e);
		}
	}

	@Override
	public List<JViewDetail> getJViewDetailList(JAttribute jAttribute)
			throws JServiceException {
		List<JViewDetail> result;
		try {
			result = this.repository.select(jAttribute);
			return result;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JViewDetail list. - "+jAttribute, e);
		}
	}

	@Override
	public List<JView> getAllJViewList(JClass jClass) throws JServiceException {
		List<JView> result;
		try {
			result = this.repository.select(jClass);
			return result;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JView list. - "+jClass, e);
		}
	}

	@Override
	public void update(JView jView) throws JServiceException {
		if(JClassType.INTERFACE.name().equals(jView.getJClass().getType())) {
			throw new JServiceException("Cannot create JView for abstract JClass. - "+jView.getJClass());
		}
		JUserEntity entity = security.getUserEntity();
		try {
			jView.setClassId(JMetaKeys.JVIEW);
			jView.setDomainId(security.getDomainId());
			jView.setUpdatedBy(entity.getObjectId());
			jView.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.update(jView);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update JView",e);
		}
	}

}
