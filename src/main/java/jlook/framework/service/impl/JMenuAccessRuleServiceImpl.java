package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.security.JMenuAccessRule;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JMenuAccessRuleRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JMenuAccessRuleService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JMenuAccessRuleService.ID)
public class JMenuAccessRuleServiceImpl implements JMenuAccessRuleService {
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JMenuAccessRuleRepository.ID)
	private JMenuAccessRuleRepository repository;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JMenuAccessRule jMenuAccessRule) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jMenuAccessRule.setClassId(JMetaKeys.JMENUACCESSRULE);
		jMenuAccessRule.setDomainId(domainId);
		jMenuAccessRule.setCreatedBy(entity.getObjectId());
		jMenuAccessRule.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jMenuAccessRule);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JMenuAccessRule. - "+entity, e);
		}
	}

}
