package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.ClientType;
import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JClassRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JClassService;
import jlook.framework.service.JTypeService;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION, "org.hibernate.exception.ConstraintViolationException", "org.springframework.dao.DataIntegrityViolationException"})
@Service(JClassService.ID)
public class JClassServiceImpl implements JClassService {
	private static Log logger = Log.getLog(JClassServiceImpl.class);
	
	@Resource(name=JClassRepository.ID)
	private JClassRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JTypeService.ID)
	private JTypeService jDataTypeService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JClass jClass) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			JClass tmp = repository.selectByName(jClass.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException("Already exist JClass({0})", new Object[]{jClass.getName()});
			}
			
			Long domainId = security.getDomainId();
			jClass.setClassId(JMetaKeys.JCLASS);
			jClass.setDomainId(domainId);
			
			jClass.setCreatedBy(entity.getObjectId());
			jClass.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			if(!ClientType.Loader.name().equals(security.getClientType())) {
				jClass.setStatus(JMetaStatus.NONE.name());
			}
			Map<String,JAttribute> jattMap = jClass.getJAttributes();
			for(JAttribute jatt : jattMap.values()) {
				jatt.setClassId(JMetaKeys.JATTRIBUTE);
				jatt.setDomainId(domainId);
				jatt.setCreatedBy(entity.getObjectId());
				jatt.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			}
			
			this.repository.insert(jClass);
			
			JType jDataType = new JType();
			jDataType.setName(jClass.getName());
			jDataType.setDescription(jClass.getDescription());
			jDataType.setJClass(jClass);
			jDataTypeService.create(jDataType);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create", e);
		}
	}

	@Override
	public JClass findByName(String name) throws JServiceException {
		try {
			JClass jcls = this.repository.selectByName(name);
			if(jcls==null) {
				throw new JObjectNotFoundException(new Object[]{"JClass",name});
			}
			return jcls;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JClass("+name+").",e);
		}
		
	}

	@Override
	public JClass getJClass(Long objectId) throws JServiceException {
		try {
			JClass jClass = this.repository.select(objectId);
			if(jClass==null) {
				throw new JObjectNotFoundException(new Object[]{"JClass",objectId});
			}
			
			return jClass;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JClass("+objectId+").",e);
		}
	}

	@Override
	public List<JClass> getJClassList() throws JServiceException {
//		JUserEntity entity = security.getUserEntity();
		try {
			return this.repository.selectList();
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JClass list.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(String name) throws JServiceException {
		JClass jClass;
		try {
			jClass = this.repository.selectByName(name);
			if(jClass==null) {
				throw new JObjectNotFoundException(new Object[]{"JClass",name});
			}
			
			if(logger.isDebugEnabled()) {
				logger.debug("\t--> "+jClass.getJAttributes());
			}
			
			JType jdt = jDataTypeService.findByJClass(jClass);
			this.jDataTypeService.remove(jdt);
			this.repository.delete(jClass);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JClass("+name+"). - ",e);
		}		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JClass jClass) throws JServiceException {
		try {
			this.repository.update(jClass);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to save JClass. - "+jClass,e);
		}
	}

	@Override
	public Map<Long, String> getReferences(Long classId)
			throws JServiceException {
		JClass jClass = this.getJClass(classId);
		try {
			List<JClass> result = repository.selectReferences(jClass);
			Map<Long, String> map = new HashMap<Long, String>();
			for(JClass ref : result) {
				map.put(ref.getObjectId(), ref.getLabel()==null ? ref.getName() : ref.getLabel());
			}
			return map;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get incoming references - "+classId,e);
		}
	}
}
