package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JAttributeRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JAttributeService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JAttributeService.ID)
public class JAttributeServiceImpl implements JAttributeService {
	
	@Resource(name=JAttributeRepository.ID)
	private JAttributeRepository repository;
	
	@Resource(name=JSecurityService.ID) 
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JAttribute jatt) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			jatt.setClassId(JMetaKeys.JATTRIBUTE);
			jatt.setDomainId(security.getDomainId());
			
			jatt.setCreatedBy(entity.getObjectId());
			jatt.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jatt);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JAttribute jatt) throws JServiceException {
		try {
			this.repository.delete(jatt);
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to remove JAttribute - "+jatt,e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JAttribute jatt) throws JServiceException {
		try {
			this.repository.update(jatt);
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to update JAttribute - "+jatt,e);
		}
	}

}
