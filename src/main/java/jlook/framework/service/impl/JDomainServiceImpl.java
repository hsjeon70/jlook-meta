package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.common.JPreference;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JDomainRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JDomainService.ID)
public class JDomainServiceImpl implements JDomainService {
	
	@Resource(name=JDomainRepository.ID)
	private JDomainRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService jPreferenceService;
	
	@Override
	public JDomain getJDomain(Long id) throws JServiceException {
		try {
			JDomain jDomain = this.repository.select(id);
			if(jDomain==null) {
				throw new JObjectNotFoundException(new Object[]{"JDomain",id});
			}
			return jDomain;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JDomain("+id+").",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long create(JDomain jDomain) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			JDomain tmp = this.repository.selectByName(jDomain.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JDomain", jDomain.getName()});
			}
			jDomain.setClassId(JMetaKeys.JDOMAIN);
			try {
				jDomain.setDomainId(security.getDomainId());
			} catch(Exception e) {
				jDomain.setDomainId(JDomainDef.SYSTEM_ID);
			}
			jDomain.setCreatedBy(entity.getObjectId());
			jDomain.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jDomain);
			
			jDomain.setDomainId(jDomain.getObjectId());
			this.repository.update(jDomain);
			
			// Domain Preference
			JContext context = JContextFactory.newInstance().getJContext();
			context.putDomainId(jDomain.getObjectId());
			List<JPreference> jPreferences = jPreferenceService.getJPreferences(JPreferenceType.DOMAIN);
			for(JPreference jpref : jPreferences) {
				jPreferenceService.create(jpref, jDomain.getObjectId(), "");
			}
			
			return jDomain.getObjectId();
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JDomain.", e);
		}
	}

	@Override
	public JDomain findByName(String name) throws JServiceException {
		try {
			JDomain jDomain = this.repository.selectByName(name);
			if(jDomain==null) {
				throw new JObjectNotFoundException(new Object[]{"JDomain", name});
			}
			return jDomain;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JDomain.(name="+name+")"+e.getMessage(), e);
		}
	}

	@Override
	public List<JDomain> getAllJDomainList() throws JServiceException {
		try {
			return this.repository.selectAllList();
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JDomain list.", e);
		}
	}

}
