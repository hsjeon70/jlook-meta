package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JBatchInstanceRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JBatchInstanceService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JBatchInstanceService.ID)
public class JBatchInstanceServiceImpl implements JBatchInstanceService {

	@Resource(name=JSecurityService.ID)
	private JSecurityService security;

	@Resource(name=JBatchInstanceRepository.ID)
	private JBatchInstanceRepository repository;

	@Override
	public List<JBatchInstance> getJBatchInstances() throws JServiceException {
		try {
			return this.repository.selectAll();
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to select all JBatchInstances",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JBatchInstance jBatchInstance) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchInstance.setDomainId(entity.getDomainId());
		jBatchInstance.setClassId(JMetaKeys.JBATCHINSTANCE);
		jBatchInstance.setCreatedBy(entity.getObjectId());
		jBatchInstance.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jBatchInstance);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JBatchInstance.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JBatchInstance jBatchInstance) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchInstance.setUpdatedBy(entity.getObjectId());
		jBatchInstance.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.update(jBatchInstance);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update JBatchInstance.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JBatchParamValue jBatchParamValue)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchParamValue.setDomainId(entity.getDomainId());
		jBatchParamValue.setClassId(JMetaKeys.JBATCHINSTANCE);
		jBatchParamValue.setCreatedBy(entity.getObjectId());
		jBatchParamValue.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jBatchParamValue);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JBatchParamValue.", e);
		}
	}

}
