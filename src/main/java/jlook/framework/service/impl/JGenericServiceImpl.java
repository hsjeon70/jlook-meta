package jlook.framework.service.impl;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;

import jlook.framework.Constants;
import jlook.framework.GlobalKeys;
import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.EncryptType;
import jlook.framework.domain.metadata.InheritanceType;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;
import jlook.framework.domain.metadata.JType;
import jlook.framework.domain.metadata.JValidation;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewDetail;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.ActionModel;
import jlook.framework.domain.model.ArrayModel;
import jlook.framework.domain.model.AttachmentModel;
import jlook.framework.domain.model.AttributeModel;
import jlook.framework.domain.model.ColumnModel;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.domain.model.TitleModel;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamName;
import jlook.framework.domain.widget.JWidgetParamType;
import jlook.framework.domain.widget.JWidgetParamValue;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.JRoleType;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JWidgetDef;
import jlook.framework.infrastructure.security.JClassPermission;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.security.spring.JPermissionEvaluator;
import jlook.framework.infrastructure.tools.JStringTemplate;
import jlook.framework.infrastructure.util.ExcelReader;
import jlook.framework.infrastructure.util.ExcelWriter;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.repository.JGenericRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JArrayService;
import jlook.framework.service.JAttachmentService;
import jlook.framework.service.JClassActionService;
import jlook.framework.service.JClassService;
import jlook.framework.service.JDataAccessRuleService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationException;
import jlook.framework.service.JViewService;
import jlook.framework.service.JWidgetService;
import jlook.framework.service.action.JActionException;
import jlook.framework.service.action.JActionType;
import jlook.framework.service.action.JObjectAction;
import jlook.framework.service.action.JObjectActionFactory;
import jlook.framework.service.util.JObjectUtil;
import jlook.framework.service.util.ObjectUtil;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION, "org.hibernate.exception.ConstraintViolationException", "org.springframework.dao.DataIntegrityViolationException"})
@Service(JGenericService.ID)
public class JGenericServiceImpl implements JGenericService {
	private static Log logger = Log.getLog(JGenericServiceImpl.class);
	
	@Value(EnvKeys.APP_HOME)
	private String home;
	
	@Value(EnvKeys.EXCEL_ERROR_DIR)
	private String excelFail;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JGenericRepository.ID)
	private JGenericRepository repository;
	
	@Resource(name=JArrayService.ID)
	private JArrayService jArrayService;
	
	@Resource(name=JAttachmentService.ID)
	private JAttachmentService jAttachmentService;
	
	@Resource(name=JDataAccessRuleService.ID)
	private JDataAccessRuleService jDataAccessRuleService;
	
	@Resource(name=JViewService.ID)
	private JViewService jViewService;
	
	@Resource(name=JStringTemplate.ID)
	private JStringTemplate template;
	
	@Resource(name=JObjectActionFactory.ID)
	private JObjectActionFactory jObjectActionFactory;
	
	@Resource(name=JClassActionService.ID)
	private JClassActionService jClassActionService;
	
	@Resource(name="passwordEncoder")
	private ShaPasswordEncoder encoder;
	
	@Resource(name=JWidgetService.ID)
	private JWidgetService jWidgetService;
	
	@Resource(name=JPermissionEvaluator.ID)
	private JPermissionEvaluator jPermissionEvaluator;
	
	@PreAuthorize("hasPermission(#classId, {'read'})")
	public JSummaryModel getSummaryData(JViewType jViewType, Long classId, PageUtil page, Map<String, String[]> paramMap)
			throws JServiceException {
		if(jViewType==null) {
			jViewType = JViewType.summary;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("-----> classId="+classId+", view="+jViewType.name()+", page="+page);
		}
		
		JClass jClass = this.jClassService.getJClass(classId);
		JView jView = this.jViewService.getSystemJView(jClass,jViewType);
		
		return getSummaryData(jView, jViewType, page, paramMap);
	}
	
	/**
	 * parentOid, parentCid handling
	 */
	private JSummaryModel getSummaryData(JView jView, JViewType jViewType, PageUtil page, Map<String, String[]> paramMap)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		JClass jObjClass = this.jClassService.getJClass(JMetaKeys.JOBJECT);
		Map<String,JAttribute> jObjMap = jObjClass.getJAttributes();
		
		JClass jClass = jView.getJClass();
		if( !JMetaStatus.CREATED_TYPE.equals(jClass.getStatus()) && !JMetaStatus.CHANGED_TYPE.equals(jClass.getStatus()) ) {
			throw new JServiceException("Cannot get summary data for unmapped JClass. {0} - {1}", new Object[]{jClass.getObjectId(), jClass.getStatus()});
		}
		
		Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
		List<JAttribute> jattList = new ArrayList<JAttribute>(jattMap.values());
		Collections.sort(jattList);
		
		// parentCid and parentOid
		JClass parentJClass = null;
		JObject parentJObject = null;
		if(page.getParentCid()!=null && page.getParentOid()!=null) {
			try {
				parentJObject = this.getJObject(page.getParentCid(), page.getParentOid());
				parentJClass = this.jClassService.getJClass(page.getParentCid());
			} catch(JServiceException e) {
				throw new JServiceException("Cannot find parent JObject. parentCid="+page.getParentCid()+", parentOid="+page.getParentOid(), e);
			}
		}
					
		JSummaryModel model = new JSummaryModel();
		try {
			// JClass SQL Filter : Dynamic binding - UserEntity, Role, preference 
			String sqlFilter = jClass.getSummaryFilter();
			if(sqlFilter!=null) {
				Map<String,Object> map = new HashMap<String,Object>();
				map.put(JStringTemplate.JUSER_KEY, entity);
				
				for(Map.Entry<String,String[]> entry : paramMap.entrySet()) {
					String[] value = entry.getValue();
					if(value!=null && value.length>0) {
						map.put(entry.getKey(), value[0]);
					}
				}
				
				if(logger.isDebugEnabled()) {
					logger.debug("SummaryFilter INPUT --> "+map);
				}
				try {
					sqlFilter = template.eval(sqlFilter, map);
					if(logger.isDebugEnabled()) {
						logger.debug("SummaryFilter --> "+sqlFilter);
					}
				} catch (JException e) {
					throw new JServiceException("Fail to eval sql filter for JClass("+jClass.getObjectId()+"). - "+sqlFilter, e);
				}
			}
			
			// make searchable and parent attributes. : todo - paramMap for search form
			List<String> parentAttrList = new ArrayList<String>();
			Map<String,String[]> keywordAttrMap = new HashMap<String,String[]>();
			Map<String,String[]> searchAttrMap = new HashMap<String,String[]>();
			for(JAttribute jatt : jattList) {
				if(!JMetaStatus.CREATED_TYPE.equals(jatt.getStatus())) {
					if(logger.isWarnEnabled()) {
						logger.warn("The attribute status is not 'CREATED'. - "+jatt.getName()+"-->"+jatt.getObjectId());
					}
					continue;
				}
				JType type = jatt.getType();
				if(parentJClass!=null && !type.isPrimitive()) {
					JClass jattJClass = type.getJClass();
					if(parentJClass.typeOf(jattJClass)) {	// check instanceof
						parentAttrList.add(jatt.getName());
					}
				}
				if(page.getKeyword()!=null && !page.getKeyword().equals("")) {
					if(jatt.isPrimary() || jatt.isSearchable()) {
						keywordAttrMap.put(jatt.getName(), new String[]{page.getKeyword()});
					}
				} 
				
				if(paramMap!=null && paramMap.size()>0) {
					String[] paramValues = paramMap.get(jatt.getName());
					if(paramValues!=null && paramValues.length>0) {
						searchAttrMap.put(jatt.getName(), paramValues);
					}
				}
			}
			
			List<ColumnModel> colModel = new ArrayList<ColumnModel>();
			if(!JViewType.title.equals(jViewType)) {
				// make ColumModel List
				colModel.add(new ColumnModel(jObjMap.get(JObject.A_OID)));
				colModel.add(new ColumnModel(jObjMap.get(JObject.A_CID)));
				if(entity.isAdministrator()) {
					colModel.add(new ColumnModel(jObjMap.get(JObject.A_DID)));
				}
				// make ColumnModel list
				Set<JViewDetail> details = jView.getDetails();
				if(logger.isDebugEnabled()) {
					logger.debug("## SUMMARY VIEW --> "+details);
				}
				
				for(JViewDetail detail : details) {
					JAttribute jatt = detail.getJAttribute();
					if(!JMetaStatus.CREATED_TYPE.equals(jatt.getStatus())) {
						if(logger.isWarnEnabled()) {
							logger.warn("The attribute status is not 'CREATED'. - "+jatt.getName()+"-->"+jatt.getObjectId());
						}
						continue;
					}
					
					if(!entity.isAdministrator() && jatt.isHidden() && jClass.getDomainId().equals(JDomainDef.SYSTEM_ID)) {
						continue;
					}
					
					if(!entity.isAdministrator() && !entity.isAdmin() && jatt.isHidden() && !jClass.getDomainId().equals(JDomainDef.SYSTEM_ID)) {
						continue;
					}
					
					ColumnModel col = new ColumnModel(detail);
					col.setLabel(detail.getLabel());
					col.setDisplayLength(detail.getLength());
					col.setSequence(detail.getSequence());
					
					colModel.add(col);
				}
				
				if(entity.isAdministrator()) {
					colModel.add(new ColumnModel(jObjMap.get(JObject.A_CREATED_BY)));
				}
				colModel.add(new ColumnModel(jObjMap.get(JObject.A_CREATED_ON)));
				
				Collections.sort(colModel);
				model.setColumnModel(colModel);
			}
			
			// select summary data
			Class<?> entityClass = this.jMetaService.getEntityClass(jClass);
			Map<String,Object> map = this.repository.selectSummaryData(entityClass.getName(), sqlFilter, page, keywordAttrMap, searchAttrMap, parentAttrList, parentJObject);
			Object data = map.get(Constants.DATA);
			if(!(data instanceof List<?>)) {
				throw new JServiceException("Invalid Result Type. - "+data);
			}
			
			// make list data : summary, detail or title
			// In case of title : TitleModel list
			if(JViewType.title.equals(jViewType)) {
				List<TitleModel> titleList = new ArrayList<TitleModel>();
				List<?> result = (List<?>)data;
				for(Object obj : result) {
					if(!(obj instanceof JObject)) {
						throw new JServiceException("Invalid Result Type. - "+obj);
					}
					
					JObject jobj = (JObject) obj;
					TitleModel tmodel = new TitleModel();
					tmodel.setObjectId(jobj.getObjectId());
					tmodel.setClassId(jobj.getClassId());
					tmodel.setValue(this.getTitleValue(jClass, jobj, 0));
					titleList.add(tmodel);
				}
				model.setData(titleList);
			} else {
				List<List<Object>> resultData = new ArrayList<List<Object>>();
				List<?> result = (List<?>)data;
				for(Object obj : result) {
					if(!(obj instanceof JObject)) {
						throw new JServiceException("Invalid Result Type. - "+obj);
					}
					
					JObject jobj = (JObject) obj;
					List<Object> row = new ArrayList<Object>();
					for(ColumnModel cm : colModel) {
						if(JObject.A_OID.equals(cm.getName())) {
							row.add(jobj.getObjectId());
						} else if(JObject.A_CID.equals(cm.getName())) {
							row.add(jobj.getClassId());
						} else if(JObject.A_DID.equals(cm.getName())) {
							row.add(jobj.getDomainId());
						} else if(JObject.A_CREATED_BY.equals(cm.getName())) {
							row.add(jobj.getCreatedBy());
						} else if(JObject.A_CREATED_ON.equals(cm.getName())) {
							row.add(jobj.getCreatedOn());
						} else if(JObject.A_UPDATED_BY.equals(cm.getName())) {
							row.add(jobj.getUpdatedBy());
						} else if(JObject.A_UPDATED_ON.equals(cm.getName())) {
							row.add(jobj.getUpdatedOn());
						} else {
							JAttribute jatt = jattMap.get(cm.getName());
							JType type = jatt.getType();
							if(type.isJArray()) {
								row.add(this.getArrayModel(jobj, jatt));
							} else if(type.isJAttachment()) {
								row.add(this.getAttachmentModel(jobj, jatt));
							} else {
								row.add(this.getValue(jobj, jatt));
							}
						}
					}
					resultData.add(row);
				}
				model.setData(resultData);
			}
			
			// standard buttons
//			if(parentJObject!=null && !entity.isAdministrator() && !parentJObject.getDomainId().equals(entity.getDomainId())) {
//				List<String> standardActions = new ArrayList<String>();
//				standardActions.addAll(Arrays.asList(JClassPermission.READ,JClassPermission.EXPORT));
//				model.setStandards(standardActions);
//			} else {
				List<JDataAccessRule> rules = jDataAccessRuleService.getJDataAccessRules(jClass);
				List<String> standardActions = this.getStandardActions(jClass.getAccessRule(), rules);
				if(standardActions.contains(JClassPermission.CREATE) && standardActions.contains(JClassPermission.UPDATE)) {
					standardActions.add(JClassPermission.IMPORT);
				}
				
				if(standardActions.contains(JClassPermission.READ)) {
					standardActions.add(JClassPermission.EXPORT);
				}
				model.setStandards(standardActions);
//			}
			
			// custom jclass actions
			List<JClassAction> actions = jClassActionService.findByJClass(jClass, JClassActionType.CLASS);
			List<ActionModel> customActions = this.getJClassActions(actions);
			model.setActions(customActions);
			
			if(JClassType.ABSTRACT_TYPE.equals(jClass.getType())) {
				model.addConfig(Constants.READONLY,true);
			} else {
				model.addConfig(Constants.READONLY,false);
			}
			model.addConfig(JClass.A_CID, jClass.getObjectId());
			model.addConfig(JClass.A_NAME, jClass.getLabel()==null ? jClass.getName() : jClass.getLabel());
			Long count = (Long)map.get(Constants.COUNT);
			if(count==null) {
				count = 0L;
			}
			page.setTotalCount(count.intValue());
			model.setPage(page);
			return model;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get summary data. - classId="+jClass.getObjectId(), e);
		}		
	}
	
	private List<ActionModel> getJClassActions(List<JClassAction> actions) {
		List<ActionModel> list = new ArrayList<ActionModel>();
		if(actions==null) {
			return list;
		}
		
		for(JClassAction action : actions) {
			list.add(new ActionModel(action));
		}
		return list;
	}
	private List<String> getStandardActions(String accessRule, List<JDataAccessRule> rules) {
		JAccessRule jAccessRule = null;
		try {
			jAccessRule = JAccessRule.valueOf(accessRule);
		} catch(Exception e) {
			jAccessRule = JAccessRule.PUBLIC;
		}
		
		List<String> actions = new ArrayList<String>();
		switch(jAccessRule) {
			case PUBLIC : 
				actions.addAll(Arrays.asList(JClassPermission.CREATE, JClassPermission.READ, JClassPermission.UPDATE, JClassPermission.DELETE));
				break;
			case PROTECTED : 
				break;
			case PRIVATE : actions.addAll(Arrays.asList(JClassPermission.READ));
				return actions;
		}
		
		for(JDataAccessRule rule : rules) {
			if(JAccessRule.PUBLIC.equals(jAccessRule)) {
				if(!rule.isCanCreate()) {
					actions.remove(JClassPermission.CREATE);
				}
				if(!rule.isCanRead()) {
					actions.remove(JClassPermission.READ);
				}
				if(!rule.isCanUpdate()) {
					actions.remove(JClassPermission.UPDATE);
				}
				if(!rule.isCanDelete()) {
					actions.remove(JClassPermission.DELETE);
				}
			} else if(JAccessRule.PROTECTED.equals(jAccessRule)){
				if(rule.isCanCreate() && !actions.contains(JClassPermission.CREATE)) {
					actions.add(JClassPermission.CREATE);
				}
				if(rule.isCanRead() && !actions.contains(JClassPermission.READ)) {
					actions.add(JClassPermission.READ);
				}
				if(rule.isCanUpdate() && !actions.contains(JClassPermission.UPDATE)) {
					actions.add(JClassPermission.UPDATE);
				}
				if(rule.isCanDelete() && !actions.contains(JClassPermission.DELETE)) {
					actions.add(JClassPermission.DELETE);
				}
			}
		}
		
		return actions;
	}
	
	
	private AttributeModel createAttributeModel(JAttribute jatt) throws JServiceException {
		return createAttributeModel(jatt, null);
	}
	private AttributeModel createAttributeModel(JAttribute jatt, Object value) throws JServiceException {
		AttributeModel model = new AttributeModel(jatt);
		JType type = jatt.getType();
		if(value!=null) {
			model.setValue(value);
		} else if(!type.isPrimitive()) {
			TitleModel title = new TitleModel();
			title.setClassId(type.getJClass().getObjectId());
			model.setValue(title);
		}
		return model;
	}
	
	@Override
	@PreAuthorize("hasPermission(#classId, 'read')")
	public JObject getJObject(Long classId, Long objectId)
			throws JServiceException {
		JClass jClass = this.jClassService.getJClass(classId);
		Class<?> clazz = this.jMetaService.getEntityClass(jClass);
		
		JObject jObject;
		try {
			jObject = this.repository.select(clazz.getName(), objectId);
			if(jObject==null) {
				throw new JObjectNotFoundException(new Object[]{jClass.getName(),objectId});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JObject. - className-"+jClass.getName()+", "+objectId, e);
		}
		return jObject;
	}
	
	@Override
	@PreAuthorize("hasPermission(#classId, 'read')")
	public JResultModel getTitleData(Long classId, Long objectId)
			throws JServiceException {
		JClass jClass = this.jClassService.getJClass(classId);
		if( !JMetaStatus.CREATED_TYPE.equals(jClass.getStatus()) && !JMetaStatus.CHANGED_TYPE.equals(jClass.getStatus()) ) {
			throw new JServiceException("Cannot get title data for unmapped JClass. {0} - {1}", new Object[]{jClass.getObjectId(), jClass.getStatus()});
		}
		
		JObject jObject = this.getJObject(classId, objectId);
		
		TitleModel title = getTitleData(jClass, jObject);
		JResultModel model = new JResultModel();
		model.addConfig(JClass.A_CID, jClass.getObjectId());
		model.addConfig(JClass.A_NAME, jClass.getLabel()==null ? jClass.getName() : jClass.getLabel());
		model.setData(title);
		
		return model;
	}
	
	protected Object getValue(JObject jobj, JAttribute jattr) throws JServiceException {
		return getValue(jobj,jattr,0);
	}
	
	protected ArrayModel getArrayModel(JObject jObject, JAttribute jatt) throws JServiceException {
		Object obj = null;
		try {
			obj = JObjectUtil.getValue(jObject, jatt);
			if(obj==null) {
				return null;
			}
		} catch (JException e) {
			throw new JServiceException("Fail to get value for JObject.",e);
		}
		if(obj instanceof JArray) {
			return JObjectUtil.getArrayModel((JArray)obj);
		} else {
			throw new JServiceException("Invalid JObject type. It is not a JArray. - "+obj);
		}
	}
	
	protected AttachmentModel getAttachmentModel(JObject jObject, JAttribute jatt) throws JServiceException {
		Object obj = null;
		try {
			obj = JObjectUtil.getValue(jObject, jatt);
			if(obj==null) {
				return null;
			}
		} catch (JException e) {
			throw new JServiceException("Fail to get value for JObject.",e);
		}
		if(obj instanceof JAttachment) {
			return new AttachmentModel((JAttachment)obj);
		} else {
			throw new JServiceException("Invalid JObject type. It is not a JAttachment. - "+obj);
		}
	}
	
	
	private Object getValue(JObject jobj, JAttribute jattr, int depth) throws JServiceException {
		if(depth>2) return jobj.getObjectId();
		
		Object value = null;
		try {
			JType type = jattr.getType();
			value = JObjectUtil.getValue(jobj, jattr);
			if(value==null) {
				return value;
			}
			if(type.isPrimitive()) {
				return JObjectUtil.getPrimitiveValue(type, value);
			}
			
			if(value instanceof JObject) {
				JObject target = (JObject)value;
				Long classId = target.getClassId();
				JClass jClass = this.jClassService.getJClass(classId);
				if(jClass==null) {
					throw new JObjectNotFoundException(new Object[]{JClass.NAME,classId});
				}
				return this.getTitleData(jClass, target);
			} else {
				return value;
			}
		} catch(JException e) {
			throw new JServiceException("Fail to get value from JObject. - "+jobj,e);
		}
		
	}
	
	protected TitleModel getTitleData(JClass jClass, JObject jObject) throws JServiceException {
		TitleModel title = new TitleModel();
		title.setClassId(jObject.getClassId());
		title.setObjectId(jObject.getObjectId());
		
		String titleValue = getTitleValue(jClass, jObject, 0);
		title.setValue(titleValue);
		return title;
	}
	
	private String getTitleValue(JClass jClass, JObject jObject, int depth) throws JServiceException {
		JClass parent = jClass.getInheritanceJClass();
		if(JObject.NAME.equals(jClass.getName()) || InheritanceType.IMPLEMENTS_TYPE.equals(jClass.getInheritanceType())) {
			return null;
		}
		
		JView jView = this.jViewService.getSystemJView(jClass,JViewType.title);
		Set<JViewDetail> details = jView.getDetails();
		
		StringBuffer sb = new StringBuffer();
		
		for(JViewDetail detail : details) {
			JAttribute jatt  = detail.getJAttribute();
			Object value = getValue(jObject, jatt, ++depth);
			if(value!=null) {
				if(value instanceof TitleModel) {
					TitleModel tmodel = (TitleModel)value;
					sb.append(tmodel.getValue()).append(",");
				} else {
					sb.append(value).append(",");
				}
			}
		}

		String parentTitle = null;
		if(parent==null) {
			parent = this.jClassService.findByName(JObject.NAME);
		}
		parentTitle = getTitleValue(parent,jObject, --depth);
		if(parentTitle==null) {
			return sb.length()==0 ? "" : sb.substring(0,sb.length()-1);
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(":").append(parentTitle);
		return sb.toString();
	}

	@Override
	@PreAuthorize("hasPermission(#jObject, 'create')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel create(JObject jObject) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		if(jObject.getClassId()==null) {
			throw new JServiceException("classId is not setted. - "+jObject);
		}
		
		JClass jClass = this.jClassService.getJClass(jObject.getClassId());
		Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
		
		JObjectAction jObjectAction = null;
		try {
			jObjectAction = this.jObjectActionFactory.newJObjectAction(this, jClass, null);
		} catch (JActionException e) {
			throw new JServiceException("Fail to create JObjectPostAction. - "+jClass.getName(),e);
		}
		
		this.validate(jattMap, jObject, null);
		
		jObject.setDomainId(security.getDomainId());
		jObject.setCreatedBy(entity.getObjectId());
		jObject.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		Class<?> clazz = this.jMetaService.getEntityClass(jClass);
		if(existsPrimaryKey(clazz.getName(), jattMap, jObject)) {
			throw new JServiceException("Duplicated primary key. - "+jObject);
		}
		
		// JObjectRule
		String jObjectFilter = jClass.getJObjectRule();
		if(jObjectFilter!=null && !jObjectFilter.equals("")) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(JStringTemplate.JOBJECT_KEY, jObject);
			map.put(JStringTemplate.JUSER_KEY, entity);
			map.put(JStringTemplate.JACTION_KEY, JStringTemplate.JACTION_CREATE);
			try {
				this.template.eval(jObjectFilter, map);
			} catch (JException e) {
				throw new JServiceException("Fail to eval JObjectFilter for JClass("+jClass.getObjectId()+"). "+jObjectFilter);
			}
		}
		
		try {
			if(jObjectAction!=null) {
				jObjectAction.doPreExecute(JActionType.create, jObject);
			}
			this.repository.insert(jObject);
			if(jObjectAction!=null) {
				jObjectAction.doPostExecute(JActionType.create, jObject);
			}
			JResultModel rmodel = new JResultModel();
			rmodel.setData(jObject.getObjectId());
			rmodel.addConfig(JObject.A_CID, jClass.getObjectId());
			rmodel.addConfig(JClass.A_NAME, jClass.getLabel()==null ? jClass.getName() : jClass.getLabel());
			return rmodel;
		} catch (JRepositoryException e) {
			throw new JServiceException("Cannot create JObject("+jObject.getClassId()+").",e);
		} catch(JActionException e) {
			throw new JServiceException("Fail to execute JObjectPostAction("+jObject.getClassId()+").",e);
		}
	}
	
	@Override
//	@PreAuthorize("hasPermission(#classId, 'create')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel create(Long classId, Map<String, String[]> params) throws JServiceException {
		JObject newObj = newJObject(classId, null, params);
		return this.create(newObj);
	}

	@Override
	@PreAuthorize("hasPermission(#classId, 'update')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel update(JObject jObject) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		JObject savedObj = this.getJObject(jObject.getClassId(), jObject.getObjectId());
		
		if(!entity.isAdministrator() && 
				(entity.isAdmin() && !entity.getDomainId().equals(savedObj.getDomainId())) && 
				(!entity.isAdmin() && !entity.getObjectId().equals(savedObj.getCreatedBy())) ) {
			throw new JSecurityException("Cannot update this object because you don't have permission. classId={0}, objectId={1}", new Object[]{jObject.getClassId(), jObject.getObjectId()});
		}
		
		JClass jClass = this.jClassService.getJClass(jObject.getClassId());
		
		JObjectAction jObjectAction = null;
		try {
			jObjectAction = this.jObjectActionFactory.newJObjectAction(this, jClass, savedObj);
		} catch (JActionException e) {
			throw new JServiceException("Fail to create JObjectPostAction. - "+jClass.getName(),e);
		}
		
		Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
		this.validate(jattMap, jObject, savedObj);
		
		savedObj.setUpdatedBy(entity.getObjectId());
		savedObj.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		
		Class<?> clazz = this.jMetaService.getEntityClass(jClass);
		if(existsPrimaryKey(clazz.getName(), jattMap, savedObj)) {
			throw new JServiceException("Duplicated primary key. - "+savedObj);
		}
		
		// JObjectRule
		String jObjectFilter = jClass.getJObjectRule();
		if(jObjectFilter!=null && !jObjectFilter.equals("")) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put(JStringTemplate.JOBJECT_KEY, savedObj);
			map.put(JStringTemplate.JUSER_KEY, entity);
			map.put(JStringTemplate.JACTION_KEY, JStringTemplate.JACTION_UPDATE);
			try {
				this.template.eval(jObjectFilter, map);
			} catch (JException e) {
				throw new JServiceException("Fail to eval JObjectFilter for JClass("+jClass.getObjectId()+"). "+jObjectFilter);
			}
		}
		
		try {
			if(jObjectAction!=null) {
				jObjectAction.doPreExecute(JActionType.update, savedObj);
			}
			this.repository.update(savedObj);
			if(jObjectAction!=null) {
				jObjectAction.doPostExecute(JActionType.update, savedObj);
			}
			
			JResultModel rmodel = new JResultModel();
			rmodel.setData(jObject.getObjectId());
			rmodel.addConfig(JObject.A_CID, jClass.getObjectId());
			rmodel.addConfig(JClass.A_NAME, jClass.getLabel()==null ? jClass.getName() : jClass.getLabel());
			return rmodel;
		} catch (JRepositoryException e) {
			throw new JServiceException("Cannot update JObject("+savedObj.getClassId()+", "+savedObj.getObjectId()+").",e);
		} catch(JActionException e) {
			throw new JServiceException("Fail to execute JObjectPostAction("+savedObj.getClassId()+").",e);
		}		
	}
	
	@Override
//	@PreAuthorize("hasPermission(#classId, 'update')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel update(Long classId, Long objectId, Map<String, String[]> params)
			throws JServiceException {
		JObject newObj = newJObject(classId, objectId, params);
		return this.update(newObj);	
	}

	@Override
	@PreAuthorize("hasPermission(#classId, 'delete')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(Long classId, Long objectId) throws JServiceException {
		JObject jObject = getJObject(classId, objectId);
		JUserEntity entity = this.security.getUserEntity();
		
		if(!entity.isAdministrator() && 
				(entity.isAdmin() && !entity.getDomainId().equals(jObject.getDomainId())) && 
				(!entity.isAdmin() && !entity.getObjectId().equals(jObject.getCreatedBy())) ) {
			throw new JSecurityException("Cannot delete this object because you don't have permission. classId={0}, objectId={1}", new Object[]{jObject.getClassId(), jObject.getObjectId()});
		}
		
		JClass jClass = this.jClassService.getJClass(classId);
		Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
		for(JAttribute jatt : jattMap.values()) {
			Object value;
			try {
				value = JObjectUtil.getValue(jObject, jatt);
				if(value==null) {
					continue;
				}
			} catch (JException e) {
				throw new JServiceException("Fail to get attribute value."+jClass.getName()+", "+jatt.getName(),e);
			}
			if(value instanceof JArray) {
				JArray jArray = (JArray)value;
				this.jArrayService.remove(jArray);
			} else if(value instanceof JAttachment) {
				JAttachment attachment = (JAttachment)value;
				this.jAttachmentService.remove(attachment);
			}
		}
		
		JObjectAction jObjectAction = null;
		try {
			jObjectAction = this.jObjectActionFactory.newJObjectAction(this, jClass, null);
		} catch (JActionException e) {
			throw new JServiceException("Fail to create JObjectPostAction. - "+jClass.getName(),e);
		}
		
		try {
			if(jObjectAction!=null) {
				jObjectAction.doPreExecute(JActionType.delete, jObject);
			}
			this.repository.delete(jObject);
			if(jObjectAction!=null) {
				jObjectAction.doPostExecute(JActionType.delete, jObject);
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JObject("+classId+", "+objectId+").",e);
		} catch(JActionException e) {
			throw new JServiceException("Fail to execute JObjectPostAction("+jObject.getClassId()+").",e);
		}
	}

	protected JObject newJObject(Long classId, Long objectId, Map<String, String[]> params) throws JServiceException {
		JClass jClass = this.jClassService.getJClass(classId);
		Class<?> entityClass = this.jMetaService.getEntityClass(jClass);
		
		Object obj;
		try {
			obj = entityClass.newInstance();
		} catch (Exception e) {
			throw new JServiceException("Fail to create domain object. - "+entityClass.getName(),e);
		}
		
		if(!(obj instanceof JObject)) {
			throw new JServiceException("Invalid domain type. - "+obj);
		}
		JObject oldObj = null; 
		JObject newObj = (JObject)obj;
		newObj.setClassId(classId);
		if(objectId!=null) {
			newObj.setObjectId(objectId);
			oldObj = this.getJObject(classId, objectId);
		}
		Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
		for(JAttribute jatt : jattMap.values()) {
			if(!JMetaStatus.CREATED_TYPE.equals(jatt.getStatus())) {
				if(logger.isWarnEnabled()) {
					logger.warn("The attribute status is not 'CREATED'. - "+jatt.getName()+"-->"+jatt.getObjectId());
				}
				continue;
			}
			String jattName = jatt.getName();
			String[] values = params.get(jattName);
			if(values==null) {
				continue;
			}
			// set value to JObject.
			JType type = jatt.getType();
			try {
				if(type.isPrimitive()) {
					if(values.length > 1) {
						throw new JServiceException("You entered more attribute values. This attribute has single value. - "+jattName);
					}
					Class<?> attrClass = JObjectUtil.getPrimitiveClass(type);
					Object value = JObjectUtil.getPrimitiveValue(type, values[0]);
					if(value!=null && !value.equals("")) {
						JObjectUtil.setValue(newObj, jatt, attrClass, value);
					}
				} else {
					JClass jattJClass = type.getJClass();
					Class<?> attrClass = this.jMetaService.getEntityClass(jattJClass);
					Long refObjectId = null;
					if(type.isJArray()) {
						Object value = null;
						if(oldObj!=null) {
							value = JObjectUtil.getValue(oldObj, jatt);
						}
						if(value==null) {
							String name = jClass.getName()+"."+jattName;
							String description = jClass.getName()+"."+jattName;
							refObjectId = jArrayService.create(name, description, values);
						} else if(value instanceof JArray){
							JArray jArray = (JArray)value;
							Set<JArrayDetail> oldValues = jArray.getValues();
							jArrayService.removeAll(oldValues);
							
							Set<JArrayDetail> details = new HashSet<JArrayDetail>();
							for(String v : values) {
								JArrayDetail detail = new JArrayDetail();
								detail.setValue(v);
								detail.setJArray(jArray);
								details.add(detail);
							}
							
							jArray.setValues(details);
							jArrayService.update(jArray);
							refObjectId = jArray.getObjectId();
						} else {
							throw new JServiceException("It is not a JArray(classId="+classId+", attr="+jatt.getName()+"). - "+value);
						}
					} else {
						if(values.length>1) {
							throw new JServiceException("You entered more attribute values. This attribute has single value. - "+jattName);
						}
						if(values!=null && values.length!=0 && values[0]!=null && !values[0].equals("")) {
							try {
								refObjectId = Long.parseLong(values[0]);
							} catch(Exception e) {
								throw new JServiceException("Reference attribute has invalid value. "+jattName+"="+values[0]);
							}
						}
					}
					if(refObjectId!=null) {
						// array and reference type
						JObject refValue = null;
						if(JClassType.CONCRETE_TYPE.equals(jattJClass.getType())) {
							refValue = this.getJObject(jattJClass.getObjectId(), refObjectId);
						} else if(JClassType.ABSTRACT_TYPE.equals(jattJClass.getType())) {
							refValue = this.getJObject(jattJClass.getObjectId(), refObjectId);
						} else {	// INTERFACE_TYPE
							refValue = new JObject();
							refValue.setClassId(jattJClass.getObjectId());
							refValue.setObjectId(refObjectId);
						}
						JObjectUtil.setValue(newObj, jatt, attrClass, refValue);
					}
				}
			} catch (JException e) {
				throw new JServiceException("Cannot set attribute value at JObject. "+jattName+"="+values,e);
			}
 		}
		return newObj;
	}
	/**
	 * Attribute validation check
	 * @param jattMap
	 * @param jObject
	 * @throws JValidationException
	 */
	protected void validate(Map<String,JAttribute> jattMap, JObject newJObj, JObject oldJObj) throws JValidationException {
		for(JAttribute jatt : jattMap.values()) {
			if(!JMetaStatus.CREATED_TYPE.equals(jatt.getStatus())) {
				if(logger.isWarnEnabled()) {
					logger.warn("The attribute status is not 'CREATED'. - "+jatt.getName()+"-->"+jatt.getObjectId());
				}
				continue;
			}
			JType type = jatt.getType();
			
			String jattName = jatt.getName();
			Object value;
			try {
				value = JObjectUtil.getValue(newJObj, jatt);
			} catch (JException e) {
				throw new JValidationException("Fail to get value from JObject. {0} - {1}", e, new Object[]{newJObj, jatt});
			}
			
			// derived
			if((value != null) && jatt.isDerived()) {
				throw new JValidationException("Invalid operation. User cannot enter any value for derived attribute. {0}={1}", new Object[]{jattName, value});
			}
			// default value
			if(value==null && type.isPrimitive()) {
				Class<?> attrClass = JObjectUtil.getPrimitiveClass(type);
				try {
					String defaultValue = jatt.getDefaultValue();
					if(defaultValue!=null && defaultValue.startsWith("$")) {
						JUserEntity entity = this.security.getUserEntity();
						if(entity!=null) {
							String propName = defaultValue.substring(1);
							try {
								Object propValue = ObjectUtil.getValue(entity, propName);
								if(propValue!=null) {
									defaultValue = propValue.toString();
								} else {
									defaultValue = null;
								}
							} catch(JException e) {
								defaultValue = null;
								logger.warn("Fail to get default value. - "+defaultValue, e);
							}
						}
					}
					if(defaultValue!=null && !defaultValue.equals("")) {
						value = JObjectUtil.getPrimitiveValue(type, defaultValue);
						JObjectUtil.setValue(newJObj, jatt, attrClass, value);
					}
				} catch (JException e) {
					throw new JValidationException("Fail to set a value to JObject. {0} - {1}", e, new Object[]{newJObj, jatt});
				}
			} 
			
			// required
			if(value==null && jatt.isRequired()) {
				throw new JValidationException("Attribute({0}) is requried.", new Object[]{jattName});
			}
			// check length
			JObjectUtil.checkLength(jatt, value);
			
			// validation
			JValidation jValidation = jatt.getJValidation();
			if(value!=null && jValidation!=null) {
				jValidation.validate(value);
			}
			
			if(EncryptType.PASSWORD.equals(jatt.getEncrypt())) {				// password type
				if(oldJObj==null && value!=null && !value.equals("")) {			// create
					value = this.encoder.encodePassword(value.toString(), null);
				} else if(oldJObj!=null && value!=null && !value.equals("")) {	// update
					try {
						Object oldValue = JObjectUtil.getValue(oldJObj, jatt);
						if(!value.equals(oldValue)) {
							value = this.encoder.encodePassword(value.toString(), null);
						}
					} catch (JException e) {
						throw new JValidationException("Fail to get value from JObject. classId={0} - {1}={2}", e, new Object[]{newJObj.getClassId(), jatt.getName(), value});
					}
				}
			}
			
			if(oldJObj!=null && !jatt.isDerived()) {
				if(type.isPrimitive()) {
					Class<?> attrClass = JObjectUtil.getPrimitiveClass(type);
					try {
						JObjectUtil.setValue(oldJObj, jatt, attrClass, value);
					} catch (JException e) {
						throw new JValidationException("Fail to set value to JObject. classId={0} - {1}={2}", e, new Object[]{newJObj.getClassId(), jatt.getName(), value});
					}
				} else if(value==null || value instanceof JObject){
					JObject tmpObj = (JObject)value;
					JClass jattJClass = type.getJClass();
					JObject newValue = null;
					try {
						if(value!=null) {
							newValue = this.getJObject(jattJClass.getObjectId(), tmpObj.getObjectId());
						}
					} catch (JServiceException e) {
						throw new JValidationException("Cannot findl reference JObject. classId={0}, objectId={1}", new Object[]{jattJClass.getObjectId(), tmpObj.getObjectId()});
					}
					try {
						Class<?> attrClass = this.jMetaService.getEntityClass(jattJClass);
						JObjectUtil.setValue(oldJObj, jatt, attrClass, newValue);
					} catch (JException e) {
						throw new JValidationException("Fail to set value to JObject. classId={0} - {1}={2}", e, new Object[]{newJObj.getClassId(), jatt.getName(), value});
					}
				} else {
					throw new JValidationException("Reference type is invalid. It is not a JObject. classId={0}, attribute={1}, value={2}", new Object[]{newJObj.getClassId(), jatt.getName(), value});
				}
			}
		}
	}
	
	protected boolean existsPrimaryKey(String entityName, Map<String,JAttribute> jattMap, JObject jObject) throws JServiceException {
		Map<String,Object> map = new HashMap<String,Object>();
		for(JAttribute jatt : jattMap.values()) {
			if(jatt.isPrimary()) {
				Object value;
				try {
					value = JObjectUtil.getValue(jObject, jatt);
				} catch (JException e) {
					throw new JServiceException("Fail to get value from JObject("+jObject+"). - "+jatt.getName(), e);
				}
				map.put(jatt.getName(), value);
			}
		}
		
		if(map.size()==0) {
			return false;
		}
		
		try {
			return this.repository.existsPrimaryKey(jObject.getObjectId(), entityName, map);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to check primary key. - "+entityName+" - "+map, e);
		}
	}
	
	/**
	 * if jViewType is null, return get search form that consists of searchable jattribute.
	 * 
	 * @param jViewType
	 * @param classId
	 * @param objectId
	 * @param parentCid
	 * @param parentOid
	 * @return
	 * @throws JServiceException
	 */
	private JResultModel getDetailView(JView jView, Long classId, Long objectId, Long parentCid, Long parentOid) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		JClass jClass = null;
		if(jView!=null) {
			jClass = jView.getJClass();
		} else {
			jClass = this.jClassService.getJClass(classId);
		}
		
		if( !JMetaStatus.CREATED_TYPE.equals(jClass.getStatus()) && !JMetaStatus.CHANGED_TYPE.equals(jClass.getStatus()) ) {
			throw new JServiceException("Cannot get detail data for unmapped JClass. {0} - {1}", new Object[]{jClass.getObjectId(), jClass.getStatus()});
		}
		
		Class<?> clazz = this.jMetaService.getEntityClass(jClass);
		// parentCid and parentOid
		JClass parentJClass = null;
		JObject parentJObject = null;
		if(parentCid!=null && parentOid!=null) {
			try {
				parentJObject = this.getJObject(parentCid, parentOid);
				parentJClass = this.jClassService.getJClass(parentCid);
			} catch(JServiceException e) {
				throw new JServiceException("Cannot find parent JObject. parentCid="+parentCid+", parentOid="+parentOid, e);
			}
		}
		
		JObjectAction jObjectAction = null;
		try {
			jObjectAction = this.jObjectActionFactory.newJObjectAction(this, jClass, null);
		} catch (JActionException e) {
			throw new JServiceException("Fail to create JObjectPostAction. - "+jClass.getName(),e);
		}
		
		JObject jObject=null;
		try {
			if(objectId!=null) {
				jObject = this.repository.select(clazz.getName(), objectId);
			}
			
			if(jObjectAction!=null) {
				if(jObject==null) {
					jObject = this.jMetaService.newInstance(jClass);
					jObject.setClassId(jClass.getObjectId());
					jObject.setDomainId(this.security.getDomainId());
				}
				jObjectAction.doPostExecute(JActionType.select, jObject);
				jObject.setClassId(jClass.getObjectId());
				jObject.setDomainId(this.security.getDomainId());
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JObject.", e);
		} catch(JActionException e) {
			throw new JServiceException("Fail to execute JObjectPostAction("+jObject.getClassId()+").",e);
		}
		
		JClass jObjClass = this.jClassService.getJClass(JMetaKeys.JOBJECT);
		Map<String,JAttribute> jObjMap = jObjClass.getJAttributes();
		
		List<AttributeModel> attModelList = new ArrayList<AttributeModel>();
		
		attModelList.add(createAttributeModel(jObjMap.get(JObject.A_OID), jObject==null? null : jObject.getObjectId()));
		if(entity.isAdministrator()) {
			attModelList.add(createAttributeModel(jObjMap.get(JObject.A_CID), jObject==null? jClass.getObjectId() : jObject.getClassId()));
			attModelList.add(createAttributeModel(jObjMap.get(JObject.A_DID), jObject==null? null : jObject.getDomainId()));
		}
		
		Iterator<JViewDetail> jViewDetailIt = null;
		Iterator<JAttribute> jAttrIt = null;
		if(jView!=null) {	// detail view
			Set<JViewDetail> details = jView.getDetails();
			jViewDetailIt = details.iterator();
		} else {				// search form
			Map<String,JAttribute> jattrs = jMetaService.getAllAttributes(jClass);
			jAttrIt = jattrs.values().iterator();
		}
		
		for(int i=0;(jViewDetailIt!=null && jViewDetailIt.hasNext()) || (jAttrIt!=null && jAttrIt.hasNext());) {
			JAttribute jatt = null;
			JViewDetail detail = null;
			if(jView!=null) {	// detail view
				detail = jViewDetailIt.next();
				jatt = detail.getJAttribute();
			} else {				// search form
				jatt = jAttrIt.next();
				if(!jatt.isSearchable()) {
					continue;
				}
			}
			
			if(!JMetaStatus.CREATED_TYPE.equals(jatt.getStatus())) {
				if(logger.isWarnEnabled()) {
					logger.warn("The attribute status is not 'CREATED'. - "+jatt.getName()+"-->"+jatt.getObjectId());
				}
				continue;
			}
			
			// hidden attribute
			if(!entity.isAdministrator() && jatt.isHidden() && jClass.getDomainId().equals(JDomainDef.SYSTEM_ID)) {
				continue;
			}
			if(!entity.isAdministrator() && !entity.isAdmin() && jatt.isHidden() && !jClass.getDomainId().equals(JDomainDef.SYSTEM_ID)) {
				continue;
			}
			
			AttributeModel attrModel = null;
			JType type = jatt.getType();
			
			if(jObject==null) {
				if(!type.isPrimitive() && type.getJClass().equals(parentJClass)) {
					attrModel = createAttributeModel(jatt,this.getTitleData(parentJClass, parentJObject));
					attrModel.setReadOnly(true);	// parent object is setted
				} else {
					attrModel = createAttributeModel(jatt);
				}
			} else if(type.isJArray()) {
				ArrayModel arrModel = this.getArrayModel(jObject, jatt);
				attrModel = createAttributeModel(jatt, arrModel);
			} else if(type.isJAttachment()) {
				AttachmentModel attModel = this.getAttachmentModel(jObject, jatt);
				attrModel = createAttributeModel(jatt,attModel);
			} else {
				if(!type.isPrimitive() && type.getJClass().equals(parentJClass)) {
					attrModel = createAttributeModel(jatt,this.getTitleData(parentJClass, parentJObject));
					attrModel.setReadOnly(true);	// parent object is setted
				} else {
					attrModel = createAttributeModel(jatt,this.getValue(jObject, jatt));
				}
			}
			
//			if(jObject==null && parentJObject==null) {
//				attrModel = createAttributeModel(jatt);
//			} else if(type.isJArray()) {
//				ArrayModel arrModel = null;
//				if(jObject!=null) {
//					arrModel = this.getArrayModel(jObject, jatt);
//					attrModel = createAttributeModel(jatt, arrModel);
//				} else if(parentJObject instanceof JArray) {	// ???
//					arrModel = JObjectUtil.getArrayModel((JArray)parentJObject);
//					attrModel = createAttributeModel(jatt, arrModel);
//				} else {
//					if(jObject==null && parentJObject != null) {
//						attrModel = createAttributeModel(jatt);
//					} else {
//						throw new JServiceException("Cannot set value to JAttribute("+jatt.getObjectId()+", "+jatt.getName()+"). Invalid Parent JObject. It is not JArray. - "+parentJObject);
//					}
//				}
//			} else if(type.isJAttachment()) {
//				AttachmentModel attModel = null;
//				if(jObject!=null) {
//					attModel = this.getAttachmentModel(jObject, jatt);
//					attrModel = createAttributeModel(jatt,attModel);
//				} else if(parentJObject instanceof JAttachment) {	// ???
//					attModel = new AttachmentModel((JAttachment)parentJObject);
//					attrModel = createAttributeModel(jatt,attModel);
//				} else {
//					if(jObject==null && parentJObject != null) {
//						attrModel = createAttributeModel(jatt);
//					} else {
//						throw new JServiceException("Cannot set value to JAttribute("+jatt.getObjectId()+", "+jatt.getName()+"). Invalid Parent JObject. It is not JAttachment. - "+parentJObject);
//					}
//				}
//			} else {
//				if(jObject!=null) {
//					attrModel = createAttributeModel(jatt,this.getValue(jObject, jatt));
//				} else if(!type.isPrimitive() && type.getJClass().equals(parentJClass)){
//					attrModel = createAttributeModel(jatt,this.getTitleData(parentJClass, parentJObject));
//					attrModel.setReadOnly(true);	// parent object is setted
//				} else {
//					attrModel = createAttributeModel(jatt);
//				}
//			}
			
			if(jView!=null) {	// detail view
				attrModel.setSequence(detail.getSequence());
				if(detail.getLabel()!=null) {
					attrModel.setLabel(detail.getLabel());
				}
				attrModel.setDisplayLength(detail.getLength());
				if(!attrModel.isReadOnly()) {
					attrModel.setReadOnly(detail.isReadOnly());
				}
			} else {				// search form
				attrModel.setSequence(20+(++i));
			}
			attModelList.add(attrModel);
		}
		
		attModelList.add(createAttributeModel(jObjMap.get(JObject.A_CREATED_ON), jObject==null ? null : jObject.getCreatedOn()));
		attModelList.add(createAttributeModel(jObjMap.get(JObject.A_UPDATED_ON), jObject==null ? null : jObject.getUpdatedOn()));
		if(entity.isAdministrator()) {
			attModelList.add(createAttributeModel(jObjMap.get(JObject.A_CREATED_BY), jObject==null ? null : jObject.getCreatedBy()));
			attModelList.add(createAttributeModel(jObjMap.get(JObject.A_UPDATED_BY), jObject==null ? null : jObject.getUpdatedBy()));
		}
		
		Collections.sort(attModelList);
		
		JResultModel rmodel = new JResultModel();
		
		// config readOnly on updating JObject
		if(jObject!=null) {	
			if(entity.isAdministrator()) {
				rmodel.addConfig(Constants.READONLY, false);
			} else if(entity.hasRole(JRoleType.ADMIN.getName())){
				rmodel.addConfig(Constants.READONLY, !jObject.getDomainId().equals(entity.getDomainId()));
			} else {
				rmodel.addConfig(Constants.READONLY, !jObject.getCreatedBy().equals(entity.getObjectId()));
			}
		} else {
			rmodel.addConfig(Constants.READONLY, false);
		}
		
		// standard buttons : I don't know why first if
//		if(parentJObject!=null && !entity.isAdministrator() && !parentJObject.getDomainId().equals(entity.getDomainId())) {
//			List<String> standardActions = new ArrayList<String>();
//			standardActions.addAll(Arrays.asList(JClassPermission.READ,JClassPermission.EXPORT));
//			rmodel.setStandards(standardActions);
//		} else {
			List<JDataAccessRule> rules = jDataAccessRuleService.getJDataAccessRules(jClass);
			List<String> standardActions = null;
			if(objectId!=null) {
				standardActions = this.getStandardActions(jClass.getAccessRule(), rules);
			} else {
				standardActions = new ArrayList<String>();
				standardActions.addAll(Arrays.asList(JClassPermission.CREATE));
			}
			rmodel.setStandards(standardActions);
//		}
		
		// custom jclass actions
		if(objectId!=null) {
			List<JClassAction> actions = jClassActionService.findByJClass(jClass, JClassActionType.OBJECT);
			List<ActionModel> customActions = this.getJClassActions(actions);
			rmodel.setActions(customActions);
		} else {
			rmodel.setActions(new ArrayList<ActionModel>());
		}
		
		rmodel.addConfig(JClass.A_CID, jClass.getObjectId());
		rmodel.addConfig(JClass.A_NAME, jClass.getLabel()==null ? jClass.getName() : jClass.getLabel());
		rmodel.setData(attModelList);
		return rmodel;
	}
	
	@Override
	@PreAuthorize("hasPermission(#classId, 'read')")
	public JResultModel getDetailData(JViewType jViewType, Long classId, Long objectId, Long parentCid, Long parentOid)
			throws JServiceException {
		JView jView = null;
		if(jViewType!=null) {
			JClass jClass = this.jClassService.getJClass(classId);
			jView = this.jViewService.getSystemJView(jClass,jViewType);
			
		}
		
		return this.getDetailView(jView, classId, objectId, parentCid, parentOid);
	}
	
	@Override
	@PreAuthorize("hasPermission(#classId, 'read')")
	public JResultModel getSearchForm(Long classId, Long objectId, Long parentCid, Long parentOid) throws JServiceException {
		return this.getDetailView(null, classId, objectId, parentCid, parentOid);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel importExcel(Long classId, ExcelReader reader)
			throws JServiceException {
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("ImportExcel="+reader.getFileName());
			}
			
			JClass jObJClass = this.jClassService.getJClass(JMetaKeys.JOBJECT);
			Map<String,JAttribute> jObjAttMap = this.jMetaService.getAllAttributes(jObJClass);
			
			JClass jClass = this.jClassService.getJClass(classId);
			Map<String,JAttribute> jattMap = this.jMetaService.getAllAttributes(jClass);
			
			Map<String,JAttribute> map = new HashMap<String,JAttribute>();
			map.putAll(jObjAttMap);
			map.putAll(jattMap);
			
			List<String[]> failList = new ArrayList<String[]>();
			List<Map<String,String>> result = new ArrayList<Map<String,String>>();
			while(reader.hasNextSheet()) {
				if(logger.isDebugEnabled()) {
					logger.debug("SheetName="+reader.getSheetName());
				}
				String[] headers = reader.getHeaders();
				String sheetName = reader.getSheetName();
				int create = 0, update=0, fail = 0;
				while(reader.hasNextRow()) {
					String[] row = reader.nextRow();
					if(logger.isDebugEnabled()) {
						logger.debug("Row --> "+row);
					}
					if(row==null) {
						break;
					}
					try {
						Long objectId = null;
						Map<String, String[]> params = new HashMap<String,String[]>();
						for(int i=0;i<headers.length;i++) {
							JAttribute jatt = map.get(headers[i]);
							if(jatt==null) {
								if(logger.isWarnEnabled()) {
									logger.warn("Cannot find JAttribute . - "+headers[i]);
								}
								continue;
							}
							if(jatt.getName().equals(JObject.A_OID)) {
								try {
									objectId = Long.parseLong(row[i]);
								} catch(Exception e) {
									if(logger.isWarnEnabled()) {
										logger.warn("FAIL to get objectId("+row[i]+") - "+e.getMessage(),e);
									}
								}
								continue;
							}
							params.put(jatt.getName(), new String[]{row[i]});
						}
						
						JObject jObject = this.newJObject(classId, objectId, params);
						if(objectId==null) {
							this.create(jObject);
							++create;
						} else {
							this.update(jObject);
							++update;
						}
					} catch(Exception e) {
						++fail;
						String[] error = new String[row.length+1];
						System.arraycopy(row, 0, error, 0, row.length);
						error[row.length] = e.getMessage();
						failList.add(error);
						logger.error("Fail to import excel row - "+row,e);
					}
				}
				
				Map<String,String> sheetMap = new HashMap<String,String>();
				sheetMap.put("sheet", sheetName);
				sheetMap.put("create", Integer.toString(create));
				sheetMap.put("update", Integer.toString(update));
				sheetMap.put("fail", Integer.toString(fail));
				result.add(sheetMap);
			}
			if(failList.size()>0) {
				ExcelWriter writer = null;
				try {
					File excelPath = new File(home, this.excelFail);
					if(!excelPath.exists()) {
						excelPath.mkdirs();
					}
					String fName = reader.getFileName();
					int idx = fName.lastIndexOf('.');
					if(idx==-1) {
						fName = fName+"_"+UUID.randomUUID().toString();
					} else {
						fName = fName.substring(0,idx)+"_"+UUID.randomUUID()+fName.substring(idx);
					}
					File file = new File(excelPath,fName);
					writer = new ExcelWriter(file);
					String[] header = reader.getHeaders();
					String[] errorHeader = new String[header.length+1];
					System.arraycopy(header, 0, errorHeader, 0, header.length);
					errorHeader[header.length] = "Error";
					writer.write(jClass.getName(), errorHeader, failList);
				} catch(IOException e) {
					throw new JServiceException("Fail to write error excel file. - "+e.getMessage(), e);
				} finally {
					try { if(writer!=null) writer.close(); } catch(Exception e) {}
				}
			}
			
			JResultModel rmodel = new JResultModel();
			rmodel.setData(result);
			return rmodel;
		} catch(Exception e) {
			throw new JServiceException("Fail to import excel file("+reader.getFileName()+"). - "+e.getMessage(),e);
		}
	}

	@Override
	public JResultModel getWidgetResultMode(Long widgetId, Long parentCid, Long parentOid, 
			Map<String, String[]> params) throws JServiceException {
		JWidgetInstance instance = this.jWidgetService.getWidgetInstance(widgetId);
		JWidgetDefinition definition = instance.getJWidgetDefinition();
		String name = definition.getName();
		JWidgetDef jWidgetDef = JWidgetDef.getJWidgetDef(name);
		if(jWidgetDef==null) {
			throw new JServiceException("Invalid name of JWidget. - "+name);
		}
		String[] paramIds = params.get(ParamKeys.OBJECT_ID);
		Long objectId = null;
		if(paramIds!=null && paramIds.length==1 && !paramIds[0].equals("")) {
			try {
				objectId = Long.parseLong(paramIds[0]);
			} catch(Exception e) {
				throw new JServiceException("Invalid parameter '"+ParamKeys.OBJECT_ID+"' - "+paramIds[0]);
			}
		}
		JView jView = instance.getJView();
		JClass jClass = jView.getJClass();
		
		JUserEntity entity = this.security.getUserEntity();
		boolean result = this.jPermissionEvaluator.hasPermission(entity, jClass.getObjectId(), "read");
		if(!result) {
			throw new JServiceException("You have no permission for this JClass("+jClass.getObjectId()+").");
		}
		
		JViewType viewType = null;
		try {
			viewType = JViewType.valueOf(jView.getType());
		} catch(Exception e) {
			throw new JServiceException("Invalid JViewType. - "+jView.getType()+", "+jView);
		}
		
		JResultModel rmodel = null;
		switch(viewType) {
		case title : 
		case summary : 
			PageUtil page = new PageUtil();
			page.setParentCid(parentCid);
			page.setParentOid(parentOid);
			String[] pNumber = params.get(ParamKeys.PAGE_NUMBER);
			if(pNumber!=null && pNumber.length==1) {
				try {
					page.setPageNumber(new Integer(pNumber[0]));
				} catch(Exception e) {}
			}
			String[] pSize = params.get(ParamKeys.PAGE_SIZE);
			if(pSize!=null && pSize.length==1) {
				try {
					page.setPageSize(new Integer(pSize[0]));
				} catch(Exception e) {}
			}
			String[] keyword = params.get(ParamKeys.KEYWORD);
			if(keyword!=null && keyword.length==1 && !keyword.equals("")) {
				page.setKeyword(keyword[0]);
			}
			rmodel = this.getSummaryData(jView, JViewType.summary, page, params);
			break;
		case detail : 
			rmodel = this.getDetailView(jView, jClass.getObjectId(), objectId, parentCid, parentOid);
			break;
		default : 
			throw new JServiceException("Invalid JWidgetDef - "+jWidgetDef);
		}
		
		switch(jWidgetDef) {
		case BasicObject : 
			rmodel.addConfig("widget", "object");
			break;
		case BasicGrid 	 : 
			rmodel.addConfig("widget", "grid");
			break;
		case GridCascade : 
			rmodel.addConfig("widget", "gridCascade");
			break;
		case MapCascade	 : 
			rmodel.addConfig("widget", "mapCascade");
			break;
		default : 
			rmodel.addConfig("widget", definition.getName());
		}
		
		Set<JWidgetParamValue> pValues = instance.getJWidgetParamValues();
		for(JWidgetParamValue pValue : pValues) {
			JWidgetParamName pName = pValue.getJWidgetParamName();
			if(JWidgetParamType.CLIENT.name().equals(pName.getType())) {
				String value = pValue.getValue();
				rmodel.addConfig(pName.getName(), (value==null) ? pName.getDefaultValue() : value);
			}
		}
		return rmodel;
	}
	
}
