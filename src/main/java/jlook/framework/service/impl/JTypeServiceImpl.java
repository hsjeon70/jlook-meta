package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JTypeRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JTypeService;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JTypeService.ID)
public class JTypeServiceImpl implements JTypeService {

	@Resource(name=JSecurityService.ID) 
	private JSecurityService security;
	
	@Resource(name=JTypeRepository.ID)
	private JTypeRepository repository;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JType jDataType) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			jDataType.setClassId(JMetaKeys.JTYPE);
			jDataType.setDomainId(security.getDomainId());
			
			jDataType.setCreatedBy(entity.getObjectId());
			jDataType.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jDataType);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create.",e);
		}
	}

	@Override
	public JType findByName(String name) throws JServiceException {
		try {
			JType jdt = this.repository.selectByName(name);
			if(jdt==null) {
				throw new JObjectNotFoundException(new Object[]{"JClass",name});				
			}
			return jdt;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JDataType("+name+")", e);
		}
	}

	@Override
	public JType findByJClass(JClass jClass) throws JServiceException {
		try {
			JType jdt = this.repository.selectByJClass(jClass);
			if(jdt==null) {
				throw new JObjectNotFoundException(new Object[]{"JClass",jClass});				
			}
			return jdt;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JDataType("+jClass+")", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JType jDataType) throws JServiceException {
		try {
			this.repository.delete(jDataType);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JDataType -"+jDataType,e);
		}
	}

	@Override
	public boolean exists(JClass jClass) throws JServiceException {
		try {
			JType jdt = this.repository.selectByJClass(jClass);
			return jdt!=null;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JDataType("+jClass+")", e);
		}
	}

	@Override
	public void update(JType jDataType) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			jDataType.setClassId(JMetaKeys.JTYPE);
			jDataType.setDomainId(security.getDomainId());
			
			jDataType.setUpdatedBy(entity.getObjectId());
			jDataType.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.update(jDataType);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update.",e);
		}
	}

}
