package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JMetaChangeLog;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JMetaChangeLogRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.action.JActionType;
import jlook.framework.service.JClassService;
import jlook.framework.service.JMetaChangeLogService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JMetaChangeLogService.ID)
public class JMetaChangeLogServiceImpl implements JMetaChangeLogService {

	@Resource(name=JMetaChangeLogRepository.ID)
	private JMetaChangeLogRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean create(JActionType action, JClass before, JClass after) throws JServiceException {
		boolean result = false;
		JClass jClass = jClassService.getJClass(JMetaKeys.JCLASS);
		Map<String,JAttribute> jattMap = jMetaService.getAllAttributes(jClass);
		if(!after.getName().equals(before.getName())) {
			result = true;
			insert(after, null, action, jattMap.get(JClass.A_NAME),before.getName(), after.getName());
			//after.setName(before.getName());
		}
		if(!after.getCategory().equals(before.getCategory())) {
			result = true;
			insert(after, null, action, jattMap.get(JClass.A_CATEGORY),before.getCategory(), after.getCategory());
			//after.setCategory(before.getCategory());
		}
		if((after.getInheritanceJClass()==null && before.getInheritanceJClass()!=null) || 
				(after.getInheritanceJClass()!=null && before.getInheritanceJClass()==null) || 
				(after.getInheritanceJClass()!=null &&!after.getInheritanceJClass().equals(before.getInheritanceJClass())) ) {
			result = true;
			insert(after, null, action, jattMap.get(JClass.A_INHERITANCE_JCLASS),
					before.getInheritanceJClass()==null ? null : before.getInheritanceJClass(), 
					after.getInheritanceJClass()==null ? null : after.getInheritanceJClass());
			//after.setInheritanceJClass(before.getInheritanceJClass());
		}
		if((after.getInheritanceType()==null && before.getInheritanceType()!=null) || 
			(after.getInheritanceType()!=null && before.getInheritanceType()==null) || 
			(after.getInheritanceType()!=null && !after.getInheritanceType().equals(before.getInheritanceType())) ) {
			result = true;
			insert(after, null, action, jattMap.get(JClass.A_INHERITANCE_TYPE),before.getInheritanceType(), after.getInheritanceType());
			//after.setInheritanceType(before.getInheritanceType());
		}
		return result;
	}
	
	private void insert(JClass jClass, JAttribute jAttribute, JActionType action, JAttribute target, Object beforeValue, Object afterValue) 
	throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		JMetaChangeLog log = new JMetaChangeLog();
		log.setClassId(JMetaKeys.JMETACHANGELOG);
		log.setDomainId(security.getDomainId());
		log.setJClass(jClass);
		log.setJAttribute(jAttribute);
		log.setAction(action.name());
		log.setTarget(target);
		log.setBeforeValue(beforeValue==null ? null : beforeValue.toString());
		log.setAfterValue(afterValue==null ? null : afterValue.toString());
		log.setCreatedBy(entity.getObjectId());
		log.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			repository.insert(log);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JMetaChangeLog. - "+e.getMessage(), e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean create(JActionType action, JAttribute before, JAttribute after)
			throws JServiceException {
		boolean result = false;
		JClass afterJClass  = null;
		JClass beforeJClass  = null;
		
		JClass jClass = jClassService.getJClass(JMetaKeys.JATTRIBUTE);
		Map<String,JAttribute> jattMap = jMetaService.getAllAttributes(jClass);
		if(JActionType.create.equals(action)) {
			afterJClass = after.getJClass();
			result = true;
			insert(afterJClass, after, action, null, null, null);
			return result;
		} else if(JActionType.delete.equals(action)) {
			beforeJClass= before.getJClass();
			result = true;
			insert(beforeJClass, before, action, null, null, null);
			return result;
		} else {
			afterJClass = after.getJClass();
			beforeJClass= before.getJClass();
		}
		
		
		if( (before==null && after!=null) || (before!=null && after==null) ||
				(before!=null && !before.getName().equals(after.getName())) ) {
			result = true;
			insert(afterJClass, after, action, jattMap.get(JAttribute.A_NAME),before==null? null : before.getName(), after==null? null : after.getName());
			if(JActionType.update.equals(action)) {
				//after.setName(before.getName());
			}
		}
		
		if( (before==null && after!=null) || (before!=null && after==null) ||
				(before!=null && !before.getType().equals(after.getType())) ) {
			result = true;
			insert(afterJClass, after, action, jattMap.get(JAttribute.A_TYPE),before==null? null : before.getType().toString(), after==null? null : after.getType().toString());
			if(JActionType.update.equals(action)) {
				//after.setType(before.getType());
			}
		}
		
		if( (before==null && after!=null) || (before!=null && after==null) ||
				(before!=null && before.isPrimary()!=after.isPrimary()) ) {
			result = true;
			insert(afterJClass, after, action, jattMap.get(JAttribute.A_PRIMARY),before==null? null : before.isPrimary(), after==null? null : after.isPrimary());
			if(JActionType.update.equals(action)) {
				//after.setPrimary(before.isPrimary());
			}
		}
		
		if( (before==null && after!=null) || (before!=null && after==null) ||
				(before!=null && before.isUnique()!=after.isUnique()) ) {
			result = true;
			insert(afterJClass, after, action, jattMap.get(JAttribute.A_UNIQUE),before==null? null : before.isUnique(), after==null? null : after.isUnique());
			if(JActionType.update.equals(action)) {
				//after.setUnique(before.isUnique());
			}
		}
		
		if( (before==null && after!=null) || (before!=null && after==null) ||
				(before!=null && before.isVirtual()!=after.isVirtual()) ) {
			result = true;
			insert(afterJClass, after, action, jattMap.get(JAttribute.A_VIRTUAL),before==null? null : before.isUnique(), after==null? null : after.isVirtual());
			if(JActionType.update.equals(action)) {
				//after.setVirtual(before.isVirtual());
			}
		}
		return result;
	}

	@Override
	public void remove(JAttribute jAttribute) throws JServiceException {
		try {
			this.repository.delete(jAttribute);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JMetaChangeLog. - "+jAttribute, e);
		}
	}

	@Override
	public void remove(JClass jClass) throws JServiceException {
		try {
			this.repository.delete(jClass);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JMetaChangeLog. - "+jClass, e);
		}
	}

}
