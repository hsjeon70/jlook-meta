package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserDomain;
import jlook.framework.domain.account.JUserRole;
import jlook.framework.domain.common.JPreference;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JUserRepository;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JUserService.ID)
public class JUserServiceImpl implements JUserService {

	@Resource(name=JUserRepository.ID)
	private JUserRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name="passwordEncoder")
	private ShaPasswordEncoder encoder;
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService jPreferenceService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long create(JUser jUser) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			JUser tmp = this.repository.selectByEmail(jUser.getEmail());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JUser",jUser.getEmail()});
			}
			Long domainId = security.getDomainId();
			JDomain jDomain = jDomainService.getJDomain(domainId);
			String password = jUser.getPassword();
			if(password==null || password.equals("")) {
				throw new JServiceException("Invalid password attribute. - "+password);
			}
			jUser.setPassword(this.encoder.encodePassword(password, null));
			jUser.setClassId(JMetaKeys.JUSER);
			jUser.setDomainId(domainId);
			jUser.setCreatedBy(entity.getObjectId());
			jUser.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jUser);
			
			JUserRole jUserRole = new JUserRole();
			jUserRole.setJUser(jUser);
			jUserRole.setJRole(JRoleDef.User.instance);
			this.create(jUserRole);
			
			JUserDomain jUserDomain = new JUserDomain();
			jUserDomain.setJUser(jUser);
			jUserDomain.setJDomain(jDomain);
			this.create(jUserDomain);
			
			List<JPreference> jPreferences = jPreferenceService.getJPreferences(JPreferenceType.USER);
			for(JPreference jpref : jPreferences) {
				jPreferenceService.create(jpref, jUser.getObjectId(), "");
			}
			
			return jUser.getObjectId();
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to register JUser. ("+jUser.getEmail()+")",e);
		}
	}
	
	@Override
	public JUser findByEmail(String email) throws JServiceException {
		try {
			JUser jUser = this.repository.selectByEmail(email);
			if(jUser==null) {
				throw new JObjectNotFoundException(new Object[]{"JUser",email});
			}
			return jUser;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JUser("+email+").", e);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void remove(String email) throws JServiceException {
		JUser tmp;
		try {
			tmp = this.repository.selectByEmail(email);
			if(tmp==null) {
				throw new JObjectNotFoundException(new Object[]{"JUser",email});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JUser", e);
		}
		
		try {
			this.repository.delete(tmp);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JUser", e);
		}	
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JUserRole jUserRole) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		jUserRole.setDomainId(entity.getDomainId());
		jUserRole.setClassId(JMetaKeys.JUSERROLE);
		jUserRole.setCreatedBy(entity.getObjectId());
		jUserRole.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jUserRole);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JUserRole", e);
		}	
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JUserDomain jUserDomain)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		jUserDomain.setDomainId(entity.getDomainId());
		jUserDomain.setClassId(JMetaKeys.JUSERDOMAIN);
		jUserDomain.setCreatedBy(entity.getObjectId());
		jUserDomain.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jUserDomain);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JUserDomain", e);
		}	
	}
}
