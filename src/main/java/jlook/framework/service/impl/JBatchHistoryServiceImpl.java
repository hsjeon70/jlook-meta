package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.batch.JBatchHistory;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JBatchHistoryRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JBatchHistoryService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JBatchHistoryService.ID)
public class JBatchHistoryServiceImpl implements JBatchHistoryService {
	
	@Resource(name=JBatchHistoryRepository.ID)
	private JBatchHistoryRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JBatchHistory jBatchHistory)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
//		jBatchHistory.setDomainId(entity.getDomainId());
		jBatchHistory.setClassId(JMetaKeys.JBATCHHISTORY);
		jBatchHistory.setCreatedBy(entity.getObjectId());
		jBatchHistory.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jBatchHistory);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JBatchHistory.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JBatchHistory jBatchHistory)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchHistory.setClassId(JMetaKeys.JBATCHHISTORY);
		jBatchHistory.setUpdatedBy(entity.getObjectId());
		jBatchHistory.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.update(jBatchHistory);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update JBatchHistory.", e);
		}
	}

}
