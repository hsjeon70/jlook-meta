package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.model.AttachmentModel;
import jlook.framework.domain.model.FileModel;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JAttachmentRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JAttachmentService;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JAttachmentService.ID)
public class JAttachmentServiceImpl implements JAttachmentService {
	private static Log logger = Log.getLog(JAttachmentServiceImpl.class);
	
	@Resource(name=JSecurityService.ID) 
	private JSecurityService security;
	
	@Resource(name=JAttachmentRepository.ID)
	private JAttachmentRepository repository;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel create(JAttachment attachment) throws JServiceException {
		JAttachment tmp;
		try {
			tmp = repository.selectByName(attachment.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JAttachment", attachment.getName()});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JAttachment("+attachment.getName()+")",e);
		}
		
		JUserEntity entity = security.getUserEntity();
		try {
			attachment.setClassId(JMetaKeys.JATTACHMENT);
			attachment.setDomainId(security.getDomainId());
			attachment.setCreatedBy(entity.getObjectId());
			attachment.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			Set<JAttachmentDetail> details = attachment.getJAttachmentDetails();
			repository.insert(attachment);
			if(details!=null) {
				for(JAttachmentDetail detail : details) {
					detail.setClassId(JMetaKeys.JATTACHMENTDETAIL);
					detail.setDomainId(security.getDomainId());
					detail.setCreatedBy(attachment.getCreatedBy());
					detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
					repository.insert(detail);
				}
			}
			if(logger.isDebugEnabled()) {
				logger.debug("JAttachment object is created. - "+attachment);
			}
			
			JResultModel rmodel = new JResultModel();
			AttachmentModel attach = new AttachmentModel(attachment);
			rmodel.setData(attach);
			return rmodel;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create a JAttachment.",e);
		}
	}

	@Override
	public JAttachment getJAttachment(Long objectId) throws JServiceException {
		try {
			JAttachment jatt = repository.select(objectId);
			if(jatt==null) {
				throw new JObjectNotFoundException(new Object[]{"JAttachment", objectId});
			}
			return jatt;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get a JAttachment.("+objectId+").",e);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public JResultModel update(JAttachment attachment) throws JServiceException {
		JAttachment tmp;
		try {
			tmp = repository.selectByName(attachment.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JAttachment", attachment.getName()});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JAttachment("+attachment.getName()+")",e);
		}
		
		try {
			JUserEntity entity = security.getUserEntity();
			attachment.setUpdatedBy(entity.getObjectId());
			attachment.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			
			Set<JAttachmentDetail> details = attachment.getJAttachmentDetails();
			repository.update(attachment);
			if(details!=null) {
				for(JAttachmentDetail detail : details) {
					detail.setUpdatedBy(attachment.getCreatedBy());
					detail.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					repository.update(detail);
				}
			}
			
			if(logger.isDebugEnabled()) {
				logger.debug("JAttachment object is updated. - "+attachment);
			}
			
			JResultModel rmodel = new JResultModel();
			AttachmentModel am = new AttachmentModel(attachment);
			rmodel.setData(am);
			return rmodel;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update a JAttachment.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(Long attachmentId) throws JServiceException {
		try {
			JAttachment attachment = this.getJAttachment(attachmentId);
			this.repository.delete(attachment);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JAttachment.", e);
		}
	}

	@Override
	public JAttachmentDetail getJAttachmentDetail(Long objectId)
			throws JServiceException {
		try {
			JAttachmentDetail detail = repository.selectDetail(objectId);
			if(detail==null) {
				throw new JObjectNotFoundException(new Object[]{"JAttachmentDetail", objectId});
			}
			return detail;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get a JAttachmentDetail.("+objectId+").",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public JResultModel create(Long objectId, String subject, String content,
			JAttachmentDetail detail) throws JServiceException {
		JAttachment attachment = this.getJAttachment(objectId);
		if(subject!=null) {
			attachment.setSubject(subject);
		}
		if(content!=null) {
			attachment.setContent(content);
		}
		JUserEntity entity = security.getUserEntity();
		detail.setJAttachment(attachment);
		detail.setClassId(JMetaKeys.JATTACHMENTDETAIL);
		detail.setDomainId(security.getDomainId());
		detail.setCreatedBy(entity.getObjectId());
		detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			// TODO : Is JAttachment updated?
			this.repository.insert(detail);
			JResultModel rmodel = new JResultModel();
			AttachmentModel am = new AttachmentModel();
			am.setObjectId(attachment.getObjectId());;
			FileModel fmodel = new FileModel(detail);
			am.addFile(fmodel);
			rmodel.setData(am);
			return rmodel;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create a JAttachmentDetail.("+objectId+").",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(Long attachmentId, Long detailId) throws JServiceException {
		try {
			JAttachment attachment = this.getJAttachment(attachmentId);
			if(attachment.getJAttachmentDetails().size()==1) {
				this.repository.delete(attachment);
			} else {
				JAttachmentDetail detail = this.repository.selectDetail(detailId);
				this.repository.delete(detail);
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JAttachmentDetail.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JAttachment attachment) throws JServiceException {
		try {
			this.repository.delete(attachment);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JAttachment.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeDetail(Long detailId) throws JServiceException {
		try {
			JAttachmentDetail detail = this.repository.selectDetail(detailId);
			this.repository.delete(detail);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JAttachmentDetail.", e);
		}
	}
	
}
