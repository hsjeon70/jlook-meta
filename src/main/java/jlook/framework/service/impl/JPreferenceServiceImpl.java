package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.common.JPreferenceDetail;
import jlook.framework.domain.model.PreferenceModel;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JPreferenceRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JPreferenceService.ID)
public class JPreferenceServiceImpl implements JPreferenceService {
	private static Log logger = Log.getLog(JPreferenceServiceImpl.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JPreferenceRepository.ID)
	private JPreferenceRepository repository;
	
	@Override
	public List<PreferenceModel> getDomainPreferenceModels(Long domainId)
			throws JServiceException {
		List<JPreference> preferences = this.getJPreferences(JPreferenceType.DOMAIN);
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("RESULT --> "+preferences);
			}
			List<PreferenceModel> result = new ArrayList<PreferenceModel>();
			for(JPreference jPref : preferences) {
				PreferenceModel pm = new PreferenceModel();
				pm.setName(jPref.getName());
				pm.setType(jPref.getType());
				pm.setDescription(jPref.getDescription());
				pm.setValue(jPref.getDefaultValue());
				
				JPreferenceDetail detail = this.repository.select(jPref,domainId);
				if(detail != null) {
					String value = detail.getValue();
					if(value!=null) {
						pm.setValue(value);
					}					
				}
				
				result.add(pm);
			}
			return result;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JPreferences.",e);
		}
	}
	
	@Override
	public List<PreferenceModel> getPreferenceModels(JPreferenceType preferenceType)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		Long objectId = null;
		switch(preferenceType) {
			case SYSTEM : 
				break;
			case DOMAIN : objectId = security.getDomainId();
				break;
			case USER : objectId = entity.getObjectId();
				break;
		}
		
		List<JPreference> preferences = this.getJPreferences(preferenceType);
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("RESULT --> "+preferences);
			}
			List<PreferenceModel> result = new ArrayList<PreferenceModel>();
			for(JPreference jPref : preferences) {
				PreferenceModel pm = new PreferenceModel();
				pm.setName(jPref.getName());
				pm.setType(jPref.getType());
				pm.setDescription(jPref.getDescription());
				pm.setValue(jPref.getDefaultValue());
				
				JPreferenceDetail detail = this.repository.select(jPref,objectId);
				if(detail != null) {
					String value = detail.getValue();
					if(value!=null) {
						pm.setValue(value);
					}					
				}
				
				result.add(pm);
			}
			return result;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JPreferences.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(String name, String description, JPreferenceType type,
			String defaultValue) throws JServiceException {
		
		JPreference jpref = new JPreference();
		jpref.setName(name);
		jpref.setDescription(description);
		jpref.setType(type.name());
		jpref.setDefaultValue(defaultValue);
		
		JUserEntity entity = security.getUserEntity();
		
		jpref.setClassId(JMetaKeys.JPREFERENCE);
		jpref.setDomainId(security.getDomainId());
		jpref.setCreatedBy(entity.getObjectId());
		jpref.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jpref);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JPreference.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void savePreperfence(Long preferenceId, Long target, String value)
			throws JServiceException {
		JPreference jpref;
		try {
			jpref = this.repository.select(preferenceId);
			if(jpref==null) {
				throw new JObjectNotFoundException(new Object[]{JPreference.NAME, preferenceId});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JPreferenceDetail.",e);
		}
		
		save(jpref,target,value);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void savePreperfence(String name, JPreferenceType type, Long target, String value)
			throws JServiceException {
		JPreference jpref = null;
		try {
			jpref = this.repository.select(name, type.name());
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to save JPreference.",e);
		}
		
		save(jpref,target,value);
	}
	
	private void save(JPreference jpref, Long target, String value) throws JServiceException {
		JPreferenceDetail detail = null;
		try {
			detail = this.repository.select(jpref, target);
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to create JPreferenceDetail.",e);
		}
				
		JUserEntity entity = security.getUserEntity();
		if(detail == null) {
			detail = new JPreferenceDetail();
			detail.setJPreference(jpref);
			detail.setTarget(target);

			detail.setClassId(JMetaKeys.JPREFERENCEDETAIL);
			detail.setDomainId(security.getDomainId());
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			detail.setValue(value);
			try {
				this.repository.insert(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to create JPreferenceDetail.",e);
			}
		} else {
			detail.setUpdatedBy(entity.getObjectId());
			detail.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			
			detail.setValue(value);
			try {
				this.repository.update(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to update JPreferenceDetail.",e);
			}
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JPreference jPreference) throws JServiceException {
		JPreferenceType type = null;
		try {
			type = JPreferenceType.valueOf(jPreference.getType());
		} catch(Exception e) {
			throw new JServiceException("Invalid JPreferenceType("+jPreference.getType()+").",e);
		}
		this.create(jPreference.getName(), jPreference.getDescription(), type, jPreference.getDefaultValue());
	}

	@Override
	public String getPreferenceValue(JPreferenceType preferenceType, String preferenceName)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		JPreference jPref = null;
		try {
			jPref = this.repository.select(preferenceName, preferenceType.name());
			if(jPref==null) {
				throw new JObjectNotFoundException("Cannot find {0}. {1}, {2}", new Object[]{"JPreference", preferenceName, preferenceType.name()});
			}
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JPreference. {0} - {1}", e, new Object[]{preferenceName, preferenceType.name()});
		}
		
		Long objectId = null;
		switch(preferenceType) {
			case SYSTEM : 
				break;
			case DOMAIN : objectId = security.getDomainId();
				break;
			case USER : objectId = entity.getObjectId();
				break;
		}
		
		JPreferenceDetail detail;
		try {
			detail = this.repository.select(jPref,objectId);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JPreferenceDetail.{0} - {1}", e, new Object[]{jPref, objectId});
		}
		
		if(detail.getValue()==null) {
			return jPref.getDefaultValue();
		}
		return detail.getValue();
	}

	@Override
	public List<JPreference> getJPreferences(JPreferenceType preferenceType)
			throws JServiceException {
		try {
			List<JPreference> preferences = this.repository.select(preferenceType.name());
			return preferences;
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to get preferences - "+preferenceType, e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JPreference jPreference, Long target, String value)
			throws JServiceException {
		this.save(jPreference, target, value);
	}

	@Override
	public void remove(JPreferenceType type, Long objectId)
			throws JServiceException {
		if(type==null) {
			throw new JServiceException("JPreferenceType is null.");
		}
		
		if(type.equals(JPreferenceType.SYSTEM)) {
			throw new JServiceException("Cannot remove system preference.");
		}
		
		List<JPreferenceDetail> details;
		try {
			details = this.repository.select(type, objectId);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JPreferenceDetail list.",e);
		}
		try {
			this.repository.delete(details);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to delete JPreferenceDetail list.",e);
		}
	}
}
