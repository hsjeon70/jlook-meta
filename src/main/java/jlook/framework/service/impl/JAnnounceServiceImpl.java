package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.common.JAnnounceMessage;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.repository.JAnnounceRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JAnnounceService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JAnnounceService.ID)
public class JAnnounceServiceImpl implements JAnnounceService {
	
	@Resource(name=JAnnounceRepository.ID)
	private JAnnounceRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JAnnounceMessage message) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		message.setClassId(JMetaKeys.JARRAY);
		message.setDomainId(security.getDomainId());
		message.setCreatedBy(entity.getObjectId());
		message.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(message);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JAnnounceMessage.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JAnnounceMessage message) throws JServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(Long objectId) throws JServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JResultModel getDetailData(Long objectId) throws JServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSummaryModel getSummaryData(PageUtil page) throws JServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JAnnounceMessage> getJAnnounceMessageList()
			throws JServiceException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
