package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchParamName;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JBatchDefinitionRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JBatchDefinitionService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JBatchDefinitionService.ID)
public class JBatchDefinitionServiceImpl implements JBatchDefinitionService {

	@Resource(name=JBatchDefinitionRepository.ID)
	private JBatchDefinitionRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JBatchDefinition jBatchDefinition) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchDefinition.setDomainId(entity.getDomainId());
		jBatchDefinition.setClassId(JMetaKeys.JBATCHDEFINITION);
		jBatchDefinition.setCreatedBy(entity.getObjectId());
		jBatchDefinition.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jBatchDefinition);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JBatchDefinition.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JBatchDefinition jBatchDefinition) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchDefinition.setUpdatedBy(entity.getObjectId());
		jBatchDefinition.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.update(jBatchDefinition);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to update JBatchDefintion.", e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JBatchParamName jBatchParamName)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jBatchParamName.setDomainId(entity.getDomainId());
		jBatchParamName.setClassId(JMetaKeys.JBATCHPARAMNAME);
		jBatchParamName.setCreatedBy(entity.getObjectId());
		jBatchParamName.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jBatchParamName);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JBatchParamName.", e);
		}
	}

}
