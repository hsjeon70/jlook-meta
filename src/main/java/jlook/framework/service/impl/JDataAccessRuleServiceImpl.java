package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JDataAccessRuleRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JDataAccessRuleService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JDataAccessRuleService.ID)
public class JDataAccessRuleServiceImpl implements JDataAccessRuleService {
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JDataAccessRuleRepository.ID)
	private JDataAccessRuleRepository repository;
	
	@Override
	public List<JDataAccessRule> getJDataAccessRules(JClass jClass)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		List<String> roles = entity.getRoleList();
		List<JDataAccessRule> rules;
		try {
			rules = this.repository.select(jClass, roles);
			return rules;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JDataAccessRules. - "+jClass.getObjectId(), e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JDataAccessRule jDataAccessRule)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jDataAccessRule.setClassId(JMetaKeys.JDATAACCESSRULE);
		jDataAccessRule.setDomainId(domainId);
		jDataAccessRule.setCreatedBy(entity.getObjectId());
		jDataAccessRule.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jDataAccessRule);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JDataAccessRule. - "+entity, e);
		}
	}

}
