package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JObjectRatingRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JClassService;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JObjectRatingService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JObjectRatingService.ID)
public class JObjectRatingServiceImpl implements JObjectRatingService {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JObjectRatingServiceImpl.class);
	
	@Resource(name=JObjectRatingRepository.ID)
	private JObjectRatingRepository repository;

	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JObjectRating jObjectRating)
			throws JServiceException {
		Long domainId = security.getDomainId();
		try {
			JObjectRating tmp = this.repository.selectByTargetClass(jObjectRating.getTargetClass());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JObjectRating",jObjectRating.getTargetClass()});
			}
			
			JUserEntity entity = security.getUserEntity();
			jObjectRating.setClassId(JMetaKeys.JOBJECTRATING);
			jObjectRating.setDomainId(domainId);
			jObjectRating.setCreatedBy(entity.getObjectId());
			jObjectRating.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(jObjectRating);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to add JObjectRating", e);
		}	
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void saveObjectRating(Long targetClassId, Long target, Double value, String comment, String writer)
			throws JServiceException {
		Long domainId = security.getDomainId();
		try {
			JClass targetClass = this.jClassService.getJClass(targetClassId);
			JObjectRating tmp = this.repository.selectByTargetClass(targetClass);
			if(tmp==null) {
				throw new JObjectNotFoundException(new Object[]{"JObjectRating",targetClassId});
			}
			
			JUserEntity entity = security.getUserEntity();
			
			JObjectRatingDetail detail = new JObjectRatingDetail();
			detail.setJObjectRating(tmp);
			detail.setValue(value);
			detail.setDomainId(tmp.getDomainId());
			detail.setTarget(target);
			detail.setComment(comment);
			if(writer==null) {
				detail.setWriter(entity.getNickname());
			} else {
				detail.setWriter(writer);
			}
			detail.setClassId(JMetaKeys.JOBJECTRATINGDETAIL);
			detail.setDomainId(domainId);
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(detail);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to save JObjectRating", e);
		}
		
	}

	@Override
	public List<JObjectRatingDetail> getJObjectRatingDetailList(Long targetClassId, Long target) throws JServiceException {
		try {
			JClass targetClass = this.jClassService.getJClass(targetClassId);
			JObjectRating objRating = this.repository.selectByTargetClass(targetClass);
			if(objRating==null) {
				throw new JObjectNotFoundException(new Object[]{"JObjectRating", targetClassId+", "+target});
			}
			
			List<JObjectRatingDetail> list = this.repository.select(objRating, target);
			
			return list;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JObjectRatingDetail list.("+targetClassId+", "+target+")",e);
		}
	}

	@Override
	public String getAverage(List<JObjectRatingDetail> detailList) {
		double sum = 0.0;
		int count=0;
		for(JObjectRatingDetail detail : detailList) {
			if(detail.getValue()==null) {
				count++;
				continue;
			}
			sum +=detail.getValue();
		}
		
		DecimalFormat nf = new DecimalFormat("##.##");
		String avg = nf.format(sum/(detailList.size()-count));
		return avg;
	}
	
}
