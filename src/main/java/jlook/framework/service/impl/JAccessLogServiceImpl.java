package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.management.JAccessLog;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JAccessLogRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JAccessLogService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JAccessLogService.ID)
public class JAccessLogServiceImpl implements JAccessLogService {
	
	@Resource(name=JAccessLogRepository.ID)
	private JAccessLogRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Value(EnvKeys.ACCESS_LOG_ENABLED)
	private boolean accessLogEnabled;
	
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JAccessLog jAccessLog) throws JServiceException {
		if(!this.accessLogEnabled) {
			return;
		}
		JUserEntity entity = security.getUserEntity();
		
		jAccessLog.setClassId(JMetaKeys.JARRAY);
//		jAccessLog.setDomainId(security.getDomainId());
		jAccessLog.setCreatedBy(entity.getObjectId());
		jAccessLog.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jAccessLog);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JAccessLog.", e);
		}
	}

}
