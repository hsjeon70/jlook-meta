package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.MenuModel;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.security.JGrantedAuthority;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JMenuRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JMenuService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JMenuService.ID)
public class JMenuServiceImpl implements JMenuService {
	private static Log logger = Log.getLog(JMenuServiceImpl.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JMenuRepository.ID)
	private JMenuRepository repository;
	
	@Override
	public JResultModel getMyMenuList() throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Collection<GrantedAuthority> authorities = entity.getAuthorities();
		List<JRole> roleList = new ArrayList<JRole>();
		for(GrantedAuthority ga : authorities) {
			if(ga instanceof JGrantedAuthority) {
				JGrantedAuthority jga = (JGrantedAuthority)ga;
				JRole role = new JRole();
				role.setObjectId(jga.getRoleId());
				roleList.add(role);
			}
		}
		List<JMenu> result = null;
		try {
			result = this.repository.select(roleList);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get Menu list.", e);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("COUNT="+result);
		}
		
		Map<Long, MenuModel> map = new HashMap<Long, MenuModel>();
		List<MenuModel> data = new ArrayList<MenuModel>();
		for(JMenu jMenu : result) {
			if(logger.isDebugEnabled()) {
				logger.debug("\t-->"+jMenu);
			}
			
			MenuModel model = new MenuModel(jMenu);
			map.put(model.getObjectId(), model);
			
			JMenu parent = jMenu.getParent();
			if(parent==null) {
				data.add(model);
				continue;
			}
			
			MenuModel pm = map.get(parent.getObjectId());
			if(pm==null) {
				if(logger.isInfoEnabled()) {
					logger.info("User doesn't have child menu because user doesn't have parent menu. - "+pm);
				}
				continue;
			}
			pm.addChild(model);
		}
		
		Collections.sort(data);
		JResultModel rmodel = new JResultModel();
		rmodel.addConfig(JObject.A_CID,JMetaKeys.JMENU);
		rmodel.setData(data);
		return rmodel;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JMenu jMenu) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jMenu.setClassId(JMetaKeys.JMENU);
		jMenu.setDomainId(domainId);
		jMenu.setCreatedBy(entity.getObjectId());
		jMenu.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jMenu);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JMenu. - "+jMenu,e);
		}
	}

	@Override
	public JResultModel getPublicMenuList() throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Collection<GrantedAuthority> authorities = entity.getAuthorities();
		List<JRole> roleList = new ArrayList<JRole>();
		for(GrantedAuthority ga : authorities) {
			if(ga instanceof JGrantedAuthority) {
				JGrantedAuthority jga = (JGrantedAuthority)ga;
				JRole role = new JRole();
				role.setObjectId(jga.getRoleId());
				roleList.add(role);
			}
		}
		List<JMenu> result = null;
		try {
			result = this.repository.selectByPublic(roleList);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get public menu list.", e);
		}
		
		Map<Long, MenuModel> map = new HashMap<Long, MenuModel>();
		List<MenuModel> data = new ArrayList<MenuModel>();
		
		for(JMenu jMenu : result) {
			if(logger.isDebugEnabled()) {
				logger.debug("\t-->"+jMenu);
			}
			
			MenuModel model = new MenuModel(jMenu);
			map.put(model.getObjectId(), model);
			
			JMenu parent = jMenu.getParent();
			if(parent==null) {
				data.add(model);
				continue;
			}
			
			MenuModel pm = map.get(parent.getObjectId());
			if(pm==null) {
				data.add(model);
				continue;
			}
			pm.addChild(model);
		}
		
		Collections.sort(data);
		JResultModel rmodel = new JResultModel();
		rmodel.addConfig(JObject.A_CID,JMetaKeys.JMENU);
		rmodel.setData(data);
		return rmodel;
	}

}
