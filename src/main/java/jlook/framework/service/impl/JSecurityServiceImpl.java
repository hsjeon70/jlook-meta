package jlook.framework.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JGroup;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.security.JBaseUser;
import jlook.framework.infrastructure.security.JGrantedAuthority;
import jlook.framework.infrastructure.security.JGroupEntity;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JSecurityService.ID)
public class JSecurityServiceImpl implements JSecurityService, UserDetailsService {
	private static Log logger = Log.getLog(JSecurityServiceImpl.class);
	
	
	@Resource(name=JContextFactory.ID)
	private JContextFactory jContextFactory;
	
	@Resource(name=JUserService.ID)
	private JUserService jUserService;
	
	public static final JUserEntity ANNONYMOUS = new JUserEntity(0L, "anonymous", "", new ArrayList<GrantedAuthority>());
	
	@Override
	public JUserEntity getUserEntity() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		JBaseUser entity;
		try {
			entity = jContext.getUserEntity();
		} catch (JContextException e) {
			throw new JSecurityException("Cannot get UserEntity.",e);
		}
		
		if(entity==null) {
			return ANNONYMOUS;
		}
		if(entity instanceof JUserEntity) {
			return (JUserEntity)entity;
		}
		throw new JSecurityException("Invalid UserEntity. - "+entity);		
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		if(logger.isInfoEnabled())
			logger.info("loadUserByUsername--> username="+username);
		JUser jUser = null;
		if(username==null||username.equals("")) {
			throw new UsernameNotFoundException("Invalid username - "+username);
		}
		try {
			jUser = jUserService.findByEmail(username);
		} catch(JObjectNotFoundException e) {
			throw new UsernameNotFoundException("Cannot find user. - "+username);
		} catch (JServiceException e) {
			throw new RuntimeException("Fail to select user.",e);
		}
		
		JGroup jGroup = jUser.getJGroup();
			
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		// add user roles
		Set<JRole> roles = jUser.getJRoles();
		if(roles!=null) {
			for(JRole role : roles) {
				authorities.add(new JGrantedAuthority(role.getName(), role.getObjectId()));
			}
		}
		
		JUserEntity entity = new JUserEntity(jUser.getObjectId(),jUser.getEmail(),jUser.getPassword(),authorities);
		entity.setNickname(jUser.getNickname());
		entity.setDomainId(jUser.getDomainId());
		if(jGroup!=null) {
			JGroupEntity group = new JGroupEntity();
			group.setObjectId(jGroup.getObjectId());
			group.setDomainId(jGroup.getDomainId());
			group.setName(jGroup.getName());
			entity.setJGroup(group);
			// add group roles
			roles = jGroup.getJRoles();
			if(roles!=null) {
				for(JRole role : roles) {
					authorities.add(new GrantedAuthorityImpl(role.getName()));
				}
			}
		}
		
		Set<JDomain> domains = jUser.getJDomains();
		for(JDomain jDomain : domains) {
			if(!entity.hasDomain(jDomain.getObjectId()) && jDomain.isActive()) {
				entity.putDomain(jDomain.getObjectId(), jDomain.getName());
			}
		}
		
		JContext jContext = this.jContextFactory.getJContext();
		try {
			jContext.putUserEntity(entity);
		} catch (JContextException e) {
			throw new RuntimeException("Fail to put UserEntity to Context.",e);
		}
		return entity;
	}

	@Override
	public Long getDomainId() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		Long domainId = jContext.getDomainId();
		if(domainId==null) {
			throw new JSecurityException("domainId is null.");
		}
		return domainId;
	}

	@Override
	public String getClientType() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		String clientType = jContext.getClientType();
		if(clientType==null) {
			throw new JSecurityException("ClientType is null.");
		}
		return clientType;
	}
}

