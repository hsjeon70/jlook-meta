package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.Set;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JArrayDetail;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JArrayRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JArrayService;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JArrayService.ID)
public class JArrayServiceImpl implements JArrayService {

	@Resource(name=JArrayRepository.ID)
	private JArrayRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long create(JArray jArray) throws JServiceException {
		// TODO primary key
//		JArray tmp;
//		try {
//			tmp = repository.selectByName(jArray.getName());
//			if(tmp!=null) {
//				throw new JDuplicatedObjectException(new Object[]{"JArray", jArray.getName()});
//			}
//		} catch (JRepositoryException e) {
//			throw new JServiceException("Fail to create JArray("+jArray.getName()+")",e);
//		}
		
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jArray.setClassId(JMetaKeys.JARRAY);
		jArray.setDomainId(domainId);
		jArray.setCreatedBy(entity.getObjectId());
		jArray.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jArray);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JArray.",e);
		}
		
		Set<JArrayDetail> details = jArray.getValues();
		for(JArrayDetail detail : details) {
			detail.setClassId(JMetaKeys.JARRAY);
			detail.setDomainId(domainId);
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			try {
				this.repository.insert(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to create JArrayDetail.",e);
			}
		}
		
		return jArray.getObjectId();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(JArray jArray) throws JServiceException {
		try {
			this.repository.deleteAll(jArray.getValues());
			this.repository.delete(jArray);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to remove JArray.",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remove(Long objectId) throws JServiceException {
		try {
			JArray jArray = this.repository.select(objectId);
			if(jArray==null) {
				throw new JObjectNotFoundException(new Object[]{"JArray", objectId});
			}
			remove(jArray);
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to remove JArray("+objectId+")",e);
		}
		
	}

	@Override
	public JArray getJArray(Long objectId) throws JServiceException {
		try {
			JArray jArray = this.repository.select(objectId);
			if(jArray==null) {
				throw new JObjectNotFoundException(new Object[]{"JArray", objectId});
			}
			return jArray;
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to remove JArray("+objectId+")",e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long create(String name, String description, String[] values) throws JServiceException {
		if(values==null || values.length==0) {
			return null;
		}
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
//		JArray tmp;
//		try {
//			tmp = repository.selectByName(name);
//			if(tmp!=null) {
//				throw new JDuplicatedObjectException(new Object[]{"JArray", name});
//			}
//		} catch (JRepositoryException e) {
//			throw new JServiceException("Fail to create JArray("+name+")",e);
//		}
		
		JArray jArray = new JArray();
		jArray.setName(name);
		jArray.setDescription(description);
		
		jArray.setClassId(JMetaKeys.JARRAY);
		jArray.setDomainId(domainId);
		jArray.setCreatedBy(entity.getObjectId());
		jArray.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jArray);
		} catch(JRepositoryException e) {
			throw new JServiceException("Fail to create JArray("+name+")", e);
		}
		for(String value : values) {
			JArrayDetail detail = new JArrayDetail();
			detail.setValue(value);
			detail.setClassId(JMetaKeys.JARRAY);
			detail.setDomainId(domainId);
			detail.setCreatedBy(entity.getObjectId());
			detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			detail.setJArray(jArray);
			
			try {
				this.repository.insert(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to create JArrayDetail("+name+")", e);
			}
		}
		return jArray.getObjectId();
	}
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void update(JArray jArray) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		jArray.setUpdatedBy(entity.getObjectId());
		jArray.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		
		Set<JArrayDetail> values = jArray.getValues();
		for(JArrayDetail detail : values) {
			if(detail.getObjectId()==null) {
				detail.setClassId(JMetaKeys.JARRAYDETAIL);
				detail.setDomainId(security.getDomainId());
				detail.setCreatedBy(entity.getObjectId());
				detail.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			} else {
				detail.setUpdatedBy(entity.getObjectId());
				detail.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			}
			
			try {
				this.repository.update(detail);
			} catch (JRepositoryException e) {
				throw new JServiceException("Fail to save JArrayDetail. - "+detail,e);
			}
		}
		try {
			this.repository.update(jArray);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to save JArray. - "+jArray,e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeAll(Set<JArrayDetail> values) throws JServiceException {
		try {
			this.repository.deleteAll(values);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to delete JArrayDetails. - "+values,e);
		}
	}

}
