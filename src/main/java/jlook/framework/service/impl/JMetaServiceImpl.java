package jlook.framework.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JObject;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.metadata.InheritanceType;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JView;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.hibernate.JSessionFactoryBean;
import jlook.framework.infrastructure.tools.HibernateGenerator;
import jlook.framework.infrastructure.tools.JavaClassGenerator;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JMetaRepository;
import jlook.framework.service.JClassService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JMetaException;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JViewService;
import jlook.framework.service.util.JObjectUtil;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JMetaService.ID)
public class JMetaServiceImpl implements JMetaService {
	private static Log logger = Log.getLog(JMetaServiceImpl.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JMetaRepository.ID)
	private JMetaRepository repository;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=HibernateGenerator.ID)
	private HibernateGenerator hibernateGen;
	
	@Resource(name=JViewService.ID)
	private JViewService jViewService;
	
	@Resource(name=JavaClassGenerator.ID)
	private JavaClassGenerator javaSourceGen;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void mapEntity(Long classId) throws JServiceException {
		JClass jClass = this.jClassService.getJClass(classId);
		String status = jClass.getStatus();
		if(JMetaStatus.CREATED_TYPE.equals(status)) {
			throw new JMetaException("Already created entity. {0} - {1}", new Object[]{classId, "mapEntity"});
		}
		
		Class<?> target = null;
		try {
			target = javaSourceGen.mapEntity(jClass);
		} catch (Exception e) {
			jClass.setStatus(JMetaStatus.MAPERROR_TYPE);
			this.jClassService.update(jClass);
			throw new JServiceException("Fail to generate java source for JClass. - "+jClass,e);
		}
		
		Long domainId = jClass.getDomainId();
		JDomain jDomain = jDomainService.getJDomain(domainId);
		try {
			hibernateGen.mapEntity(jDomain, jClass, target);
		} catch (Exception e) {
			jClass.setStatus(JMetaStatus.MAPERROR_TYPE);
			this.jClassService.update(jClass);
			throw new JServiceException("Fail to execute template engine. classId={0}", e, new Object[]{classId});
		} 
		try {
			this.createJView(jClass);
			jClass.setStatus(JMetaStatus.CREATED_TYPE);
			Map<String,JAttribute> jattMap = jClass.getJAttributes();
			for(JAttribute jatt : jattMap.values()) {
				jatt.setStatus(JMetaStatus.CREATED_TYPE);
			}
		} catch(JServiceException e) {
			jClass.setStatus(JMetaStatus.MAPERROR_TYPE);
			throw new JServiceException("Fail to create JView for JClass. classId={0}", e, new Object[]{classId});
		} finally {
			this.jClassService.update(jClass);
		}
	}

	@Override
	public Map<String,JAttribute> getAllAttributes(JClass jClass)
			throws JServiceException {
		Map<String,JAttribute> jattMap = jClass.getJAttributes();
		JClass parent = jClass.getInheritanceJClass();
		if(parent!=null || InheritanceType.EXTENDS_TYPE.equals(jClass.getInheritanceType())) {
			Map<String,JAttribute> paMap = getAllAttributes(parent);
			jattMap.putAll(paMap);
		}
		return jattMap;
	}

	@Override
	public Class<?> getEntityClass(JClass jClass) throws JServiceException {
		if( !JMetaStatus.CREATED_TYPE.equals(jClass.getStatus()) && !JMetaStatus.CHANGED_TYPE.equals(jClass.getStatus()) ) {
			throw new JServiceException("Cannot get entity class for JClass that was not created. {0} - {1}", new Object[]{jClass.getObjectId(), jClass.getStatus()});
		}
		
		String className = JObjectUtil.getClassName(jClass);
		try {
			return this.javaSourceGen.getClass(className);
		} catch (JException e) {
			throw new JServiceException("Cannot find class. - "+className, e);
		}
	}

	@Override
	public JObject newInstance(JClass jClass) throws JServiceException {
		Class<?> clazz = this.getEntityClass(jClass);
		try {
			Object obj = clazz.newInstance();
			if(obj instanceof JObject) {
				return (JObject)obj;
			} else {
				throw new JServiceException("Invalid domain object. - "+jClass.getName());
			}
		} catch (InstantiationException e) {
			throw new JServiceException("Cannot instanciate domain object. - "+e.getMessage(),e);
		} catch (IllegalAccessException e) {
			throw new JServiceException("Cannot instanciate domain object. - "+e.getMessage(),e);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void createJView(JClass jClass) throws JServiceException {
		this.createDetailView(jClass);
		this.createSummaryView(jClass);
		this.createTitleView(jClass);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void createDetailView(JClass jClass) throws JServiceException {
		Map<String,JAttribute> jattMap = this.getAllAttributes(jClass);
		List<JAttribute> jattList = new ArrayList<JAttribute>(jattMap.values());
		Collections.sort(jattList);
		
		List<JAttribute> detailList = new ArrayList<JAttribute>();
//		detailList.add(jObjMap.get(JObject.A_OID));
//		detailList.add(jObjMap.get(JObject.A_CID));
//		detailList.add(jObjMap.get(JObject.A_DID));
		detailList.addAll(jattList);
//		detailList.add(jObjMap.get(JObject.A_CREATED_BY));
//		detailList.add(jObjMap.get(JObject.A_CREATED_ON));
//		detailList.add(jObjMap.get(JObject.A_UPDATED_BY));
//		detailList.add(jObjMap.get(JObject.A_UPDATED_ON));
		
		String viewName = JViewType.detail.getSystemViewName(jClass);
		this.jViewService.create(viewName, viewName, jClass, JViewType.detail, detailList);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void createSummaryView(JClass jClass) throws JServiceException {
		Map<String,JAttribute> jattMap = this.getAllAttributes(jClass);
		List<JAttribute> jattList = new ArrayList<JAttribute>(jattMap.values());
		Collections.sort(jattList);
		
		List<JAttribute> summaryList = new ArrayList<JAttribute>();
//		summaryList.add(jObjMap.get(JObject.A_OID));
//		summaryList.add(jObjMap.get(JObject.A_CID));
//		summaryList.add(jObjMap.get(JObject.A_DID));
		for(JAttribute jatt : jattList) {
			if(jatt.isPrimary() || jatt.isSearchable()) {
				summaryList.add(jatt);
			}
		}
//		summaryList.add(jObjMap.get(JObject.A_CREATED_BY));
//		summaryList.add(jObjMap.get(JObject.A_CREATED_ON));
		
		String viewName = JViewType.summary.getSystemViewName(jClass);
		this.jViewService.create(viewName, viewName, jClass, JViewType.summary, summaryList);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void createTitleView(JClass jClass) throws JServiceException {
		Map<String,JAttribute> jattMap = this.getAllAttributes(jClass);
		List<JAttribute> jattList = new ArrayList<JAttribute>(jattMap.values());
		Collections.sort(jattList);
		
		List<JAttribute> titleList = new ArrayList<JAttribute>();
//		titleList.add(jObjMap.get(JObject.A_OID));
//		titleList.add(jObjMap.get(JObject.A_CID));
		
		for(JAttribute jatt : jattList) {
			if(jatt.isPrimary()) {
				titleList.add(jatt);
			}
		}
		
		String viewName = JViewType.title.getSystemViewName(jClass);
		this.jViewService.create(viewName, viewName, jClass, JViewType.title, titleList);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void unmapEntity(Long classId) throws JServiceException {
		JClass jClass = this.jClassService.getJClass(classId);
		String status = jClass.getStatus();
		if(JMetaStatus.NONE_TYPE.equals(status)) {
			throw new JMetaException("Cannot unmap JClass because status is '"+JMetaStatus.NONE_TYPE+"'. {0} - {1}", new Object[]{classId, "unmapEntity"});
		}
		
		// delete JView
		this.removeJView(jClass);
		
		// session.createQuery("delete popolamento.Spectra").executeUpdate();
		Long domainId = jClass.getDomainId();
		JDomain jDomain = jDomainService.getJDomain(domainId);
		Class<?> target = this.getEntityClass(jClass);
		try {
			hibernateGen.unmapEntity(jDomain, jClass, target);
		} catch (JException e) {
			jClass.setStatus(JMetaStatus.MAPERROR_TYPE);	// TODO MAPERROR?????
			this.jClassService.update(jClass);
			throw new JServiceException("Fail to unmap hibernate config for JClass. - "+jClass,e);
		}
		
		try {
			javaSourceGen.unmapEntity(jClass);
			jClass.setStatus(JMetaStatus.UNMAPPED_TYPE);
		} catch (JException e) {
			jClass.setStatus(JMetaStatus.MAPERROR_TYPE);
			throw new JServiceException("Fail to unmap java class for JClass. {0}", e, new Object[]{classId});
		} finally {
			this.jClassService.update(jClass);
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeJView(JClass jClass) throws JServiceException {
		this.removeDetailView(jClass);
		this.removeSummaryView(jClass);
		this.removeTitleView(jClass);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeDetailView(JClass jClass) throws JServiceException {
		try {
			JView jView = this.jViewService.getSystemJView(jClass, JViewType.detail);
			this.jViewService.remove(jView);
		} catch(JObjectNotFoundException e) {
			logger.warn("JView was already removed. JClass="+jClass+" - "+e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeSummaryView(JClass jClass) throws JServiceException {
		try {
			JView jView = this.jViewService.getSystemJView(jClass, JViewType.summary);
			this.jViewService.remove(jView);
		} catch(JObjectNotFoundException e) {
			logger.warn("JView was already removed. JClass="+jClass+" - "+e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removeTitleView(JClass jClass) throws JServiceException {
		try {
			JView jView = this.jViewService.getSystemJView(jClass, JViewType.title);
			this.jViewService.remove(jView);
		} catch(JObjectNotFoundException e) {
			logger.warn("JView was already removed. JClass="+jClass+" - "+e.getMessage());
		}
	}

	@Override
	public void updateDBSchema() throws JServiceException {
		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		Configuration config = bean.getConfiguration();
		new SchemaUpdate(config).execute(true, true);
		
		bean.validateDatabaseSchema();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void recreateJView(JClass jClass) throws JServiceException {
		List<JView> list = this.jViewService.getAllJViewList(jClass);
		for(JView jView : list) {
			this.jViewService.remove(jView);
		}
		this.createJView(jClass);
	}

	@Override
	public void buildMeta() throws JServiceException {
		// TODO Auto-generated method stub
		
	}
}
