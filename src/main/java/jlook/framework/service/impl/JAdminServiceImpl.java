package jlook.framework.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserRole;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.JRoleType;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.service.JAdminService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JRoleService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;

@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JAdminService.ID)
public class JAdminServiceImpl implements JAdminService {

	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JUserService.ID)
	private JUserService jUserService;
	
	@Resource(name=JRoleService.ID)
	private JRoleService jRoleService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void register(JDomain jDomain, JUser jUser) throws JServiceException {
		Long domainId = jDomainService.create(jDomain);
		
		
		JContextFactory factory = JContextFactory.newInstance();
		JContext context = factory.getJContext();
		context.putDomainId(domainId);
		
		jUserService.create(jUser);
		
		JRole role = jRoleService.findByName(JRoleType.ADMIN.getName());
		JUserRole jUserRole = new JUserRole();
		jUserRole.setJUser(jUser);
		jUserRole.setJRole(role);
		this.jUserService.create(jUserRole);
	}

}
