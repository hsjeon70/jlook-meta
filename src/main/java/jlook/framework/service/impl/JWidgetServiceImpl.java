package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetInstance;
import jlook.framework.domain.widget.JWidgetParamName;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JWidgetRepository;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JWidgetService;

@Service(JWidgetService.ID)
public class JWidgetServiceImpl implements JWidgetService {

	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JWidgetRepository.ID)
	private JWidgetRepository repository;
	
	@Override
	public void create(JWidgetDefinition jWidgetDefinition)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jWidgetDefinition.setClassId(JMetaKeys.JWIDGETDEFINITION);
		jWidgetDefinition.setDomainId(domainId);
		jWidgetDefinition.setCreatedBy(entity.getObjectId());
		jWidgetDefinition.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jWidgetDefinition);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JWidgetDefinition. - "+jWidgetDefinition,e);
		}
	}

	@Override
	public void create(JWidgetParamName jWidgetParamName)
			throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		Long domainId = security.getDomainId();
		
		jWidgetParamName.setClassId(JMetaKeys.JWIDGETPARAMNAME);
		jWidgetParamName.setDomainId(domainId);
		jWidgetParamName.setCreatedBy(entity.getObjectId());
		jWidgetParamName.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		try {
			this.repository.insert(jWidgetParamName);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JWidgetDefinition. - "+jWidgetParamName,e);
		}
	}

	@Override
	public JWidgetInstance getWidgetInstance(Long objectId)
			throws JServiceException {
		JWidgetInstance jwi;
		try {
			jwi = this.repository.selectInstance(objectId);
			if(jwi==null) {
				throw new JObjectNotFoundException(new Object[]{"JWidgetInstance", "objectId="+objectId});
			}
			return jwi;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JWidgetInstance. - "+objectId, e);
		}
	}

}
