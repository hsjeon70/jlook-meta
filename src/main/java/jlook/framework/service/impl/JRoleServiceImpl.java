package jlook.framework.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.repository.JRoleRepository;
import jlook.framework.service.JDuplicatedObjectException;
import jlook.framework.service.JObjectNotFoundException;
import jlook.framework.service.JRoleService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true, rollbackForClassName={GlobalKeys.SERVICE_EXCEPTION})
@Service(JRoleService.ID)
public class JRoleServiceImpl implements JRoleService {

	@Resource(name=JRoleRepository.ID)
	private JRoleRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void create(JRole role) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		try {
			JRole tmp = this.repository.selectByName(role.getName());
			if(tmp!=null) {
				throw new JDuplicatedObjectException(new Object[]{"JRole",role.getName()});
			}
			
			role.setClassId(JMetaKeys.JROLE);
			role.setDomainId(security.getDomainId());
			role.setCreatedBy(entity.getObjectId());
			role.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			
			this.repository.insert(role);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JRole.("+role.getName()+")",e);
		}
	}

	@Override
	public JRole findByName(String name) throws JServiceException {
		try {
			JRole jRole = this.repository.selectByName(name);
			if(jRole==null) {
				throw new JObjectNotFoundException(new Object[]{"JRole",name});
			}
			return jRole;
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to get JRole.("+name+")",e);
		}
	}

}
