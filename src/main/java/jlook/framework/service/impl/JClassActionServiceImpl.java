package jlook.framework.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JClassActionRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JClassActionService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

@Service(JClassActionService.ID)
public class JClassActionServiceImpl implements JClassActionService {

	@Resource(name=JClassActionRepository.ID)
	private JClassActionRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	public List<JClassAction> findByJClass(JClass jClass, JClassActionType type)
			throws JServiceException {
		try {
			return this.repository.selectByJClass(jClass, type);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to find JClassAction list by JClass. - "+jClass+", type="+type, e);
		}
	}

	@Override
	public void create(JClassAction jClassAction) throws JServiceException {
		JUserEntity entity = security.getUserEntity();
		
		jClassAction.setClassId(JMetaKeys.JCLASSACTION);
		jClassAction.setDomainId(security.getDomainId());
		jClassAction.setCreatedBy(entity.getObjectId());
		jClassAction.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		try {
			this.repository.insert(jClassAction);
		} catch (JRepositoryException e) {
			throw new JServiceException("Fail to create JClassAction. - "+jClassAction, e);
		}
	}

}
