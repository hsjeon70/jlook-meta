package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.common.JAnnounceMessage;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.util.PageUtil;


public interface JAnnounceService {
	public static final String ID = "jAnnounceService";
	
	public void create(JAnnounceMessage message) throws JServiceException;
	public void update(JAnnounceMessage message) throws JServiceException;
	public void remove(Long objectId) throws JServiceException;
	public JResultModel getDetailData(Long objectId) throws JServiceException;
	public JSummaryModel getSummaryData(PageUtil page) throws JServiceException;
	public List<JAnnounceMessage> getJAnnounceMessageList() throws JServiceException;
}
