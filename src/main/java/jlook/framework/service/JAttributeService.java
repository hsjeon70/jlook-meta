package jlook.framework.service;

import jlook.framework.domain.metadata.JAttribute;

public interface JAttributeService {
	public static final String ID = "jAttributeService";
	
	public void create(JAttribute jatt) throws JServiceException;
	public void remove(JAttribute jatt) throws JServiceException;
	public void update(JAttribute jatt) throws JServiceException;
}
