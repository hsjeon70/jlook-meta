package jlook.framework.service;

import java.util.List;
import java.util.Map;

import jlook.framework.domain.metadata.JClass;


public interface JClassService {
	public static final String ID = "jClassService";
	
	public void create(JClass jClass) throws JServiceException;
	public JClass findByName(String name) throws JServiceException;
	public JClass getJClass(Long objectId) throws JServiceException;
	
	public List<JClass> getJClassList() throws JServiceException;
	
	public void remove(String name) throws JServiceException;
	public void update(JClass jClass) throws JServiceException;
	
	public Map<Long,String> getReferences(Long classId) throws JServiceException;
}	
