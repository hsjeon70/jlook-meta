package jlook.framework.service;

import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.model.JResultModel;

public interface JMenuService {
	public static final String ID = "jMenuService";
	
	public JResultModel getMyMenuList() throws JServiceException;
	public JResultModel getPublicMenuList() throws JServiceException;
	
	public void create(JMenu jMenu) throws JServiceException;
}
