package jlook.framework.service;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JValidationException.TEXT)
public class JValidationException extends JServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TEXT = "Invalid value. {0} - {1}";
	
	public JValidationException(Object[] parameters) {
		super(TEXT,parameters);
	}
	
	public JValidationException(String msg, Object[] parameters) {
		super(msg, parameters);
	}
	
	public JValidationException(Throwable cause, Object... parameters) {
		super(cause, parameters);
	}
	
	public JValidationException(String msg, Throwable cause, Object[] parameters) {
		super(msg, cause, parameters);
	}
}
