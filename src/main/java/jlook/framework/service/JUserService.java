package jlook.framework.service;

import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserDomain;
import jlook.framework.domain.account.JUserRole;

public interface JUserService {
	public static final String ID = "jUserService";
	
	public Long create(JUser jUser) throws JServiceException;
	public JUser findByEmail(String email) throws JServiceException;
	public void remove(String email) throws JServiceException;
	
	public void create(JUserRole jUserRole) throws JServiceException;
	public void create(JUserDomain jUserDomain) throws JServiceException;
	
}
