package jlook.framework.service;

import java.util.List;

import jlook.framework.domain.account.JDomain;


public interface JDomainService {
	public static final String ID = "jDomainService";
	
	public Long create(JDomain jDomain) throws JServiceException;
	public JDomain getJDomain(Long id) throws JServiceException;
	public JDomain findByName(String name) throws JServiceException;
	public List<JDomain> getAllJDomainList() throws JServiceException;
}
