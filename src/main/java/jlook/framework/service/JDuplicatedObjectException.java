package jlook.framework.service;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JDuplicatedObjectException.TEXT)
public class JDuplicatedObjectException extends JServiceException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TEXT = "Already exist {0} - {1}";
	
	public  JDuplicatedObjectException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JDuplicatedObjectException(String msg, Object[] parameters) {
		super(msg,parameters);
	}
	
	public JDuplicatedObjectException(String msg, Throwable cause, Object[] parameters) {
		super(msg, cause, parameters);
	}
}
