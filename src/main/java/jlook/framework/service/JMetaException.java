package jlook.framework.service;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JMetaException.TEXT)
public class JMetaException extends JServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TEXT = "Meta Error find {0}. - {1}";
	
	public JMetaException(Object[] parameters) {
		super(TEXT,parameters);
	}
	
	public JMetaException(String msg, Object[] parameters) {
		super(msg, parameters);
	}
	
	public JMetaException(Throwable cause, Object... parameters) {
		super(cause, parameters);
	}
	
	public JMetaException(String msg, Throwable cause, Object[] parameters) {
		super(msg, cause, parameters);
	}
}
