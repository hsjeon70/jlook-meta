package jlook.framework.service;

import jlook.framework.infrastructure.security.JUserEntity;

import org.springframework.security.core.userdetails.UserDetails;


public interface JSecurityService {
	public static final String ID = "jSecurityService";
	
	public static final String SESSION_KEY = "SPRING_SECURITY_CONTEXT";
	
	public JUserEntity getUserEntity() throws JSecurityException;
	public Long getDomainId() throws JSecurityException;
	public String getClientType() throws JSecurityException;
	
	public UserDetails loadUserByUsername(String username) throws JServiceException;
	
	
}
