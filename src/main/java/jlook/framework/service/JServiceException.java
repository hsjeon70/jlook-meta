package jlook.framework.service;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.annotation.Message;

@Message(text=JServiceException.TEXT)
public class JServiceException extends JException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Service Error";
	
	public JServiceException(String message) {
		super(message);
	}
	public JServiceException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JServiceException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JServiceException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JServiceException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JServiceException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
