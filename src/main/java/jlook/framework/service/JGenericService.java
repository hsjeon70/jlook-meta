package jlook.framework.service;

import java.util.Map;

import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.util.ExcelReader;
import jlook.framework.infrastructure.util.PageUtil;


public interface JGenericService {
	public static final String ID = "jGenericService";
	
	public JSummaryModel getSummaryData(JViewType jViewType, Long classId, PageUtil page, Map<String,String[]> paramMap) throws JServiceException;
	public JResultModel create(Long classId, Map<String,String[]> params) throws JServiceException;
	public JResultModel update(Long classId, Long objectId, Map<String,String[]> params) throws JServiceException;
	public void remove(Long classId, Long objectId) throws JServiceException;
	public JResultModel getDetailData(JViewType jViewType, Long classId, Long objectId, Long parentCid, Long parentOid) throws JServiceException;
	public JResultModel getTitleData(Long classId, Long objectId) throws JServiceException;
	
	public JResultModel getSearchForm(Long classId, Long objectId, Long parentCid, Long parentOid) throws JServiceException;
	
	public JResultModel create(JObject jObject) throws JServiceException;
	public JResultModel update(JObject jObject) throws JServiceException;
	
	// TODO : move to JMetaService ? 
	public JObject getJObject(Long classId, Long objectId) throws JServiceException;
	
	public JResultModel importExcel(Long classId, ExcelReader reader) throws JServiceException;
	
	public JResultModel getWidgetResultMode(Long widgetId, Long parentCid, Long parentOid, Map<String,String[]> params) throws JServiceException;
	
}
