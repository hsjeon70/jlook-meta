package jlook.framework.interfaces;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.domain.JClassType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.metadata.InheritanceType;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JTypeService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class JDemoController {
	private static Log logger = Log.getLog(JDemoController.class);
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JTypeService.ID)
	private JTypeService jDataTypeService;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	private String USERINFO_ENAME = "USERINFO";
	
	
	@RequestMapping(value = "/demo/meta/userinfo", method = RequestMethod.GET)
	public String loadMetaUserInfo() throws JServiceException {
		JClass jUserClass = jClassService.findByName(JUser.NAME);
		
		JClass jClass = new JClass();
		jClass.setClassId(JMetaKeys.JCLASS);
		jClass.setName(USERINFO_ENAME);
		jClass.setDescription("BUSPIA USER INFORMATION");
		jClass.setCategory("domain");
		jClass.setInheritanceJClass(jUserClass);
		jClass.setInheritanceType(InheritanceType.EXTENDS_TYPE);
		jClass.setType(JClassType.CONCRETE_TYPE);
		//jClass.setStatus(JClassStatus.CREATED.name());
		jGenericService.create(jClass);
		
		JAttribute jatt = null;
		jatt = create("address",JPrimitive.STRING_TYPE,100, false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("hobby",JMetaKeys.JArray.getName(),15, false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		if(logger.isDebugEnabled()) {
			logger.debug("### --> "+jClass.getJAttributes());
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/demo/meta/emp", method = RequestMethod.GET)
	public String loadMetaEmp() throws JServiceException {
		JClass jClass = new JClass();
		jClass.setClassId(JMetaKeys.JCLASS);
		jClass.setName("DEPT");
		jClass.setDescription("DEPT ENTITY");
		jClass.setLabel("Dept");
		jClass.setCategory("domain");
		jClass.setType(JClassType.CONCRETE_TYPE);
		jGenericService.create(jClass);
		
		JAttribute jatt = null;
		jatt = create("DEPTNO",JPrimitive.INTEGER_TYPE,2,true,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("DNAME",JPrimitive.STRING_TYPE,14,false,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("LOC",JPrimitive.STRING_TYPE,13,false,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jClass = new JClass();
		jClass.setClassId(JMetaKeys.JCLASS);
		jClass.setName("EMP");
		jClass.setDescription("EMP ENTITY");
		jClass.setLabel("Emp");
		jClass.setCategory("domain");
		jClass.setType(JClassType.CONCRETE_TYPE);
		jGenericService.create(jClass);
		
		jatt = create("EMPNO",JPrimitive.INTEGER_TYPE,4,true,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("ENAME",JPrimitive.STRING_TYPE,10,false,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("JOB",JPrimitive.STRING_TYPE,9,false,true);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("MGR","EMP",15,false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("HIREDATE",JPrimitive.DATE_TYPE,null,false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("SAL",JPrimitive.INTEGER_TYPE,10,false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("COMM",JPrimitive.INTEGER_TYPE,10,false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		jatt = create("DEPT","DEPT",15,false,false);
		jatt.setJClass(jClass);
		jGenericService.create(jatt);
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/demo/data/emp", method = RequestMethod.GET)
	public String loadDataEmp() throws JServiceException {
		JClass jclsDept = jClassService.findByName("DEPT");
		JClass jclsEmp  = jClassService.findByName("EMP");
		
		Long deptOID = jclsDept.getObjectId();
		Long empOID  = jclsEmp.getObjectId();
		
		this.jGenericService.create(deptOID, makeDeptMap("10","ACCOUNTING",	"NEW YORK"));
		this.jGenericService.create(deptOID, makeDeptMap("20","RESEARCH",	"DALLAS"));
		this.jGenericService.create(deptOID, makeDeptMap("30","SALES",		"CHICAGO"));
		this.jGenericService.create(deptOID, makeDeptMap("40","OPERATIONS",	"BOSTON"));

		this.jGenericService.create(empOID, makeEmpMap("7839","KING",	"PRESIDENT",null, 	"12/17/1980", "5000", null, "1"));		// 9
		this.jGenericService.create(empOID, makeEmpMap("7698","BLAKE",	"MANAGER", 	"1", "12/17/1980", "2850", null, "3"));		// 6
		this.jGenericService.create(empOID, makeEmpMap("7782","CLARK",	"MANAGER", 	"1", "12/17/1980", "2450", null, "1"));		// 7
		this.jGenericService.create(empOID, makeEmpMap("7566","JONES",	"MANAGER", 	"1", "12/17/1980", "2975", null, "2"));		// 4
		this.jGenericService.create(empOID, makeEmpMap("7788","SCOTT",	"ANALYST", 	"4", "12/17/1980", "3000", null, "2"));		// 8
		this.jGenericService.create(empOID, makeEmpMap("7902","FORD",	"ANALYST", 	"4", "12/17/1980", "3000", null, "2"));		//13
		this.jGenericService.create(empOID, makeEmpMap("7499","ALLEN",	"SALESMAN", "2", "02/20/1981", "1600", "30", "3"));		// 2
		this.jGenericService.create(empOID, makeEmpMap("7521","WARD",	"SALESMAN", "2", "02/22/1981", "1250", "500", "3"));	// 3
		this.jGenericService.create(empOID, makeEmpMap("7654","MARTIN","SALESMAN", "2", "12/17/1980", "1250", "1400", "3"));	// 5
		this.jGenericService.create(empOID, makeEmpMap("7900","JAMES",	"CLERK", 	"2", "12/17/1980", "950", null, "3"));		//12
		this.jGenericService.create(empOID, makeEmpMap("7876","ADAMS",	"CLERK", 	"5", "12/17/1980", "1100", null, "2"));		//11
		this.jGenericService.create(empOID, makeEmpMap("7934","MILLER","CLERK", 	"3", "12/17/1980", "1300", null, "1"));		//14
		this.jGenericService.create(empOID, makeEmpMap("7369","SMITH",	"CLERK", 	"6", "12/17/1980", "800", null, "2"));		// 1
		this.jGenericService.create(empOID, makeEmpMap("7844","TURNER","SALESMAN", "6", "12/17/1980", "1500", "0", "3"));		//10
		
		return "redirect:/";
	}
	
	private Map<String,String[]> makeEmpMap(String empno, String ename, String job, String mgr, String hiredate, String sal, String comm, String dept) {
		Map<String,String[]> params = new HashMap<String,String[]>();
		params.put("EMPNO", 	new String[]{empno});
		params.put("ENAME", 	new String[]{ename});
		params.put("JOB", 		new String[]{job});
		params.put("MGR", 		new String[]{mgr});
		params.put("HIREDATE", 	new String[]{hiredate});
		params.put("SAL", 		new String[]{sal});
		params.put("COMM",		new String[]{comm});
		params.put("DEPT", 		new String[]{dept});
		return params;
	}
	
	private Map<String,String[]> makeDeptMap(String deptNo, String dname, String loc) {
		Map<String,String[]> params = new HashMap<String,String[]>();
		params.put("DEPTNO", new String[]{deptNo});
		params.put("DNAME", new String[]{dname});
		params.put("LOC", new String[]{loc});
		return params;
	}
	
	private JAttribute create(String name, String type, Integer length, boolean primary, boolean search) throws JServiceException {
		JAttribute jatt = new JAttribute();
		jatt.setClassId(JMetaKeys.JATTRIBUTE);
		jatt.setName(name);
		jatt.setLabel(name);
		JType jdt = jDataTypeService.findByName(type);
		jatt.setType(jdt);
		jatt.setLength(length);
		jatt.setSearchable(search);
		jatt.setPrimary(primary);
		return jatt;
	}
}
