package jlook.framework.interfaces.metadata;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JEnumType;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.JInvalidParamException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish EnumService
 * @author hsjeon70
 *
 */
@Controller
public class JEnumController {

	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * @publish Summary view API for enumeration validation service.
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jenum/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JENUM, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/jenum/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JENUM, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/jenum/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JENUM, objectId);
	}
	
	@RequestMapping(value = "/jenum/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JENUM, objectId);
	}
	
	@RequestMapping(value = "/jenum/create", method = RequestMethod.POST)
	public ModelAndView create(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false)  String contentType,
			@RequestParam(value=JEnum.A_NAME,			required=true)  String name,
			@RequestParam(value=JEnum.A_DESCRIPTION,	required=false) String description,
			@RequestParam(value=JEnum.A_TYPE,			required=true)  String type) throws JControllerException {
		try {
			JEnumType.valueOf(type);
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{JEnum.A_TYPE, type});
		}
		JEnum jEnum = new JEnum();
		jEnum.setClassId(JMetaKeys.JENUM);
		jEnum.setName(name);
		jEnum.setDescription(description);
		jEnum.setType(type);
		try {
			JResultModel rmodel = jGenericService.create(jEnum);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jenum/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JEnum. - "+name,e);
		}
	}
	
	@RequestMapping(value = "/jenum/save", method = RequestMethod.POST)
	public ModelAndView save(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false)  String contentType,
			@RequestParam(value=JEnum.A_OID,			required=true)  Long objectId,
			@RequestParam(value=JEnum.A_NAME,			required=true)  String name,
			@RequestParam(value=JEnum.A_DESCRIPTION,	required=false) String description,
			@RequestParam(value=JEnum.A_TYPE,			required=true)  String type) throws JControllerException {
		try {
			JEnumType.valueOf(type);
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{JEnum.A_TYPE, type});
		}
		JEnum jEnum = new JEnum();
		jEnum.setClassId(JMetaKeys.JENUM);
		jEnum.setName(name);
		jEnum.setDescription(description);
		jEnum.setType(type);
		try {
			JResultModel rmodel = jGenericService.update(jEnum);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jenum/save");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to save JEnum. - "+name,e);
		}
	}
	
	// TODO JEnumDetail API
	@RequestMapping(value = "/jenumdetail/summary", method = RequestMethod.GET)
	public ModelAndView getSummaryJEnumDetails(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=true) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		Long classId = JMetaKeys.JENUMDETAIL;
		Long parentCid = JMetaKeys.JENUM;
		return controller.getSummary(contentType, classId, pageNumber, pageSize, keyword, parentOid, parentCid, request);
	}
	
	@RequestMapping(value = "/jenumdetail/create", method = RequestMethod.POST)
	public ModelAndView createJEnumDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false)  String contentType,
			@RequestParam(value=JEnumDetail.A_VALUE,	required=true)  String value,
			@RequestParam(value=JEnumDetail.A_LABEL,	required=false) String label,
			@RequestParam(value=JEnumDetail.A_SEQUENCE,	required=true)  Integer sequence,
			@RequestParam(value=ParamKeys.PARENT_OID,	required=true)  Long parentOid) throws JControllerException {
	
		JEnumDetail detail = new JEnumDetail();
		detail.setClassId(JMetaKeys.JENUMDETAIL);
		detail.setValue(value);
		detail.setLabel(label);
		detail.setSequence(sequence);
		
		JEnum jEnum = new JEnum();
		jEnum.setObjectId(parentOid);
		detail.setJEnum(jEnum);
		
		try {
			JResultModel rmodel = this.jGenericService.create(detail);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jenumdetail/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JEnumDetail.",e);
		}
		
	}
	
	@RequestMapping(value = "/jenumdetail/save", method = RequestMethod.POST)
	public ModelAndView updateJEnumDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false)  String contentType,
			@RequestParam(value=JEnumDetail.A_VALUE,	required=true)  String value,
			@RequestParam(value=JEnumDetail.A_LABEL,	required=false) String label,
			@RequestParam(value=JEnumDetail.A_SEQUENCE,	required=true)  Integer sequence,
			@RequestParam(value=ParamKeys.PARENT_OID,	required=true)  Long parentOid) throws JControllerException {
	
		JEnumDetail detail = new JEnumDetail();
		detail.setClassId(JMetaKeys.JENUMDETAIL);
		detail.setValue(value);
		detail.setLabel(label);
		detail.setSequence(sequence);
		
		JEnum jEnum = new JEnum();
		jEnum.setObjectId(parentOid);
		detail.setJEnum(jEnum);
		
		try {
			JResultModel rmodel = this.jGenericService.update(detail);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jenumdetail/save");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JEnumDetail.",e);
		}
		
	}
	
	@RequestMapping(value = "/jenumdetail/remove", method = RequestMethod.DELETE)
	public ModelAndView removeJEnumDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JENUMDETAIL, objectId);
	}
}
