package jlook.framework.interfaces.metadata;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JClassService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish ClassService
 * @author hsjeon70
 *
 */
@Controller
public class JClassController {

	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	@Resource(name=JClassService.ID)
	private JClassService service;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	/**
	 * @publish Summary view API for class service.
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jclass/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JCLASS, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/jclass/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JCLASS, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/jclass/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JCLASS, objectId);
	}
	
	@RequestMapping(value = "/jclass/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JCLASS, objectId);
	}
	
	/**
	 * This API creates physical table from entity metadata in database.
	 * 
	 * @publish Creating physical table form entity metadata in database.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jclass/map", method = RequestMethod.POST)
	public ModelAndView mapJClass(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,	required=true) Long classId) throws JControllerException {
		try {
			jMetaService.mapEntity(classId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to map JClass. - "+classId,e);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("jclass/map");
		return mview;
	}
	
	/**
	 * This API drops physical table from entity metadata in database.
	 * 
	 * @publish Dropping physical table form entity metadata in database.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jclass/unmap", method = RequestMethod.POST)
	public ModelAndView unmapJClass(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,	required=true) Long classId) throws JControllerException {
		try {
			jMetaService.unmapEntity(classId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to map JClass. - "+classId,e);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("jclass/unmap");
		return mview;
	}
	
	/**
	 * This API responses incoming reference entity list for an entity.
	 * @publish Responses incoming reference entity list for an entity.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @return incoming reference entity list
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jclass/references", method = RequestMethod.GET)
	public ModelAndView getIncomings(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,	required=true) Long classId) throws JControllerException {
		Map<Long, String> map;
		try {
			map = this.service.getReferences(classId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get references for JClass. - "+classId,e);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("jclass/references");
		mview.addObject(JResultModel.DATA_KEY, map);
		return mview;
	}
}
