package jlook.framework.interfaces.metadata;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class JViewController {
	
	@RequestMapping(value = "/jview/regenerate/{classId}", method = RequestMethod.POST)
	public ModelAndView regenerate(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@PathVariable(ParamKeys.CLASS_ID) Long classId) throws JControllerException {
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("jclass/references");
		return mview;
	}
}
