package jlook.framework.interfaces.account;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jlook.framework.GlobalKeys;
import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.loader.metadata.JPreferenceDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * Domain management Open API
 * 
 * @publish DomainService
 * @author hsjeon70
 *
 */
@Controller
public class JDomainController {
	private static Log logger = Log.getLog(JDomainController.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * 
	 * @publish Summary view API for domain service.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param pageNumber	URL Parameter 	: Current page number 
	 * @param pageSize		URL Parameter	: Display page size
	 * @param keyword		URL Parameter  	: Search word
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	Summary view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/jdomain/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JDOMAIN, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	/**
	 * 
	 * @publish	Detail view API for domain service
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param objectId		URL Parameter 	: ObjectId for entity
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	Detail view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/jdomain/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JDOMAIN, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/jdomain/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JDOMAIN, objectId);
	}
	
	@RequestMapping(value = "/jdomain/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JDOMAIN, objectId);
	}
	
	@RequestMapping(value = "/jdomain/create", method = RequestMethod.DELETE)
	public ModelAndView create(
			@RequestHeader(value=JHeader.JCONTENT_TYPE,  required=false) String contentType,
			@RequestParam(value="name",			required=true) String name,
			@RequestParam(value="description",	required=false) String description) throws JControllerException {
		JDomain jDomain = new JDomain();
		jDomain.setClassId(JMetaKeys.JDOMAIN);
		jDomain.setName(name);
		jDomain.setDescription(description);
		try {
			JResultModel rmodel = this.jGenericService.create(jDomain);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jdomain/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JDomain.",e);
		}
	}
	
	@RequestMapping(value = "/jdomain/update", method = RequestMethod.DELETE)
	public ModelAndView update(
			@RequestHeader(value=JHeader.JCONTENT_TYPE,  required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId,
			@RequestParam(value="name",					required=true) String name,
			@RequestParam(value="description",			required=false) String description) throws JControllerException {
		JDomain jDomain = new JDomain();
		jDomain.setObjectId(objectId);
		jDomain.setClassId(JMetaKeys.JDOMAIN);
		jDomain.setName(name);
		jDomain.setDescription(description);
		try {
			JResultModel rmodel = this.jGenericService.update(jDomain);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jdomain/update");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JDomain.",e);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jdomain/version", method=RequestMethod.GET)
	public ModelAndView getVersion(@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			HttpServletRequest request) throws JControllerException {
		Long domainId;
		try {
			domainId = security.getDomainId();
		} catch (JSecurityException e) {
			throw new JControllerException("Fail to get domainId.", e);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("getVersion --> domainId="+domainId);
		}
		
		JContentType type = JContentType.getJContentType(contentType);
		if(!type.equals(JContentType.JSON)) {
			throw new JControllerException("Not supported Content-Type. This API can support only "+JContentType.JSON_MIME_TYPE+" type.");
		}
		
		try {
			HttpSession session = request.getSession();
			Map<String,String> dpMap = (Map<String,String>)session.getAttribute(GlobalKeys.DOMAIN_PREFERENCES_KEY);
			String version = dpMap.get(JPreferenceDef.DOMAIN_VERSION.instance.getName());
			ModelAndView mview = type.create("domain/version");
			mview.addObject(JResultModel.DATA_KEY,version);
			return mview;
		} catch (Exception e) {
			throw new JControllerException("Fail to get preference 'domain version' from session.", e);
		}
	}
	
}
