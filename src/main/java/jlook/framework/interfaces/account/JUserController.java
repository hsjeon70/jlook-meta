package jlook.framework.interfaces.account;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.account.JGroup;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish UserService
 * @author hsjeon70
 *
 */
@Controller
public class JUserController {

	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * @publish Summary view API for user service.
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/juser/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JUSER, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/juser/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JUSER, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/juser/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JUSER, objectId);
	}
	
	@RequestMapping(value = "/juser/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JUSER, objectId);
	}
	
	@RequestMapping(value = "/juser/create", method = RequestMethod.POST)
	public ModelAndView create(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=JUser.A_NICKNAME,	required=true) String nickname,
			@RequestParam(value=JUser.A_EMAIL,		required=true) String email,
			@RequestParam(value=JUser.A_PASSWORD,	required=true) String password,
			@RequestParam(value=JUser.A_MOBILE,		required=false) String mobile,
			@RequestParam(value=JUser.A_JGROUP,		required=false) Long jGroupId) throws JControllerException {
		
		JUser jUser = new JUser();
		jUser.setClassId(JMetaKeys.JUSER);
		jUser.setNickname(nickname);
		jUser.setEmail(email);
		jUser.setPassword(password);
		jUser.setMobile(mobile);
		
		JGroup jGroup = new JGroup();
		jGroup.setObjectId(jGroupId);
		jUser.setJGroup(jGroup);
		
		try {
			JResultModel rmodel = jGenericService.create(jGroup);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("juser/create");
			mview.addObject(JResultModel.DATA_KEY,rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JUser.",e);
		}
	}
}
