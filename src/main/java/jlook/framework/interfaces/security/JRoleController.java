package jlook.framework.interfaces.security;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * Role management service
 * 
 * @publish RoleService
 * @author hsjeon70
 *
 */
@Controller
public class JRoleController {

	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * @publish	Summary view API for role service.
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jrole/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JROLE, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/jrole/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JROLE, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/jrole/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JROLE, objectId);
	}
	
	@RequestMapping(value = "/jrole/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JROLE, objectId);
	}
	
}
