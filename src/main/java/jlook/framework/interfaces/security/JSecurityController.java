package jlook.framework.interfaces.security;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jlook.framework.ClientType;
import jlook.framework.GlobalKeys;
import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JPreferenceKeys;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.PreferenceModel;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.web.view.JSonView;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.JInvalidParamException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JAttachmentService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * This service provides authentication and user registration service.
 *  
 * @publish SecurityService
 * @author hsjeon70
 *
 */
@Controller
public class JSecurityController {
	private static Log logger = Log.getLog(JSecurityController.class);
	
	@Resource(name=JAttachmentService.ID)
	private JAttachmentService jAttachService;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JUserService.ID)
	private JUserService jUserService;
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService jPreferenceService;
	
	@RequestMapping(value = "/security/info")
	public void info(HttpServletRequest req, HttpServletResponse res) {
		HttpSession hsession = req.getSession();
		Enumeration<?> names = hsession.getAttributeNames();
		while(names.hasMoreElements()) {
			String name = (String)names.nextElement();
			Object value = hsession.getAttribute(name);
			if(logger.isInfoEnabled()) {
				logger.info("\t--> "+name+"="+value);
			}
		}
	}
	
	@RequestMapping(value = "/security/signInFrm")
	public ModelAndView signInFrm(@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			HttpServletRequest request) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("---> signInFrm");
		}
		try {
			List<JDomain> domainList = this.jDomainService.getAllJDomainList();
			request.setAttribute(GlobalKeys.DOMAINS_KEY, domainList);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JDomain list.", e);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("security/signIn");
		JResultModel model = new JResultModel();
		model.setResult(JResultModel.ERROR);
		model.setMessage("Your session was timeout.");
		model.setData("session.timeout");
		model.addConfig(JResultModel.LOGIN_URL_KEY, "/service/security/signInFrm");
		mview.addObject(JResultModel.DATA_KEY, model);
		return mview;
//		return "security/signIn";
	}
	
	@RequestMapping(value = "/security/failure")
	public void failure(HttpServletRequest request) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("---> signIn/failure");
		}
		Exception cause = (Exception)request.getAttribute(GlobalKeys.ERROR_CAUSE);
		throw new JControllerException("Fail to singIn - "+cause.getMessage(), cause);
	}
	
	/**
	 *
	 * @param request
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/security/signIn")
	public ModelAndView signIn(HttpServletRequest request) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("---> signIn");
		}
		JUserEntity entity = null;
		HttpSession session = request.getSession();
		SecurityContext securityCtx = (SecurityContext)session.getAttribute(JSecurityService.SESSION_KEY);
		if(securityCtx!=null) {
			Authentication auth = securityCtx.getAuthentication();
			if(auth!=null) {
				entity = (JUserEntity)auth.getPrincipal();
			}
		}
		
		if(entity==null) {
			throw new JControllerException("Fail to signIn");
		}
		
		String client;
		try {
			client = security.getClientType();
		} catch(JSecurityException e) {
			throw new JControllerException("Fail to get ClientType.", e);
		}
		
		Long domainId = null;
		try {
			JDomain domain = null;
			ModelAndView mview = null;
			if(!ClientType.Web.name().equals(client)) {
				domainId = security.getDomainId();
				mview = new ModelAndView(JSonView.VIEWNAME);
			} else {
				domainId = (Long)session.getAttribute(GlobalKeys.DID_KEY);
				mview = new ModelAndView("main");
			}
			
			domain = this.jDomainService.getJDomain(domainId);
			if(!domain.isActive()) {
				session.invalidate();
				throw new JSecurityException("Your domain is not active.");
			}
			if(!entity.hasDomain(domainId)) {
				session.invalidate();
				throw new JSecurityException("You don't have permission for current applicatoin.");
			}
			
			// Set domainId to JContext
			JContextFactory jContextFactory = JContextFactory.newInstance();
			jContextFactory.getJContext().putDomainId(domainId);
			
			List<PreferenceModel> result = this.jPreferenceService.getDomainPreferenceModels(domainId);
			mview.addObject(JResultModel.DATA_KEY, result);
			
			Map<String,String> dpMap = new HashMap<String,String>();
			for(PreferenceModel model : result) {
				dpMap.put(model.getName(), model.getValue());
			}
			
			session.setAttribute(GlobalKeys.DOMAIN_PREFERENCES_KEY, dpMap);
			session.setAttribute(GlobalKeys.DOMAIN_KEY, domain);
			session.removeAttribute(GlobalKeys.DID_KEY);
			
			String redirectUrl = dpMap.get(JPreferenceKeys.DOMAIN_SIGNIN_REDIRECT_URL);
			if(redirectUrl!=null && !redirectUrl.equals("") && ClientType.Web.name().equals(client)) {
				mview = new ModelAndView("redirect:"+redirectUrl);
			}
			return mview;
		} catch(JServiceException e) {
			throw new JControllerException("Fail to get JDomain("+domainId+")", e);
		}
	}
	
	@RequestMapping(value = "/security/signOut", method=RequestMethod.GET)
	public ModelAndView signOut(HttpServletRequest request) {
		HttpSession hsession = request.getSession();
		hsession.invalidate();
		
		if(logger.isDebugEnabled()) {
			logger.debug("@@ ---> signOut");
		}
		ModelAndView mview = new ModelAndView(JSonView.VIEWNAME);
		mview.addObject(JResultModel.DATA_KEY, null);
		return mview;
	}
	
	@RequestMapping(value = "/security/invalid", method=RequestMethod.GET)
	public Object invalidSession(HttpServletRequest request, HttpServletResponse response) 
	throws JControllerException {
		HttpSession hsession = request.getSession();
		hsession.invalidate();
		response.addHeader("SESSION", "timeout");
		
		String client = null;
		try {
			client = security.getClientType();
		} catch (JSecurityException e) {
			throw new JControllerException("Fail to get ClientType.", e);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("@@ invalid session ---------->"+client);
		}
		if(ClientType.Web.name().equals(client)) {
			return "security/signIn";
		} else {
//			return new ModelAndView(JSonView.JSONVIEW);
			throw new JControllerException("session timeout");
		}
	}
	
	/**
	 * @param request
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/security/registration")
	public String registration(HttpServletRequest request) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("---> registration");
		}
		
		try {
			List<JDomain> domainList = this.jDomainService.getAllJDomainList();
			request.setAttribute(GlobalKeys.DOMAINS_KEY, domainList);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JDomain list.", e);
		}
		
		return "security/register";
	}
	
	/**
	 * This API provides user registration service.
	 * 
	 * @publish User registration service
	 * @param nickname	URL Parameter : user nickname
	 * @param email		URL Parameter : user email address
	 * @param password	URL Parameter : user password
	 * @param picture	URL Parameter : JAttachment objectId for registered user profile
	 * @param domainId	URL Parameter : domainId for user
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/security/register", method = RequestMethod.POST)
	public String register( 	@RequestParam(JUser.A_NICKNAME) String nickname, 
							@RequestParam(JUser.A_EMAIL) String email, 
							@RequestParam(JUser.A_PASSWORD) String password,
							@RequestParam(value=ParamKeys.PICTURE, required=false) Long picture,
							@RequestParam(value=ParamKeys.DOMAIN_ID, required=false) Long domainId) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("register user. --> "+email);
		}
		
		if(nickname==null || nickname.equals("")) {
			throw new JInvalidParamException(new Object[]{JUser.A_NICKNAME, email});
		}
		if(email==null || email.equals("")) {
			throw new JInvalidParamException(new Object[]{JUser.A_EMAIL, email});
		}
		if(password==null || password.equals("")) {
			throw new JInvalidParamException(new Object[]{JUser.A_EMAIL, email});
		}
		
		try {
			String clientType = security.getClientType();
			if(ClientType.Web.name().equals(clientType) && domainId!=null) {
				JContextFactory.newInstance().getJContext().putDomainId(domainId);
			}
		} catch (JSecurityException e) {
			throw new JControllerException("Fail to get ClientType.",e);
		}
		
		JAttachment attachment = null;
		if(picture!=null) {
			try {
				attachment = jAttachService.getJAttachment(picture);
			} catch (JServiceException e) {
				throw new JControllerException("Fail to find profile picture. - "+picture);
			}
		}
		
		JUser jUser = new JUser();
		jUser.setEmail(email);
		jUser.setNickname(nickname);
		jUser.setPassword(password);
		if(attachment!=null) {
			jUser.setPicture(attachment);
		}
		
		try {
			jUserService.create(jUser);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JUser.", e);
		}
		
//		ModelAndView mview = new ModelAndView(JSonView.VIEWNAME);
//		mview.addObject(JResultModel.DATA_KEY, null);
//		return mview;
		return "redirect:/";
	}
	
	/**
	 * This API deletes signed user.
	 * 
	 * @publish user unregisteration service
	 * @param request
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/security/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(HttpServletRequest request) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("---> remove");
		}
		
		try {
			JUserEntity entity = security.getUserEntity();
			this.jUserService.remove(entity.getUsername());
	
			ModelAndView mview = new ModelAndView(JSonView.VIEWNAME);
			return mview;
		} catch(JServiceException e) {
			throw new JControllerException("Fail to remove JUser.", e);
		}
	}
}
