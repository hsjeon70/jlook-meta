package jlook.framework.interfaces;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

import jlook.framework.JContentType;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.hibernate.JSessionFactoryBean;
import jlook.framework.infrastructure.tools.OpenAPIGenerator;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish MainUtilService
 * 
 * @author hsjeon70
 *
 */
@Controller
public class MainController {
	private static Log logger = Log.getLog(MainController.class);
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=OpenAPIGenerator.ID)
	private OpenAPIGenerator openAPIGenerator;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	@Resource(name=jlook.framework.infrastructure.loader.InitDataLoader.ID)
	private jlook.framework.infrastructure.loader.InitDataLoader dataLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.MetadataLoader.ID)
	private jlook.framework.infrastructure.loader.MetadataLoader metadataLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.EnumLoader.ID)
	private jlook.framework.infrastructure.loader.EnumLoader enumLoader;
	
	@Resource(name=jlook.framework.infrastructure.loader.ViewLoader.ID)
	private jlook.framework.infrastructure.loader.ViewLoader viewLoader;
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public ModelAndView gotoMain() {
		ModelAndView mview = JContentType.HTML.create("main");
		return mview;
	}
	
	/**
	 *	'/service/page/test' --> /WEB-INF/jsp/page/test.jsp
	 *	'/service/page/test/index --> /WEB-INF/jsp/page/test/index.jsp
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/page/*", method = RequestMethod.GET)
	public String redirectPage(HttpServletRequest request) {
		String requestUri = request.getRequestURI();
		if(logger.isDebugEnabled()) {
			logger.debug("--> RequestURI = "+requestUri);
		}
		
		String uri = requestUri.substring((request.getContextPath()+"/service/").length());
		return uri;
	}

	/**
	 * Load metadata initially.
	 * 
	 * @param response
	 * @throws IOException
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/metadata/load", method = RequestMethod.GET)
	public String loadMetadata(HttpServletRequest request, HttpServletResponse response) throws JControllerException {
		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		String password = request.getParameter("password");
		if(password==null || !password.equals("1q2w3e4r")) {
			throw new JControllerException("Invalid access.");
		}
		bean.dropDatabaseSchema();
		bean.createDatabaseSchema();
		
		try {
			this.enumLoader.setDataLoadEnabled(true);
			this.enumLoader.doInit();
			
			this.metadataLoader.setDataLoadEnabled(true);
			this.metadataLoader.doInit();
			
			this.dataLoader.setDataLoadEnabled(true);
			this.dataLoader.doInit();
			
			this.viewLoader.setDataLoadEnabled(true);
			this.viewLoader.doInit();
			return "redirect:/";
		} catch(Exception e) {
			throw new JControllerException("Cannot load metadata. - "+e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * This API update database schema.
	 * 
	 * @publish Database schema update API
	 * @return	"redirect:/"
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/database/schema/update", method = RequestMethod.GET)
	public String updateDBSchema() 
	throws JControllerException {
		try {
			this.jMetaService.updateDBSchema();
			return "redirect:/";
		} catch (JServiceException e) {
			throw new JControllerException("Fail to update database schema.",e);
		}
	}
	
	/**
	 * @publish Generate OpenAPI Docs.
	 * @return "redirect:/service/main"
	 * @throws IOException
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generate/openapi", method = RequestMethod.GET)
	public String generateOpenAPI() throws IOException, JControllerException {
		try {
			this.openAPIGenerator.generate();
			return "redirect:/service/main";
		} catch (JException e) {
			throw new JControllerException("Fail to generate.",e);
		}
	}
	
	@RequestMapping(value = "/test/generic", method = RequestMethod.GET)
	public ModelAndView getTestMain() throws JControllerException {
		try {
			List<JClass> result = jClassService.getJClassList();
			JContentType jContentType = JContentType.HTML;
			if(logger.isDebugEnabled()) {
				logger.debug("--> ContentType = "+jContentType.name());
			}
			ModelAndView mview = jContentType.create("test/generic");
			mview.addObject(JResultModel.DATA_KEY, result);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get TestMain page.", e);
		}
	}
	
	@RequestMapping(value = "/test/openapi", method = RequestMethod.GET)
	public ModelAndView testOpenAPI() throws JControllerException {
		try {
			List<JClass> result = jClassService.getJClassList();
			JContentType jContentType = JContentType.HTML;
			if(logger.isDebugEnabled()) {
				logger.debug("--> ContentType = "+jContentType.name());
			}
			ModelAndView mview = jContentType.create("test/openapi");
			mview.addObject(JResultModel.DATA_KEY, result);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get TestMain page.", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/docs/openapi", method = RequestMethod.GET)
	public ModelAndView getOpenAPIDocs(HttpServletRequest request, HttpServletResponse response) throws JControllerException {
		try {
			JContentType jContentType = JContentType.HTML;
			if(logger.isDebugEnabled()) {
				logger.debug("--> ContentType = "+jContentType.name());
			}
			
			String paramCategory = request.getParameter("category");
			String paramService = request.getParameter("service");
			ServletContext context = request.getSession().getServletContext();
			Set<String> categorySet = context.getResourcePaths("/WEB-INF/openapi");
			Map<String, List<String>> categorys = new HashMap<String, List<String>>();
			for(String category : categorySet) {
				Set<String> serviceSet = context.getResourcePaths(category);
				List<String> serviceList = new ArrayList<String>();
				for(String fName : serviceSet) {
					if(logger.isDebugEnabled()) {
						logger.debug("\t--> "+fName);
					}
					int idx = fName.lastIndexOf("/");
					fName = fName.substring(idx+1);
					idx = fName.indexOf(".xml");
					fName = fName.substring(0,idx);
					serviceList.add(fName.replace("Controller", "Service"));
				}
				Collections.sort(serviceList);
				category = category.substring("/WEB-INF/openapi/".length(), category.length()-1);
				categorys.put(category.toUpperCase(), serviceList);
			}
			
			request.setAttribute("menus", categorys);
			ModelAndView mview = jContentType.create("docs/openapi");
			if(paramService==null) {
				return mview;
			}
			
			paramService = "/WEB-INF/openapi/"+paramCategory.toLowerCase()+"/"+paramService+".xml";
			if(logger.isDebugEnabled()) {
				logger.debug("service xml path --> "+paramService);
			}
			URL serviceXml = context.getResource(paramService);
			if(serviceXml==null) {
				throw new JControllerException("Cannot find open api xml file. - "+paramService);
			}
			TransformerFactory factory = TransformerFactory.newInstance();
			org.springframework.core.io.Resource resource = ApplicationContextHolder.getResource("classpath:template/openapi-template.xsl");
			if(resource==null) {
				throw new JControllerException("Cannot find xsl template - "+"template/openapi-template.xsl");
			}
			StreamSource xslSource = new StreamSource(resource.getInputStream());
			StreamSource xmlSource = new StreamSource(serviceXml.openStream());
			
			Transformer transform = factory.newTransformer(xslSource);
			StringWriter writer = new StringWriter();
			transform.transform(xmlSource, new StreamResult(writer));
			mview = jContentType.create("docs/openapispec");
			mview.addObject(JResultModel.DATA_KEY, writer.toString());
			return mview;
		} catch (Exception e) {
			throw new JControllerException("Fail to get TestMain page.", e);
		}
	}
}
