package jlook.framework.interfaces;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


public abstract class JBaseController {
	Log logger = Log.getLog(JBaseController.class);
	public static final String ID = "jBaseController";
	
	@Resource(name=JGenericService.ID)
	protected JGenericService jGenericService;
	
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE,  required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true)  Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		@SuppressWarnings("unchecked")
		Map<String,String[]> params  =request.getParameterMap();
		
		if(classId==null||classId.equals("")) {
			throw new JInvalidParamException(new Object[]{ParamKeys.CLASS_ID, classId});
		}
		
		PageUtil page = new PageUtil(pageNumber,pageSize,keyword);
		page.setParentOid(parentOid);
		page.setParentCid(parentCid);
		String[] orderBy = request.getParameterValues(ParamKeys.ORDER_BY);
		if(orderBy!=null) {
			page.setOrderBy(orderBy);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("SUMMARY DATA -------> classId="+classId+", page="+page+", paramMap="+params);
		}
		
		try {
			JContentType jContentType = JContentType.getJContentType(contentType);
			JViewType type = JViewType.summary;
			if(jContentType.equals(JContentType.EXCEL)) {
				type = JViewType.detail;
			}
			JSummaryModel model = this.jGenericService.getSummaryData(type, classId, page, params);
			ModelAndView mview = jContentType.create("generic/summary");
			mview.addObject(JSummaryModel.DATA_KEY, model);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail get summary data.",e);
		}
	}
	
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("REMOVE DATA ------> classId="+classId+", objectId="+objectId);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			this.jGenericService.remove(classId, objectId);
			ModelAndView mview = jContentType.create("generic/remove");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to remove JObject("+classId+", "+objectId+"). ", e);
		}
	}
	
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("DETAIL DATA ------> classId="+classId+", objectId="+objectId);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			JResultModel rmodel = this.jGenericService.getDetailData(JViewType.detail, classId, objectId, parentCid, parentOid);
			ModelAndView mview = jContentType.create("generic/detail");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JObject("+classId+", "+objectId+"). ", e);
		}
	}
	
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("TITLE DATA ------> classId="+classId+", objectId="+objectId);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			JResultModel model = this.jGenericService.getTitleData(classId, objectId);
			ModelAndView mview = jContentType.create("generic/title");
			mview.addObject(JResultModel.DATA_KEY, model);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JObject("+classId+", "+objectId+"). ", e);
		}
	}
	
	public ModelAndView getTitles(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD,		required=false) String keyword) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("TITLE DATA ------> classId="+classId+", keyword="+keyword);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			PageUtil page = new PageUtil(pageNumber,pageSize,keyword);
			JResultModel model = this.jGenericService.getSummaryData(JViewType.title, classId, page, null);
			ModelAndView mview = jContentType.create("generic/title");
			mview.addObject(JResultModel.DATA_KEY, model);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JObject("+classId+", "+keyword+"). ", e);
		}
	}
	
	public ModelAndView getWidget(String contentType, Long widget, Long parentCid, Long parentOid, HttpServletRequest request) throws JControllerException {
		try {
			JContentType jContentType = JContentType.getJContentType(contentType);
			@SuppressWarnings("unchecked")
			Map<String,String[]> params  =request.getParameterMap();
			JResultModel model = this.jGenericService.getWidgetResultMode(widget, parentCid, parentOid, params);
			ModelAndView mview = jContentType.create("generic/widget");
			mview.addObject(JResultModel.DATA_KEY, model);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JWidget("+widget+"). ", e);
		}
	}
}
