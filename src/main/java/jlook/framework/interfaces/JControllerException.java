package jlook.framework.interfaces;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.annotation.Message;

@Message(text=JControllerException.TEXT)
public class JControllerException extends JException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Controller Error";
	
	public JControllerException(String message) {
		super(message);
	}
	
	public JControllerException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JControllerException(String msg, Throwable th) {
		super(msg, th);
	}
	
	public JControllerException(String msg, Object[] parameters) {
		super(msg, parameters);
	}
	
	public JControllerException(Throwable cause, Object[] parameters) {
		super(cause, parameters);
	}
}
