package jlook.framework.interfaces;

import jlook.framework.Constants;

public interface ParamKeys {
	public static final String OBJECT_ID = "objectId";
	public static final String CLASS_ID = "classId";
	public static final String DOMAIN_ID = "domainId";
	
	public static final String WIDGET	 = "widget";
	
	public static final String PARENT_CID 	= "parentCid";
	public static final String PARENT_OID 	= "parentOid";
	public static final String KEYWORD 		= "keyword";
	public static final String PAGE_NUMBER 	= "pageNumber";
	public static final String PAGE_SIZE 	= "pageSize";
	public static final String ORDER_BY		= "orderBy[]";
	
	public static final String VERSION = "version";
	
	// security
	public static final String PICTURE 	= "picture";
	
	// file
	public static final String UPLOAD_KEY = "upload";
	public static final String REFERER_KEY = "Referer";
	public static final String SUBJECT = "subject";
	public static final String CONTENT = "content";
	public static final String COMMENTS = "comments";
	
	// -------------- response config keys
	public static final String COUNT = "count";
	public static final String PREFIX_URI = "prefixUri";
	public static final String READONLY = Constants.READONLY;
	
}
