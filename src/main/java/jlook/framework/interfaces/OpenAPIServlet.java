package jlook.framework.interfaces;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.web.view.JViewResolver;
import jlook.framework.service.JGenericService;

import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.UrlBasedViewResolver;


/**
 * TODO Not implemented
 * @author hsjeon70
 *
 */
public class OpenAPIServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Log logger = Log.getLog(OpenAPIServlet.class);
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws IOException, ServletException {
		try {
			doExecute((HttpServletRequest)req, (HttpServletResponse)res);
		} catch(Throwable th) {
			throw new ServletException(th);
		} finally {
			
		}
	}
	
	public void doExecute(HttpServletRequest request, HttpServletResponse response) 
	throws JControllerException {
		String requestUri = request.getRequestURI();
		if(logger.isDebugEnabled()) {
			logger.debug("\tgeneric open api ------>"+requestUri);
		}
		
		int idx = requestUri.indexOf(Constants.OPENAPI_URI_PREFIX);
		JContentType jContentType = JContentType.getJContentType(request.getHeader(JHeader.JCONTENT_TYPE));
		String viewName = requestUri.substring(idx+Constants.OPENAPI_URI_PREFIX.length());
		
		HttpMethod method = HttpMethod.valueOf(request.getMethod());
		OpenAPI api = OpenAPI.getJOpenAPI(viewName, method);
		if(logger.isDebugEnabled()) {
			logger.debug("JOpenAPI --> target="+api.getTarget()+" -> operation="+api);
		}
		
		ModelAndView mview = jContentType.create(Constants.OPENAPI_VIEWNAME);
		JGenericService jGenericService = (JGenericService)ApplicationContextHolder.getBean(JGenericService.ID);
//		switch(api) {
//			case summary 	: JSummaryModel smodel = jGenericService.getSummaryData(classId, page);
//							  mview.addObject(JResultModel.DATA_KEY, smodel);
//				break;
//			case detail		: JResultModel rmodel = jGenericService.getDetailData(classId, objectId, parentCID, parentOID);
//							  mview.addObject(JResultModel.DATA_KEY, rmodel);
//				break;
//			case title		:
//				break;
//			case create		:
//				break;
//			case save		: 
//				break;
//			case remove		:
//				break;
//		}
		
		
		JViewResolver jViewResolver = (JViewResolver)ApplicationContextHolder.getBean(JViewResolver.ID);
		View view = null;
		try {
			view = jViewResolver.resolveViewName(mview.getViewName(), Locale.getDefault());
			if(view==null) {
				UrlBasedViewResolver defaultViewResolver = (UrlBasedViewResolver)ApplicationContextHolder.getBean(Constants.JSTLVIEW_RESOLVER_ID);
				view = defaultViewResolver.resolveViewName(mview.getViewName(), Locale.getDefault());
			}
		} catch (Exception e) {
			throw new JControllerException("Fail to get JView.("+requestUri+") - "+mview.getViewName(),e);
		}
		
		if(view!=null) {
			try {
				view.render(mview.getModel(), request, response);
			} catch (Exception e) {
				throw new JControllerException("Fail to render message by JView.("+requestUri+") - "+view,e);
			}
			return;
		} else {
			
		}
	}
}
