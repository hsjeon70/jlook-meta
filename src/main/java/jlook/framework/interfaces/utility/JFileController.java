package jlook.framework.interfaces.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.model.AttachmentModel;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.loader.metadata.JPreferenceDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.JInvalidParamException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JAttachmentService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JServiceException;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * @publish FileService
 * @author hsjeon70
 *
 */
@Controller
public class JFileController {
	private static Log logger = Log.getLog(JFileController.class);
	
	@Value(EnvKeys.APP_HOME)
	private String home;
	
	@Value(EnvKeys.FILE_UPLOAD_DIR)
	private String uploadDir;
	
	@Value(EnvKeys.WEB_IMAGE_PATH)
	private String imagePath;
	
	@Value(EnvKeys.MOBILE_APP_PATH)
	private String mobilePath;
	
	
	@Resource(name=JAttachmentService.ID)
	private JAttachmentService jAttachmentService;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService jPreferenceService;
	
	
	public JFileController() {
	}
	
	/**
	 * Anonymous user can upload a new file by this API.
	 *  
	 * @publish file upload API for file service.
	 * @param file
	 * @param referer
	 * @return
	 * @throws JServiceException
	 */
	@RequestMapping(value = "/attachment/upload", method = RequestMethod.POST) 
	public ModelAndView handleFileUpload(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestHeader(value="User-Agent", required=false) String userAgent,
			@RequestParam(value=ParamKeys.UPLOAD_KEY, required=true) MultipartFile file, 
			@RequestParam(value=ParamKeys.SUBJECT, required=false) String subject, 
			@RequestParam(value=ParamKeys.CONTENT, required=false) String content, 
			@RequestParam(value=ParamKeys.COMMENTS, required=false) String comments, 
			@RequestHeader(ParamKeys.REFERER_KEY) String referer) throws JControllerException {
		
		if(logger.isDebugEnabled()) {
			logger.debug("handleFileUpload --> "+file+", Referer"+referer);
		}
		
		if (!file.isEmpty()) {
			InputStream in=null;
			File target = null;
			try {
				in = file.getInputStream();
				String fileName  = file.getOriginalFilename();
				target = writeFile(in, fileName);
				String type = file.getContentType();
				long size = file.getSize();
				String savedName = target.getName();
				
				JAttachment attachment = null;
				attachment = new JAttachment();
				attachment.setName("Attachment-"+UUID.randomUUID().toString());
				attachment.setSubject(subject);
				attachment.setContent(content);
				
				JAttachmentDetail detail = new JAttachmentDetail();
				detail.setDefaultYn("Y");
				detail.setName(fileName);
				detail.setComments(comments);
				detail.setType(type);
				detail.setAbsolutePath(target.getAbsolutePath());
				detail.setReferer(referer);
				detail.setLength(size);
				detail.setSavedName(savedName);
				detail.setPath(target.getParent());
				
				attachment.addJAttachmentDetail(detail);
				
				JResultModel rmode = jAttachmentService.create(attachment);
				
				JContentType jContentType = null;
				if(userAgent!=null && userAgent.indexOf("MSIE")>=0) {
					jContentType = JContentType.TEXT;
				} else {
					jContentType = JContentType.getJContentType(contentType);
				}
				ModelAndView mview = jContentType.create("file/upload");
				mview.addObject(JResultModel.DATA_KEY, rmode);
				return mview;
			} catch (IOException e) {
				throw new JControllerException("Fail to write uploaded file("+file.getOriginalFilename()+"). - "+e.getMessage(),e);
			} catch(JServiceException e) {
				throw new JControllerException("Fail to save JAttachment. - "+e.getMessage(), e);
			} 
		} else {
			throw new JControllerException("There is no file in request."); 
		}
	}
	
	/**
	 * Upload a new file to existing JAttachment.
	 * 
	 * @publish Upload a new file to existing JAttachment.
	 * @param contentType
	 * @param file
	 * @param objectId	JAttachment objectId
	 * @param referer
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/attachment/upload/{objectId}", method = RequestMethod.POST) 
	public ModelAndView handleFileUpload(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestHeader(value="User-Agent", required=false) String userAgent,
			@RequestParam(value=ParamKeys.UPLOAD_KEY, required=true) MultipartFile file, 
			@RequestParam(value=ParamKeys.SUBJECT, required=false) String subject, 
			@RequestParam(value=ParamKeys.CONTENT, required=false) String content, 
			@RequestParam(value=ParamKeys.COMMENTS, required=false) String comments, 
			@PathVariable(ParamKeys.OBJECT_ID) Long objectId, 
			@RequestHeader(ParamKeys.REFERER_KEY) String referer) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("handleFormUpload2 --> "+file+", Referer"+referer);
		}
		
		if (!file.isEmpty()) {
			InputStream in=null;
			File target = null;
			try {
				in = file.getInputStream();
				String fileName  = file.getOriginalFilename();
				target = writeFile(in, fileName);
				String type = file.getContentType();
				long size = file.getSize();
				String savedName = target.getName();
				
				JAttachmentDetail detail = new JAttachmentDetail();
				detail.setDefaultYn("N");
				detail.setName(fileName);
				detail.setComments(comments);
				detail.setType(type);
				detail.setAbsolutePath(target.getAbsolutePath());
				detail.setReferer(referer);
				detail.setLength(size);
				detail.setSavedName(savedName);
				detail.setPath(target.getParent());
				
				JResultModel rmodel = jAttachmentService.create(objectId, subject, content, detail);
			
				JContentType jContentType = null;
				if(userAgent!=null && userAgent.indexOf("MSIE")>=0) {
					jContentType = JContentType.TEXT;
				} else {
					jContentType = JContentType.getJContentType(contentType);
				}
				ModelAndView mview = jContentType.create("file/upload");
				mview.addObject(JResultModel.DATA_KEY, rmodel);
				return mview;
			} catch (IOException e) {
				throw new JControllerException("Fail to write uploaded file("+file.getOriginalFilename()+"). - "+e.getMessage(),e);
			} catch(JServiceException e) {
				throw new JControllerException("Fail to save JAttachment. - "+e.getMessage(), e);
			} 
		} else {
			throw new JControllerException("There is no file in request."); 
		}
	}
	
	/**
	 * This API removes uploaded file
	 * 
	 * @publish	Uploaded file remove API
	 * @param contentType
	 * @param objectId uploaded file objectId
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/attachment/file/{objectId}", method = RequestMethod.DELETE) 
	public ModelAndView removeAttachmentFile(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@PathVariable(ParamKeys.OBJECT_ID) Long objectId) throws JControllerException {
		try {
			this.jAttachmentService.removeDetail(objectId);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("file/remove");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to remove JAttachmentDetail. - "+objectId, e);
		}
	}
	
	/**
	 * @publish attachment information
	 * @param contentType
	 * @param objectId
	 * @param request
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/attachment/{objectId}", method = RequestMethod.GET)
	public ModelAndView getAttachmentDetailList(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@PathVariable(ParamKeys.OBJECT_ID) Long objectId, HttpServletRequest request) throws JControllerException {
		
		JAttachment jatt;
		try {
			jatt = this.jAttachmentService.getJAttachment(objectId);
			Set<JAttachmentDetail> details = jatt.getJAttachmentDetails();
			Map<String,Object> config = new HashMap<String,Object>();
			config.put(ParamKeys.COUNT,details.size());
			config.put(ParamKeys.PREFIX_URI, request.getContextPath()+"/"+this.imagePath);
			
			AttachmentModel model = new AttachmentModel(jatt);
			JContentType jContentType = JContentType.getJContentType(contentType); 
			ModelAndView mview = jContentType.create("file/details");
			mview.addObject(JResultModel.DATA_KEY, model);
			mview.addObject(JResultModel.CONFIG_KEY, config);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JAttachmentDetail.", e);
		}
	}
	
	/**
	 * 
	 * @param contentType
	 * @param objectId
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/attachment/download/{objectId}", method = RequestMethod.GET) 
	public ModelAndView download(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@PathVariable(ParamKeys.OBJECT_ID) Long objectId) throws JControllerException {
		
		
		JAttachmentDetail detail = null;
		try {
			detail = this.jAttachmentService.getJAttachmentDetail(objectId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JAttachmentDetail - "+e.getMessage(), e);
		}
		
		String savedName = detail.getSavedName();
		String path = detail.getPath();
		
		File file = new File(path,savedName);
		if(!file.exists()) {
			throw new JControllerException("Cannot find the file. ("+objectId+") - "+file.getAbsoluteFile());
		}
		
		ModelAndView mview = JContentType.FILE.create("file/download");
		mview.addObject(JResultModel.DATA_KEY, file);
		mview.addObject(Constants.FILE_NAME, detail.getName());
		return mview;
		
	}
	
	
	@RequestMapping(value = "/mobile/download", method = RequestMethod.GET) 
	public ModelAndView downloadMobileApp(HttpServletRequest request, HttpServletResponse response) 
	throws JControllerException {
		Long domainId = null;
		try {
			domainId = Long.parseLong(request.getParameter(ParamKeys.DOMAIN_ID));
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{"domainId", request.getParameter(ParamKeys.DOMAIN_ID)});
		}
		
		JDomain domain = null;
		try {
			domain = jDomainService.getJDomain(domainId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JDomain.", e);
		}
		
		String version;
		try {
			version = this.jPreferenceService.getPreferenceValue(JPreferenceType.DOMAIN, JPreferenceDef.DOMAIN_VERSION.instance.getName());
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get domain version preferece.", e);
		}
		
		if(version==null || version.equals("")) {
			throw new JControllerException("Cannot download mobile application file because domain version preference is not configured.");
		}

		String fileName = getMobileAppName(domain.getName(), version);
		File mobilePath = new File(home, this.mobilePath);
		if(!mobilePath.exists()) {
			throw new JControllerException("Cannot find mobile application direcotry. - "+mobilePath);
		}
		
		File appFile = new File(mobilePath, fileName);
		if(!appFile.exists()) {
			throw new JControllerException("Cannot find mobile application file. - "+appFile);
		}
		
		ModelAndView mview = JContentType.FILE.create("file/download");
		mview.addObject(JResultModel.DATA_KEY, appFile);
		return mview;
	}
	
	private String getMobileAppName(String domainName, String version) {
		return domainName+"_v"+version+".apk";
	}
	
	private File writeFile(InputStream in, String fileName) 
	throws IOException {
		UUID uid = UUID.randomUUID();
		String id = uid.toString();
		int idx = fileName.lastIndexOf(".");
		if(idx==-1) {
			fileName = fileName+"_"+id;
		} else {
			fileName = fileName.substring(0,idx)+"_"+id+fileName.substring(idx);
		}
		File uploadPath = new File(home, this.uploadDir);
//		if(!uploadPath.exists()) {
//			uploadPath.mkdirs();
//		}
		File target = new File(uploadPath, fileName);
		FileOutputStream fo = null;
		BufferedOutputStream bo = null;
		BufferedInputStream bi = null;
		try {
			bi = new BufferedInputStream(in);
			fo = new FileOutputStream(target);
			bo = new BufferedOutputStream(fo);
			byte[] buff = new byte[1024];
			while(true) {
				int len = bi.read(buff);
				if(len==-1) break;
				
				bo.write(buff,0,len);
				bo.flush();
			}
			return target;
		} finally {
			try { if(bo!=null) bo.close(); } catch(Exception e) {}
			try { if(fo!=null) fo.close(); } catch(Exception e) {}
			try { if(bi!=null) bi.close(); } catch(Exception e) {}
		}
	}
}
