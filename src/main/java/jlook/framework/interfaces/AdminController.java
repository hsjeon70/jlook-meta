package jlook.framework.interfaces;

import java.util.Properties;

import javax.annotation.Resource;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.service.JAdminService;
import jlook.framework.service.JServiceException;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {

	@Resource(name=JAdminService.ID)
	private JAdminService jAdminService;
	
	@Resource(name="environment")
	private Properties environment;
	
	@RequestMapping(value = "/domain/registration", method = RequestMethod.GET)
	public String registerForm() {
		return "domain/register";
	}
	
	@RequestMapping(value = "/domain/register", method = RequestMethod.POST)
	public String register(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value="name",			required=true) String domainName,
			@RequestParam(value="description",	required=true) String domainDesc,
			@RequestParam(value="username",		required=true) String username,
			@RequestParam(value="email",		required=true) String email,
			@RequestParam(value="password",		required=true) String password) throws JControllerException {
		JDomain jDomain = new JDomain();
		jDomain.setName(domainName);
		jDomain.setDescription(domainDesc);
		jDomain.setActive(false);
		
		JUser jUser = new JUser();
		jUser.setNickname(username);
		jUser.setEmail(email);
		jUser.setPassword(password);
		try {
			jAdminService.register(jDomain, jUser);
		} catch (JServiceException e) {
			throw new JControllerException("Cannot create JDomain and admin JUser - "+e.getMessage(),e);
		}
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/admin/environment", method = RequestMethod.GET)
	public ModelAndView envProperties(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value="name", 	required=false) String name, 
			@RequestParam(value="value", 	required=false) String value) throws JControllerException {
		if(name!=null && value!=null) {
			environment.setProperty(name,value);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("admin/environment");
		mview.addObject(JResultModel.DATA_KEY, environment);
		return mview;
	}
	
	@RequestMapping(value = "/admin/sysprops", method = RequestMethod.GET)
	public ModelAndView jdbcProperties(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType) throws JControllerException {
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("admin/sysprops");
		mview.addObject(JResultModel.DATA_KEY, System.getProperties());
		return mview;
	}
}
