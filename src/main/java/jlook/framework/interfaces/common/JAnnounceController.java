package jlook.framework.interfaces.common;

import java.text.ParseException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.account.JGroup;
import jlook.framework.domain.account.JUser;
import jlook.framework.domain.common.JAnnounceDetail;
import jlook.framework.domain.common.JAnnounceMessage;
import jlook.framework.domain.common.JAnnounceTargetType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.JInvalidParamException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish AnnounceService
 * @author hsjeon70
 *
 */
@Controller
public class JAnnounceController {
	private static Log logger = Log.getLog(JAnnounceController.class);
	
	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * @publish Summary view API for announce service.
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jannouncemessage/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JANNOUNCEMESSAGE, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/jannouncemessage/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JANNOUNCEMESSAGE, objectId, parentCid, parentOid);
	}
	
	
	@RequestMapping(value = "/jannouncemessage/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JANNOUNCEMESSAGE, objectId);
	}
	
	@RequestMapping(value = "/jannouncemessage/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JANNOUNCEMESSAGE, objectId);
	}
	
	@RequestMapping(value = "/jannouncemessage/create", method=RequestMethod.POST)
	public ModelAndView createAnnounceMessage(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=JAnnounceMessage.A_TITLE,		required=true) String title,
			@RequestParam(value=JAnnounceMessage.A_MESSAGE,		required=true) String message,
			@RequestParam(value=JAnnounceMessage.A_FROMDATE,		required=true) String fromDate,
			@RequestParam(value=JAnnounceMessage.A_TODATE,		required=true) String toDate,
			@RequestParam(value=JAnnounceMessage.A_ACTIVE,		required=true) boolean active) throws JControllerException {
		
		Long classId = JMetaKeys.JANNOUNCEMESSAGE;
		
		JAnnounceMessage jAnnounceMessage = new JAnnounceMessage();
		jAnnounceMessage.setClassId(classId);
		try {
			jAnnounceMessage.setFromDate(DateUtil.parseDate(fromDate));
		} catch (ParseException e) {
			throw new JInvalidParamException(e,new Object[]{JAnnounceMessage.A_FROMDATE, fromDate});
		}
		try {
			jAnnounceMessage.setToDate(DateUtil.parseDate(toDate));
		} catch (ParseException e) {
			throw new JInvalidParamException(e, new Object[]{JAnnounceMessage.A_TODATE, toDate});
		}
		
		jAnnounceMessage.setTitle(title);
		jAnnounceMessage.setMessage(message);
		jAnnounceMessage.setActive(active);
		try {
			JResultModel rmodel = this.jGenericService.create(jAnnounceMessage);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jannouncemessage/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JAnnounceMessage.", e);
		}
	}
	
	@RequestMapping(value = "/jannouncemessage/save", method=RequestMethod.POST)
	public ModelAndView saveAnnounceMessage(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId,
			@RequestParam(value=JAnnounceMessage.A_TITLE,		required=true) String title,
			@RequestParam(value=JAnnounceMessage.A_MESSAGE,		required=true) String message,
			@RequestParam(value=JAnnounceMessage.A_FROMDATE,		required=true) String fromDate,
			@RequestParam(value=JAnnounceMessage.A_TODATE,		required=true) String toDate,
			@RequestParam(value=JAnnounceMessage.A_ACTIVE,		required=true) boolean active) throws JControllerException {
		
		Long classId = JMetaKeys.JANNOUNCEMESSAGE;
		
		JAnnounceMessage jAnnounceMessage = new JAnnounceMessage();
		jAnnounceMessage.setObjectId(objectId);
		jAnnounceMessage.setClassId(classId);
		try {
			jAnnounceMessage.setFromDate(DateUtil.parseDate(fromDate));
		} catch (ParseException e) {
			throw new JInvalidParamException(e,new Object[]{JAnnounceMessage.A_FROMDATE, fromDate});
		}
		try {
			jAnnounceMessage.setToDate(DateUtil.parseDate(toDate));
		} catch (ParseException e) {
			throw new JInvalidParamException(e, new Object[]{JAnnounceMessage.A_TODATE, toDate});
		}
		jAnnounceMessage.setTitle(title);
		jAnnounceMessage.setMessage(message);
		jAnnounceMessage.setActive(active);
		try {
			JResultModel rmodel = jGenericService.update(jAnnounceMessage);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jannouncemessage/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JAnnounceMessage.", e);
		}
	}
	
	// JANNOUNCEDETAIL API --------------------
	
	@RequestMapping(value = "/jannouncedetail/summary", method = RequestMethod.GET)
	public ModelAndView getAnnounceDetails(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=true) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		Long classId 	= JMetaKeys.JANNOUNCEDETAIL;
		Long parentCid 	= JMetaKeys.JANNOUNCEMESSAGE;
		return controller.getSummary(contentType, classId, pageNumber, pageSize, keyword, parentOid, parentCid, request);
	}
	
	@RequestMapping(value = "/jannouncedetail/create", method=RequestMethod.POST)
	public ModelAndView createAnnounceDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 		required=false) String contentType,
			@RequestParam(value=JAnnounceDetail.A_JDOMAIN,	required=false) Long jDomain,
			@RequestParam(value=JAnnounceDetail.A_JGROUP,	required=false) Long jGroup,
			@RequestParam(value=JAnnounceDetail.A_JUSER,	required=false) Long jUser,
			@RequestParam(value=ParamKeys.PARENT_OID, 		required=true) Long parentOid) throws JControllerException {
		
		Long classId 	= JMetaKeys.JANNOUNCEDETAIL;
//		Long parentCid 	= JMetaKeys.JANNOUNCEMESSAGE;
		
		JAnnounceDetail jAnnounceDetail = new JAnnounceDetail();
		jAnnounceDetail.setClassId(classId);
		if(jDomain!=null) {
			jAnnounceDetail.setJDomain(new JDomain(jDomain));
		}
		if(jGroup!=null) {
			jAnnounceDetail.setJGroup(new JGroup(jGroup));
		}
		if(jUser!=null) {
			jAnnounceDetail.setJUser(new JUser(jUser));
		}
		JAnnounceMessage jAnnounceMessage = new JAnnounceMessage();
		jAnnounceMessage.setObjectId(parentOid);
		jAnnounceDetail.setJAnnounceMessage(jAnnounceMessage);
		try {
			JResultModel rmodel = jGenericService.create(jAnnounceDetail);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jannouncedetail/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JAnnounceMessage.", e);
		}
	}
	
	@RequestMapping(value = "/jannouncedetail/remove", method=RequestMethod.POST)
	public ModelAndView removeAnnounceDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		
		Long classId 	= JMetaKeys.JANNOUNCEDETAIL;
		try {
			jGenericService.remove(classId, objectId);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jannouncedetail/remove");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create JAnnounceMessage.", e);
		}
	}
}
