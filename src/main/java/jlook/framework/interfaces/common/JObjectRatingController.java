package jlook.framework.interfaces.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JObjectRatingService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish RatingCommentService
 * @author hsjeon70
 *
 */
@Controller
public class JObjectRatingController {
	private static Log logger = Log.getLog(JObjectRatingController.class);
	
	public static final String CONFIG_AVG = "avg";
	
	@Resource(name=JObjectRatingService.ID)
	private JObjectRatingService service;
	
	/**
	 * @publish Summary view API for object rating and comment
	 * @param contentType
	 * @param classId	Target classId 
	 * @param target	Target objectId
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/objectrating/summary", method = RequestMethod.GET)
	public ModelAndView getObjectRatingDetailList(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value="classId", 	required=true) Long classId,
			@RequestParam(value="target", 	required=true) Long target) throws JControllerException {
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		
		List<JObjectRatingDetail> detailList;
		try {
			detailList = service.getJObjectRatingDetailList(classId, target);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JObjectRatingDetail list.", e);
		}
		
		String avg = service.getAverage(detailList);
		Map<String,Object> config = new HashMap<String,Object>();
		config.put(CONFIG_AVG,avg);
		config.put(ParamKeys.COUNT, detailList.size());
		
		ModelAndView mview = jContentType.create("jobjectrating/summary");
		mview.addObject(JResultModel.DATA_KEY, detailList);
		mview.addObject(JResultModel.CONFIG_KEY, config);
		
		return mview;
	}
	
	/**
	 * 
	 * @publish Creating API for object rating and comment
	 * @param contentType
	 * @param classId	Target classId
	 * @param target	Target objectId
	 * @param value		rating value(0~10)
	 * @param comment	comment text string
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/objectrating/star/create", method = RequestMethod.POST)
	public ModelAndView createObjectRating(	
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam("classId") Long classId, 
			@RequestParam("target") Long target, 
			@RequestParam(value="value", 	required=true) Double value,
			@RequestParam(value="writer", 	required=false) String writer) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("saveObjectRating --> classId="+classId+", target="+target+", value="+value+", writer="+writer);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			service.saveObjectRating(classId, target, value, null, writer);
			ModelAndView mview = jContentType.create("jobjectrating/create");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to save JObjectRating.", e);
		}	
	}
	
	/**
	 * 
	 * @publish Creating API for object rating and comment
	 * @param contentType
	 * @param classId	Target classId
	 * @param target	Target objectId
	 * @param value		rating value(0~10)
	 * @param comment	comment text string
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/objectrating/comment/create", method = RequestMethod.POST)
	public ModelAndView createObjectRating(	
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam("classId") Long classId, 
			@RequestParam("target") Long target, 
			@RequestParam(value="comment", 	required=true) String comment,
			@RequestParam(value="writer", 	required=false) String writer) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("saveObjectRating --> classId="+classId+", target="+target+", writer="+writer+", comment="+comment);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			service.saveObjectRating(classId, target, null, comment, writer);
			ModelAndView mview = jContentType.create("jobjectrating/create");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to save JObjectRating.", e);
		}	
	}
	
	/**
	 * 
	 * @publish Creating API for object rating and comment
	 * @param contentType
	 * @param classId	Target classId
	 * @param target	Target objectId
	 * @param value		rating value(0~10)
	 * @param comment	comment text string
	 * @param writer	writer name or nickname
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/objectrating/create", method = RequestMethod.POST)
	public ModelAndView createObjectRating(	
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam("classId") Long classId, 
			@RequestParam("target") Long target, 
			@RequestParam(value="value", 	required=false) Double value,
			@RequestParam(value="comment", 	required=false) String comment,
			@RequestParam(value="writer", 	required=false) String writer) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("saveObjectRating --> classId="+classId+", target="+target+", value="+value+", comment="+comment+", writer="+writer);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		try {
			service.saveObjectRating(classId, target, value, comment, writer);
			ModelAndView mview = jContentType.create("jobjectrating/create");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to save JObjectRating.", e);
		}	
	}
	
}
