package jlook.framework.interfaces.common;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.common.JPreference;
import jlook.framework.domain.common.JPreferenceDetail;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.PreferenceModel;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JBaseController;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.interfaces.JInvalidParamException;
import jlook.framework.interfaces.ParamKeys;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * @publish PreferenceService
 * @author hsjeon70
 *
 */
@Controller
public class JPreferenceController {
	private static Log logger = Log.getLog(JPreferenceController.class);
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService service;
	
	@Resource(name=JBaseController.ID)
	private JBaseController controller;
	
	@Resource(name=JGenericService.ID)
	private JGenericService jGenericService;
	
	/**
	 * @publish Summary view API for preference service
	 * @param contentType
	 * @param pageNumber
	 * @param pageSize
	 * @param keyword
	 * @param parentOid
	 * @param parentCid
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/jpreference/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return controller.getSummary(contentType, JMetaKeys.JPREFERENCE, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/jpreference/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return controller.getDetail(contentType, JMetaKeys.JDOMAIN, objectId, parentCid, parentOid);
	}
	
	@RequestMapping(value = "/jpreference/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return controller.getTitle(contentType, JMetaKeys.JPREFERENCE, objectId);
	}
	
	@RequestMapping(value = "/jpreference/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return controller.remove(contentType, JMetaKeys.JPREFERENCE, objectId);
	}
	
	@RequestMapping(value = "/jpreference/{type}", method = RequestMethod.GET)
	public ModelAndView getPreference(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@PathVariable String type) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("preference type --> "+type);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		JPreferenceType preferenceType = null;
		try {
			preferenceType = JPreferenceType.valueOf(type.toUpperCase());
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{"preference type", type});
		}
		
		List<PreferenceModel> preferences;
		try {
			preferences = service.getPreferenceModels(preferenceType);
			ModelAndView mview = jContentType.create("jprefrence/list");
			mview.addObject(JResultModel.DATA_KEY, preferences);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get JPreference("+type+").",e);
		}
	}
	
	@RequestMapping(value = "/jpreference/save", method = RequestMethod.POST)
	public ModelAndView savePreference(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=JPreference.A_TYPE,		required=true) String type,
			@RequestParam(value=JPreference.A_NAME,		required=true) String name,
			@RequestParam(value=JPreferenceDetail.A_TARGET,	required=true) Long target,
			@RequestParam(value=JPreferenceDetail.A_VALUE,	required=false) String value) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("preference type --> "+type);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		JPreferenceType preferenceType = null;
		try {
			preferenceType = JPreferenceType.valueOf(type.toUpperCase());
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{"preference type", type});
		}
		
		try {
			this.service.savePreperfence(name, preferenceType, target, value);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to save Preference.", e);
		}
		ModelAndView mview = jContentType.create("jpreference/save");
		return mview;
	}
	
	@RequestMapping(value = "/jpreference/create", method = RequestMethod.POST)
	public ModelAndView createPreference(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=JPreference.A_TYPE,		required=true) String type,
			@RequestParam(value=JPreference.A_NAME,			required=true) String name,
			@RequestParam(value=JPreference.A_DESCRIPTION,	required=false) String description,
			@RequestParam(value=JPreference.A_DEFAULT_VALUE,required=false) String defaultValue) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("preference type --> "+type);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		JPreferenceType preferenceType = null;
		try {
			preferenceType = JPreferenceType.valueOf(type.toUpperCase());
		} catch(Exception e) {
			throw new JInvalidParamException(new Object[]{"preference type", type});
		}
		
		if(name==null || name.equals("")) {
			throw new JInvalidParamException(new Object[]{"preference name", name});
		}
		
		try {
			this.service.create(name, description, preferenceType, defaultValue);
			ModelAndView mview = jContentType.create("jpreference/create");
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to create Preference.", e);
		}
	}
	
}
