package jlook.framework.interfaces.common;

import javax.annotation.Resource;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.JControllerException;
import jlook.framework.service.JMenuService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @publish MenuService
 * @author hsjeon70
 *
 */
@Controller
public class JMenuController {
	private static Log logger = Log.getLog(JMenuController.class);
	
	@Resource(name=JMenuService.ID)
	private JMenuService jMenuService;
	
	@RequestMapping(value = "/jmenu/list", method=RequestMethod.GET)
	public ModelAndView getMyMenuList(@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("getMyMenuList --> ");
		}
		
		try {
			JResultModel rmodel = this.jMenuService.getMyMenuList();
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jmenu/list");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get menu list.", e);
		}
	}
	
	@RequestMapping(value = "/jmenu/public", method=RequestMethod.GET)
	public ModelAndView getPublicMenuList(@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType) throws JControllerException {
		if(logger.isDebugEnabled()) {
			logger.debug("getPublicMenuList --> ");
		}
		
		try {
			JResultModel rmodel = this.jMenuService.getPublicMenuList();
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("jmenu/public");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get menu list.", e);
		}
	}
}
