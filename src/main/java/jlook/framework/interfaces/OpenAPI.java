package jlook.framework.interfaces;


import org.springframework.http.HttpMethod;


/**
 * URI Convention : ${context}/service/openapi/${domain.name}/${jclass.name}/operation
 * 	operation : summary(GET), detail(GET), save(PUT), create(POST), remove(DELETE)
 * 
 * @author hsjeon70
 *
 */
public enum OpenAPI {
	summary,
	detail,
	title,
	save,
	create,
	remove;
	
	OpenAPI() {	
	}
	
	private String target;
	
	

	public String getTarget() {
		return target;
	}



	public void setTarget(String target) {
		this.target = target;
	}



	/**
	 * /${jclass.name}/operation
	 * 
	 * @param requestUri
	 * @return
	 */
	public static OpenAPI getJOpenAPI(String requestUri, HttpMethod method) throws JControllerException {
		int idx = requestUri.indexOf("/");
		String jclassName = null, operation = null;
		
		if(idx==-1) {
			jclassName = requestUri;
			operation = summary.name();
		} else {
			jclassName = requestUri.substring(0,idx);
			operation = requestUri.substring(idx+1);
		}
		
		OpenAPI api = null;
		try {
			api = OpenAPI.valueOf(operation);
			api.setTarget(jclassName);
		} catch(Exception e) {
			throw new JControllerException("Invalid requestUri. - "+requestUri);
		}
		
		switch(method) {
		case GET 	: if(api.equals(summary) || api.equals(detail) || api.equals(title)) return api;
		case PUT 	: if(api.equals(save)) 	return api;
		case POST 	: if(api.equals(create)) return api;
		case DELETE : if(api.equals(remove)) return api;
		}
		throw new JControllerException("Invalid operation for requestUri("+method+", "+requestUri+"). - "+api);
	}
	
	
}
