package jlook.framework.interfaces;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.util.ExcelReader;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.service.JClassService;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


/**
 * Generic Open API for entity.<br>
 * This API provides generic service to access any entity.
 * 
 * @publish GenericService
 * @author hsjeon70(hsjeon70@gmail.com)
 *
 */
@Controller(JBaseController.ID)
public class JGenericController extends JBaseController {
	private static Log logger = Log.getLog(JGenericController.class);
	
	@Resource(name=JGenericService.ID)
	private JGenericService service;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	/**
	 * This API responses summary view data for entity.
	 * @publish General summary view API
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param pageNumber	URL Parameter 	: Current page number 
	 * @param pageSize		URL Parameter	: Display page size
	 * @param keyword		URL Parameter  	: Search word
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	Summary view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/summary", method = RequestMethod.GET)
	public ModelAndView getSummary(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		
		return super.getSummary(contentType, classId, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	/**
	 * This API responses detail view data for entity.
	 * @publish General detail view API
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param objectId		URL Parameter 	: ObjectId for entity
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	Detail view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/detail", method = RequestMethod.GET)
	public ModelAndView getDetail(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		return super.getDetail(contentType, classId, objectId, parentCid, parentOid);
	}
	
	/**
	 * This API responses title view data for entity.
	 * @publish General title view API
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param objectId		URL Parameter 	: ObjectId for entity
	 * @return	Title view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/title", method = RequestMethod.GET)
	public ModelAndView getTitle(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId) throws JControllerException {
		return super.getTitle(contentType, classId, objectId);
	}
	
	/**
	 * This API removes an entity.
	 * 
	 * @publish General API for removing an entity
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param objectId		URL Parameter 	: ObjectId for entity
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generic/remove", method = RequestMethod.DELETE)
	public ModelAndView remove(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=true) Long objectId) throws JControllerException {
		return super.remove(contentType, classId, objectId);
	}
	
	
	/**
	 * This API creates an entity.<br>
	 * 
	 * @publish General API for creating an entity.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId	URL Parameter 	: ClassId for entity
	 * @param request	URL Parameter 	: URL parameters for attribute name and value of entity.
	 * @return	objectId of creating entity
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generic/create", method = RequestMethod.POST)
	public ModelAndView create(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			HttpServletRequest request) throws JControllerException {
		
		if(classId==null||classId.equals("")) {
			throw new JInvalidParamException(new Object[]{ParamKeys.CLASS_ID, classId});
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("CREATE DATA -------> classId="+classId);
		}
		
		JContentType jContentType = JContentType.getJContentType(contentType);
		@SuppressWarnings("unchecked")
		Map<String,String[]> params  =request.getParameterMap();
		try {
			JResultModel rmodel = this.service.create(classId, params);
			ModelAndView mview = jContentType.create("generic/create");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch(JServiceException e) {
			throw new JControllerException("Fail to create("+classId+").",e);
		}
	}
	
	/**
	 * This API updates an entity.
	 * 
	 * @publish General API for updating an entity.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId	URL Parameter 	: ClassId for entity
	 * @param objectId	URL Parameter 	: ObjectId for entity
	 * @param request	URL Parameter 	: URL parameters for attribute name and value of entity.
	 * @return	objectId of creating entity
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generic/update", method = RequestMethod.POST)
	public ModelAndView save(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=false) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			HttpServletRequest request) throws JControllerException {
		
		logger.warn("##"+request.getParameterMap().toString());
		if(classId==null||classId.equals("")) {
			throw new JInvalidParamException(new Object[]{ParamKeys.CLASS_ID, classId});
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("SAVE DATA -------> classId="+classId+", objectId="+objectId);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		@SuppressWarnings("unchecked")
		Map<String,String[]> params  =request.getParameterMap();
		try {
			JResultModel rmodel = this.service.update(classId, objectId, params);
			ModelAndView mview = jContentType.create("generic/update");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch(JServiceException e) {
			throw new JControllerException("Fail to save("+classId+").",e);
		}
	}
	
	/**
	 * 
	 * This API responses search form meta data for entity.
	 * 
	 * @publish General API to responses meta data for search form
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param objectId		URL Parameter 	: ObjectId for entity
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	search form meta data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/searchform", method = RequestMethod.GET)
	public ModelAndView getSearchForm(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.OBJECT_ID,	required=false) Long objectId,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid) throws JControllerException {
		JResultModel rmodel;
		try {
			rmodel = this.jGenericService.getSearchForm(classId, objectId, parentCid, parentOid);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("generic/searchform");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get detail view. classId="+classId,e);
		}
	}
	
	/**
	 * This API responses search result data for an entity.
	 * 
	 * @publish General API to response search result
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param pageNumber	URL Parameter 	: Current page number 
	 * @param pageSize		URL Parameter	: Display page size
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @param request		URL Parameter 	: URL parameter for attribute name and value that searches an entity.
	 * @return	search result data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/search", method = RequestMethod.GET)
	public ModelAndView getSearch(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		JSummaryModel smodel;
		@SuppressWarnings("unchecked")
		Map<String,String[]> params  =request.getParameterMap();
		try {
			PageUtil page = new PageUtil();
			page.setPageNumber(pageNumber==null? 1 : pageNumber);
			page.setPageSize(pageSize==null? PageUtil.DEFAULT_PAGE_SIZE : pageSize);
			page.setParentCid(parentCid);
			page.setParentOid(parentOid);
			smodel = this.jGenericService.getSummaryData(JViewType.summary, classId, page, params);
			JContentType jContentType = JContentType.getJContentType(contentType);
			ModelAndView mview = jContentType.create("generic/search");
			mview.addObject(JResultModel.DATA_KEY, smodel);
			return mview;
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get detail view. classId="+classId,e);
		}
	}
	
	/**
	 * This API responses title view list for an entity.
	 * 
	 * @publish General API to response title view list
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param pageNumber	URL Parameter 	: Current page number 
	 * @param pageSize		URL Parameter	: Display page size
	 * @param keyword		URL Parameter  	: Search word
	 * @return	title view list for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/titles", method = RequestMethod.GET)
	public ModelAndView getTitles(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, 	required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD,		required=false) String keyword) throws JControllerException {
		return super.getTitles(contentType, classId, pageNumber, pageSize, keyword);
	}
	
	/**
	 * This API responses reference entity list that references an entity.
	 * @publish Responses reference entity list that references an entity.
	 * @param contentType	HTTP Header 	: 'jlook.content.type'
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @return incoming reference entity list
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generic/references", method = RequestMethod.GET)
	public ModelAndView getReferences(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,	required=true) Long classId) throws JControllerException {
		Map<Long, String> map;
		try {
			map = this.jClassService.getReferences(classId);
		} catch (JServiceException e) {
			throw new JControllerException("Fail to get references for JClass. - "+classId,e);
		}
		JContentType jContentType = JContentType.getJContentType(contentType);
		ModelAndView mview = jContentType.create("generic/references");
		mview.addObject(JResultModel.DATA_KEY, map);
		return mview;
	}
	
	/**
	 * This API import excel file for classId.
	 * 
	 * @publish excel import API
	 * @param file
	 * @param classId
	 * @return
	 * @throws JControllerException
	 */
	@RequestMapping(value = "/generic/excel/import/{classId}", method = RequestMethod.POST)
	public ModelAndView importExcel(
			@RequestParam(value=ParamKeys.UPLOAD_KEY, required=true) MultipartFile file, 
			@PathVariable("classId") Long classId) throws JControllerException {
		ExcelReader reader;
		try {
			reader = new ExcelReader(file.getOriginalFilename(), file.getInputStream());
		} catch (IOException e) {
			throw new JControllerException("Fail to create ExcelReader. - "+e.getMessage(), e);
		}
		
		try {
			JResultModel rmodel = this.jGenericService.importExcel(classId, reader);
			JContentType jContentType = JContentType.JSON;
			ModelAndView mview = jContentType.create("excel/import");
			mview.addObject(JResultModel.DATA_KEY, rmodel);
			return mview;
		} catch(JServiceException e) {
			throw new JControllerException("Fail to import excel file. - "+e.getMessage(), e);
		} 
	}
	
	/**
	 * This API responses summary view data for entity.
	 * @publish General summary view API
	 * @param contentType	HTTP Header 	: 'jlook.content.type' 
	 * @param classId		URL Parameter 	: ClassId for entity
	 * @param pageNumber	URL Parameter 	: Current page number 
	 * @param pageSize		URL Parameter	: Display page size
	 * @param keyword		URL Parameter  	: Search word
	 * @param parentOid		URL Parameter	: ObjectId for parent entity
	 * @param parentCid		URL Parameter 	: ClassId for parent entity
	 * @return	Summary view data for entity
	 * @throws JControllerException	
	 */
	@RequestMapping(value = "/generic/export", method = RequestMethod.GET)
	public ModelAndView exportExcel(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@RequestParam(value=ParamKeys.CLASS_ID,		required=true) Long classId,
			@RequestParam(value=ParamKeys.PAGE_NUMBER, 	required=false) Integer pageNumber, 
			@RequestParam(value=ParamKeys.PAGE_SIZE, 	required=false) Integer pageSize, 
			@RequestParam(value=ParamKeys.KEYWORD, 		required=false) String keyword, 
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		return super.getSummary(JContentType.EXCEL_MIME_TYPE, classId, pageNumber, pageSize, keyword, parentCid, parentOid, request);
	}
	
	@RequestMapping(value = "/generic/widget/{widget}", method = RequestMethod.GET)
	public ModelAndView getWidget(
			@RequestHeader(value=JHeader.JCONTENT_TYPE, required=false) String contentType,
			@PathVariable(ParamKeys.WIDGET) Long widget,
			@RequestParam(value=ParamKeys.PARENT_CID, 	required=false) Long parentCid, 
			@RequestParam(value=ParamKeys.PARENT_OID, 	required=false) Long parentOid,
			HttpServletRequest request) throws JControllerException {
		return super.getWidget(contentType, widget, parentCid, parentOid, request);
	}
}
