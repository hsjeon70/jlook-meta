package jlook.framework.interfaces;

import jlook.framework.infrastructure.annotation.Message;

@Message(text=JInvalidParamException.TEXT)
public class JInvalidParamException extends JControllerException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Invalid parameter. {0} - {1}";
	
	public JInvalidParamException(Object[] parameter) {
		super(TEXT, parameter);
	}
	
	public JInvalidParamException(String msg, Object[] parameter) {
		super(msg, parameter);
	}
	
	public JInvalidParamException(Throwable cause, Object[] parameter) {
		super(cause, parameter);
	}
}
