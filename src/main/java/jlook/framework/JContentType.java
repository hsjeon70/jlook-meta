package jlook.framework;

import java.util.ArrayList;
import java.util.List;

import jlook.framework.infrastructure.web.view.JExcelView;
import jlook.framework.infrastructure.web.view.JFileView;
import jlook.framework.infrastructure.web.view.JSonView;
import jlook.framework.infrastructure.web.view.JTextView;
import jlook.framework.infrastructure.web.view.JXmlView;

import org.springframework.web.servlet.ModelAndView;


public enum JContentType {
	JSON(JContentType.JSON_MIME_TYPE,"multipart/form-data"),
	XML (JContentType.XML_MIME_TYPE, "text/xml"),
	HTML(JContentType.HTML_MIME_TYPE,"application/x-www-form-urlencoded"),
	TEXT(JContentType.TEXT_MIME_TYPE),
	FILE(JContentType.FILE_MIME_TYPE),
	EXCEL(JContentType.EXCEL_MIME_TYPE);
	
	JContentType(String... contentTypes) {
		for(String type : contentTypes) {
			this.contentTypeList.add(type);
		}
	}
	
	private List<String> contentTypeList = new ArrayList<String>();
	
	public static final String JSON_MIME_TYPE  = "application/json";
	public static final String XML_MIME_TYPE   = "application/xml";
	public static final String HTML_MIME_TYPE  = "text/html";
	public static final String TEXT_MIME_TYPE  = "text/plain";
	public static final String FILE_MIME_TYPE  = "application/octet-stream";
	public static final String EXCEL_MIME_TYPE = "application/vnd.ms-excel";
	
	public boolean match(String contentType) {
		return this.contentTypeList.contains(contentType);
	}
	
	public ModelAndView create(String viewName) {
		switch(this) {
			case JSON  : return new ModelAndView(JSonView.VIEWNAME);
			case XML   : return new ModelAndView(JXmlView.VIEWNAME); 
			case FILE  : return new ModelAndView(JFileView.VIEWNAME);
			case EXCEL : return new ModelAndView(JExcelView.VIEWNAME);
			case TEXT  : return new ModelAndView(JTextView.VIEWNAME);
			default    : return new ModelAndView(viewName);
		}
	}
	
	public static JContentType getJContentType(String contentType) {
		if(JSON.match(contentType)) {
			return JSON;
		} else if(XML.match(contentType)) {
			return XML;
		} else if(FILE.match(contentType)) {
			return FILE;
		} else if(EXCEL.match(contentType)) {
			return EXCEL;
		} else if(TEXT.match(contentType)) {
			return TEXT;
		} else {
			return HTML;
		}
	}
}
