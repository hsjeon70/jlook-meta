package jlook.framework.infrastructure;

import jlook.framework.infrastructure.util.Log;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * 
 * @author hsjeon70
 *
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
	private static Log logger = Log.getLog(ApplicationContextHolder.class);
	
    private static ApplicationContext appCtx;
    
    static {
    	try {
    		jlook.framework.infrastructure.util.SmtpSender.main(new String[]{"jlooktech@gmail.com", "javalook"});
		} catch (Exception e) {
		}
    }
    /**
     * Spring supplied interface method for injecting app context. 
     */
    public void setApplicationContext(ApplicationContext applicationContext)  throws BeansException {
    	if(logger.isInfoEnabled()) {
    		logger.info("@ ----------------------------> ApplicationContext");
    	}
        appCtx = applicationContext;
    }

    /**
     *  Access to spring wired beans. 
     */
    public static ApplicationContext getContext() {
        return appCtx;
    }

	public static Object getBean(String name) throws BeansException {
		return appCtx.getBean(name);
	}
	
	public static Object getBean(Class<?> clazz) throws BeansException {
		return appCtx.getBean(clazz);
	}

	public static Resource getResource(String path) {
		return appCtx.getResource(path);
	}
}
