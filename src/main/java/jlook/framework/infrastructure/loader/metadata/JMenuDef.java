package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JWidgetType;
import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.common.JMenuTarget;
import jlook.framework.domain.metadata.JAccessRule;

public enum JMenuDef {
	ACCOUNT("acount","Account Management","Account management", null, null, null, null, 1, JAccessRule.PROTECTED_TYPE),
	DOMAIN("domain","Domain","Domain management", JMenuDef.ACCOUNT.instance, "/service/generic/summary?classId="+JMetaKeys.JDOMAIN, JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	GROUP("group","Group","Group management", JMenuDef.ACCOUNT.instance, "/service/generic/summary?classId="+JMetaKeys.JGROUP, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	USER("user","User","User management", JMenuDef.ACCOUNT.instance, "/service/generic/summary?classId="+JMetaKeys.JUSER, JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE),
	
	SECURITY("security","Security Management","Security management", null, null, null, null, 2, JAccessRule.PROTECTED_TYPE),
	ROLE("role","Role","Role management", JMenuDef.SECURITY.instance, "/service/generic/summary?classId="+JMetaKeys.JROLE, JMenuTarget.MAIN,JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	DATAACCESSRULE("dataaccessrule","Data Access Rule","Data Access Rule management", JMenuDef.SECURITY.instance, "/service/generic/summary?classId="+JMetaKeys.JDATAACCESSRULE, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	MENUACCESSRULE("menuaccessrule","Menu Access Rule","Menu Access Rule management", JMenuDef.SECURITY.instance, "/service/generic/summary?classId="+JMetaKeys.JMENUACCESSRULE, JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE),
	WIDGETACCESSRULE("widgetaccessrule","Widget Access Rule","Widget Access Rule management", JMenuDef.SECURITY.instance, "/service/generic/summary?classId="+JMetaKeys.JWIDGETACCESSRULE, JMenuTarget.MAIN, JWidgetType.GRID, 40, JAccessRule.PROTECTED_TYPE),
	
	METADATA("metadata","Metadata Management","Metadata management", null, null, null, null, 3, JAccessRule.PROTECTED_TYPE),
	CLASS("class","Class","Class management", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JCLASS, JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	TYPE("type","Type","Type management", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JTYPE, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	VIEW("view","View","View management", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JVIEW, JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE),
	ACTION("action","Action","Action management", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JCLASSACTION, JMenuTarget.MAIN, JWidgetType.GRID, 40, JAccessRule.PROTECTED_TYPE),
	VALIDATION("validation","Validation","Validation management", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JVALIDATION, JMenuTarget.MAIN, JWidgetType.GRID, 50, JAccessRule.PROTECTED_TYPE),
	METACHANGELOG("metachangelog","ChangeLog","Metadata Change log", JMenuDef.METADATA.instance, "/service/generic/summary?classId="+JMetaKeys.JMETACHANGELOG, JMenuTarget.MAIN, JWidgetType.GRID, 60, JAccessRule.PROTECTED_TYPE),
	
	BATCH("batch","Batch Management","Batch management", null, null, null, null, 4, JAccessRule.PROTECTED_TYPE),
	BATCHDEFINITION("batchdef","Batch Definition","Batch Definition", JMenuDef.BATCH.instance, "/service/generic/summary?classId="+JMetaKeys.JBATCHDEFINITION, JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	BATCHINSTANCE("batchinst","Batch Instance","Batch Instance", JMenuDef.BATCH.instance, "/service/generic/summary?classId="+JMetaKeys.JBATCHINSTANCE, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	BATCHHISTORY("batchhist","Batch History","Batch History", JMenuDef.BATCH.instance, "/service/generic/summary?classId="+JMetaKeys.JBATCHHISTORY, JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE),
	
	WIDGET("widget","Widget Management","Widget management", null, null, null, null, 5, JAccessRule.PROTECTED_TYPE),
	WIDGETDEFINITION("widgetdef","Widget Definition","Widget Definition", JMenuDef.WIDGET.instance, "/service/generic/summary?classId="+JMetaKeys.JWIDGETDEFINITION, JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	WIDGETINSTANCE("widgetinst","Widget Instance","Widget Instance", JMenuDef.WIDGET.instance, "/service/generic/summary?classId="+JMetaKeys.JWIDGETINSTANCE, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	
	COMMON("common","Common Service","Common Service", null, null, null, null, 6, JAccessRule.PROTECTED_TYPE),
	MENU("menu","Menu","Menu Management", JMenuDef.COMMON.instance, "/service/generic/summary?classId="+JMetaKeys.JMENU, JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	PREFERENCE("preference","Preference","Preference Management", JMenuDef.COMMON.instance, "/service/generic/summary?classId="+JMetaKeys.JPREFERENCE, JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	ANNOUNCEMENT("announcement","Announcement","Announcement Management", JMenuDef.COMMON.instance, "/service/generic/summary?classId="+JMetaKeys.JANNOUNCEMESSAGE, JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE),
	SPECOPENAPI("specopenapi","API Speicificaiton","API Speicificaiton", JMenuDef.COMMON.instance, "/service/docs/openapi", JMenuTarget.DOCUMENT, JWidgetType.HTML, 40, JAccessRule.PROTECTED_TYPE),
	TESTOPENAPI("testopenapi","Test Open API","Test Open API", JMenuDef.COMMON.instance, "/service/page/openapi", JMenuTarget.OVERLAY, JWidgetType.HTML, 50, JAccessRule.PROTECTED_TYPE),
	
	EXAMPLES("examples","Examples","Examples", null, null, null, null, 7, JAccessRule.PROTECTED_TYPE),
	USERINFO("userinfo","UserInfo","UserInfo Entity", JMenuDef.EXAMPLES.instance, "/service/generic/summary?classId=47", JMenuTarget.MAIN, JWidgetType.GRID, 10, JAccessRule.PROTECTED_TYPE),
	DEPT("dept","Dept","Dept Entity", JMenuDef.EXAMPLES.instance, "/service/generic/summary?classId=45", JMenuTarget.MAIN, JWidgetType.GRID, 20, JAccessRule.PROTECTED_TYPE),
	EMP("emp","Emp","Emp Entity", JMenuDef.EXAMPLES.instance, "/service/generic/summary?classId=46", JMenuTarget.MAIN, JWidgetType.GRID, 30, JAccessRule.PROTECTED_TYPE);
	
	JMenuDef(String name, String label, String description, JMenu parent, String actionUrl, String target, String widget, Integer sequence, String accessRule) {
		this.instance = new JMenu();
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setName(name);
		this.instance.setLabel(label);
		this.instance.setDescription(description);
		this.instance.setActive(true);
		this.instance.setActionUrl(actionUrl);
		this.instance.setTarget(target);
		this.instance.setParent(parent);
		this.instance.setWidget(widget);
		this.instance.setSequence(sequence);
		if(accessRule==null) {
			accessRule = JAccessRule.PROTECTED_TYPE;
		}
		this.instance.setAccessRule(accessRule);
	}
	
	public JMenu instance;
}
