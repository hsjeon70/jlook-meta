package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.account.JUser;

public enum JUserDef {
	admin(1L, JDomainDef.SYSTEM.instance.getDomainId(),	"admin", "admin@jlook.com",	"1q2w3e4r"),
	batch(2L, JDomainDef.SYSTEM.instance.getDomainId(),	"batch", "batch@jlook.com",	"batch1234"),
	jlooktech(3L, JDomainDef.SYSTEM.instance.getDomainId(),	"jlooktech", "jlooktech@gmail.com",	"1q2w3e4r"),
	dosirak(4L, JDomainDef.DOSIRAK.instance.getDomainId(), "dosirak", "dosirak@gmail.com",	"1111"),
	buspia(5L, JDomainDef.BUSPIA.instance.getDomainId(), "buspia", "buspia@gmail.com",	"1111"),
	demo(6L, JDomainDef.DEMO.instance.getDomainId(), "demo", "demo@gmail.com",	"1111");
	
	JUserDef(Long objectId, Long domainId, String username, String email, String password) {
		this.instance = new JUser();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setEmail(email);
		this.instance.setNickname(username);
		this.instance.setPassword(password);
	}
	
	public JUser instance;
}
