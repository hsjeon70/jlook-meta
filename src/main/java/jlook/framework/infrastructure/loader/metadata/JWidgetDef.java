package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.widget.JWidgetDefinition;

public enum JWidgetDef {
	BasicObject(1L, "BasicObjectWidget","Object detail view widget"),
	BasicGrid(2L, "BasicGridWidget","Grid view widget"),
	GridCascade(3L, "GridCascadeWidget","Grid cascade widget"),
	MapCascade(4L, "MapCascadeWidget","Map cascade widget")
	;
	
	JWidgetDef(Long objectId, String name, String description) {
		this.instance = new JWidgetDefinition();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setName(name);
		this.instance.setDescription(description);
	}
	
	public JWidgetDefinition instance;
	
	public static JWidgetDef getJWidgetDef(String name) {
		if(name==null) {
			return null;
		}
		
		if(name.equals(BasicObject.instance.getName())) {
			return BasicObject;
		} else if(name.equals(BasicGrid.instance.getName())) {
			return BasicGrid;
		} else if(name.equals(GridCascade.instance.getName())) {
			return GridCascade;
		} else if(name.equals(MapCascade.instance.getName())) {
			return MapCascade;
		} else {
			return null;
		}
	}
}
