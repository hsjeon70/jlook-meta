package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.security.JRole;
import jlook.framework.infrastructure.JRoleType;

public enum JRoleDef {
	User(1L, JRoleType.USER, "General User"),
	Administrator(2L, JRoleType.ADMINISTRATOR, "System Administrator"),
	Admin(3L, JRoleType.ADMIN, "Domain Administrator");
	
	JRoleDef(Long objectId, JRoleType roleType, String description) {
		this.instance = new JRole();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(JDomainDef.SYSTEM.instance.getDomainId());
		this.instance.setName(roleType.getName());
		this.instance.setDescription(description);
	}
	
	public JRole instance;
	
	public static final String ADMINISTRATOR = "Administrator";
	public static final String ADMIN = "Admin";
	public static final String USER = "User";
	
	public static final long USER_ID = 1L;
	public static final long ADMINISTRATOR_ID = 2L;
	public static final long ADMIN_ID = 3L;
}
