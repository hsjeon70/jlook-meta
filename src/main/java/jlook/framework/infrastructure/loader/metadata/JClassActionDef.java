package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.HttpMethod;
import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.JWidgetType;
import jlook.framework.domain.metadata.JActionTarget;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JClassAction;
import jlook.framework.domain.metadata.JClassActionType;

public enum JClassActionDef {
	JCLASS_LIST(JMetaKeys.JCLASS, "JClassDataList", "List", JClassActionType.ALL_TYPE, "View data list for JClass", "/service/generic/summary?classId=$objectId", HttpMethod.GET, JWidgetType.GRID, JActionTarget.TAB),
	JVIEW_GENERATE(JMetaKeys.JCLASS, "JViewReGenerate", "GenView", JClassActionType.OBJECT_TYPE, "Re-generate JView for JClass", "/service/jview/regenerate/{classId}", HttpMethod.POST, JWidgetType.NONE, null)
	;
	JClassActionDef(Long classId, String name, String label, String type, String description, String targetUrl, String httpMethod, String widget, String target) {
		this.instance = new JClassAction();
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setJClass(new JClass(classId));
		this.instance.setName(name);
		this.instance.setType(type);
		this.instance.setLabel(label);
		this.instance.setDescription(description);
		this.instance.setTargetUrl(targetUrl);
		this.instance.setTargetMethod(httpMethod);
		this.instance.setWidget(widget);
		this.instance.setTarget(target);
	}
	public JClassAction instance;
}
