package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.domain.security.JRole;

public enum JDataAccessRuleDef {
	JDOMAIN_ADMINISTRATOR(JMetaKeys.JDOMAIN, JRoleDef.Administrator.instance, true, true, true, true),
	JDOMAIN_ADMIN(JMetaKeys.JDOMAIN, JRoleDef.Admin.instance, false, true, true, false),
	JDOMAIN_USER(JMetaKeys.JDOMAIN, JRoleDef.User.instance, false, true, false, false),
	
	JANNOUNCEMESSAGE_ADMINISTRATOR(JMetaKeys.JANNOUNCEMESSAGE, JRoleDef.Administrator.instance, true, true, true, true),
	JANNOUNCEMESSAGE_ADMIN(JMetaKeys.JANNOUNCEMESSAGE, JRoleDef.Admin.instance, true, true, true, true),
	JANNOUNCEMESSAGE_USER(JMetaKeys.JANNOUNCEMESSAGE, JRoleDef.User.instance, false, true, false, false),
	
	JANNOUNCEDETAIL_ADMINISTRATOR(JMetaKeys.JANNOUNCEDETAIL, JRoleDef.Administrator.instance, true, true, true, true),
	JANNOUNCEDETAIL_ADMIN(JMetaKeys.JANNOUNCEDETAIL, JRoleDef.Admin.instance, true, true, true, true),
	JANNOUNCEDETAIL_USER(JMetaKeys.JANNOUNCEDETAIL, JRoleDef.User.instance, false, false, false, false),
	
	JPREFERENCE_ADMINISTRATOR(JMetaKeys.JPREFERENCE, JRoleDef.Administrator.instance, true, true, true, true),
	JPREFERENCE_ADMIN(JMetaKeys.JPREFERENCE, JRoleDef.Admin.instance, false, true, false, false),
	JPREFERENCE_USER(JMetaKeys.JPREFERENCE, JRoleDef.User.instance, false, true, false, false),
	
	JPREFERENCEDETAIL_ADMINISTRATOR(JMetaKeys.JPREFERENCEDETAIL, JRoleDef.Administrator.instance, true, true, true, true),
	JPREFERENCEDETAIL_ADMIN(JMetaKeys.JPREFERENCEDETAIL, JRoleDef.Admin.instance, false, true, true, false),
	JPREFERENCEDETAIL_USER(JMetaKeys.JPREFERENCEDETAIL, JRoleDef.User.instance, false, true, true, false),
	;
	
	JDataAccessRuleDef(Long classId, JRole jRole, boolean create, boolean read, boolean update, boolean delete) {
		this.instance = new JDataAccessRule();
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setJClass(new JClass(classId));
		this.instance.setJRole(jRole);
		this.instance.setCanCreate(create);
		this.instance.setCanRead(read);
		this.instance.setCanUpdate(update);
		this.instance.setCanDelete(delete);
	}
	public JDataAccessRule instance;
}
