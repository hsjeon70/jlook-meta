package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.JMetaKeys;
import jlook.framework.domain.common.JObjectRating;
import jlook.framework.domain.metadata.JClass;

public enum JObjectRatingDef {
	FoodStore(1L, 2L, new JClass(JMetaKeys.JANNOUNCEMESSAGE), "Announce Message Rating");
	
	JObjectRatingDef(Long objectId, Long domainId, JClass targetClass, String description) {
		this.instance = new JObjectRating();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setTargetClass(targetClass);
		this.instance.setDescription(description);
	}
	
	public JObjectRating instance;
}
