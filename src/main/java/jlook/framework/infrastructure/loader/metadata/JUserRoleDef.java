package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.account.JUser;
import jlook.framework.domain.account.JUserRole;
import jlook.framework.domain.security.JRole;

public enum JUserRoleDef {
	admin_Administrator(1L,1L,JUserDef.admin.instance, JRoleDef.Administrator.instance),
	admin_Admin(1L,1L,JUserDef.admin.instance, JRoleDef.Admin.instance),
	demo_Admin(1L,1L,JUserDef.demo.instance, JRoleDef.Admin.instance),
	dosirak_Admin(1L,1L,JUserDef.dosirak.instance, JRoleDef.Admin.instance),
	buspia_Admin(1L,1L,JUserDef.buspia.instance, JRoleDef.Admin.instance);
	
	JUserRoleDef(Long objectId, Long domainId, JUser jUser, JRole jRole) {
		this.instance = new JUserRole();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setJUser(jUser);
		this.instance.setJRole(jRole);
	}
	
	public JUserRole instance;
}
