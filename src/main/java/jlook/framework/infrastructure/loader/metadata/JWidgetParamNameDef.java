package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.widget.JWidgetDefinition;
import jlook.framework.domain.widget.JWidgetParamName;
import jlook.framework.domain.widget.JWidgetParamType;

/**
 ATTR1=$nickname, ATTR2=@name
 
 * @author hsjeon70
 *
 */
public enum JWidgetParamNameDef {
	BasicObject_SetAttributes		(JWidgetDef.BasicObject.instance, 	"set_attributes",			JWidgetParamType.SERVER.name(), "set attributes initially",	null),
	BasicGrid_OverrideObjectWidget	(JWidgetDef.BasicGrid.instance, 	"override_object_widget",	JWidgetParamType.CLIENT.name(), "override object widget",	null),
	GridCascade_GridClassList		(JWidgetDef.GridCascade.instance, 	"grid_class_list",			JWidgetParamType.CLIENT.name(), "grid class list",			null),
	MapCascade_MappingClassList		(JWidgetDef.MapCascade.instance,  	"map_class_list",			JWidgetParamType.CLIENT.name(), "map class list",			null),
	;
	
	JWidgetParamNameDef(JWidgetDefinition jwidget, String name, String type, String description, String defaultValue) {
		this.instance = new JWidgetParamName();
		this.instance.setJWidgetDefinition(jwidget);
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setName(name);
		this.instance.setType(type);
		this.instance.setDescription(description);
		this.instance.setDefaultValue(defaultValue);
	}
	
	public JWidgetParamName instance;
}
