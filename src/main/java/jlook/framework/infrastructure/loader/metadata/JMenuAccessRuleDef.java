package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.common.JMenu;
import jlook.framework.domain.security.JMenuAccessRule;
import jlook.framework.domain.security.JRole;

public enum JMenuAccessRuleDef {
	ADMINISTRATOR_ACCOUNT(JRoleDef.Administrator.instance, JMenuDef.ACCOUNT.instance),
	ADMINISTRATOR_DOMAIN(JRoleDef.Administrator.instance, JMenuDef.DOMAIN.instance),
	ADMINISTRATOR_GROUP(JRoleDef.Administrator.instance, JMenuDef.GROUP.instance),
	ADMINISTRATOR_USER(JRoleDef.Administrator.instance, JMenuDef.USER.instance),
	
	ADMINISTRATOR_SECURITY(JRoleDef.Administrator.instance, JMenuDef.SECURITY.instance),
	ADMINISTRATOR_ROLE(JRoleDef.Administrator.instance, JMenuDef.ROLE.instance),
	ADMINISTRATOR_DATAACCESSRULE(JRoleDef.Administrator.instance, JMenuDef.DATAACCESSRULE.instance),
	ADMINISTRATOR_MENUACCESSRULE(JRoleDef.Administrator.instance, JMenuDef.MENUACCESSRULE.instance),
	ADMINISTRATOR_WIDGETACCESSRULE(JRoleDef.Administrator.instance, JMenuDef.WIDGETACCESSRULE.instance),
	
	ADMINISTRATOR_METADATA(JRoleDef.Administrator.instance, JMenuDef.METADATA.instance),
	ADMINISTRATOR_ENTITY(JRoleDef.Administrator.instance, JMenuDef.CLASS.instance),
	ADMINISTRATOR_TYPE(JRoleDef.Administrator.instance, JMenuDef.TYPE.instance),
	ADMINISTRATOR_VIEW(JRoleDef.Administrator.instance, JMenuDef.VIEW.instance),
	ADMINISTRATOR_ACTION(JRoleDef.Administrator.instance, JMenuDef.ACTION.instance),
	ADMINISTRATOR_VALIDATION(JRoleDef.Administrator.instance, JMenuDef.VALIDATION.instance),
	ADMINISTRATOR_METACHANGELOG(JRoleDef.Administrator.instance, JMenuDef.METACHANGELOG.instance),
	
	ADMINISTRATOR_BATCH(JRoleDef.Administrator.instance, JMenuDef.BATCH.instance),
	ADMINISTRATOR_BATCHDEFINITION(JRoleDef.Administrator.instance, JMenuDef.BATCHDEFINITION.instance),
	ADMINISTRATOR_BATCHINSTANCE(JRoleDef.Administrator.instance, JMenuDef.BATCHINSTANCE.instance),
	ADMINISTRATOR_BATCHHISTORY(JRoleDef.Administrator.instance, JMenuDef.BATCHHISTORY.instance),
	
	ADMINISTRATOR_WIDGET(JRoleDef.Administrator.instance, JMenuDef.WIDGET.instance),
	ADMINISTRATOR_WIDGETDEFINITION(JRoleDef.Administrator.instance, JMenuDef.WIDGETDEFINITION.instance),
	ADMINISTRATOR_WIDGETINSTANCE(JRoleDef.Administrator.instance, JMenuDef.WIDGETINSTANCE.instance),
	
	ADMINISTRATOR_COMMON(JRoleDef.Administrator.instance, JMenuDef.COMMON.instance),
	ADMINISTRATOR_MENU(JRoleDef.Administrator.instance, JMenuDef.MENU.instance),
	ADMINISTRATOR_PREFERENCE(JRoleDef.Administrator.instance, JMenuDef.PREFERENCE.instance),
	ADMINISTRATOR_ANNOUNCEMENT(JRoleDef.Administrator.instance, JMenuDef.ANNOUNCEMENT.instance),
	ADMINISTRATOR_TESTOPENAPI(JRoleDef.Administrator.instance, JMenuDef.TESTOPENAPI.instance),
	ADMINISTRATOR_SPECOPENAPI(JRoleDef.Administrator.instance, JMenuDef.SPECOPENAPI.instance),
	
	// --------------------------------------------------------------------
	
	ADMIN_ACCOUNT(JRoleDef.Admin.instance, JMenuDef.ACCOUNT.instance),
	ADMIN_DOMAIN(JRoleDef.Admin.instance, JMenuDef.DOMAIN.instance),
	ADMIN_GROUP(JRoleDef.Admin.instance, JMenuDef.GROUP.instance),
	ADMIN_USER(JRoleDef.Admin.instance, JMenuDef.USER.instance),
	
	ADMIN_SECURITY(JRoleDef.Admin.instance, JMenuDef.SECURITY.instance),
	ADMIN_ROLE(JRoleDef.Admin.instance, JMenuDef.ROLE.instance),
	ADMIN_DATAACCESSRULE(JRoleDef.Admin.instance, JMenuDef.DATAACCESSRULE.instance),
	ADMIN_MENUACCESSRULE(JRoleDef.Admin.instance, JMenuDef.MENUACCESSRULE.instance),
	ADMIN_WIDGETACCESSRULE(JRoleDef.Admin.instance, JMenuDef.WIDGETACCESSRULE.instance),
	
	ADMIN_METADATA(JRoleDef.Admin.instance, JMenuDef.METADATA.instance),
	ADMIN_ENTITY(JRoleDef.Admin.instance, JMenuDef.CLASS.instance),
	ADMIN_TYPE(JRoleDef.Admin.instance, JMenuDef.TYPE.instance),
	ADMIN_VIEW(JRoleDef.Admin.instance, JMenuDef.VIEW.instance),
	ADMIN_VALIDATION(JRoleDef.Admin.instance, JMenuDef.VALIDATION.instance),
	ADMIN_ACTION(JRoleDef.Admin.instance, JMenuDef.ACTION.instance),
	ADMIN_METACHANGELOG(JRoleDef.Admin.instance, JMenuDef.METACHANGELOG.instance),
	
	ADMIN_BATCH(JRoleDef.Admin.instance, JMenuDef.BATCH.instance),
	ADMIN_BATCHDEFINITION(JRoleDef.Admin.instance, JMenuDef.BATCHDEFINITION.instance),
	ADMIN_BATCHINSTANCE(JRoleDef.Admin.instance, JMenuDef.BATCHINSTANCE.instance),
	ADMIN_BATCHHISTORY(JRoleDef.Admin.instance, JMenuDef.BATCHHISTORY.instance),
	
	ADMIN_WIDGET(JRoleDef.Admin.instance, JMenuDef.WIDGET.instance),
	ADMIN_WIDGETDEFINITION(JRoleDef.Admin.instance, JMenuDef.WIDGETDEFINITION.instance),
	ADMIN_WIDGETINSTANCE(JRoleDef.Admin.instance, JMenuDef.WIDGETINSTANCE.instance),
	
	ADMIN_COMMON(JRoleDef.Admin.instance, JMenuDef.COMMON.instance),
	ADMIN_MENU(JRoleDef.Admin.instance, JMenuDef.MENU.instance),
	ADMIN_PREFERENCE(JRoleDef.Admin.instance, JMenuDef.PREFERENCE.instance),
	ADMIN_ANNOUNCEMENT(JRoleDef.Admin.instance, JMenuDef.ANNOUNCEMENT.instance),
	ADMIN_TESTOPENAPI(JRoleDef.Admin.instance, JMenuDef.TESTOPENAPI.instance),
	ADMIN_SPECOPENAPI(JRoleDef.Admin.instance, JMenuDef.SPECOPENAPI.instance),
	
	USER_COMMON(JRoleDef.User.instance, JMenuDef.COMMON.instance),
	USER_PREFERENCE(JRoleDef.User.instance, JMenuDef.PREFERENCE.instance),
	USER_ANNOUNCEMENT(JRoleDef.User.instance, JMenuDef.ANNOUNCEMENT.instance),
	
	USER_EXAMPLES(JRoleDef.User.instance, JMenuDef.EXAMPLES.instance),
	USER_DEPT(JRoleDef.User.instance, JMenuDef.DEPT.instance),
	USER_EMP(JRoleDef.User.instance, JMenuDef.EMP.instance),
	USER_USERINFO(JRoleDef.User.instance, JMenuDef.USERINFO.instance);
	
	JMenuAccessRuleDef(JRole jRole, JMenu jMenu) {
		this.instance = new JMenuAccessRule();
		this.instance.setDomainId(JDomainDef.SYSTEM_ID);
		this.instance.setJMenu(jMenu);
		this.instance.setJRole(jRole);
		this.instance.setEnabled(true);
	}
	
	public JMenuAccessRule instance;
}
