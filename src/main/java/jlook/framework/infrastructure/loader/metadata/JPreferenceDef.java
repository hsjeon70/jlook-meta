package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.JPreferenceKeys;
import jlook.framework.domain.common.JPreference;

public enum JPreferenceDef {
	APPLICATION_NAME(1L,JDomainDef.SYSTEM_ID,JPreferenceKeys.APPLICATION_NAME,"Application name",JPreferenceType.SYSTEM,"Mobile Gateway Platform"),
	APPLICATION_VERSION(2L,JDomainDef.SYSTEM_ID,JPreferenceKeys.APPLICATION_VERSION,"Application version",JPreferenceType.SYSTEM,"0.20"),
	APPLICATION_ADMINISTRATOR(3L,JDomainDef.SYSTEM_ID,JPreferenceKeys.APPLICATION_ADMINISTRATOR,"Application administrator email address",JPreferenceType.SYSTEM,"hsjeon70@gmail.com"),
	DOMAIN_VERSION(4L,JDomainDef.SYSTEM_ID,JPreferenceKeys.DOMAIN_VERSION,"Domain version",JPreferenceType.DOMAIN,null),
	DOMAIN_ADMINISTRATOR(5L,JDomainDef.SYSTEM_ID,JPreferenceKeys.DOMAIN_ADMINISTRATOR,"Domain administrator email address",JPreferenceType.DOMAIN,null),
	DOMAIN_REDIRECT_URL(5L,JDomainDef.SYSTEM_ID,JPreferenceKeys.DOMAIN_SIGNIN_REDIRECT_URL,"Domain redirect url after signing in",JPreferenceType.DOMAIN,null),
	USER_CONTACT_INFO(6L,JDomainDef.SYSTEM_ID,JPreferenceKeys.USER_CONTACT_INFO,"User contact information",JPreferenceType.USER,null),
	USER_REDIRECT_URL(6L,JDomainDef.SYSTEM_ID,JPreferenceKeys.USER_SIGNIN_REDIRECT_URL,"User redirect url after signing in",JPreferenceType.USER,null);
	
	JPreferenceDef(Long objectId, Long domainId, String name, String description, JPreferenceType type, String defaultValue) {
		this.instance = new JPreference();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setName(name);
		this.instance.setDescription(description);
		this.instance.setType(type.name());
		this.instance.setDefaultValue(defaultValue);
	}
	
	public JPreference instance;
}
