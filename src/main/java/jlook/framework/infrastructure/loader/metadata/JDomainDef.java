package jlook.framework.infrastructure.loader.metadata;

import jlook.framework.domain.account.JDomain;

public enum JDomainDef {
	SYSTEM(JDomainDef.SYSTEM_ID, JDomainDef.SYSTEM_ID,"SYSTEM","SYSTEM App"),
	DOSIRAK(2L, 2L,"DOSIRAK","DOSIRAK App"),
	BUSPIA(3L, 3L,"BUSPIA","BUSPIA App"),
	DEMO(4L, 4L,"DEMO","DEMO App");
	
	JDomainDef(Long objectId, Long domainId,String name,String description) {
		this.instance = new JDomain();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setName(name);
		this.instance.setActive(true);
		this.instance.setDescription(description);
	}
	
	public static final long SYSTEM_ID = 1L;
	public JDomain instance;
}
