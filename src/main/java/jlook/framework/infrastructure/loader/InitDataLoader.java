package jlook.framework.infrastructure.loader;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.loader.metadata.JClassActionDef;
import jlook.framework.infrastructure.loader.metadata.JDataAccessRuleDef;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JMenuAccessRuleDef;
import jlook.framework.infrastructure.loader.metadata.JMenuDef;
import jlook.framework.infrastructure.loader.metadata.JObjectRatingDef;
import jlook.framework.infrastructure.loader.metadata.JPreferenceDef;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;
import jlook.framework.infrastructure.loader.metadata.JUserDef;
import jlook.framework.infrastructure.loader.metadata.JUserRoleDef;
import jlook.framework.infrastructure.loader.metadata.JWidgetDef;
import jlook.framework.infrastructure.loader.metadata.JWidgetParamNameDef;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassActionService;
import jlook.framework.service.JDataAccessRuleService;
import jlook.framework.service.JDomainService;
import jlook.framework.service.JMenuAccessRuleService;
import jlook.framework.service.JMenuService;
import jlook.framework.service.JObjectRatingService;
import jlook.framework.service.JPreferenceService;
import jlook.framework.service.JRoleService;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JUserService;
import jlook.framework.service.JWidgetService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Application Lifecycle Listener implementation class InitDataListner
 *
 */
@Component(InitDataLoader.ID)
public class InitDataLoader extends BaseLoader {
	private static Log logger = Log.getLog(InitDataLoader.class);
	
	public static final String ID = "initDataLoader";
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JUserService.ID)
	private JUserService jUserService;
	
	@Resource(name=JRoleService.ID)
	private JRoleService jRoleService;
	
	@Resource(name=JObjectRatingService.ID)
	private JObjectRatingService jObjectRatingService;
	
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	@Resource(name=JPreferenceService.ID)
	private JPreferenceService jPreferenceService;
	
	@Resource(name=JMenuService.ID)
	private JMenuService jMenuService;
	
	@Resource(name=JMenuAccessRuleService.ID)
	private JMenuAccessRuleService jMenuAccessRuleService;
	
	@Resource(name=JDataAccessRuleService.ID)
	private JDataAccessRuleService jDataAccessRuleService;
	
	@Resource(name=JClassActionService.ID)
	private JClassActionService jClassActionService;
	
	@Resource(name=JWidgetService.ID)
	private JWidgetService jWidgetService;
	
	private boolean loaded;
	
	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}
	/**
     * Default constructor. 
     */
    public InitDataLoader() {
    }
    
    @PostConstruct
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}
			
		}
    }
    
    private void initialize() throws JServiceException, JContextException {
    	
    	JPreferenceDef[] jPreferenceDefs = JPreferenceDef.values();
    	for(JPreferenceDef def : jPreferenceDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jPreferenceService.create(def.instance);
    	}
    	
    	JDomainDef[] jDomainDefs = JDomainDef.values();
    	for(JDomainDef def : jDomainDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
			jDomainService.create(def.instance);
    	}
    	
    	JRoleDef[] jRoleDefs = JRoleDef.values();
    	for(JRoleDef def : jRoleDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
			jRoleService.create(def.instance);
    	}
    	
    	JUserDef[] jUserDefs = JUserDef.values();
    	for(JUserDef def : jUserDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jUserService.create(def.instance);
    	}
		
    	JUserRoleDef[] jUserRoleDefs = JUserRoleDef.values();
    	for(JUserRoleDef def : jUserRoleDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jUserService.create(def.instance);
    	}
    	
    	jContext.putDomainId(JDomainDef.SYSTEM.instance.getObjectId());
    	
    	JObjectRatingDef[] jObjectRatingDefs = JObjectRatingDef.values();
    	for(JObjectRatingDef def : jObjectRatingDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jObjectRatingService.create(def.instance);
    	}
    	
    	JMenuDef[] jMenuDefs = JMenuDef.values();
    	for(JMenuDef def : jMenuDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jMenuService.create(def.instance);
    	}
    	
    	JMenuAccessRuleDef[] jMenuAccessRuleDefs = JMenuAccessRuleDef.values();
    	for(JMenuAccessRuleDef def : jMenuAccessRuleDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jMenuAccessRuleService.create(def.instance);
    	}
    	
    	JDataAccessRuleDef[] jDataAccessRuleDefs = JDataAccessRuleDef.values();
    	for(JDataAccessRuleDef def : jDataAccessRuleDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jDataAccessRuleService.create(def.instance);
    	}
    	
    	JClassActionDef[] jClassActionDefs = JClassActionDef.values();
    	for(JClassActionDef def : jClassActionDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jClassActionService.create(def.instance);
    	}
    	
    	JWidgetDef[] jWidgetDef = JWidgetDef.values();
    	for(JWidgetDef def : jWidgetDef) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jWidgetService.create(def.instance);
    	}
    	
    	JWidgetParamNameDef[] jWidgetParamNameDef = JWidgetParamNameDef.values();
    	for(JWidgetParamNameDef def : jWidgetParamNameDef) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jWidgetService.create(def.instance);
    	}
    	
        this.loaded = true;
	}
	
	
}
