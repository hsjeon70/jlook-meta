package jlook.framework.infrastructure.loader;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.domain.JClassType;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JClassService;
import jlook.framework.service.JMetaService;
import jlook.framework.service.JServiceException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



@Component(ViewLoader.ID)
public class ViewLoader extends BaseLoader {
	private static Log logger = Log.getLog(ViewLoader.class);
	
	public static final String ID = "viewLoader";
	
	private boolean loaded;
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	@Resource(name=JClassService.ID)
	private JClassService jClassService;
	
	@Resource(name=JMetaService.ID)
	private JMetaService jMetaService;
	
	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}
	
	
	 @PostConstruct
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}
			
		}
    }
	 
	private void initialize() throws JServiceException, JContextException {
		List<JClass> jClassList = this.jClassService.getJClassList();
		for(JClass jClass : jClassList) {
			if(JClassType.INTERFACE.name().equals(jClass.getType())) {
				continue;
			}
			
			this.jMetaService.createJView(jClass);
		}
	}
}
