package jlook.framework.infrastructure.loader;

public enum Hobby {
	Hiking,
	Tennis,
	Soccer,
	Baseball;
}
