package jlook.framework.infrastructure.loader;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.domain.HttpMethod;
import jlook.framework.domain.JBoolean;
import jlook.framework.domain.JMetaStatus;
import jlook.framework.domain.JClassType;
import jlook.framework.domain.JEnumType;
import jlook.framework.domain.JPreferenceType;
import jlook.framework.domain.JWidgetType;
import jlook.framework.domain.batch.JBatchParamType;
import jlook.framework.domain.common.JAnnounceTargetType;
import jlook.framework.domain.common.JMenuTarget;
import jlook.framework.domain.metadata.EncryptType;
import jlook.framework.domain.metadata.InheritanceType;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.domain.metadata.JActionTarget;
import jlook.framework.domain.metadata.JClassActionType;
import jlook.framework.domain.metadata.JClassMapType;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.widget.JWidgetParamType;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.batch.JBatchResult;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.loader.util.JEnumUtil;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JServiceException;
import jlook.framework.service.JValidationService;
import jlook.framework.service.action.JActionType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component(EnumLoader.ID)
public class EnumLoader extends BaseLoader {
	private static Log logger = Log.getLog(EnumLoader.class);
	
	public static final String ID = "enumLoader";
	
	private boolean loaded;
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	@Resource(name=JValidationService.ID)
	private JValidationService jValidationService;
	
	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}
	
	
	 @PostConstruct
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}
			
		}
    }
	 
	 private void initialize() throws JServiceException, JContextException {
		 Class<?>[] enums = new Class<?>[]{
				 JBoolean.class, 		JClassType.class,		JAccessRule.class,
				 JMetaStatus.class, 	InheritanceType.class, 	EncryptType.class, 
				 JClassActionType.class,JClassMapType.class, 	JActionType.class,
				 JEnumType.class, 		JViewType.class, 		JPreferenceType.class, 	
				 JAnnounceTargetType.class,	JWidgetType.class, 		HttpMethod.class, 
				 JMenuTarget.class,		JActionTarget.class,	JWidgetParamType.class,
				 JBatchResult.class,		JBatchParamType.class, 	Hobby.class
				 };
		 for(Class<?> enumCls : enums) {
			JEnum jEnum = JEnumUtil.getJEnum(enumCls);
			jValidationService.create(jEnum);
		 }
	 }
}
