package jlook.framework.infrastructure.loader.util;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import jlook.framework.domain.JBoolean;
import jlook.framework.domain.JEnumType;
import jlook.framework.domain.metadata.JEnum;
import jlook.framework.domain.metadata.JEnumDetail;
import jlook.framework.service.JServiceException;

public class JEnumUtil {
	
	public static JEnum getJEnum(Class<?> enumCls) throws JServiceException {
		try {
			String name = enumCls.getName();
			JEnum jEnum = new JEnum();
			jEnum.setName(enumCls.getSimpleName());
			jEnum.setDescription(name);
			if(enumCls.equals(JBoolean.class)) {
				jEnum.setType(JEnumType.RADIO.name());
			} else {
				jEnum.setType(JEnumType.SELECT.name());
			}
			Method m = enumCls.getMethod("values", new Class<?>[0]);
			Object[] objs = (Object[])m.invoke(enumCls, new Object[0]);
			Set<JEnumDetail> enumValues = new HashSet<JEnumDetail>();
			for(int i=0;i<objs.length;i++) {
				Class<?> cls = objs[i].getClass();
				m = cls.getMethod("name", new Class<?>[0]);
				String value = (String)m.invoke(objs[i], new Object[0]);
				JEnumDetail detail = new JEnumDetail();
				detail.setJEnum(jEnum);
				detail.setValue(objs[i].toString());
				detail.setLabel(value);
				detail.setSequence(new Integer(i+1));
				enumValues.add(detail);
			}
			jEnum.setValues(enumValues);
			
			return jEnum;
		} catch (Exception e) {
			throw new JServiceException("Cannot create JEnum.", e);
		} 
		
	}
}
