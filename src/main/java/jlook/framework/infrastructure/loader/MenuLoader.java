package jlook.framework.infrastructure.loader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.junit.BaseLoader;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JServiceException;

@Component(MenuLoader.ID)
public class MenuLoader extends BaseLoader {
	private static Log logger = Log.getLog(MenuLoader.class);
	
	public static final String ID = "menuLoader";
	
	@Value(EnvKeys.DATA_LOAD_ENALBED)
	private boolean dataLoadEnabled;
	
	private boolean loaded;
	
	public void setDataLoadEnabled(boolean dataLoadEnabled) {
		this.dataLoadEnabled = dataLoadEnabled;
	}
	@Override
	public void doInit() throws Exception {
		if(this.loaded) {
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.dataLoadEnabled);
		}
		
		try {
			if(!dataLoadEnabled) {
				return;
			}
			super.setUp();
			initialize();
		} finally {
			super.tearDown();
			if(logger.isDebugEnabled()) {
				logger.debug("It is started. ---------> "+this.dataLoadEnabled);
			}
			
		}
	}
	
	private void initialize() throws JServiceException, JContextException {
		
	}
}
