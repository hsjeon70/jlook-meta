package jlook.framework.infrastructure;

public interface EnvKeys {
	public static final String ENVIRONMENT = "environment";
	
	public static final String EXCEL_ERROR_DIR = "#{environment['excelErrorDir']}";
	public static final String EXCEL_UPLOAD_DIR = "#{environment['excelUploadDir']}";
	
	public static final String APP_HOME = "#{systemProperties['app.home']}";
	public static final String FILE_UPLOAD_DIR = "#{environment['fileUploadDir']}";
	
	// jlook-servlet.xml 
	public static final String FILE_UPLOAD_TMP = "fileUploadTmp";
	
	public static final String WEB_IMAGE_PATH = "#{environment['webImagePath']}";
	public static final String MOBILE_APP_PATH = "#{environment['mobileAppPath']}";
	
	public static final String ZIPCODE_MASTER_PATH = "#{environment['zipcode.master.file']}";
	
	// client testing
	public static final String TEST_TARGET_HOST = "test.targetHost";
	
	public static final String DATA_LOAD_ENALBED = "#{environment['dataLoadEnabled']}";
	
	// management
	public static final String ACCESS_LOG_ENABLED = "#{environment['accessLogEnabled']}"; 
	
	// OpenAPIGenerator
	public static final String INTERFACES_SOURCE_DIR	="#{environment['interfaces.sourceDir']}";
	public static final String INTERFACES_PACKAGE_NAME 	= "#{environment['interfaces.packageName']}";
	public static final String OPENAPI_TARGET_DIR 		= "#{environment['openapi.targetDir']}";
	
	
	public static final String FILE_BATCH_DIR = "fileBatchDir";
	public static final String FILE_BATCH_TMP = "fileBatchTmp";
	
}
