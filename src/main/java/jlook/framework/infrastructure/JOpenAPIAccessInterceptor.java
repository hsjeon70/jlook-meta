package jlook.framework.infrastructure;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class JOpenAPIAccessInterceptor extends HandlerInterceptorAdapter {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JOpenAPIAccessInterceptor.class);
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		
		
		return true;
	}
}
