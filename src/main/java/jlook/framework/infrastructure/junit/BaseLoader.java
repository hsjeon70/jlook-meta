package jlook.framework.infrastructure.junit;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import jlook.framework.ClientType;
import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.loader.metadata.JRoleDef;
import jlook.framework.infrastructure.loader.metadata.JUserDef;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-integration.xml",  
		"classpath:spring/context-persistence.xml"
})
public abstract class BaseLoader {
	private static Log logger = Log.getLog(BaseLoader.class);
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	
	@Rule
    public TestName name= new TestName();
	
	@Resource(name=JContextFactory.ID)
	protected JContextFactory jContextFactory;
	
	protected JContext jContext;
	
	@Before
	public void setUp() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug(">>>>>>>>>>>>>>>>>>>>>>>> "+getClass().getName()+"-"+name.getMethodName());
		}
		jContext = jContextFactory.getJContext();
		jContext.putClientType(ClientType.Loader.name());
		jContext.putDomainId(JDomainDef.SYSTEM_ID);
		
		// signIn
		jContext.putUserEntity(getUserEntity());
	}
	
	protected JUserEntity getUserEntity() {
		Long objectId 	= 1L;
		String email 	= JUserDef.admin.instance.getEmail();
		String password = JUserDef.admin.instance.getPassword();
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new GrantedAuthorityImpl(JRoleDef.Administrator.instance.getName()));
		
		JUserEntity entity = new JUserEntity(objectId, email, password, authorities);
		entity.setDomainId(JUserDef.admin.instance.getDomainId());
		
		return entity;
	}
	
	protected void changeDomainId(long domainId) {
		jContext.putDomainId(domainId);
	}
	
	@After
	public void tearDown() throws Exception {
		if(jContext!=null) jContext.removeAll();
	}
	
	public abstract void doInit() throws Exception;
}
