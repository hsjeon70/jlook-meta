package jlook.framework.infrastructure.junit.service;

import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.service.JSecurityService;

import org.junit.After;
import org.junit.Before;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


public class JAuthServiceTester extends JServiceTester {

	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		// signIn
		UserDetails user = security.loadUserByUsername(Keys.EMAIL);
		SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
	}

	@After
	public void tearDown() throws Exception {
		
		super.tearDown();
	}
}
