package jlook.framework.infrastructure.junit.service;

import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-test.xml",  
		"classpath:spring/context-persistence.xml",
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml"
})
public class JServiceTester {
	private static Log logger = Log.getLog(JServiceTester.class);
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	
	@Rule
    public TestName name= new TestName();
	
	@Resource(name=JContextFactory.ID)
	protected JContextFactory jContextFactory;
	
	protected JContext jContext;
	
	@Before
	public void setUp() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug(">>>>>>>>>>>>>>>>>>>>>>>> "+getClass().getName()+"-"+name.getMethodName());
		}
		jContext = jContextFactory.getJContext();
		jContext.putClientType(Keys.CLINET_TYPE);
		jContext.putDomainId(Keys.DOMAIN_ID);
	}

	@After
	public void tearDown() throws Exception {
		jContext.removeAll();
	}
}
