package jlook.framework.infrastructure.junit.client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import jlook.framework.GlobalEnv;
import jlook.framework.JHeader;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.gson.GsonFactory;
import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.http.JHttpClient;
import jlook.framework.infrastructure.http.JHttpRequest;
import jlook.framework.infrastructure.http.JHttpResponse;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;

import com.google.gson.Gson;

public abstract class ClientTester {
	private static Log logger = Log.getLog(ClientTester.class);
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	
	private String targetHost=Keys.TARGET_URL;
	
	private static ClientTester thisObj;
	
	@Rule
    public TestName name= new TestName();
	
	protected JHttpClient httpclient;
	protected JHttpRequest request;
	protected JHttpResponse response;
	private GsonFactory gsonfactory = new GsonFactory();
	
	@Before
	public void setUp() throws Exception {
		ClassLoader cloader = ClientTester.class.getClassLoader();
		String filePath = "properties/env-"+System.getProperty(GlobalEnv.MODE.getName())+".properties";
		InputStream in = cloader.getResourceAsStream(filePath);
		if(in==null) {
			throw new Exception("Cannot load environment properties. - "+filePath);
		}
		
		Properties env = new Properties();
		env.load(in);
		in.close();
		
		this.targetHost = env.getProperty(EnvKeys.TEST_TARGET_HOST, Keys.TARGET_URL);
		
		if(logger.isDebugEnabled()) {
			logger.debug(">>>>>>>>>>>>>> ["+this.targetHost+"] "+getClass().getSimpleName()+"-"+name.getMethodName());
		}
		
		
		this.httpclient = new JHttpClient();
		this.httpclient.setTargetHost(targetHost);
		thisObj = this;
		this.gsonfactory.doInit();
	}
	
	@After
	public void tearDown() throws Exception {
		String content = response.getContent();
		int code = response.getStatusCode();
		if(code==200 && code!=302) {
			Gson gson = this.gsonfactory.create();
			JResultModel rdata = (JResultModel)gson.fromJson(content, JResultModel.class);
			if(logger.isDebugEnabled()) {
				logger.debug(""+rdata);
			}
			assertTrue("RESULT(ERORR) - "+rdata.getMessage(),rdata.getResult().equals("success"));
		}
		if(response!=null) response.release();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(thisObj!=null) thisObj.shutdown();
	}

	
	protected void newHttpRequest(HttpMethod method, String targetUri) {
		this.request = this.httpclient.newHttpRequest(method, targetUri);
		this.request.addHeader(JHeader.JCLIENT_TYPE, Keys.CLINET_TYPE);
		this.request.addHeader(JHeader.JDOMAIN_ID, Keys.DOMAIN_ID.toString());
	}
	
	protected void execute(JHttpRequest request) throws ClientProtocolException, IOException {
		this.response = this.httpclient.execute(request);
		int code = this.response.getStatusCode();
		assertTrue("Fail to Http request - "+response.getStatusLine(), code==200 || code==302);
	}
	
	protected void shutdown() {
		this.httpclient.shutdown();
	}
}
