package jlook.framework.infrastructure.junit.client;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import jlook.framework.GlobalKeys;
import jlook.framework.infrastructure.http.HttpMethod;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

import org.junit.After;
import org.junit.Before;


public abstract class AuthClientTester extends ClientTester {
	private static Log logger = Log.getLog(AuthClientTester.class);
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		signIn();
	}
	
	@After
	public void tearDown() throws Exception {
		signOut();
	}
	
	protected void signIn() {
		super.newHttpRequest(HttpMethod.POST, GlobalKeys.SIGNIN_URI);
		request.setParameter("j_username", Keys.EMAIL);
		request.setParameter("j_password", Keys.PASSWORD);
		
		try {
			super.execute(request);
			int statusCode = response.getStatusCode();
			assertTrue("Invalid StatusCode - "+statusCode, statusCode==302);
			if(statusCode==302) {
				String location = response.getHeader("Location");
				response.release();
				super.newHttpRequest(HttpMethod.GET, location);
				super.execute(request);
			} else {
				throw new IOException("Fail to signIn. - StatusCode="+statusCode);
			}
		} catch (IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			response.release();
		}
	}
	
	protected void signOut() {
		super.newHttpRequest(HttpMethod.GET, GlobalKeys.SIGNOUT_URI);
		try {
			super.execute(request);
		} catch(IOException e) {
			logger.error("Fail - "+e.getMessage(), e);
			fail("Fail - "+e.getMessage());
		} finally {
			response.release();
		}
	}
}
