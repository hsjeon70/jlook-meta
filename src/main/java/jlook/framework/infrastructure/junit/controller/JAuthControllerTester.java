package jlook.framework.infrastructure.junit.controller;

import javax.annotation.Resource;

import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JSecurityService;

import org.junit.After;
import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class JAuthControllerTester extends JControllerTester {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JAuthControllerTester.class);
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	protected SecurityContext securityCtx;
	protected MockHttpServletRequest request;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		
		// signIn
		UserDetails user = security.loadUserByUsername(Keys.EMAIL);
		SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
	}

	@After
	public void tearDown() throws Exception {
		
		super.tearDown();
	}
	
}
