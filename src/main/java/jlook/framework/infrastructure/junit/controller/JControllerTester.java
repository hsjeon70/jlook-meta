package jlook.framework.infrastructure.junit.controller;

import static org.junit.Assert.fail;

import java.util.Map;

import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.gson.GsonFactory;
import jlook.framework.infrastructure.junit.Keys;
import jlook.framework.infrastructure.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  
		"classpath:spring/context-test.xml",  
		"classpath:spring/context-common.xml",  
		"classpath:spring/context-security.xml",  
		"classpath:spring/context-infrastructure.xml",  
		"classpath:spring/context-persistence.xml"
})
public abstract class JControllerTester {
	private static Log logger = Log.getLog(JControllerTester.class);
	
	static {
		String mode = System.getProperty(GlobalEnv.MODE.getName());
		if(mode==null || mode.equals("")) {
			System.setProperty(GlobalEnv.MODE.getName(), Keys.MODE);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.MODE.getName()+"="+Keys.MODE);
		}
		String home = System.getProperty(GlobalEnv.HOME.getName());
		if(home==null || home.equals("")) {
			System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
			logger.warn("SETTING ENVIRONMENT - "+GlobalEnv.HOME.getName()+"="+Keys.HOME);
		}
	}
	
	@Rule
    public TestName name= new TestName();
	
	@Resource(name=JContextFactory.ID)
	protected JContextFactory jContextFactory;
	
	protected JContext jContext;
	
	protected ModelAndView resultMView;
	
	@Resource(name=GsonFactory.ID)
	private GsonFactory gsonFactory;
	
	@Before
	public void setUp() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug(">>>>>>>>>>>>>>>>>>>>>>>> "+getClass().getName()+"-"+name.getMethodName());
		}
		jContext = jContextFactory.getJContext();
		jContext.putClientType(Keys.CLINET_TYPE);
		jContext.putDomainId(Keys.DOMAIN_ID);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@After
	public void tearDown() throws Exception {
		jContext.removeAll();
		
		Gson gson = gsonFactory.create();
		if(resultMView!=null) {
			Map<String,Object> model = resultMView.getModel();
			Object data = model.get(JResultModel.DATA_KEY);
			Object config = model.get(JResultModel.CONFIG_KEY);
			
			JResultModel rmodel = new JResultModel();
			rmodel.setData(data);
			if(config!=null && config instanceof Map) {
				rmodel.addConfig((Map)config);
			}
			try {
				if(logger.isDebugEnabled()) {
					logger.debug("-->"+data);
				}
				String gsonString = gson.toJson(rmodel);
				if(logger.isInfoEnabled()) {
					logger.info("JSON RESULT ----> "+gsonString);
				}
			} catch(Exception e) {
				logger.error("Fail - "+e.getMessage(), e);
				fail("Fail - "+e.getMessage());
			}
		}
	}
	
}
