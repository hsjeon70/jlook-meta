package jlook.framework.infrastructure.batch;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import jlook.framework.GlobalEnv;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JViewType;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.util.PageUtil;
import jlook.framework.service.JGenericService;
import jlook.framework.service.JServiceException;

public class JClassBatchTask extends JBatchTaskSupport {
	public static final String PARAM_SOURCE_CID = "sourceCid";
	public static final String PARAM_UNIT_COUNT	= "unitCount";
	
	private JGenericService jGenericService;
	
	@Override
	public void doExecute() throws JBatchException {
		jGenericService = (JGenericService)this.context.getBean(JGenericService.ID);
		Object sourceCid = context.getParameter(PARAM_SOURCE_CID);
		Long classId = null;
		if(sourceCid instanceof Long) {
			classId = (Long)sourceCid;
		} else {
			throw new JBatchException("Invalid parameter("+PARAM_SOURCE_CID+"). It is not a Long. - "+sourceCid);
		}
		
		Object unitCount = context.getParameter(PARAM_UNIT_COUNT);
		Integer pageSize = null;
		if(unitCount instanceof Integer) {
			pageSize = (Integer)unitCount;
		} else {
			throw new JBatchException("Invalid parameter("+PARAM_UNIT_COUNT+"). It is not a Integer. - "+unitCount);
		}
		
		PageUtil page = new PageUtil();
		page.setPageSize(pageSize);
		page.setPageNumber(1);
		do {
			JSummaryModel smodel = null;
			try {
				smodel = getSummary(classId, page);
				page = smodel.getPage();
			} catch (JServiceException e) {
				throw new JBatchException("Fail to get summary data. - "+classId+", JBatchInstanceId="+context.getInstanceId(), e);
			}
			try {
				writeFile(smodel, page.getPageNumber());
				if(page.getPageNumber() * page.getPageSize() >= page.getTotalCount()) {
					break;
				}
				page.setPageNumber(page.getPageNumber()+1);
			} catch (IOException e) {
				throw new JBatchException("Fail to write batch file.", e);
			}
		} while(true);
	}
	
	private JSummaryModel getSummary(Long classId, PageUtil page) throws JServiceException {
		JSummaryModel smodel = jGenericService.getSummaryData(JViewType.detail, classId, page, null);
		return smodel;
	}
	
	private void writeFile(JSummaryModel smodel, int page) throws IOException  {
//		List<ColumnModel> model = smodel.getColumnModel();
		@SuppressWarnings("unchecked")
		List<List<Object>> data = (List<List<Object>>)smodel.getData();
		if(data.size()==0) {
			return;
		}
		String appHome = System.getProperty(GlobalEnv.APP_HOME);
		if(appHome==null) {
			appHome = System.getProperty("user.home");
		}
		File batchDir = new File(appHome,(String)context.getEnvironment(EnvKeys.FILE_BATCH_DIR));
		if(!batchDir.exists()) {
			batchDir.mkdirs();
		}
		File batchTmp = new File(appHome,(String)context.getEnvironment(EnvKeys.FILE_BATCH_TMP));
		if(!batchTmp.exists()) {
			batchTmp.mkdirs();
		}
		String name = (String)smodel.getConfig(JClass.A_NAME);
		String fileName = name+"_"+context.getInstanceId()+"_["+context.currentDateTime()+"]-"+context.uuid()+"_"+page+".dat";
		File batchFile = new File(batchDir, fileName);
		FileOutputStream fo = null;
		BufferedOutputStream bo = null;
		try {
			fo = new FileOutputStream(batchFile);
			bo = new BufferedOutputStream(fo);
			for(List<Object> row : data) {
				String line = row.toString();
				bo.write(line.getBytes());
			}
			File tempFile = new File(batchTmp, fileName);
			tempFile.createNewFile();
		} catch(Exception e) {
			throw new IOException("Cannot write data to file - "+batchFile.getAbsolutePath(), e);
		} finally {
			try { if(bo!=null) bo.close(); } catch(Exception e){}
			try { if(fo!=null) fo.close(); } catch(Exception e){}
		}
	}
}
