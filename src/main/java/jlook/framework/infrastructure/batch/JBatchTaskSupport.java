package jlook.framework.infrastructure.batch;

public abstract class JBatchTaskSupport implements JBatchTask {
	protected JBatchContext context;
	
	@Override
	public void setJBatchContext(JBatchContext context) {
		this.context = context;
	}
	
	@Override
	public void doInit() throws JBatchException {
		
	}

	@Override
	abstract public void doExecute() throws JBatchException;

	@Override
	public void doEnd() throws JBatchException {
		
	}
	

}
