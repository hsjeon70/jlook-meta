package jlook.framework.infrastructure.batch.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.domain.batch.JBatchParamName;
import jlook.framework.domain.batch.JBatchParamType;
import jlook.framework.domain.batch.JBatchParamValue;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.batch.JBatchContext;
import jlook.framework.infrastructure.batch.JBatchException;
import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.service.JGenericService;

public class JBatchContextImpl implements JBatchContext {
	public static final String DEFAULT_DATETIME_FORMAT = "yyyyMMdd-hhmmss";
	
	private String name;
	private Long instanceId;
	private Long definitionId;
	
	private String server;
	private Properties environment;
	private Map<String,Object> map;
	private Map<String,Object> paramMap;
	
	private String uuid;
	
	protected JBatchContextImpl(JBatchInstance jBatchInstance) throws JBatchException {
		this.name = jBatchInstance.getName();
		
		this.instanceId = jBatchInstance.getObjectId();
		this.server = jBatchInstance.getServer();
		this.definitionId = jBatchInstance.getJBatchDefinition().getObjectId();
		this.uuid = UUID.randomUUID().toString();
		
		this.map = new HashMap<String, Object>();
		this.paramMap = new HashMap<String, Object>();
		
		JBatchDefinition jBatchDefinition = jBatchInstance.getJBatchDefinition();
		Map<String,JBatchParamName> params = jBatchDefinition.getJBatchParamNames();
		// default value
		for(JBatchParamName param : params.values()) {
			String type = param.getType();
			String defaultValue = param.getDefaultValue();
			Object value = toValue(type, defaultValue);
			this.paramMap.put(param.getName(), value);
		}
		// param value
		Set<JBatchParamValue> values = jBatchInstance.getJBatchParamValues();
		for(JBatchParamValue pvalue : values) {
			JBatchParamName pname = pvalue.getJBatchParamName();
			String value = pvalue.getValue();
			if(value==null || value.equals("")) {
				continue;
			}
			
			String type = pname.getType();
			Object paramValue = toValue(type,value);
			this.paramMap.put(pname.getName(), paramValue);
		}
	}
	
	protected void setEnvironment(Properties environment) {
		this.environment = environment;
	}
	
	private Object toValue(String type, String value) throws JBatchException {
		if(value==null) return null;
		Object paramValue = null;
		JBatchParamType paramType = JBatchParamType.valueOf(type);
		try {
			switch(paramType) {
			case BOOLEAN : paramValue = new Boolean(value);
				break;
			case INTEGER : paramValue = new Integer(value);
				break;
			case LONG : paramValue = new Long(value);
				break;
			case FLOAT : paramValue = new Float(value);
				break;
			case DOUBLE : paramValue = new Double(value);
				break;
			case STRING : paramValue = value;
				break;
			case DATE : paramValue = DateUtil.parseDate(value);
				break;
			case TIME : DateUtil.parseTime(value);
				break;
			}
			return paramValue;
		} catch(Exception e) {
			throw new JBatchException("Invalid param value. - type="+type+", value="+value, e);
		}
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String getServer() {
		return this.server;
	}
	
	@Override
	public void put(String key, Object value) {
		this.map.put(key, value);
	}

	@Override
	public Object get(String key) {
		return this.map.get(key);
	}


	@Override
	public Object getParameter(String name) {
		return this.paramMap.get(name);
	}

	@Override
	public Long getInstanceId() {
		return this.instanceId;
	}

	@Override
	public Long getDefinitionId() {
		return this.definitionId;
	}

	@Override
	public Object getBean(String beanName) {
		return ApplicationContextHolder.getBean(beanName);
	}

	@Override
	public String currentDateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		return sdf.format(Calendar.getInstance().getTime());
	}

	@Override
	public Object getEnvironment(String name) {
		return this.environment.get(name);
	}

	@Override
	public String uuid() {
		return this.uuid;
	}

	@Override
	public JGenericService getJGenericService() {
		return (JGenericService)this.getBean(JGenericService.ID);
	}

}
