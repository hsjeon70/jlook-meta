package jlook.framework.infrastructure.batch.impl;

import java.sql.Timestamp;
import java.util.Properties;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import jlook.framework.ClientType;
import jlook.framework.GlobalEnv;
import jlook.framework.domain.batch.JBatchDefinition;
import jlook.framework.domain.batch.JBatchHistory;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.batch.JBatchContext;
import jlook.framework.infrastructure.batch.JBatchException;
import jlook.framework.infrastructure.batch.JBatchResult;
import jlook.framework.infrastructure.batch.JBatchTask;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JBatchHistoryService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

public class JBatchExecutor implements Runnable {
	private static Log  logger = Log.getLog(JBatchExecutor.class);
	
	public static final String DEFAULT_BATCH_USER = "batch@jlook.com";
	
	private JBatchInstance jBatchInstance;
	private JBatchHistoryService jBatchHistoryService;
	private JSecurityService security;
	private JContextFactory jContextFactory;
	private Properties environment;
	
	public void setJBatchInstance(JBatchInstance jBatchInstance) {
		this.jBatchInstance = jBatchInstance;
	}
	
	public void setJBatchHistoryService(JBatchHistoryService jBatchHistoryService) {
		this.jBatchHistoryService = jBatchHistoryService;
	}
	
	public void setJSecurityService(JSecurityService security) {
		this.security = security;
	}
	
	public void setJContextFactory(JContextFactory jContextFactory) {
		this.jContextFactory = jContextFactory;
	}
	
	public void setEnvironment(Properties environment) {
		this.environment = environment;
	}
	
	
	
	@Override
	public void run() {
		Long startTime = System.currentTimeMillis();
		JBatchHistory history = new JBatchHistory();
		history.setName(this.jBatchInstance.getName());
		history.setDomainId(this.jBatchInstance.getDomainId());
		history.setJBatchDefinition(this.jBatchInstance.getJBatchDefinition());
		history.setJBatchInstance(this.jBatchInstance);
		history.setServer(System.getProperty(GlobalEnv.APP_SERVER));
		history.setStartTime(new Timestamp(startTime));
		try {
			this.jBatchHistoryService.create(history);
		} catch (JServiceException e) {
			logger.error("Fail to write batch log. - "+history, e);
		}
		
		JContext jContext = this.jContextFactory.getJContext();
		JBatchContextImpl context = null;
		try {
			context = new JBatchContextImpl(this.jBatchInstance);
			context.setEnvironment(environment);
			
			// authentication
			String batchUser = (String)context.getParameter(JBatchContext.PARAM_BATCH_USER);
			if(batchUser==null) {
				batchUser = DEFAULT_BATCH_USER;
			}
			UserDetails user = this.security.loadUserByUsername(batchUser);
			SecurityContextHolder.getContext().setAuthentication(
	                new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities()));
			JUserEntity entity = (JUserEntity)user;
			entity.setDomainId(this.jBatchInstance.getDomainId());
			jContext.putDomainId(this.jBatchInstance.getDomainId());
			jContext.putClientType(ClientType.Batch.name());
			jContext.putUserEntity(entity);
			
			doExecute(context);
			history.setResult(JBatchResult.success.name());
		} catch(Exception e) {
			history.setResult(JBatchResult.fail.name());
			history.setErrorMessage(e.getMessage());
			logger.error("Fail to execute batch.", e);
		} finally {
			Long endTime = System.currentTimeMillis();
			history.setEndTime(new Timestamp(endTime));
			String elapseTime = Double.toString((endTime-startTime)/1000.);
			history.setElapseTime(elapseTime);
			try {
				this.jBatchHistoryService.update(history);
			} catch (JServiceException e) {
				logger.error("Fail to write batch log. - "+history, e);
			}
			
			if(jContext!=null) {
				jContext.removeAll();
			}
		}
	}
	
	public void doExecute(JBatchContext context) throws JException {
		String taskClass = null;
		JBatchDefinition jBatchDefinition = this.jBatchInstance.getJBatchDefinition();
		taskClass = jBatchDefinition.getTaskClass();
		if(taskClass==null) {
			throw new JBatchException("TaskClass is null. - "+jBatchDefinition);
		}
		
		JBatchTask task = null;
		try {
			Class<?> clazz = Class.forName(taskClass);
			Object obj  = clazz.newInstance();
			if(obj instanceof JBatchTask) {
				task = (JBatchTask)obj;
			} else {
				throw new JBatchException("Invalid JTask object.("+obj+") - "+taskClass);
			}
		} catch (Exception e) {
			throw new JException("Cannot create class and object for JBatch.", e);
		}
		
		task.setJBatchContext(context);
		try {
			task.doInit();
			task.doExecute();
		} catch(JBatchException e) {
			throw new JException("Fail to execute JTask. - "+e.getMessage(), e);
		} finally {
			try {
				task.doEnd();
			} catch (JBatchException e) {
				throw new JException("Fail to execute doEnd method for JTask.", e);
			}
		}
	}

}
