package jlook.framework.infrastructure.batch.impl;

import java.util.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.domain.batch.JBatchInstance;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JBatchHistoryService;
import jlook.framework.service.JBatchInstanceService;
import jlook.framework.service.JSecurityService;

import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component(JBatchManager.ID)
public class JBatchManager {
	private static Log logger = Log.getLog(JBatchManager.class);
	public static final String ID = "jBatchManager";
	
	@Resource(name="scheduler")
	private TaskScheduler scheduler;
	
	@Resource(name=JBatchInstanceService.ID)
	private JBatchInstanceService jBatchIntanceService;
	
	private Map<Long,ScheduledFuture<?>> map;
	
	@Resource(name=JBatchHistoryService.ID)
	private JBatchHistoryService jBatchHistoryService;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JContextFactory.ID)
	private JContextFactory jContextFactory;
	
	@Resource(name=EnvKeys.ENVIRONMENT)
	private Properties envProperties;
	
	@PostConstruct
	public void startup() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. ---------> "+this.scheduler);
		}
		
		map = new HashMap<Long,ScheduledFuture<?>>();
		
		List<JBatchInstance> batchs = this.jBatchIntanceService.getJBatchInstances();
		String currentServer = System.getProperty(GlobalEnv.APP_SERVER);
		for(JBatchInstance batch : batchs) {
			if((currentServer!=null && batch.getServer()!=null) && 
					(currentServer!=null && !currentServer.equals(batch.getServer()))) {
				continue;
			}
			JBatchExecutor executor = newExecutor(batch);
			ScheduledFuture<?> future = scheduledBatch(batch, executor);
			if(future==null) {
				continue;
			}
			map.put(batch.getObjectId(), future);
			if(logger.isInfoEnabled()) {
				logger.info("JBatch is scheduled.("+batch.getObjectId()+") - "+batch.getDescription());
			}
		}

		if(logger.isDebugEnabled()) {
			logger.debug("It is started. ---------> ");
		}
	}
	
	private JBatchExecutor newExecutor(JBatchInstance jBatchInstance) {
		JBatchExecutor t = new JBatchExecutor();
		t.setJBatchInstance(jBatchInstance);
		t.setEnvironment(this.envProperties);
		t.setJSecurityService(security);
		t.setJContextFactory(jContextFactory);
		t.setJBatchHistoryService(this.jBatchHistoryService);
		return t;
	}
	
	private ScheduledFuture<?> scheduledBatch(JBatchInstance jBatchInstance, JBatchExecutor jTaskExecutor) {
		ScheduledFuture<?> future = null;
		if(jBatchInstance.getCronExpression()!=null && !jBatchInstance.getCronExpression().equals("")) {
			CronTrigger cron = new CronTrigger(jBatchInstance.getCronExpression());
			future = this.scheduler.schedule(jTaskExecutor, cron);
		} else if(jBatchInstance.getFixedRate()!=null && jBatchInstance.getFixedRate()>0) {
			if(jBatchInstance.getFixedDelay()!=null && jBatchInstance.getFixedDelay()>0) {
				Calendar current = Calendar.getInstance();
				Date startTime = new Date();
				startTime.setTime(current.getTimeInMillis()+jBatchInstance.getFixedDelay());
				future = this.scheduler.scheduleWithFixedDelay(jTaskExecutor, startTime, jBatchInstance.getFixedRate());
			} else {
				future = this.scheduler.scheduleAtFixedRate(jTaskExecutor, jBatchInstance.getFixedRate());
			}
		} else if(jBatchInstance.getFixedDelay()!=null && jBatchInstance.getFixedDelay()>0) {
			future = this.scheduler.scheduleWithFixedDelay(jTaskExecutor, jBatchInstance.getFixedDelay());
		} else {
			logger.warn("Invalid batch instance. - "+jBatchInstance);
		}
		return future;
	}
	
	public void addJBatch(JBatchInstance jBatchInstance) {
		String currentServer = System.getProperty(GlobalEnv.APP_SERVER);
		if((currentServer!=null && jBatchInstance.getServer()!=null) && 
				(currentServer!=null && !currentServer.equals(jBatchInstance.getServer()))) {
			return;
		}
		
		JBatchExecutor executor = newExecutor(jBatchInstance);
		ScheduledFuture<?> future = scheduledBatch(jBatchInstance, executor);
		
		map.put(jBatchInstance.getObjectId(), future);
		if(logger.isInfoEnabled()) {
			logger.info("JBatchInstance is scheduled.("+jBatchInstance.getObjectId()+") - "+jBatchInstance.getDescription());
		}
	}
	
	public void updateJBatch(JBatchInstance jBatchInstance) {
		if(map.containsKey(jBatchInstance.getObjectId())) {
			ScheduledFuture<?> future = map.get(jBatchInstance.getObjectId());
			future.cancel(true);
			map.remove(jBatchInstance.getObjectId());
		}
		addJBatch(jBatchInstance);
	}
	
	public void removeJBatch(JBatchInstance jBatchInstance) {
		if(!map.containsKey(jBatchInstance.getObjectId())) {
			return;
		}
		ScheduledFuture<?> future = map.get(jBatchInstance.getObjectId());
		future.cancel(true);
		if(logger.isInfoEnabled()) {
			logger.info("JBatchInstance is unscheduled.("+jBatchInstance.getObjectId()+") - "+jBatchInstance.getDescription());
		}
	}
	
	@PreDestroy
	public void destroy() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is destorying. ---------> ");
		}
		for(ScheduledFuture<?> future : map.values()) {
			future.cancel(true);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is destroyed. ---------> ");
		}
	}
}
