package jlook.framework.infrastructure.batch;

public interface JBatchTask {
	
	public void setJBatchContext(JBatchContext context);
	public void doInit() throws JBatchException;
	public void doExecute() throws JBatchException;
	public void doEnd() throws JBatchException;
}
