package jlook.framework.infrastructure.batch;

import jlook.framework.service.JGenericService;

public interface JBatchContext {
	public static final String PARAM_BATCH_USER = "batchUser";
	
	public String getName() ;
	public String getServer();
	public Long getInstanceId();
	public Long getDefinitionId();
	public String uuid();
	
	public Object getParameter(String name);
	public Object getEnvironment(String name);
	
	public JGenericService getJGenericService();
	
	public void put(String key, Object value);
	public Object get(String key);
	public String currentDateTime();
	public Object getBean(String beanName);
}
