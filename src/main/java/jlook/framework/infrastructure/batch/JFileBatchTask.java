package jlook.framework.infrastructure.batch;

import java.io.File;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.util.Log;

public class JFileBatchTask extends JBatchTaskSupport {
	private static Log logger = Log.getLog(JFileBatchTask.class);
	
	public static final String PARAM_SOURCE_DIR = "sourceDir";
	public static final String PARAM_SOURCE_TMP = "sourceTmp";
	public static final String PARAM_DELETE_FILE = "deleteFile";
	
	private File sourceDir;
	private File sourceTmp;
	private ThreadPoolTaskExecutor taskExecutor;
	
	@Override
	public void doInit() throws JBatchException {
		String appHome = System.getProperty(GlobalEnv.APP_HOME);
		if(appHome==null) {
			appHome = System.getProperty("user.home");
		}
		
		String envSourceDir = (String)context.getParameter(PARAM_SOURCE_DIR);
		String envSourceTmp = (String)context.getParameter(PARAM_SOURCE_TMP);
		if(envSourceDir==null) {
			throw new JBatchException("Cannot find 'sourceDir' from environment.");
		}
		if(envSourceTmp==null) {
			throw new JBatchException("Cannot find 'sourceTmp' from environment.");
		}
		this.sourceDir = new File(appHome, envSourceDir);
		this.sourceTmp = new File(appHome, envSourceTmp);
		
		if(!this.sourceDir.exists()||!this.sourceTmp.exists()) {
			throw new JBatchException("Cannot find 'sourceDir' or `sourceTmp' directory. - "+this.sourceDir+", "+this.sourceTmp);
		}
		
		taskExecutor = (ThreadPoolTaskExecutor)context.getBean("taskExecutor");
	}
	
	@Override
	public void doExecute() throws JBatchException {
		if(logger.isDebugEnabled()) {
			logger.debug(context.getName()+" is executing.... - "+context.getInstanceId());
		}
		do {
			File[] listFile = this.sourceTmp.listFiles();
			if(listFile==null || listFile.length==0) {
				return;
			}
			for(File sourceFile : listFile) {
				processFile(sourceFile);
			}
		} while(true);
	}

	private void processFile(File file) {
		String fileName = file.getName();
		file.delete();
		File source = new File(this.sourceDir, fileName);
		if(!source.exists()) {
			logger.error("Cannot find source file in sourceDir. - "+source);
			return;
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("Process source file. - "+source);
		}
		// TODO : PROCESSING FILE
//		this.taskExecutor.execute(task);
		
		Boolean deleteFile = (Boolean)context.getParameter(PARAM_DELETE_FILE);
		if(deleteFile!=null && deleteFile.booleanValue()) {
			source.delete();
		}
	}
}
