package jlook.framework.infrastructure.batch;

import jlook.framework.infrastructure.JException;

public class JBatchException extends JException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String TEXT = "Task Error";
	
	public JBatchException(String message) {
		super(message);
	}
	public JBatchException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JBatchException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JBatchException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JBatchException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JBatchException(String message, Throwable cause) {
		super(message, cause);
	}
}
