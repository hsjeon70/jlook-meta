package jlook.framework.infrastructure.batch;

import jlook.framework.infrastructure.util.Log;

public class EchoBatch extends JBatchTaskSupport {
	private static Log logger = Log.getLog(EchoBatch.class);
	
	@Override
	public void doExecute() throws JBatchException {
		if(logger.isInfoEnabled()) {
			logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> executing. "+this.context);
		}
	}

}
