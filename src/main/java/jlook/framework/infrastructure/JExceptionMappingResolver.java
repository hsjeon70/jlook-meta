package jlook.framework.infrastructure;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.JContentType;
import jlook.framework.JHeader;
import jlook.framework.domain.JErrorInfo;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;


public class JExceptionMappingResolver extends SimpleMappingExceptionResolver {
	private static Log logger = Log.getLog(JExceptionMappingResolver.class);
	
	@Override
	protected ModelAndView doResolveException(
			HttpServletRequest request, HttpServletResponse response, 
			Object handler, Exception ex) {
		if(logger.isDebugEnabled()) {
			logger.debug("\t handle Exception --> "+ex);
		}
		
		if(logger.isErrorEnabled()) {
			logger.error(ex.getMessage(), ex);
		}
		String contentType = request.getHeader(JHeader.JCONTENT_TYPE);
		JContentType jContentType = JContentType.getJContentType(contentType);
		if(JContentType.HTML.equals(jContentType)) {
			return super.doResolveException(request, response, handler, ex);
		}
		JErrorInfo info = new JErrorInfo(ex);
		
		JResultModel rdata = new JResultModel();
		rdata.setResult(JResultModel.ERROR);
		rdata.setMessage(info.getMessage());
		rdata.setData(info);
		
		ModelAndView mview = jContentType.create("error/default");
		mview.addObject(JResultModel.DATA_KEY, rdata);
		return mview;
	}
}
