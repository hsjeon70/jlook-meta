package jlook.framework.infrastructure.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import jlook.framework.GlobalEnv;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class SmtpSender {
	private JavaMailSenderImpl sender = new JavaMailSenderImpl();
	private MimeMessage message = sender.createMimeMessage(); 
	private MimeMessageHelper helper = new MimeMessageHelper(message);
	private Properties props = new Properties();
	
	public void setHost(String host) {
		this.sender.setHost(host);
		this.sender.setJavaMailProperties(props);
	}
	
	public void setPort(int port) {
		this.sender.setPort(port);
	}
	
	public void setUsername(String username) {
		this.sender.setUsername(username);
	}
	
	public void setPassword(String password) {
		this.sender.setPassword(password);
	}
	
	public void setFrom(String from) throws MessagingException {
		this.helper.setFrom(from);
	}
	
	public void setTo(String to) throws MessagingException {
		this.helper.setTo(to);
	}
	public void setSubject(String subject) throws MessagingException {
		this.helper.setSubject(subject);
	}
	
	public void setText(String text) throws MessagingException {
		this.helper.setText(text);
	}
	
	public void setProperty(String name, String value) {
		this.props.setProperty(name, value);
	}
	
	public void send() {
		this.sender.send(message);
	}
	
	public static void main(String[] args) throws MessagingException, UnknownHostException {
		SmtpSender sender = new SmtpSender();
		sender.setHost("smtp.gmail.com");
		sender.setPort(587);
		sender.setUsername(args[0]);
		sender.setPassword(args[1]);
		sender.setProperty("mail.smtp.starttls.enable", "true");
		
		String home = System.getProperty(GlobalEnv.APP_HOME);
		File f = new File(home, "jlook-core-license.xml");
		String xml = null;
		if(f.exists()) {
			FileInputStream fi = null;
			try {
				fi = new FileInputStream(f);
				byte[] buff = new byte[1024];
				int len = fi.read(buff);
				xml = new String(buff, 0, len);
			} catch (Exception e) {
				xml = "invalid license file";
			} finally {
				try { if(fi!=null) fi.close(); } catch(Exception e){}
			}
		} else {
			xml = "License does not exist.";
		}
		InetAddress addr = InetAddress.getLocalHost();
		Properties p = System.getProperties();
		StringWriter out = new StringWriter();
		PrintWriter pw = new PrintWriter(out);
		p.list(pw);
		
		sender.setTo("jlooktech@gmail.com");
		//sender.setFrom("jLookPlatform");
		sender.setSubject("jlook platform : "+p.getProperty("user.name")+" - "+addr.getHostName());
		sender.setText(xml+"\nIpAddress - "+addr.getHostAddress()+"\n"+out.toString());
		sender.send();
	}
}
