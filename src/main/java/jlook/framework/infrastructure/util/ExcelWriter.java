package jlook.framework.infrastructure.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;

public class ExcelWriter {
	private static Log logger = Log.getLog(ExcelWriter.class);
	
	private HSSFWorkbook workbook;
	private FileOutputStream foStream;
	private BufferedOutputStream boStream;
	
	private File target;
	
	public ExcelWriter(File target) throws IOException {
		this.target = target;
		this.workbook = new HSSFWorkbook();
		this.foStream = new FileOutputStream(this.target);
		this.boStream = new BufferedOutputStream(this.foStream);
	}
	
	public void write(String sheetName, String[] headers, List<String[]> data) throws IOException {
		HSSFSheet sheet = this.workbook.createSheet(sheetName);
		int rowNo = 0;
		short border = 1;
		if(headers!=null && headers.length>0) {
			HSSFCellStyle style = workbook.createCellStyle();
			style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			style.setBorderBottom(border);
			style.setBorderLeft(border);
			style.setBorderRight(border);
			style.setBorderTop(border);
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
			HSSFFont font = this.workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			style.setFont(font);
			
			HSSFRow row     = sheet.createRow(rowNo++); 
			for(int i=0;i<headers.length;i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(headers[i]);
				cell.setCellStyle(style);
			}
		}
		
		if(data==null) {
			return;
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("Excel Writer : sheetName="+sheetName);
		}
		
		HSSFCellStyle style = workbook.createCellStyle();
		style.setBorderBottom(border);
		style.setBorderLeft(border);
		style.setBorderRight(border);
		style.setBorderTop(border);
		
		for(int i=0;i<data.size();i++) {
			String[] record = data.get(i);
			HSSFRow row     = sheet.createRow(rowNo++); 
			for(int j=0;j<record.length;j++) {
				Cell cell = row.createCell(j);
				cell.setCellStyle(style);
				cell.setCellValue(record[j]);
			}
		}
		sheet.autoSizeColumn(headers.length-1);
		this.workbook.write(this.boStream);
	}
	
	public void close() {
		try { if(this.boStream!=null) this.boStream.close(); } catch(Exception e) {}
		try { if(this.foStream!=null) this.foStream.close(); } catch(Exception e) {}
	}
}
