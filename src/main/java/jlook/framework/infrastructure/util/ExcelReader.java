package jlook.framework.infrastructure.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class ExcelReader {
	private static Log logger = Log.getLog(ExcelReader.class);
	
	private String fileName;
	private HSSFWorkbook  workbook;
	private HSSFSheet sheet;
	
	private int numberOfSheet;
	private int numberOfRow;
	private int currentSheet;
	private int currentRow;
	private String[] headers;
	private Map<String,String> headerType;
	
	public ExcelReader(String fileName, InputStream inStream) throws IOException {
		this.fileName = fileName;
		POIFSFileSystem fileSystem   = new POIFSFileSystem(inStream);
		this.workbook = new HSSFWorkbook(fileSystem);
		this.numberOfSheet = this.workbook.getNumberOfNames();
		if(this.numberOfSheet==0) {
			this.numberOfSheet = 1;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("numberOfSheet="+this.numberOfSheet);
		}
	}
	
	public boolean hasNextSheet() {
		boolean result = this.currentSheet < this.numberOfSheet;
		if(result) {
			this.sheet = workbook.getSheetAt(currentSheet++);
			this.currentRow = 0;
			this.numberOfRow =  this.sheet.getPhysicalNumberOfRows();
			if(logger.isDebugEnabled()) {
				logger.debug("numberOfRow="+this.numberOfRow);
			}
			this.headers = nextRow();
		}
		return result;
	}
	
	public String[] getHeaders() {
		return this.headers;
	}
	
	public String getSheetName() {
		return this.sheet==null ? null : this.sheet.getSheetName();
	}
	
	public boolean hasNextRow() {
		return this.currentRow < this.numberOfRow;
	}
	
	public String getFileName() {
		return this.fileName;
	}
	
	public String[] nextRow() {
		HSSFRow row = this.sheet.getRow(this.currentRow++);
		String[] result = null;
		List<String> titles = null;
		if(this.headers!=null) {
			result = new String[this.headers.length];
		} else {
			titles = new ArrayList<String>();
		}
		
		if(row==null) {
			return result;
		}
		
		int cells = 0;
		if(this.headers==null) {
			cells = row.getPhysicalNumberOfCells();
		} else {
			cells = this.headers.length;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("numberOfColumns="+cells);
		}
		for(int i=0;i<cells;i++) {
			 HSSFCell cell= row.getCell(i);
			 if(logger.isDebugEnabled()) {
				 logger.debug("Column("+row.getRowNum()+", "+i+") --> "+cell);
			 }
			 if(cell != null){
				 String value = null;
				 switch(cell.getCellType()){    
	             	case HSSFCell.CELL_TYPE_FORMULA:
	             		value = cell.getCellFormula();
	             		break;
	             	case HSSFCell.CELL_TYPE_NUMERIC:
	             		value =toNumber(cell.getNumericCellValue());
	             		break;
	             	case HSSFCell.CELL_TYPE_STRING:
	             		value =""+cell.getStringCellValue();
	             		break;
	             	case HSSFCell.CELL_TYPE_BLANK:
	             		value =""+cell.getBooleanCellValue();
	             		break;
	             	case HSSFCell.CELL_TYPE_ERROR:
	             		value=""+cell.getErrorCellValue();
	             		break;
	             	default:
	             		value = "";
				 }
				 if(this.headers==null) {
					 titles.add(value);
				 } else {
					 result[i] = value;
				 }
				 if(logger.isDebugEnabled()) {
					 logger.debug("	\tcolumn value --> "+value);
				 }
			 } else {
				 if(this.headers==null) {
					 titles.add("");
				 } else {
					 result[i] = "";
				 }
				 if(logger.isDebugEnabled()) {
					 logger.debug("	\tcolumn value --> "+"");
				 }
			 }
		}
		if(this.headers==null) {
			result = new String[titles.size()];
			for(int i=0;i<result.length;i++) {
				result[i] = titles.get(i);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		File file = new File("/Users/hsjeon70/Documents/TEST.xls");
		FileInputStream fi = new FileInputStream(file);
		ExcelReader reader = new ExcelReader(file.getName(), fi);
		while(reader.hasNextSheet()) {
			logger.info("-----------------> "+reader.getSheetName());
			logger.info("headers="+reader.getHeaders());
			while(reader.hasNextRow()) {
				String[] row = reader.nextRow();
				logger.info("Row="+row);
			}
		}
	}
	
	private String toNumber(double value) {
		if(Math.floor(value)-value == 0) {
			return String.valueOf((int)value);
		}
		return String.valueOf(value);
	}
}
