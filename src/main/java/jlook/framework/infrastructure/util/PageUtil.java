package jlook.framework.infrastructure.util;

import java.util.ArrayList;
import java.util.List;

public class PageUtil {
	public static final int DEFAULT_PAGE_SIZE = 15;
	private int pageNumber;
	private int pageSize;
	private int totalCount;
	
	private String keyword;
	private Long parentOid;
	private Long parentCid;
	private List<String> orderBy;
	
	public PageUtil() {
		this(1,DEFAULT_PAGE_SIZE,null);
	}
	
	public PageUtil(Integer pageNumber, Integer pageSize, String keyword) {
		if(pageNumber==null) {
			this.pageNumber = 1;
		} else {
			this.pageNumber = pageNumber.intValue();
		}
		if(pageSize==null) {
			this.pageSize = DEFAULT_PAGE_SIZE;
		} else {
			this.pageSize = pageSize.intValue();
		}
		this.keyword = keyword;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		if(pageNumber==null) {
			this.pageNumber = 1;
		} else {
			this.pageNumber = pageNumber;
		}
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		if(pageSize==null) {
			this.pageSize = PageUtil.DEFAULT_PAGE_SIZE;
		} else {
			this.pageSize = pageSize;
		}
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getParentOid() {
		return parentOid;
	}

	public void setParentOid(Long parentOid) {
		this.parentOid = parentOid;
	}

	public Long getParentCid() {
		return parentCid;
	}

	public void setParentCid(Long parentCid) {
		this.parentCid = parentCid;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<String> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<String> orderBy) {
		this.orderBy = orderBy;
	}
	
	public void setOrderBy(String[] orderBy) {
		this.orderBy = new ArrayList<String>();
		for(String item : orderBy) {
			this.orderBy.add(item);
		}
	}


	@Override
	public String toString() {
		return "PageUtil [pageNumber=" + pageNumber + ", pageSize=" + pageSize
				+ ", totalCount=" + totalCount + ", keyword=" + keyword
				+ ", parentOid=" + parentOid + ", parentCid=" + parentCid + "]";
	}
}
