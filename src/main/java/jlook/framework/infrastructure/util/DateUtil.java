package jlook.framework.infrastructure.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static final String DEFAULT_DATETIME_FORMAT = "MM/dd/yyyy hh:mm:ss";
	public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
	public static final String DEFAULT_TIME_FORMAT = "hh:mm:ss";
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		return sdf.format(date);
	}
	
	public static String formatDateTime(Date datetime) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		return sdf.format(datetime);
	}
	public static String format(Timestamp timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		return sdf.format(timestamp);
	}
	
	public static Date parseDateTime(String datetime) throws ParseException {
		if(datetime==null) {
			return null;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		return sdf.parse(datetime);
	}
	
	public static Date parseDate(String date) throws ParseException {
		if(date==null) {
			return null;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		return sdf.parse(date);
	}
	
	public static Date parseTime(String time) throws ParseException {
		if(time==null) {
			return null;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
		return sdf.parse(time);
	}
}
