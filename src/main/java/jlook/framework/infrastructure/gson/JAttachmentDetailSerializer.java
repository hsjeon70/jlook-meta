package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;

import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JAttachmentDetailSerializer implements JsonSerializer<JAttachmentDetail> {
	private static Log logger = Log.getLog(JAttachmentDetailSerializer.class);
	
	@Override
	public JsonElement serialize(JAttachmentDetail src, Type typeOfSrc, JsonSerializationContext context) {
		if(logger.isTraceEnabled()) {
			logger.trace("JAttachmentDetail --> "+src);
		}
		JsonObject jobj = new JsonObject();
		jobj.addProperty("objectId", src.getObjectId());
		jobj.addProperty("name", src.getName());
		jobj.addProperty("type", src.getType());
		jobj.addProperty("length", src.getLength());
		jobj.addProperty("file", src.getSavedName());
		jobj.addProperty("defaultYn", src.getDefaultYn());
		
		return jobj;
	}

}
