package jlook.framework.infrastructure.gson;

import java.sql.Timestamp;

import javax.annotation.PostConstruct;

import jlook.framework.domain.JErrorInfo;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.util.Log;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component(GsonFactory.ID)
public class GsonFactory {
	public static final String ID = "gsonFactory";
	
	private static Log logger = Log.getLog(GsonFactory.class);
	private GsonBuilder builder = new GsonBuilder();
	
	@PostConstruct
	public void doInit() {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		builder.registerTypeAdapter(java.util.Date.class, new DateSerialzier());
		builder.registerTypeAdapter(java.sql.Date.class, new DateSerialzier());
		builder.registerTypeAdapter(Timestamp.class, new TimestampSerialzier());
		builder.registerTypeAdapter(JObjectRatingDetail.class, new JObjectRatingDetailSerialzier());
		builder.registerTypeAdapter(JAttachmentDetail.class, new JAttachmentDetailSerializer());
		builder.registerTypeAdapter(JErrorInfo.class, new JErrorInfoSerializer());
		
		builder.registerTypeAdapter(JResultModel.class, new JResultModelDeserializer());
		builder.serializeNulls();
		builder.setExclusionStrategies(new GsonExclusionStrategy());
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started.");
		}
	}
	
	public Gson create() {
		return builder.create();
	}
}