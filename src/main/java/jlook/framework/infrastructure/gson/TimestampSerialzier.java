package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;
import java.sql.Timestamp;

import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class TimestampSerialzier implements JsonSerializer<Timestamp> {
	private static Log logger = Log.getLog(TimestampSerialzier.class);
	
	@Override
	public JsonElement serialize(Timestamp src, Type typeOfSrc,
				JsonSerializationContext context) {
		if(logger.isTraceEnabled()) {
			logger.trace("Timestamp --> "+src);
		}
		return new JsonPrimitive(DateUtil.format(src));
	}

}
