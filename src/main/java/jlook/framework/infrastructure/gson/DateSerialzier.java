package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;
import java.util.Date;

import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateSerialzier implements JsonSerializer<Date> {
	private static Log logger = Log.getLog(DateSerialzier.class);
	
	@Override
	public JsonElement serialize(Date src, Type typeOfSrc,
				JsonSerializationContext context) {
		if(logger.isTraceEnabled()) {
			logger.trace("Timestamp --> "+src);
		}
		return new JsonPrimitive(DateUtil.formatDate(src));
	}

}
