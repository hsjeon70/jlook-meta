package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;

import jlook.framework.domain.JErrorInfo;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JErrorInfoSerializer implements JsonSerializer<JErrorInfo> {
	private static Log logger = Log.getLog(JErrorInfoSerializer.class);
	
	@Override
	public JsonElement serialize(JErrorInfo src, Type typeOfSrc, JsonSerializationContext context) {
		if(logger.isTraceEnabled()) {
			logger.trace("JErrorInfo --> "+src);
		}
		JsonObject jobj = new JsonObject();
		jobj.addProperty("exception", src.getException()==null ? "" : src.getException().toString());
		jobj.addProperty("message", src.getMessage()==null ? "" : src.getMessage());
		jobj.addProperty("reason", src.getReason()==null ? "" : src.getReason());
		jobj.addProperty("cause", src.getCause()==null ? "" : src.getCause().toString());
		jobj.addProperty("rootMessage", src.getRootMessage()==null ? "" : src.getRootMessage());
		jobj.addProperty("rootCause", src.getRootCause()==null ? "" : src.getRootCause().toString());
		
		return jobj;
	}

}
