package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;
import java.sql.Timestamp;

import jlook.framework.domain.account.JUser;
import jlook.framework.domain.common.JObjectRatingDetail;
import jlook.framework.infrastructure.util.DateUtil;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JObjectRatingDetailSerialzier implements JsonSerializer<JObjectRatingDetail> {
	private static Log logger = Log.getLog(JObjectRatingDetailSerialzier.class);
	
	@Override
	public JsonElement serialize(JObjectRatingDetail src, Type typeOfSrc,
				JsonSerializationContext context) {
		if(logger.isTraceEnabled()) {
			logger.trace("Timestamp --> "+src);
		}
		JsonObject jobj = new JsonObject();
		jobj.addProperty("objectId", src.getObjectId());
		jobj.addProperty("value", src.getValue());
		jobj.addProperty("comment", src.getComment());
		String writer = src.getWriter();
		if(writer==null) {
			jobj.addProperty("writer","");
		} else {
			jobj.addProperty("writer", writer);
		}
		
		Timestamp createdOn = src.getCreatedOn();
		jobj.addProperty("createdOn", createdOn==null? null : DateUtil.format(createdOn));
		return jobj;
	}

}
