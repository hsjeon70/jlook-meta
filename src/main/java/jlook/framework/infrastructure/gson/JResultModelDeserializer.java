package jlook.framework.infrastructure.gson;

import java.lang.reflect.Type;

import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class JResultModelDeserializer implements JsonDeserializer<JResultModel> {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JResultModelDeserializer.class);
	
	@Override
	public JResultModel deserialize(JsonElement gson, Type type,
			JsonDeserializationContext context) throws JsonParseException {
//		if(logger.isDebugEnabled()) {
//			logger.debug("--> "+gson);
//		}
		
		JResultModel rdata = new JResultModel();
		if(gson.isJsonObject()) {
			JsonObject jobj = (JsonObject)gson;
			JsonElement result = jobj.get("result");
			JsonElement message = jobj.get("message");
			JsonElement data = jobj.get("data");
			
			rdata.setResult(result.getAsString());
			if(!message.isJsonNull()) {
				rdata.setMessage(message.getAsString());
			}
			if(!data.isJsonNull()) {
				rdata.setData(data.toString());
			}
		} else if(gson.isJsonPrimitive()) {
			throw new JsonParseException("Invalid Response message. - "+gson.toString());
		}
		
		return rdata;
	}

}
