package jlook.framework.infrastructure;

import javax.annotation.PostConstruct;

import jlook.framework.infrastructure.util.Log;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

//@Component(JClassLoaderFactoryBean.ID)
public class JClassLoaderFactoryBean implements FactoryBean<JClassLoader> {
	private static Log logger = Log.getLog(JClassLoaderFactoryBean.class);
	
	public static final String ID = "jClassLoaderFactoryBean";
	
	private JClassLoader jClassLoader;
	
	@PostConstruct
	public void doInit() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		this.jClassLoader = new JClassLoader();
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started. --------->");
		}
	}
	
	@Override
	public JClassLoader getObject() throws Exception {
		return this.jClassLoader;
	}

	@Override
	public Class<JClassLoader> getObjectType() {
		return JClassLoader.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
