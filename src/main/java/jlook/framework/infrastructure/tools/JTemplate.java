package jlook.framework.infrastructure.tools;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

public class JTemplate {
	private Template template;
	private VelocityContext context;
	
	public JTemplate(Template template) {
		this.template = template;
		this.context = new VelocityContext();
	}
	
	public void put(String key, Object value) {
		this.context.put(key, value);
	}
	
	public String execute() throws JTemplateException {
		StringWriter writer = null;
		try {
			writer = new StringWriter();
			this.template.merge(this.context, writer);
			return writer.toString();
		} catch(Exception e) {
			throw new JTemplateException("Fail to execute template.", e);
		} finally {
			try { if(writer!=null) writer.close(); } catch(Exception e){}
		}
	}
}
