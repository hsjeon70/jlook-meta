package jlook.framework.infrastructure.tools;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import jlook.framework.JHeader;
import jlook.framework.domain.docs.Header;
import jlook.framework.domain.docs.OpenAPI;
import jlook.framework.domain.docs.OpenAPIService;
import jlook.framework.domain.docs.Parameter;
import jlook.framework.infrastructure.EnvKeys;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.interfaces.ParamKeys;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.thoughtworks.qdox.JavaDocBuilder;
import com.thoughtworks.qdox.model.Annotation;
import com.thoughtworks.qdox.model.DocletTag;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaParameter;
import com.thoughtworks.qdox.model.JavaSource;


/**
 *
 * @author hsjeon70
 *
 */
@Component(OpenAPIGenerator.ID)
public class OpenAPIGenerator {
	public static final String ID = "openAPIGenerator";
	private static Log logger = Log.getLog(OpenAPIGenerator.class);
	
	@Value(EnvKeys.INTERFACES_SOURCE_DIR)
	private String ifSourceDir;
	
	@Value(EnvKeys.INTERFACES_PACKAGE_NAME)
	private String ifPackageName;
	
	@Value(EnvKeys.OPENAPI_TARGET_DIR)
	private String openApiTargetDir;
	
	private Class<?>[] apiModelList = new Class<?>[]{
			java.util.ArrayList.class,
			java.util.HashMap.class,
			jlook.framework.domain.docs.Header.class,
			jlook.framework.domain.docs.Parameter.class,
			jlook.framework.domain.docs.OpenAPI.class,
			jlook.framework.domain.docs.OpenAPIService.class
		};
	
	private JAXBContext context;
	private Map<String,String> nameMap = new HashMap<String,String>();
	
	@PostConstruct
	public void doInit() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		nameMap.put("JHeader.JCONTENT_TYPE", JHeader.JCONTENT_TYPE);
		nameMap.put("ParamKeys.OBJECT_ID", ParamKeys.OBJECT_ID);
		nameMap.put("ParamKeys.CLASS_ID", ParamKeys.CLASS_ID);
		nameMap.put("ParamKeys.DOMAIN_ID", ParamKeys.DOMAIN_ID);
		nameMap.put("ParamKeys.PARENT_CID", ParamKeys.PARENT_CID);
		nameMap.put("ParamKeys.PARENT_OID", ParamKeys.PARENT_OID);
		nameMap.put("ParamKeys.KEYWORD", ParamKeys.KEYWORD);
		nameMap.put("ParamKeys.PAGE_NUMBER", ParamKeys.PAGE_NUMBER);
		nameMap.put("ParamKeys.PAGE_SIZE", ParamKeys.PAGE_SIZE);
		nameMap.put("ParamKeys.VERSION", ParamKeys.VERSION);
		nameMap.put("ParamKeys.PICTURE", ParamKeys.PICTURE);
		nameMap.put("UPLOAD_KEY", ParamKeys.UPLOAD_KEY);
		nameMap.put("REFERER_KEY", ParamKeys.REFERER_KEY);
		try {
			context = JAXBContext.newInstance(apiModelList);
		} catch (JAXBException e) {
			throw new JException("Fail to create JAXBContext. - "+e.getMessage(),e);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is started. --------->"+nameMap);
		}
	}
	
	public void generate() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("ifSourceDir --> "+ifSourceDir+", "+new File(".").getAbsolutePath());
		}
		File dir = new File(ifSourceDir);
		JavaDocBuilder builder = new JavaDocBuilder();
		builder.addSourceTree(dir);
		
		JavaSource[] sources = builder.getSources();
		for(JavaSource source : sources) {
			JavaClass[] javaClasses = source.getClasses();
			for(JavaClass javaClass : javaClasses) {
				String name = javaClass.getName();
				if(!name.endsWith("Controller")) {
					continue;
				}
				String comment = javaClass.getComment();
				
				DocletTag[] tags = javaClass.getTags();
				DocletTag tag = getOpenAPI(tags);
				if(tag==null) {
					continue;
				}
				
				OpenAPIService service = new OpenAPIService();
				service.setName(name.replace("Controller","Service"));
				service.setCategory(javaClass.getPackageName());
				service.setDescription(javaClass.getComment());
				
				if(logger.isInfoEnabled()) {
					logger.info(name+" ---> ##################### : "+tag.getValue());
					logger.info(comment);
				}
				JavaMethod[] javaMethods = javaClass.getMethods();
				for(JavaMethod javaMethod : javaMethods) {
					tags = javaMethod.getTags();
					tag = getOpenAPI(tags);
					if(tag==null) {
						continue;
					}
					
					if(logger.isInfoEnabled()) {
						logger.info(javaMethod.getName()+ "Method ================================");
						logger.info(javaMethod.getComment());
					}
					OpenAPI api = new OpenAPI();
					api.setName(tag.getValue());
					api.setDescription(javaMethod.getComment());
					
					Annotation[] anns = javaMethod.getAnnotations();
					for(Annotation ann : anns) {
						if(ann.getType().getValue().endsWith("RequestMapping")) {
							String url = (String)ann.getNamedParameter("value");
							api.setUri(url.replace("\"", ""));
							String method = (String)ann.getNamedParameter("method");
							api.setMethod(method==null?"GET":method.substring("RequestMethod.".length()));
						}
					}
					
					if(api.getUri()==null) {
						throw new RuntimeException("Cannot fine url. -------> ");
					}
					for(DocletTag t : tags) {
						String paramValue = t.getValue();
						if(t.getName().equals("return")) {
							api.setResponse(paramValue);
							continue;
						}
						if(t.getName().equals("publish")) {
							api.setName(paramValue);
							continue;
						}
						
						if(t.getName().equals("throws")) {
							api.setException(t.getValue());
							continue;
						}
						String[] params = t.getParameters();
						JavaParameter parameter = javaMethod.getParameterByName(params[0]);
						if(parameter==null) {
							throw new RuntimeException("Cannot find parameter. -------> "+params[0]);
						}
						anns = parameter.getAnnotations();
						for(Annotation ann : anns) {
							if(ann.getType().getValue().endsWith("RequestHeader")) {
								Header header = new Header();
								String headerName = (String)ann.getNamedParameter("value");
								if(this.nameMap.containsKey(headerName)) {
									header.setName(this.nameMap.get(headerName));
								} else {
									header.setName(headerName);
								}
								String[] ps = t.getParameters();
								String desc = "";
								for(int i=1;i<ps.length;i++) {
									desc += ps[i]+" ";
								}
								header.setDescription(desc.trim());
								header.setType(parameter.getType().getValue());
								header.setRequired((String)ann.getNamedParameter("required"));
								api.addHeader(header);
								if(logger.isInfoEnabled()) {
									logger.info("\t"+header);
								}
							} else if(ann.getType().getValue().endsWith("RequestParam")) {
								Parameter p = new Parameter();
								String pName = (String)ann.getNamedParameter("value");
								if(this.nameMap.containsKey(pName)) {
									p.setName(this.nameMap.get(pName));
								} else {
									p.setName(pName);
								}
								String[] ps = t.getParameters();
								String desc = "";
								for(int i=1;i<ps.length;i++) {
									desc += ps[i]+" ";
								}
								p.setDescription(desc.trim());
								p.setRequired((String)ann.getNamedParameter("required"));
								p.setType(parameter.getType().getValue());
								api.addParameter(p);
								if(logger.isInfoEnabled()) {
									logger.info("\t"+p);
								}
							}
						}
					}
					
					service.addOpenAPI(api);
					
					try {
						createFile(service);
					} catch (JException e) {
						logger.error("Fail to create xml file. - "+service.getName(), e);
					}
				}				
			}
		}
	}
	
	public void createFile(OpenAPIService service) throws JException {
		Marshaller ms;
		FileWriter fWriter = null;
		try {
			String category = service.getCategory();
			if(category.equals(this.ifPackageName)) {
				category = "general";
			} else {
				category = category.substring(this.ifPackageName.length()+1);
			}
			File dir = new File(this.openApiTargetDir+category);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			fWriter = new FileWriter(new File(dir, service.getName()+".xml"));
			ms = context.createMarshaller();
			ms.marshal(service, fWriter);
		} catch (Exception e) {
			throw new JException("Fail to generate OpenAPI docs. ", e);
		} finally {
			try { if(fWriter!=null) fWriter.close(); } catch(Exception e){}
		}
	}
	
	public DocletTag getOpenAPI(DocletTag[] tags) {
		for(DocletTag tag : tags) {
			if(tag.getName().equals("publish")) {
				return tag;
			}
		}
		
		return null;
	}
}
