package jlook.framework.infrastructure.tools;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.infrastructure.util.Log;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Component;

@Component(JTemplateEngine.ID)
public class JTemplateEngine {
	private static Log logger = Log.getLog(JTemplateEngine.class);
	
	public static final String ID = "jTemplateEngine";
	
	@Resource(name="velocityEnv")
	private Properties properties;
	
	private VelocityEngine engine;
	
	@PostConstruct
	public void doInit() {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		if(logger.isDebugEnabled()) {
			properties.list(System.out);
		}
		
		this.engine = new VelocityEngine();
		ExtendedProperties eprops = ExtendedProperties.convertProperties(this.properties);
	    this.engine.setExtendedProperties(eprops);
	    this.engine.init();
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started. --------->"+this.engine);
		}
	}
	
	public JTemplate newJTemplate(JTemplateType type) throws JTemplateException {
		return new JTemplate(this.engine.getTemplate(type.getFile()));
	}
	
	public JTemplate newJTemplate(String file) throws JTemplateException {
		return new JTemplate(this.engine.getTemplate(file));
	}
}
