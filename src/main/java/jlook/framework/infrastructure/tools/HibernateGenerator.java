package jlook.framework.infrastructure.tools;

import java.io.File;
import java.io.FileWriter;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.GlobalEnv;
import jlook.framework.GlobalKeys;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.hibernate.JSessionFactoryBean;
import jlook.framework.infrastructure.jdbc.HibernateRepository;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JGenericRepository;
import jlook.framework.repository.impl.JGenericRepositoryImpl;
import jlook.framework.service.util.JObjectUtil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.springframework.stereotype.Component;

@Component(HibernateGenerator.ID)
public class HibernateGenerator extends HibernateRepository {
	public static final String ID = "hibernateGenerator";
	private static Log logger = Log.getLog(HibernateGenerator.class);
	
	@Resource(name=JTemplateEngine.ID)
	private JTemplateEngine engine;
	
	@Resource(name=JGenericRepository.ID)
	private JGenericRepositoryImpl jGenericRepository;
	
//	public static final String CONFIG_DIR = GlobalKeys.CONFIG_PATH;
	public static final String CONFIG_DIR = GlobalKeys.CLASSES_PATH;
	private File configTargetDir;
	
	@PostConstruct
	public void doInit() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		
		if(this.configTargetDir == null) {
			this.configTargetDir = new File(System.getProperty(GlobalEnv.APP_HOME), CONFIG_DIR);
			if(!this.configTargetDir.exists()) {
				this.configTargetDir.mkdirs();
			}
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started.");
		}
	}
	
	public void mapEntity(JDomain jDomain, JClass jClass, Class<?> target) throws JException {
		JTemplate jTemplate;
		try {
			jTemplate = engine.newJTemplate(JTemplateType.hibernate_config);
			jTemplate.put("entity", jClass);
			jTemplate.put("domain", jDomain);
			
			String xmlConfig  = jTemplate.execute();
			String className = JObjectUtil.getClassName(jClass);
			if(className!=null) {
				className = className.replace(".", "/");
			}
			String fileName = className+".hbm.xml";
//			String fileName = JObjectUtil.getConfigPath(jClass)+".hbm.xml";
			File xmlFile = writeFile(fileName, xmlConfig);
			if(logger.isDebugEnabled()) {
				logger.debug("## --> Written xml config file - "+xmlFile.getAbsolutePath());
			}
			ClassLoader cloader= target.getClassLoader();
			if(logger.isDebugEnabled()) {
				logger.debug("\t--> "+cloader);
			}
			
//			JSpringSessionFactoryBean bean = (JSpringSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
			JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
			bean.addConfigFile(xmlFile);
//			Configuration config = bean.getConfiguration();
//			
//			Thread.currentThread().setContextClassLoader(bean.getJClassLoader());
//			config.addFile(xmlFile);
//			SessionFactory sessionFactory = config.buildSessionFactory();
			/**
			 * TODO : REPOSITORY SessionFactory change?
			 */
//			jGenericRepository.setSessionFactory(sessionFactory);
			
			if(logger.isDebugEnabled()) {
//				logger.debug("## --> Build SessionFactory - "+sessionFactory.toString());
				logger.debug("## --> Generating JClass is completed. - "+jClass);
			}
		} catch (JTemplateException e) {
			throw new JException("Fail to generate hibernate configuration. {0}", e, new Object[]{jClass});
		} catch(Exception e) {
			throw new JException("Fail to generate hibernate configuration. {0}", e, new Object[]{jClass});
		}
	}
	
	public void unmapEntity(JDomain jDomain, JClass jClass, Class<?> target) throws JException {
		
		String className = JObjectUtil.getClassName(jClass);
		if(className!=null) {
			className = className.replace(".", "/");
		}
		String fileName = className+".hbm.xml";
		File file = new File(this.configTargetDir, fileName);
		boolean result = file.delete();
		if(logger.isDebugEnabled()) {
			logger.debug("Hibernate config file delete --> "+result);
		}
		
		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		Configuration config = bean.getConfiguration();
		Thread.currentThread().setContextClassLoader(bean.getJClassLoader());
//		new SchemaUpdate(config).execute(true, true);
		SessionFactory sessionFactory = config.buildSessionFactory();
		/**
		 * TODO : REPOSITORY SessionFactory change?
		 */
		jGenericRepository.setSessionFactory(sessionFactory);
		
	}
	
	private File writeFile(String fileName, String config) throws JException {
		FileWriter fw = null;
		File file = null;
		try {
			file = new File(this.configTargetDir, fileName);
			File parent = file.getParentFile();
			if(!parent.exists()) {
				parent.mkdirs();
			}
			fw = new FileWriter(file);
			fw.write(config);
			return file;
		} catch(Exception e) {
			throw new JException("Fail to write configuration file. - "+file,e);
		} finally {
			try { if(fw!=null) fw.close(); } catch(Exception e) {}
		}
	}
	
	public void updateSchema() throws JException {
		JSessionFactoryBean bean = (JSessionFactoryBean)ApplicationContextHolder.getBean("&mySessionFactory");
		Configuration config = bean.getConfiguration();
		
		new SchemaUpdate(config).execute(true, true);
	}
}
