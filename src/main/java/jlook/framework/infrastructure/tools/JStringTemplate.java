package jlook.framework.infrastructure.tools;

import java.util.Map;

import org.mvel2.MVEL;
import org.springframework.stereotype.Component;

import jlook.framework.infrastructure.JException;

@Component(JStringTemplate.ID)
public class JStringTemplate {
	public static final String ID = "jStringTemplate";
	
	public static final String JUSER_KEY = "juser";
	public static final String JOBJECT_KEY = "jobject";
	public static final String JACTION_KEY = "jaction";
	
	public static final String JACTION_CREATE = "create";
	public static final String JACTION_UPDATE = "update";
	
	public String eval(String expression, Map<String,Object> map) throws JException {
		Object result = null; 
		try {
			result = MVEL.eval(expression, map);
		} catch(Exception e) {
			throw new JException("Fail to eval expression["+expression+"] - "+e.getMessage(),e);
		}
		if(result instanceof String) {
			return (String)result;
		}
		
		return result==null ? "" : result.toString();
	}
}
