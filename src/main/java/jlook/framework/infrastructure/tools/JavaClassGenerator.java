package jlook.framework.infrastructure.tools;

import java.io.File;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Map;

import javassist.CannotCompileException;
import javassist.ClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.LoaderClassPath;
import javassist.NotFoundException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.framework.GlobalKeys;
import jlook.framework.domain.JObject;
import jlook.framework.domain.JPrimitive;
import jlook.framework.domain.metadata.JAttribute;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.metadata.JType;
import jlook.framework.infrastructure.JClassLoader;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.util.JObjectUtil;

import org.springframework.stereotype.Component;

@Component(JavaClassGenerator.ID)
public class JavaClassGenerator {
	private static Log logger = Log.getLog(JavaClassGenerator.class);
	public static final String ID = "javaClassGenerator";
	
	public static final String CLASSES_DIR = GlobalKeys.CLASSES_PATH;
	private ClassPool pool;
	
	private File classTargetDir;
	
	@Resource(name=JClassLoader.ID)
	private JClassLoader jClassLoader;
	
	@PostConstruct
	public void doInit() throws JException {
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting.");
		}
		
		this.pool = new ClassPool();//ClassPool.getDefault();
		
		classTargetDir = this.jClassLoader.getClassPath();
		if(!classTargetDir.exists()) {
			classTargetDir.mkdirs();
		}
		
		try {
			this.pool.appendClassPath(classTargetDir.getAbsolutePath());
		} catch (NotFoundException e) {
			throw new JException("Fail to append classpath. - "+classTargetDir.getAbsolutePath());
		}
//		LoaderClassPath lcPath = new LoaderClassPath(this.getClass().getClassLoader());
		LoaderClassPath lcPath = new LoaderClassPath(this.jClassLoader);
		this.pool.appendClassPath(lcPath);
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> "+lcPath.toString());
		}
				
		ClassPath cpath = this.pool.appendSystemPath();
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> "+cpath.toString());
		}
		if(logger.isDebugEnabled()) {
			logger.debug("It is started. --------->");
		}
	}
	
	public Class<?> mapEntity(JClass jClass) throws JException {
		String className = JObjectUtil.getClassName(jClass);
		URL url = pool.find(className);
		if(url!=null) {
			logger.warn("Already created class.  - "+url.toString());
//			throw new JException("Already created class.  - "+url.toString());
		}
		
		CtClass ctClass;
		try {
			ctClass = pool.get(className);
			updateEntityClass(ctClass, jClass);
		} catch (NotFoundException e) {
			ctClass = pool.makeClass(className);
			ctClass.setModifiers(Modifier.PUBLIC);
			newEntityClass(ctClass, jClass);
		}
		
		try {
			ctClass.writeFile(this.classTargetDir.getPath());
			return ctClass.toClass();
		} catch (Exception e) {
			throw new JException("Fail to create file for this class("+jClass+")",e);
		}
	}
	
	/**
	 * TODO : update entity class
	 * @param ctClass
	 * @param jClass
	 * @throws JException
	 */
	protected void updateEntityClass(CtClass ctClass, JClass jClass) throws JException {
		ctClass.defrost();
		
		String superJClassName = null;
		if(jClass.getInheritanceJClass()==null || jClass.getInheritanceJClass().equals("")) {
			superJClassName = JObject.class.getName();
		} else {
			JClass superJClass = jClass.getInheritanceJClass();
			superJClassName = JObjectUtil.getClassName(superJClass);
		}
		try {
			CtClass superCtClass = pool.get(superJClassName);
			ctClass.setSuperclass(superCtClass);
		} catch (Exception e) {
			throw new JException("Cannot get super class. - "+superJClassName, e);
		} 
		
		// TODO NOT IMPLEMENTED ----------->
		
	}
	
	protected void newEntityClass(CtClass ctClass, JClass jClass) throws JException {
		String superJClassName = null;
		if(jClass.getInheritanceJClass()==null || jClass.getInheritanceJClass().equals("")) {
			superJClassName = JObject.class.getName();
		} else {
			JClass superJClass = jClass.getInheritanceJClass();
			superJClassName = JObjectUtil.getClassName(superJClass);
		}
		try {
			CtClass superCtClass = pool.get(superJClassName);
			ctClass.setSuperclass(superCtClass);
		} catch (Exception e) {
			throw new JException("Cannot get super class. - "+superJClassName, e);
		} 
		
		Map<String,JAttribute> jattMap = jClass.getJAttributes();
		for(JAttribute jatt : jattMap.values()) {
			CtField f = newField(ctClass, jatt);
			try {
				ctClass.addField(f);
			} catch (CannotCompileException e) {
				throw new JException("Cannot add field to class("+jClass+"). - "+jatt, e);
			}
			CtMethod mSet;
			try {
				mSet = CtNewMethod.getter(getter(jatt), f);
				ctClass.addMethod(mSet);
			} catch (CannotCompileException e) {
				throw new JException("Fail to create getter method. - "+jatt,e);
			}
			
			CtMethod mGet;
			try {
				mGet = CtNewMethod.setter(setter(jatt), f);
				ctClass.addMethod(mGet);
			} catch (CannotCompileException e) {
				throw new JException("Fail to create setter method. - "+jatt,e);
			}
		}
	}
	
	/**
	 * TODO : implements
	 * @param jClass
	 * @throws JException
	 */
	public void unmapEntity(JClass jClass) throws JException {
		String className = JObjectUtil.getClassName(jClass);
		if(className!=null) {
			className = className.replace(".", "/");
		}
		String fileName = className+".class";
		File file = new File(this.classTargetDir, fileName);
		boolean result = file.delete();
		if(logger.isDebugEnabled()) {
			logger.debug("Java class file delete --> "+result);
		}
	}
	
	/**
	 * 
	 * @param className
	 * @return
	 * @throws JException
	 */
	public Class<?> getClass(String className) throws JException {
		try {
//			set CLASSPATH=jlook/classes, find custom entity by system classloader but JUser cannot find by system classloader.
//			ClassLoader cloader = this.pool.getClassLoader();
//			return cloader.loadClass(className);
			return this.jClassLoader.loadClass(className);
		} catch (Exception e) {
			throw new JException("Cannot find the class("+className+").", e);
		}
	}
	
//	public JClassLoader getClassLoader() {
//		return this.jClassLoader;
//	}
	
	private String getter(JAttribute jatt) {
		String name = jatt.getName();
		JType type = jatt.getType();
		if(type.isPrimitive() && JPrimitive.BOOLEAN_TYPE.equals(type.getName())) {
			return "is"+Character.toUpperCase(name.charAt(0))+name.substring(1);
		} else {
			return "get"+Character.toUpperCase(name.charAt(0))+name.substring(1);
		}
	}
	
	private String setter(JAttribute jatt) {
		String name = jatt.getName();
		return "set"+Character.toUpperCase(name.charAt(0))+name.substring(1);
	}
	
	private CtField newField(CtClass ctClass, JAttribute jatt) throws JException {
		String name = jatt.getName();
		JType type = jatt.getType();
		String typeName = null;
		if(type.isPrimitive()) {
			JPrimitive pt = null;
			try {
				pt = JPrimitive.getPrimitive(type.getName());
			} catch(Exception e) {
				throw new JException("Invalid primitive type. - "+type.getName(), e);
			}
			typeName = pt.getJavaClass().getName();
		} else {
			JClass refJClass = type.getJClass();
			typeName = JObjectUtil.getClassName(refJClass);
		}
		CtField f;
		try {
			f = new CtField(pool.get(typeName), name, ctClass);
		} catch (Exception e) {
			throw new JException("Cannot create field. - "+typeName+" "+ name, e);
		}
		f.setModifiers(Modifier.PRIVATE);
		
		return f;
	}
}
