package jlook.framework.infrastructure.tools;

public enum JTemplateType {
	hibernate_config("template/hibernate-config.vm"),
	domain_javasource("template/domain-javasource.vm");
	
	JTemplateType(String file) {
		this.file = file;
	}
	
	private String file;
	
	public String getFile() {
		return this.file;
	}
}
