package jlook.framework.infrastructure.tools;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.annotation.Message;

@Message(text=JTemplateException.TEXT)
public class JTemplateException extends JException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Template Error";
	
	public JTemplateException(String message) {
		super(message);
	}
	public JTemplateException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JTemplateException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JTemplateException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JTemplateException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JTemplateException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
