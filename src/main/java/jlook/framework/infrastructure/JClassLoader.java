package jlook.framework.infrastructure;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import jlook.framework.GlobalEnv;
import jlook.framework.GlobalKeys;
import jlook.framework.infrastructure.junit.Keys;

@Component(JClassLoader.ID)
public class JClassLoader extends ClassLoader {
	private static Log logger = LogFactory.getLog(JClassLoader.class);
	public static final String ID = "jClassLoader";
	
	private File clasPath;
	private String directory;
	
	public JClassLoader() {
		this(JClassLoader.class.getClassLoader());
	}
	
	public JClassLoader(ClassLoader parent) {
		super(parent);
		String appHome = System.getProperty(GlobalEnv.APP_HOME);
		clasPath = new File(appHome, directory==null ? GlobalKeys.CLASSES_PATH : directory);;
	}
	
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	public File getClassPath() {
		return this.clasPath;
	}
	
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		// Our goal is to get a Class object
		// First, see if we've already dealt with this one
	    Class<?> clazz = findLoadedClass( name );
	    if(clazz!=null) {
	    	if(logger.isTraceEnabled()) {
	    		logger.trace(">> Found the class("+name+") from JClassLoader. It was loaded already.");
	    	}
	    	return clazz;
	    }
	    
	    File filePath = new File(clasPath, name.replace(".", "/")+".class");
        if(!filePath.exists()) {
		    try {
		    	clazz = getParent().loadClass(name);
		    	if(logger.isTraceEnabled()) {
		    		ClassLoader cloader = clazz.getClassLoader();
		    		logger.trace(">> Loading ClassLoader -> "+cloader);
		    	}
		    } catch(Throwable e) {
		    	if(logger.isTraceEnabled()) {
		    		logger.trace(">> Cannot load the class('"+name+"') from parent ClassLoader.");
		    	}
		    }
		    if(clazz!=null) {
		    	if(logger.isTraceEnabled()) {
		    		logger.trace(">> Found the class("+name+") from super ClassLoader. It was loaded already.");
		    	}
		    	return clazz;
		    }
		    throw new ClassNotFoundException("Cannot find class - "+name);
        }
	    
        InputStream input = null;
        ByteArrayOutputStream  buffer = null;
        try {
            input = new FileInputStream(filePath);
            if(logger.isDebugEnabled()) {
            	logger.debug("--> Loading the class. -"+name);
            }
            buffer = new ByteArrayOutputStream();
            int data = input.read();

            while(data != -1){
                buffer.write(data);
                data = input.read();
            }

            input.close();

            byte[] classData = buffer.toByteArray();
            if(logger.isDebugEnabled()) {
            	logger.info(">> Loaded the class('"+name+"') from JClassLoader.");
            }
            return defineClass(name, classData, 0, classData.length);
        } catch (IOException e) {
            throw new RuntimeException("Cannot load the class. - "+name); 
        } finally {
        	try { if(input!=null) input.close(); } catch(Exception e){}
        	try { if(buffer!=null) buffer.close(); } catch(Exception e){}
        }
    }
	
	
	@Override
	public InputStream getResourceAsStream(String name) {
		InputStream inStream = null;
		try {
			inStream = super.getResourceAsStream(name);
			if(inStream!=null) {
				return inStream;
			}
		} catch(Exception e) {
			if(logger.isInfoEnabled()) {
				logger.info("Cannot load resource. - "+name);
			}
		}
		File resource = new File(clasPath, name);
		if(!resource.exists()) {
			return null;
		}
		
		try {
			inStream = new FileInputStream(resource);
			return inStream;
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	@SuppressWarnings("restriction")
	public static void main(String[] args) throws Exception {
		String fileName = "MyClass.java";
		String className = "MyClass";
		String methodName = "sayHello";
		String parameterName = "name";
		System.setProperty(GlobalEnv.HOME.getName(), Keys.HOME);
		
		String appHome = System.getProperty(GlobalEnv.APP_HOME);
	    File classHome = new File(appHome, "classes");
	    File f = new File(classHome, fileName);
	    
		FileWriter fileWriter=new FileWriter(f,false);
		fileWriter.write("package test;\n");
		fileWriter.write("public class "+ className +" {\n");
		fileWriter.write("public String "+methodName +"(String "+parameterName+") {\n");
		fileWriter.write("System.out.println(\" Testing\");\n");
		fileWriter.write("return "+parameterName +"+ \" is dumb\";\n }\n}");
		fileWriter.flush();
		fileWriter.close();
		
		String[] source = { new String(fileName) };
		com.sun.tools.javac.Main.compile(source);
		
		ClassLoader parentClassLoader = JClassLoader.class.getClassLoader();
		JClassLoader classLoader = new JClassLoader(parentClassLoader);
	    Class<?> myObjectClass = classLoader.loadClass("test."+className);
	    
	    if(logger.isDebugEnabled()) {
	    	logger.debug("--> "+myObjectClass);
	    }
	}

	@Override
	public String toString() {
		return "JClassLoader [parent=" + super.getParent() + "]";
	}
}
