package jlook.framework.infrastructure.http;

public enum HttpMethod {
	GET,
	POST,
	PUT,
	DELETE;
}
