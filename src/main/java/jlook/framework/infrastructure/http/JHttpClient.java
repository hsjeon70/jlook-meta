package jlook.framework.infrastructure.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import jlook.framework.infrastructure.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

public class JHttpClient {
	private static Log logger = Log.getLog(JHttpClient.class);
	
	private HttpClient httpclient;
	private JHttpRequest request;
	private JHttpResponse response;
	private String targetHost;
	private String url;
	
	public JHttpClient() {
		this.httpclient = new DefaultHttpClient();
	}
	
	public void setTargetHost(String targetHost) {
		this.targetHost = targetHost;
	}
	
	public JHttpRequest newHttpRequest(HttpMethod method, String targetUrl) {
		HttpRequestBase req = null;
		url = targetUrl.startsWith("http") ? targetUrl : targetHost+targetUrl;
		if(logger.isDebugEnabled()) {
			logger.debug("HTTP Request --> "+url);
		}
		switch(method) {
		case GET : req = new HttpGet();
			break;
		case POST : req = new HttpPost(url);
			break;
		}
		
		this.request = new JHttpRequest(req);
		return this.request;
	}
	
	public JHttpResponse execute(JHttpRequest request) throws IOException {
		HttpRequestBase target = request.getTarget();
		HttpMethod method = HttpMethod.valueOf(request.getMethod());
		switch(method) {
		case GET : String query = request.getQueryString();
					HttpGet get = ((HttpGet)target);
			try {
				get.setURI(new URI(this.url+"?"+query));
			} catch (URISyntaxException e) {
				throw new IOException(e);
			}
			break;
		case POST : HttpEntity entity = request.getHttpEntityForPost();
					((HttpPost)target).setEntity(entity);
			break;
		}
		
		this.response = new JHttpResponse(httpclient.execute(target));
		if(logger.isDebugEnabled()) {
			logger.debug(response.getStatusLine() + " <-- " + this.request.getTargetUrl());
		}
		
		return this.response;
	}
	
	public void shutdown() {
		if(this.httpclient==null) {
			return;
		}
		this.httpclient.getConnectionManager().shutdown();
	}
}
