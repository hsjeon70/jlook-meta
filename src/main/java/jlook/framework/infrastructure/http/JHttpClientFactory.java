package jlook.framework.infrastructure.http;

import org.springframework.stereotype.Component;

@Component(JHttpClientFactory.ID)
public class JHttpClientFactory {

	public static final String ID = "jHttpClientFactory";
	
	public JHttpClient getJHttpClient() {
		return new JHttpClient();
	}
}
