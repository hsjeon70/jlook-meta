package jlook.framework.infrastructure.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

public class JHttpResponse {
	
	private HttpResponse target;
	private String content;
	
	public JHttpResponse(HttpResponse target) {
		this.target = target;
	}
	
	public int getStatusCode() {
		return this.target.getStatusLine().getStatusCode();
	}
	
	public String getStatusLine() {
		return this.target.getStatusLine().toString();
	}
	
	public String getHeader(String name) {
		Header header = this.target.getFirstHeader(name);
		return header==null ? null : header.getValue();
	}
	
	public String[] getHeaders(String name) {
		Header[] headers = this.target.getHeaders(name);
		if(headers==null) {
			return new String[0];
		}
		
		String[] values = new String[headers.length];
		for(int i=0;i<headers.length;i++) {
			values[i] = headers[i].getValue();
		}
		
		return values;
	}
	
	public void release() {
		try {
			EntityUtils.consume(this.target.getEntity());
		} catch (IOException e) {
			// empty
		}
	}
	
	public String getContent() throws IOException {
		if(this.content!=null) {
			return this.content;
		}
		HttpEntity entity = target.getEntity();
		InputStream instream = null;
		BufferedReader  reader = null;
		StringWriter sw = null;
		try {
			instream = entity.getContent();
			reader = new BufferedReader(new InputStreamReader(instream));
			sw = new StringWriter();
	        String line = null;
			while((line = reader.readLine())!=null) {
				sw.write(line);
			}
			
			this.content = sw.toString();
			return this.content;
		} finally {
			try { if(sw!=null) sw.close(); } catch(Exception e){}
			try { if(reader!=null) reader.close(); } catch(Exception e){}
			try { if(instream!=null) instream.close(); } catch(Exception e){}
		}
	}
}
