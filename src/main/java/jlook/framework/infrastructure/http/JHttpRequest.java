package jlook.framework.infrastructure.http;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;

public class JHttpRequest  {
	
	private HttpRequestBase target;
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	
	public JHttpRequest(HttpRequestBase target) {
		this.target = target;
	}
	
	protected HttpRequestBase getTarget() {
		return this.target;
	}
	
	public String getTargetUrl() {
		return this.target.getURI().toString();
	}
	public String getMethod() {
		return this.target.getMethod();
	}
	
	public void setParameter(String name, String value) {
		this.params.add(new BasicNameValuePair(name, value));
	}
	
	public HttpEntity getHttpEntityForPost() throws UnsupportedEncodingException {
		return new UrlEncodedFormEntity(params);
	}
	
	public String getQueryString() {
		StringBuffer query = new StringBuffer();
		for(NameValuePair pair : params) {
			query.append(pair.getName()).append("=").append(pair.getValue()).append("&");
		}
		return query.toString();
	}
	
	public void addHeader(String name, String value) {
		this.target.addHeader(name, value);
	}
}
