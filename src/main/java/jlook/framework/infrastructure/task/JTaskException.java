package jlook.framework.infrastructure.task;

import jlook.framework.infrastructure.JException;

public class JTaskException extends JException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String TEXT = "Batch Task Error";
	
	public JTaskException(String message) {
		super(message);
	}
	public JTaskException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JTaskException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JTaskException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JTaskException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JTaskException(String message, Throwable cause) {
		super(message, cause);
	}
}
