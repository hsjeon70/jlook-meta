package jlook.framework.infrastructure.task;

import jlook.framework.infrastructure.util.Log;

public class EchoTask extends JTaskSupport {
	private static Log logger = Log.getLog(EchoTask.class);
	
	@Override
	public void doExecute() throws JTaskException {
		if(logger.isDebugEnabled()) {
			logger.debug("########################################"+this.retryCount+"/"+this.maxRetryCount);
		}
		
		throw new JTaskException("Error****************");
	}

}
