package jlook.framework.infrastructure.task;

public interface JTask {
	
	public void setJTaskContext(JTaskContext context);
	public void doInit(int maxRetryCount) throws JTaskException;
	public void doExecute() throws JTaskException;
	public void doEnd(JTaskException ex) throws JTaskException;
	
	public int getMaxRetryCount();
	public int getRetryCount();
	public void addRetryCount();
}
