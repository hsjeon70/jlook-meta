package jlook.framework.infrastructure.task;

public abstract class JTaskSupport implements JTask {
	protected JTaskContext context;
	protected int maxRetryCount;
	protected int retryCount;
	
	@Override
	public void setJTaskContext(JTaskContext context) {
		this.context = context;
	}

	@Override
	public void doInit(int maxRetryCount) throws JTaskException {
		this.maxRetryCount = maxRetryCount;
	}

	@Override
	abstract public void doExecute() throws JTaskException;
	
	@Override
	public void doEnd(JTaskException ex) throws JTaskException {
	}
	
	@Override
	public int getMaxRetryCount() {
		return this.maxRetryCount;
	}
	
	@Override
	public int getRetryCount() {
		return this.retryCount;
	}
	
	@Override
	public void addRetryCount() {
		++this.retryCount;
	}
	
}
