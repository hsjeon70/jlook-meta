package jlook.framework.infrastructure.task;

public interface JTaskContext {
	public Object getEnvironment(String name);
	public Object getBean(String beanName);
}
