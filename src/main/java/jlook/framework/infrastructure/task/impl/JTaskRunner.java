package jlook.framework.infrastructure.task.impl;

import jlook.framework.infrastructure.task.JTask;
import jlook.framework.infrastructure.task.JTaskException;
import jlook.framework.infrastructure.util.Log;

public class JTaskRunner implements Runnable {
	private Log logger = Log.getLog(JTaskRunner.class);
	
	private JTask task;
	private JTaskExecutorImpl jTaskExecutor;
	
	public JTaskRunner(JTaskExecutorImpl jTaskExecutor, JTask task) {
		this.jTaskExecutor = jTaskExecutor;
		this.task = task;
	}
	
	@Override
	public void run() {
		JTaskException ex = null;
		try {
			task.doExecute();
		} catch (JTaskException e) {
			logger.error("Fail to execute JTask. - "+task,e);
			ex = e;
			try {
				if(task.getRetryCount()<task.getMaxRetryCount()) {
					this.jTaskExecutor.run(this);
				}
			} catch (JTaskException e1) {
				logger.error("Fail to execute JTaskRunner. - "+e.getMessage(), e);
			}
		} finally {
			task.addRetryCount();
			try {
				task.doEnd(ex);
			} catch (JTaskException e) {
				logger.error("Fail to exeucte JTaskRunner.doEnd(). - "+e.getMessage(), e);
			}
		}
	}

}
