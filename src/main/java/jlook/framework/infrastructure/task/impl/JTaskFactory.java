package jlook.framework.infrastructure.task.impl;

import org.springframework.stereotype.Component;

import jlook.framework.infrastructure.task.JTask;
import jlook.framework.infrastructure.task.JTaskException;

@Component(JTaskFactory.ID)
public class JTaskFactory {
	public static final String ID = "jTaskFactory";
	
	public JTask newJTask(String className) throws JTaskException {
		Class<?> clazz = null; 
		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new JTaskException("Cannot find class for JTask. - "+className, e);
		}
		
		Object obj  = null;
		try {
			obj = clazz.newInstance();
		} catch(Exception e) {
			throw new JTaskException("Cannot create a instance for JTask. - "+className, e);
		}
		
		if(obj instanceof JTask) {
			JTask task = (JTask)obj;
			return task;
		} else {
			throw new JTaskException("Invalid JTask object.- "+className);
		}
	}

}
