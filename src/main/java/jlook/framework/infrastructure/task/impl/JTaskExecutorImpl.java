package jlook.framework.infrastructure.task.impl;

import javax.annotation.Resource;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import jlook.framework.infrastructure.task.JTask;
import jlook.framework.infrastructure.task.JTaskException;
import jlook.framework.infrastructure.task.JTaskExecutor;

@Component(JTaskExecutor.ID)
public class JTaskExecutorImpl implements JTaskExecutor {
	
	@Resource(name="taskExecutor")
	private ThreadPoolTaskExecutor taskExecutor;

	@Resource(name=JTaskFactory.ID)
	private JTaskFactory jTaskFactory;
	
	
	protected void run(JTaskRunner runner) throws JTaskException {
		this.taskExecutor.execute(runner, 1000*60);
	}

	@Override
	public void run(String taskClassName) throws JTaskException {
		run(taskClassName,0);
	}

	@Override
	public void run(String taskClassName, int maxRetryCount)
			throws JTaskException {
		JTask task = this.jTaskFactory.newJTask(taskClassName);
		task.doInit(maxRetryCount);
		JTaskRunner runner = new JTaskRunner(this, task);
		this.taskExecutor.execute(runner);
	}
	
}
