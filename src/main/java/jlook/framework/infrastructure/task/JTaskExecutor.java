package jlook.framework.infrastructure.task;

public interface JTaskExecutor {
	public static final String ID = "jTaskExecutor";
	
	public void run(String taskClassName) throws JTaskException;
	public void run(String taskClassName, int maxRetryCount) throws JTaskException;
}
