package jlook.framework.infrastructure.jdbc;

import javax.annotation.Resource;

import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.repository.JRepositoryException;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;


public class HibernateRepository {
	private JContextFactory jContextFactory;
	protected HibernateTemplate template;
	
//	@Resource(name=HibernateConfig.ID)
//	public void setSessionFactory(HibernateConfig config) {
//		this.template = new HibernateTemplate(config.getSessionFactory());
//	}
	
	@Resource(name="mySessionFactory") 
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.template = new HibernateTemplate(sessionFactory);
	}
	
	@Resource(name=JContextFactory.ID)
	public void setJContextFactory(JContextFactory jContextFactory) {
		this.jContextFactory = jContextFactory;
	}
	
	protected Long getDomainId() {
		JContext context = this.jContextFactory.getJContext();
		return context.getDomainId();
	}
	
	protected boolean isAdministrator() throws JRepositoryException{
		JContext context = this.jContextFactory.getJContext();
		try {
			JUserEntity entity = (JUserEntity)context.getUserEntity();
			if(entity==null) {
				return false;
			}
			return entity.isAdministrator();
		} catch (JContextException e) {
			throw new JRepositoryException("Fail to get UserEntity.", e);
		}
	}
}
