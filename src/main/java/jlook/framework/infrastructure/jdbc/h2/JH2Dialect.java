package jlook.framework.infrastructure.jdbc.h2;

import java.sql.Types;

import org.hibernate.dialect.H2Dialect;

public class JH2Dialect extends H2Dialect {
	
	public JH2Dialect() {
		super();
		registerHibernateType(Types.BOOLEAN, "boolean");
	}
}
