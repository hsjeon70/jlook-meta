package jlook.framework.infrastructure.hibernate;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.Reference;

import org.hibernate.Cache;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.TypeHelper;
import org.hibernate.classic.Session;
import org.hibernate.engine.FilterDefinition;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.stat.Statistics;

public class JSessionFactory implements SessionFactory {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected SessionFactory sessionFactory;
	
	public JSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	protected void setSessionFactory(SessionFactory sessionFactory) {
		SessionFactory tmp = null;
		synchronized(this.sessionFactory) {
			tmp = this.sessionFactory;
			this.sessionFactory = sessionFactory;
		}
		if(!tmp.isClosed()) {
			tmp.close();
		}
	}

	public Session openSession() throws HibernateException {
		return sessionFactory.openSession();
	}

	public Session openSession(Interceptor interceptor)
			throws HibernateException {
		return sessionFactory.openSession(interceptor);
	}

	public Session openSession(Connection connection) {
		return sessionFactory.openSession(connection);
	}

	public Session openSession(Connection connection, Interceptor interceptor) {
		return sessionFactory.openSession(connection, interceptor);
	}

	public Session getCurrentSession() throws HibernateException {
		return sessionFactory.getCurrentSession();
	}

	public StatelessSession openStatelessSession() {
		return sessionFactory.openStatelessSession();
	}

	public StatelessSession openStatelessSession(Connection connection) {
		return sessionFactory.openStatelessSession(connection);
	}

	public ClassMetadata getClassMetadata(Class entityClass) {
		return sessionFactory.getClassMetadata(entityClass);
	}

	public ClassMetadata getClassMetadata(String entityName) {
		return sessionFactory.getClassMetadata(entityName);
	}

	public CollectionMetadata getCollectionMetadata(String roleName) {
		return sessionFactory.getCollectionMetadata(roleName);
	}

	public Map<String, ClassMetadata> getAllClassMetadata() {
		return sessionFactory.getAllClassMetadata();
	}

	public Map getAllCollectionMetadata() {
		return sessionFactory.getAllCollectionMetadata();
	}

	public void close() throws HibernateException {
		sessionFactory.close();
	}

	public boolean isClosed() {
		return sessionFactory.isClosed();
	}

	public Cache getCache() {
		return sessionFactory.getCache();
	}

	public void evict(Class persistentClass) throws HibernateException {
		sessionFactory.evict(persistentClass);
	}

	public void evict(Class persistentClass, Serializable id)
			throws HibernateException {
		sessionFactory.evict(persistentClass, id);
	}

	public void evictEntity(String entityName) throws HibernateException {
		sessionFactory.evictEntity(entityName);
	}

	public void evictEntity(String entityName, Serializable id)
			throws HibernateException {
		sessionFactory.evictEntity(entityName, id);
	}

	public void evictCollection(String roleName) throws HibernateException {
		sessionFactory.evictCollection(roleName);
	}

	public void evictCollection(String roleName, Serializable id)
			throws HibernateException {
		sessionFactory.evictCollection(roleName, id);
	}

	public void evictQueries(String cacheRegion) throws HibernateException {
		sessionFactory.evictQueries(cacheRegion);
	}

	public void evictQueries() throws HibernateException {
		sessionFactory.evictQueries();
	}

	public Set getDefinedFilterNames() {
		return sessionFactory.getDefinedFilterNames();
	}

	public FilterDefinition getFilterDefinition(String filterName)
			throws HibernateException {
		return sessionFactory.getFilterDefinition(filterName);
	}

	public boolean containsFetchProfileDefinition(String name) {
		return sessionFactory.containsFetchProfileDefinition(name);
	}

	public Reference getReference() throws NamingException {
		return sessionFactory.getReference();
	}

	public Statistics getStatistics() {
		return sessionFactory.getStatistics();
	}

	public TypeHelper getTypeHelper() {
		return sessionFactory.getTypeHelper();
	}
	
	
}
