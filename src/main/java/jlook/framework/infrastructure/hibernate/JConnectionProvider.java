package jlook.framework.infrastructure.hibernate;

import java.util.Properties;

import javax.sql.DataSource;

import jlook.framework.infrastructure.util.Log;
import org.hibernate.HibernateException;
import org.hibernate.connection.DatasourceConnectionProvider;

public class JConnectionProvider extends DatasourceConnectionProvider {
	private static Log logger = Log.getLog(JConnectionProvider.class);
	
	public static DataSource dataSource;
	
	public void configure(Properties props) throws HibernateException {
		if(logger.isTraceEnabled()) {
			props.list(System.out);
		}
		super.setDataSource(dataSource);
	}
}


