package jlook.framework.infrastructure.hibernate;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import jlook.framework.infrastructure.JClassLoader;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;

import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.Dialect;
import org.hibernate.tool.hbm2ddl.DatabaseMetadata;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.SQLExceptionTranslator;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SpringSessionContext;
import org.springframework.orm.hibernate3.SpringTransactionFactory;

public class JSessionFactoryBean implements FactoryBean<SessionFactory>,
		InitializingBean, DisposableBean, PersistenceExceptionTranslator {
	private static Log logger = Log.getLog(JSessionFactoryBean.class);
	
	private Class<? extends Configuration> configurationClass = Configuration.class;
	private DataSource dataSource;

	private String[] mappingResources;
	private Properties hibernateProperties;

	private boolean useTransactionAwareDataSource = false;
	private boolean exposeTransactionAwareSessionFactory = true;
	private SQLExceptionTranslator jdbcExceptionTranslator;

	private Configuration configuration;
	
	private JSessionFactory sessionFactory;
	private JConfigLoader jConfigLoader;
	private JClassLoader jClassLoader;
	
	public void setJConfigLoader(JConfigLoader jConfigLoader) {
		this.jConfigLoader = jConfigLoader;
	}
	
	public void setJClassLoader(JClassLoader jClassLoader) {
		this.jClassLoader = jClassLoader;
	}
	
	public JClassLoader getJClassLoader() {
		return this.jClassLoader;
	}
	
	public void setMappingResources(String[] mappingResources) {
		this.mappingResources = mappingResources;
	}
	
	public void setHibernateProperties(Properties hibernateProperties) {
		this.hibernateProperties = hibernateProperties;
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Return the DataSource to be used by the SessionFactory.
	 */
	public DataSource getDataSource() {
		return this.dataSource;
	}
	
	public void setUseTransactionAwareDataSource(boolean useTransactionAwareDataSource) {
		this.useTransactionAwareDataSource = useTransactionAwareDataSource;
	}

	/**
	 * Return whether to use a transaction-aware DataSource for the SessionFactory.
	 */
	protected boolean isUseTransactionAwareDataSource() {
		return this.useTransactionAwareDataSource;
	}
	
	public void setExposeTransactionAwareSessionFactory(boolean exposeTransactionAwareSessionFactory) {
		this.exposeTransactionAwareSessionFactory = exposeTransactionAwareSessionFactory;
	}

	/**
	 * Return whether to expose a transaction-aware proxy for the SessionFactory.
	 */
	protected boolean isExposeTransactionAwareSessionFactory() {
		return this.exposeTransactionAwareSessionFactory;
	}
	
	protected final SessionFactory getSessionFactory() {
		if (this.sessionFactory == null) {
			throw new IllegalStateException("SessionFactory not initialized yet");
		}
		return this.sessionFactory;
	}
	
	@Override
	public DataAccessException translateExceptionIfPossible(RuntimeException ex) {
		if (ex instanceof HibernateException) {
			return convertHibernateAccessException((HibernateException) ex);
		}
		return null;
	}

	@Override
	public void destroy() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.info("\t--> Closing Hibernate SessionFactory");
		}
		try {
			beforeSessionFactoryDestruction();
		}
		finally {
			this.sessionFactory.close();
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> afterPropertiesSet");
		}
		buildSessionFactory();
		afterSessionFactoryCreation();
	}

	@Override
	public SessionFactory getObject() throws Exception {
		return this.sessionFactory;
	}

	@Override
	public Class<? extends SessionFactory> getObjectType() {
		return (this.sessionFactory != null ? this.sessionFactory.getClass() : SessionFactory.class);
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	protected DataAccessException convertHibernateAccessException(HibernateException ex) {
		if (this.jdbcExceptionTranslator != null && ex instanceof JDBCException) {
			JDBCException jdbcEx = (JDBCException) ex;
			return this.jdbcExceptionTranslator.translate(
					"Hibernate operation: " + jdbcEx.getMessage(), jdbcEx.getSQL(), jdbcEx.getSQLException());
		}
		return SessionFactoryUtils.convertHibernateAccessException(ex);
	}
	
	protected void beforeSessionFactoryDestruction() {
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> beforeSessionFactoryDestruction");
		}
	}
	
	protected void afterSessionFactoryCreation() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("\t--> afterSessionFactoryCreation");
		}
	}
	
	public void buildSessionFactory() throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("It is building.");
		}
		// Create Configuration instance.
		Configuration config = BeanUtils.instantiateClass(this.configurationClass);
		
		//****************************************
		jConfigLoader.setConfiguration(config);
		jConfigLoader.doInit();
		//****************************************
		
		JConnectionProvider.dataSource=this.getDataSource();
		
		// Analogous to Hibernate EntityManager's Ejb3Configuration:
		// Hibernate doesn't allow setting the bean ClassLoader explicitly,
		// so we need to expose it as thread context ClassLoader accordingly.
		Thread currentThread = Thread.currentThread();
		ClassLoader threadContextClassLoader = currentThread.getContextClassLoader();
		currentThread.setContextClassLoader(this.jClassLoader);
	
		try {
			if (isExposeTransactionAwareSessionFactory()) {
				// Set Hibernate 3.1+ CurrentSessionContext implementation,
				// providing the Spring-managed Session as current Session.
				// Can be overridden by a custom value for the corresponding Hibernate property.
				config.setProperty(
						Environment.CURRENT_SESSION_CONTEXT_CLASS, SpringSessionContext.class.getName());
			}

			// Makes the Hibernate Session aware of the presence of a Spring-managed transaction.
			// Also sets connection release mode to ON_CLOSE by default.
			config.setProperty(Environment.TRANSACTION_STRATEGY, SpringTransactionFactory.class.getName());
			// Set Spring-provided DataSource as Hibernate ConnectionProvider.
			config.setProperty(Environment.CONNECTION_PROVIDER, JConnectionProvider.class.getName());
			

			if (this.hibernateProperties != null) {
				// Add given Hibernate properties to Configuration.
				config.addProperties(this.hibernateProperties);
			}
			
			if (this.mappingResources != null) {
				// Register given Hibernate mapping definitions, contained in resource files.
				for (String mapping : this.mappingResources) {
					Resource resource = new ClassPathResource(mapping.trim(), this.jClassLoader);
					config.addInputStream(resource.getInputStream());
				}
			}

			
			// Tell Hibernate to eagerly compile the mappings that we registered,
			// for availability of the mapping information in further processing.
			config.buildMappings();
			this.configuration = config;

			if(logger.isInfoEnabled()) {
				logger.info("Building new Hibernate SessionFactory");
			}
			
			// Build SessionFactory instance.
			SessionFactory sessionFactory = config.buildSessionFactory();
			if(this.sessionFactory!=null) {
				this.sessionFactory.setSessionFactory(sessionFactory);
			} else {
				this.sessionFactory = new JSessionFactory(sessionFactory);
			}
		} finally {
			// Reset original thread context ClassLoader.
			currentThread.setContextClassLoader(threadContextClassLoader);
		}
	}
	
	public void addConfigFile(File configFile) throws JException {
		Thread currentThread = Thread.currentThread();
		ClassLoader threadContextClassLoader = currentThread.getContextClassLoader();
		currentThread.setContextClassLoader(this.jClassLoader);
		
		try {
			this.configuration.addFile(configFile);
			SessionFactory sessionFactory = this.configuration.buildSessionFactory();
			if(this.sessionFactory!=null) {
				this.sessionFactory.setSessionFactory(sessionFactory);
			} else {
				this.sessionFactory = new JSessionFactory(sessionFactory);
			}
		} catch(Exception e) {
			throw new JException("Fail to add config file("+configFile+"). - "+e.getMessage(), e);
		} finally {
			// Reset original thread context ClassLoader.
			currentThread.setContextClassLoader(threadContextClassLoader);
		}
	}
	
	public void dropDatabaseSchema() throws DataAccessException {
		logger.info("Dropping database schema for Hibernate SessionFactory");
		HibernateTemplate hibernateTemplate = new HibernateTemplate(getSessionFactory());
		hibernateTemplate.execute(
			new HibernateCallback<Object>() {
				public Object doInHibernate(Session session) throws HibernateException, SQLException {
					Connection con = session.connection();
					Dialect dialect = Dialect.getDialect(configuration.getProperties());
					String[] sql = configuration.generateDropSchemaScript(dialect);
					executeSchemaScript(con, sql);
					return null;
				}
			}
		);
	}

	
	public void createDatabaseSchema() throws DataAccessException {
		logger.info("Creating database schema for Hibernate SessionFactory");
		DataSource dataSource = getDataSource();
		try {
			HibernateTemplate hibernateTemplate = new HibernateTemplate(getSessionFactory());
			hibernateTemplate.execute(
				new HibernateCallback<Object>() {
					public Object doInHibernate(Session session) throws HibernateException, SQLException {
						Connection con = session.connection();
						Dialect dialect = Dialect.getDialect(configuration.getProperties());
						String[] sql = configuration.generateSchemaCreationScript(dialect);
						executeSchemaScript(con, sql);
						return null;
					}
				}
			);
		}
		finally {
			
		}
	}
	
	protected void executeSchemaScript(Connection con, String[] sql) throws SQLException {
		if (sql != null && sql.length > 0) {
			boolean oldAutoCommit = con.getAutoCommit();
			if (!oldAutoCommit) {
				con.setAutoCommit(true);
			}
			try {
				Statement stmt = con.createStatement();
				try {
					for (String sqlStmt : sql) {
						executeSchemaStatement(stmt, sqlStmt);
					}
				}
				finally {
					JdbcUtils.closeStatement(stmt);
				}
			}
			finally {
				if (!oldAutoCommit) {
					con.setAutoCommit(false);
				}
			}
		}
	}

	/**
	 * Execute the given schema SQL on the given JDBC Statement.
	 * <p>Note that the default implementation will log unsuccessful statements
	 * and continue to execute. Override this method to treat failures differently.
	 * @param stmt the JDBC Statement to execute the SQL on
	 * @param sql the SQL statement to execute
	 * @throws SQLException if thrown by JDBC methods (and considered fatal)
	 */
	protected void executeSchemaStatement(Statement stmt, String sql) throws SQLException {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing schema statement: " + sql);
		}
		try {
			stmt.executeUpdate(sql);
		}
		catch (SQLException ex) {
			if (logger.isWarnEnabled()) {
				logger.warn("Unsuccessful schema statement: " + sql, ex);
			}
		}
	}

	public void validateDatabaseSchema() throws DataAccessException {
		logger.info("Validating database schema for Hibernate SessionFactory");
		DataSource dataSource = getDataSource();
		try {
			HibernateTemplate hibernateTemplate = new HibernateTemplate(getSessionFactory());
			hibernateTemplate.setFlushMode(HibernateTemplate.FLUSH_NEVER);
			hibernateTemplate.execute(
				new HibernateCallback<Object>() {
					public Object doInHibernate(Session session) throws HibernateException, SQLException {
						Connection con = session.connection();
						Dialect dialect = Dialect.getDialect(configuration.getProperties());
						DatabaseMetadata metadata = new DatabaseMetadata(con, dialect, false);
						configuration.validateSchema(dialect, metadata);
						return null;
					}
				}
			);
		}
		finally {
			
		}
	}
	
	public final Configuration getConfiguration() {
		if (this.configuration == null) {
			throw new IllegalStateException("Configuration not initialized yet");
		}
		return this.configuration;
	}
}
