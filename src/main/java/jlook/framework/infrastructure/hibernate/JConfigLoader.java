package jlook.framework.infrastructure.hibernate;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import jlook.framework.GlobalEnv;
import jlook.framework.GlobalKeys;
import jlook.framework.infrastructure.util.Log;

import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

@Component(JConfigLoader.ID)
public class JConfigLoader {
	private static Log logger = Log.getLog(JConfigLoader.class);
	
	public static final String ID = "jConfigLoader";
	public static final String DEFAULT_KEY = "default";
	
	private Configuration config;
	private String classPath;
	private Map<String, List<String>> map = new HashMap<String,List<String>>();
	
	public void setConfiguration(Configuration config) {
		this.config = config;
	}
	
	public void doInit() {
		String home = System.getProperty(GlobalEnv.APP_HOME);
		File classesPath = new File(home, GlobalKeys.CLASSES_PATH);
		this.classPath = classesPath.getAbsolutePath();
		File configPath = new File(home, GlobalKeys.CLASSES_PATH);
		loadDomainConfig(configPath);
		
//		loadDomainClass(classesPath);
//		
//		ClassLoader cloader = this.javaClassGenerator.getClassLoader();
//		for(List<String> classes : this.map.values()) {
//			for(String className : classes) {
//				Class<?> clazz;
//				try {
//					clazz = cloader.loadClass(className);
//					config.addClass(clazz);
//				} catch (ClassNotFoundException e) {
//					logger.error("Cannot load the class("+className+"). - "+e.getMessage());
//				}
//			}
//		}
	}
	
	public void loadDomainConfig(File configFile) {
		if(configFile.isFile() && configFile.getName().endsWith(".xml")) {
			String fileName = configFile.getAbsolutePath();
			fileName = fileName.substring(this.classPath.length()+1);
			if(logger.isDebugEnabled()) {
				logger.debug("\tfound xml config - "+fileName);
			}
			try {
				String xmlConfig = loadConfig(configFile);
				this.config.addXML(xmlConfig);
			} catch (IOException e) {
				logger.error("Fail to load xml config("+configFile+"). - "+e.getMessage());
			}
			return;
		}
		File[] configFiles = configFile.listFiles();
		if(configFiles==null) {
			return;
		}
		for(File file : configFiles) {
			loadDomainConfig(file);
		}
	}
	
	public String loadConfig(File configFile) throws IOException {
		FileInputStream fi = null;
		BufferedInputStream bis = null;
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			fi = new FileInputStream(configFile);
			bis = new BufferedInputStream(fi);
			byte[] buff = new byte[1024];
			while(true) {
				int len = bis.read(buff);
				if(len==-1) {
					break;
				}
				baos.write(buff, 0, len);
			}
			
			return new String(baos.toByteArray());
		} finally {
			try { if(bis!=null) 	bis.close();	} catch(Exception e) {}
			try { if(fi!=null)  	fi.close(); 	} catch(Exception e) {}
			try { if(baos!=null)  	baos.close(); 	} catch(Exception e) {}
		}
	}
	
	public void loadDomainClass(File child) {
		if(child.isFile() && child.getName().endsWith(".class")) {
			String fileName = child.getAbsolutePath();
			fileName = fileName.substring(this.classPath.length()+1, fileName.length()-".class".length());
			if(logger.isDebugEnabled()) {
				logger.debug("\t--> "+fileName);
			}
			
			int idx = fileName.indexOf("/");
			String key = null;
			if(idx==-1) {
				key = DEFAULT_KEY;
			} else {
				key = fileName.substring(0, idx);
			}
			List<String> list = map.get(key);
			if(list==null) {
				list = new ArrayList<String>();
				map.put(key, list);
			}
			list.add(fileName.replace("/", "."));
			return;
		}
		File[] children = child.listFiles();
		if(children==null) {
			return;
		}
		for(File f : children) {
			loadDomainClass(f);
		}
	}
	
	public void print() {
		if(logger.isDebugEnabled()) {
			logger.debug("####################################");
			for(String key : map.keySet()) {
				logger.debug(key+" ------------------>");
				List<String> list = map.get(key);
				for(String className : list) {
					logger.debug("\t"+className);
				}
			}
		}
	}
	public static void main(String[] args) {
		JConfigLoader loader = new JConfigLoader();
		loader.classPath = "/Users/hsjeon70/Applications/jlook/classes";
		File file = new File(loader.classPath);
		loader.loadDomainClass(file);
		loader.print();
	}
}
