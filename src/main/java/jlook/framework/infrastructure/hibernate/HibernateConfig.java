package jlook.framework.infrastructure.hibernate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import jlook.framework.GlobalEnv;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;

import org.hibernate.SessionFactory;

/**
 * Not used
 * @author hsjeon70
 *
 */
//@Component(HibernateConfig.ID)
public class HibernateConfig {
	private static Log logger = Log.getLog(HibernateConfig.class);
	
	public static final String ID = "hibernateConfig";
	
	private String configPath;
//	private Configuration config;
	private SessionFactory sessionFactory;
	
	protected static DataSource dataSource;
	
	@Resource(name="defaultDataSource")
	public void setDataSource(DataSource dataSource) {
		HibernateConfig.dataSource = dataSource;
	}
	
	@PostConstruct
	public void doInit() throws JException {
		this.configPath = "hibernate/hibernate-"+System.getProperty(GlobalEnv.APP_MODE)+".xml";
		if(logger.isDebugEnabled()) {
			logger.debug("It is starting. config file --> "+this.configPath);
		}
		
//		this.config = new Configuration().configure(this.configPath);
//		this.sessionFactory = this.config.buildSessionFactory();
		
		if(logger.isDebugEnabled()) {
			logger.debug("It is started...");
		}
	}
	
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}
}
