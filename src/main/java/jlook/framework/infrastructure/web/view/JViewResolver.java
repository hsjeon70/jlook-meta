package jlook.framework.infrastructure.web.view;

import java.util.List;
import java.util.Locale;

import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;


public class JViewResolver implements ViewResolver {
	private static Log logger = Log.getLog(JViewResolver.class);
	public static final String ID = "jViewResolver";
	
	private Class<?>[] jaxbModelList;
	
	public void setJaxbModelList(List<String> jaxbModelList) throws JException {
		this.jaxbModelList = new Class<?>[jaxbModelList.size()];
		for(int i=0;i<this.jaxbModelList.length;i++) {
			String className = jaxbModelList.get(i);
			try {
				this.jaxbModelList[i] = Class.forName(className);
			} catch (ClassNotFoundException e) {
				throw new JViewException("Cannot find class. - "+className, e);
			}
		}
	}
	
	@Override
	public View resolveViewName(String viewName, Locale locale) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> resolveViewName - "+viewName);
		}
		View view = null;
		if(JSonView.VIEWNAME.equals(viewName)) {
			view = new JSonView();
		} else if(JXmlView.VIEWNAME.equals(viewName)) {
			view = new JXmlView(this.jaxbModelList);
		} else if(JFileView.VIEWNAME.equals(viewName)) {
			view = new JFileView();
		} else if(JExcelView.VIEWNAME.equals(viewName)) {
			view = new JExcelView();
		} else if(JTextView.VIEWNAME.equals(viewName)) {
			view = new JTextView();
		}
		
		return view;
	}

}
