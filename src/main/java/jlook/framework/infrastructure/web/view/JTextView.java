package jlook.framework.infrastructure.web.view;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.gson.GsonFactory;
import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.view.AbstractView;

import com.google.gson.Gson;


public class JTextView extends AbstractView {
	private static Log logger = Log.getLog(JTextView.class);
	
	public static final String VIEWNAME = "JTEXTVIEW";
	public static final String CONTENT_TYPE = JContentType.TEXT_MIME_TYPE+";"+Constants.HTTP_CHARSET;
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> renderMergedOutputModel - "+model);
		}
		Object obj = model.get(JResultModel.DATA_KEY);
		
		JResultModel rmodel = null;
		if(obj instanceof JResultModel) {
			rmodel = (JResultModel)obj;
		} else {
			rmodel = new JResultModel();
			rmodel.setData(obj);
		}
		
		Object config = model.get(JResultModel.CONFIG_KEY);
		if(config instanceof Map) {
			rmodel.addConfig((Map<String,Object>)config);
		}
		if(rmodel.getResult()==null) {
			rmodel.setResult(JResultModel.SUCCESS);
		}
		
		GsonFactory gsonFactory = (GsonFactory)ApplicationContextHolder.getBean(GsonFactory.ID);
		Gson gson = gsonFactory.create();
		String jsonResult = gson.toJson(rmodel);
		
		response.setContentType(CONTENT_TYPE);
		PrintWriter out=null;
		try {
			out = response.getWriter();
			out.println(jsonResult);
			out.flush();
		} catch (IOException e) {
			throw new JViewException("Fail to write json string. - "+e.getMessage(), e);
		} finally {
			if(out!=null) out.close();
		}		
	}
	
	private void writeFileToClient(HttpServletResponse res, File file, String fileName) throws ServletException {
		if(fileName==null) {
			fileName = file.getName();
		}
		res.setContentType(CONTENT_TYPE);
		res.setContentLength((int)file.length());
		res.setHeader("Content-Disposition", "attachment;filename="+fileName+";");
		OutputStream out = null;
		BufferedOutputStream bo = null;
		FileInputStream fi = null;
		BufferedInputStream bi = null;
		try {
			fi = new FileInputStream(file);
			bi = new BufferedInputStream(fi);
			out = res.getOutputStream();
			bo = new BufferedOutputStream(out);
			byte[] buff = new byte[1024];
			while(true) {
				int len = bi.read(buff);
				if(len==-1) break;
				
				bo.write(buff,0,len);
			}
		} catch (Exception e) {
			throw new ServletException("Fail to read the file("+file+").",e);
		} finally {
			try { if(bi!=null) bi.close(); } catch(Exception e){}
			try { if(fi!=null) fi.close(); } catch(Exception e){}
			try { if(bo!=null) bo.close(); } catch(Exception e){}
			try { if(out!=null) out.close(); } catch(Exception e){}
		}
	}
}
