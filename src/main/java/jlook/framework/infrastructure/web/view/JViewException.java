package jlook.framework.infrastructure.web.view;

import jlook.framework.infrastructure.JException;

public class JViewException extends JException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String TEXT = "Fail to render view.";
	
	public JViewException(String message) {
		super(message);
	}
	public JViewException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JViewException(String message, Object[] parameters) {
		super(message, parameters);
	}
	
	public JViewException(String message, Throwable cause, Object[] parameters) {
		super(message, cause, parameters);
	}
	
	public JViewException(Throwable cause, Object[]  parameters) {
		super(cause,parameters);
	}
	
	public JViewException(String message, Throwable cause) {
		super(message, cause);
	}
}
