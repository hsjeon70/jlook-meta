package jlook.framework.infrastructure.web.view;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.util.JAttachmentDetail;
import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.view.AbstractView;


public class JFileView extends AbstractView {
	private static Log logger = Log.getLog(JFileView.class);
	
	public static final String VIEWNAME = "JFILEVIEW";
	public static final String CONTENT_TYPE = JContentType.FILE_MIME_TYPE+";"+Constants.HTTP_CHARSET;
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> renderMergedOutputModel - "+model);
		}
		Object obj = model.get(JResultModel.DATA_KEY);
		if(logger.isInfoEnabled()) {
			logger.info("\t--> "+obj);
		}
		
		File file = null;
		if(obj instanceof File) {
			file = (File)obj;
		} else {
			throw new JViewException("Not Supported Model Object. - "+obj);
		}
		String fileName = (String)model.get(Constants.FILE_NAME);
		try {
			writeFileToClient(response, file, fileName);
		} catch (ServletException e) {
			throw new JViewException("Fail to read file. - "+e.getMessage(), e);
		}
	}
	
	private void writeFileToClient(HttpServletResponse res, File file, String fileName) throws ServletException {
		if(fileName==null) {
			fileName = file.getName();
		}
		res.setContentType(CONTENT_TYPE);
		res.setContentLength((int)file.length());
		res.setHeader("Content-Disposition", "attachment;filename="+fileName+";");
		OutputStream out = null;
		BufferedOutputStream bo = null;
		FileInputStream fi = null;
		BufferedInputStream bi = null;
		try {
			fi = new FileInputStream(file);
			bi = new BufferedInputStream(fi);
			out = res.getOutputStream();
			bo = new BufferedOutputStream(out);
			byte[] buff = new byte[1024];
			while(true) {
				int len = bi.read(buff);
				if(len==-1) break;
				
				bo.write(buff,0,len);
			}
		} catch (Exception e) {
			throw new ServletException("Fail to read the file("+file+").",e);
		} finally {
			try { if(bi!=null) bi.close(); } catch(Exception e){}
			try { if(fi!=null) fi.close(); } catch(Exception e){}
			try { if(bo!=null) bo.close(); } catch(Exception e){}
			try { if(out!=null) out.close(); } catch(Exception e){}
		}
	}
}
