package jlook.framework.infrastructure.web.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.model.ColumnModel;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.domain.model.JSummaryModel;
import jlook.framework.domain.model.TitleModel;
import jlook.framework.domain.util.JArray;
import jlook.framework.domain.util.JAttachment;
import jlook.framework.domain.util.JAttachmentDetail;

import org.springframework.web.servlet.view.AbstractView;

public class JExcelView extends AbstractView {
	public static final String VIEWNAME = "EXCELVIEW";
	public static final String CONTENT_TYPE = JContentType.EXCEL_MIME_TYPE+";"+Constants.HTTP_CHARSET;
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> renderMergedOutputModel - "+model);
		}
		Object obj = model.get(JResultModel.DATA_KEY);
		JSummaryModel smodel = null;
		if(obj instanceof JResultModel) {
			smodel = (JSummaryModel)obj;
		} else {
			throw new JViewException("Cannot render excel view because of invalid model. It supports only summary view for ExcelView - "+obj);
		}
		
		List<ColumnModel> columns = smodel.getColumnModel();
		List<?> excelData = null;
		Object data = smodel.getData();
		if(data instanceof List) {
			excelData = (List<?>)data;
		}
		
		String fileName = (String)smodel.getConfig(JClass.A_NAME)+"_excel.xls";
		response.setContentType(CONTENT_TYPE);
		response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");
		PrintWriter out=null;
		try {
			out = response.getWriter();
			out.println("<table border='1'>");
			out.println("<tr>");
			for(ColumnModel column : columns) {
				out.print("<th  bgcolor='gray'>");
				out.print(column.getName());
				out.print("</th>");
			}
			out.println("</tr>");
			for(Object rowData : excelData) {
				List<?> row = null;
				if(rowData instanceof List<?>) {
					row = (List<?>)rowData;
				} else {
					throw new JViewException("Invalid excel data. - "+row);
				}
				out.print("<tr>");
				for(Object value : row) {
					out.print("<td>");
					if(value instanceof TitleModel) {
						TitleModel tmodel = (TitleModel)value;
						out.print(tmodel!=null ? tmodel.getValue() : "");
					} else if(value instanceof JArray) {
						JArray jArray = (JArray)value;
						out.print(jArray!=null ? jArray.getValues() : "" );
					} else if(value instanceof JAttachment) {
						JAttachment jattach = (JAttachment)value;
						if(jattach!=null) {
							Set<JAttachmentDetail> details = jattach.getJAttachmentDetails();
							for(JAttachmentDetail detail : details) {
								out.print("<a href'"+request.getContextPath()+"/service/file/download?objectId="+detail.getObjectId()+"'>"+detail.getName()+"</a><br/>");
							}
						} 
					} else {
						out.print(value);
					}
					out.print("</td>");
				}
				out.println("</tr>");
			}
			
			out.println("</table>");
			out.flush();
		} catch (IOException e) {
			throw new JViewException("Fail to write excel view. - "+e.getMessage(), e);
		} finally {
			if(out!=null) out.close();
		}
	}

}
