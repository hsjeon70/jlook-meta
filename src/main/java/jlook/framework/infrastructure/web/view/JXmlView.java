package jlook.framework.infrastructure.web.view;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.util.Log;

import org.springframework.web.servlet.view.AbstractView;



public class JXmlView extends AbstractView {
	private static Log logger = Log.getLog(JXmlView.class);
	
	public static final String VIEWNAME = "JXMLVIEW";
	public static final String CONTENT_TYPE = JContentType.XML_MIME_TYPE+";"+Constants.HTTP_CHARSET;
	
	private JAXBContext context;
	
	public JXmlView(Class<?>[] jaxbModelList) throws JException {
		 try {
			context = JAXBContext.newInstance(jaxbModelList);
		} catch (JAXBException e) {
			throw new JViewException("Fail to create JAXBContext. - "+e.getMessage(),e);
		}
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> renderMergedOutputModel - "+model);
		}
		Object obj = model.get(JResultModel.DATA_KEY);
		if(logger.isInfoEnabled()) {
			logger.info("\t--> "+obj);
		}
		JResultModel rmodel = null;
		if(obj instanceof JResultModel) {
			rmodel = (JResultModel)obj;
		} else {
			rmodel = new JResultModel();
			rmodel.setData(obj);
		}
		rmodel.addConfig("aaa","bbb");
		try {
			response.setContentType(CONTENT_TYPE);
			PrintWriter out = response.getWriter();
			Marshaller ms = context.createMarshaller();
			ms.marshal(rmodel, out);
			out.close();
		} catch(Exception e) {
			throw new JViewException("Fail to render reponse by xml view.", e);
		}
	}
}
