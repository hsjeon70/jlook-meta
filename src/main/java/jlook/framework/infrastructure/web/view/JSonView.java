package jlook.framework.infrastructure.web.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.Constants;
import jlook.framework.JContentType;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.gson.GsonFactory;
import jlook.framework.infrastructure.util.Log;


import org.springframework.web.servlet.view.AbstractView;

import com.google.gson.Gson;

public class JSonView extends AbstractView {
	private static Log logger = Log.getLog(JSonView.class);
	
	public static final String VIEWNAME = "JSONVIEW";
	public static final String CONTENT_TYPE = JContentType.JSON_MIME_TYPE+";"+Constants.HTTP_CHARSET;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("--> renderMergedOutputModel - "+model);
		}
		Object obj = model.get(JResultModel.DATA_KEY);
		
		JResultModel rmodel = null;
		if(obj instanceof JResultModel) {
			rmodel = (JResultModel)obj;
		} else {
			rmodel = new JResultModel();
			rmodel.setData(obj);
		}
		
		Object config = model.get(JResultModel.CONFIG_KEY);
		if(config instanceof Map) {
			rmodel.addConfig((Map<String,Object>)config);
		}
		if(rmodel.getResult()==null) {
			rmodel.setResult(JResultModel.SUCCESS);
		}
		
		GsonFactory gsonFactory = (GsonFactory)ApplicationContextHolder.getBean(GsonFactory.ID);
		Gson gson = gsonFactory.create();
		String jsonResult = gson.toJson(rmodel);
		
		response.setContentType(CONTENT_TYPE);
		PrintWriter out=null;
		try {
			out = response.getWriter();
			out.println(jsonResult);
			out.flush();
		} catch (IOException e) {
			throw new JViewException("Fail to write json string. - "+e.getMessage(), e);
		} finally {
			if(out!=null) out.close();
		}		
	}
}
