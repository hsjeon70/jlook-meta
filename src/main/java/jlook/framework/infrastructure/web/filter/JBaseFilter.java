package jlook.framework.infrastructure.web.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.domain.JErrorInfo;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.gson.GsonFactory;
import jlook.framework.infrastructure.util.Log;

import com.google.gson.Gson;

public abstract class JBaseFilter implements Filter {
	private static Log logger = Log.getLog(JBaseFilter.class);
	
	protected FilterConfig config;
	protected JContextFactory jContextFactory;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
		this.jContextFactory = JContextFactory.newInstance();
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	protected void sendJsonErrorReponse(JErrorInfo info, HttpServletResponse response) 
	throws IOException {
		if(logger.isErrorEnabled()) {
			logger.error("\t## --> "+info);
		}
		
		JResultModel rmode = new JResultModel();
		rmode.setResult(JResultModel.ERROR);
		rmode.setMessage(info.getMessage());
		rmode.setData(info.getReason());
		
		GsonFactory gsonFactory = (GsonFactory)ApplicationContextHolder.getBean(GsonFactory.ID);
		Gson gson = gsonFactory.create();
		response.setContentType("application/json;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println(gson.toJson(rmode));
	}
	
}
