package jlook.framework.infrastructure.web.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jlook.framework.ClientType;
import jlook.framework.Constants;
import jlook.framework.GlobalKeys;
import jlook.framework.domain.JErrorInfo;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.util.Log;

public class JServiceFilter extends JBaseFilter {
	private static Log logger = Log.getLog(JServiceFilter.class);
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		HttpSession session = request.getSession();
		
		String requestUri = request.getRequestURI();
		if(logger.isDebugEnabled()) {
			logger.debug("@ ####### "+request.getMethod()+": "+requestUri+"-"+session.getId());
		}
		
		String openApiUril = request.getContextPath()+Constants.OPENAPI_URI_PREFIX;
		if(requestUri.startsWith(openApiUril)) {
			String toUri = requestUri.substring(openApiUril.length());
			String prefixUri = "/service"+Constants.OPENAPI_URI_PREFIX;
			RequestDispatcher dispatcher = request.getRequestDispatcher(prefixUri+toUri);
			
			if(logger.isDebugEnabled()) {
				logger.debug("\tJOPEN-API ------->"+prefixUri+toUri);
			}
			
			dispatcher.forward(request, response);
			return;
		}
		
		JContext jContext = this.jContextFactory.getJContext();
		String clientType = null;
		try {
			clientType = jContext.getClientType();
			if(logger.isDebugEnabled()) {
				logger.debug("\t--> clientType="+clientType);
			}
			chain.doFilter(request, response);
		} catch(Exception th) {
			JErrorInfo info = new JErrorInfo(th);
			if(!ClientType.Web.name().equals(clientType)) {
				super.sendJsonErrorReponse(info, response);
			} else {
				request.setAttribute(GlobalKeys.ERROR_INFO,info);
				throw new ServletException(request.getRequestURI()+" - "+th.getMessage(), th);
			}
		} 
	}
}
