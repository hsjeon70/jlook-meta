package jlook.framework.infrastructure.web.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jlook.framework.AppMode;
import jlook.framework.ClientType;
import jlook.framework.GlobalEnv;
import jlook.framework.GlobalKeys;
import jlook.framework.JHeader;
import jlook.framework.domain.JErrorInfo;
import jlook.framework.domain.JObject;
import jlook.framework.domain.account.JDomain;
import jlook.framework.domain.management.JAccessLog;
import jlook.framework.domain.model.JResultModel;
import jlook.framework.infrastructure.ApplicationContextHolder;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.service.JAccessLogService;
import jlook.framework.service.JSecurityService;
import jlook.framework.service.JServiceException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;


public class JGlobalFilter extends JBaseFilter {
	private static Log logger = Log.getLog(JGlobalFilter.class);
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		long startTime = System.currentTimeMillis();
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		HttpSession session = request.getSession();
		
		String requestUri = request.getRequestURI();
		if(logger.isDebugEnabled()) {
			logger.debug("@ ####### "+request.getMethod()+": "+requestUri);
			@SuppressWarnings("unchecked")
			Enumeration<String> names = request.getHeaderNames();
			while(names.hasMoreElements()) {
				String name = names.nextElement();
				logger.debug("\t--> "+name+"="+request.getHeader(name));
			}
		}
		String domainId = null;
		JErrorInfo einfo = null;
		ClientType clientType = null;
		JContext jContext = this.jContextFactory.getJContext();
		try {
			try {
				clientType = ClientType.valueOf(request.getHeader(JHeader.CLIENT.getKey()));
			} catch(Exception e) {
				if(logger.isInfoEnabled()) {
					logger.info("Cannot find '"+JHeader.CLIENT.getKey()+"' at http header. --> SET - "+ClientType.Web);
				}
				clientType = ClientType.Web;
			}
			jContext.putClientType(clientType.name());
			
			domainId = handleDomainId(jContext, request, clientType);
			
			JUserEntity entity = null;
			SecurityContext securityCtx = (SecurityContext)session.getAttribute(JSecurityService.SESSION_KEY);
			if(securityCtx!=null) {
				Authentication auth = securityCtx.getAuthentication();
				if(auth!=null) {
					entity = (JUserEntity)auth.getPrincipal();
				}
			}
			if(logger.isDebugEnabled()) {
				logger.debug("\t--> User - "+entity);
			}
			if(entity!=null) {
				jContext.putUserEntity(entity);
			} else {
				if(logger.isDebugEnabled()) {
					logger.debug("JUserEntity is null.");
				}
			}
			
			chain.doFilter(request, response);
			if(requestUri.startsWith(request.getContextPath()+"/service")) {
				doPostHandle(domainId, clientType, request, response);
			}
		} catch(Throwable th) {
			einfo = new JErrorInfo(th);
			if(clientType.equals(ClientType.Web)) {
				request.setAttribute(GlobalKeys.ERROR_INFO,einfo);
				throw new ServletException(th);
			} else {
				super.sendJsonErrorReponse(einfo, response);
			}
		} finally {
			long endTime = System.currentTimeMillis();
			double elapseTime = (endTime-startTime)/1000.;
			if(logger.isInfoEnabled()) {
				logger.info("@ ------> "+request.getMethod()+": "+requestUri+" - "+elapseTime+" sec.");
			}
			
			handleAccessLog(request, domainId, clientType, einfo, elapseTime);
			// ----------------------------------------------
			if(jContext!=null) jContext.removeAll();
		}
	}
	
	private void handleAccessLog(HttpServletRequest request, String domainId, ClientType clientType, JErrorInfo einfo, double elapseTime) {
		JAccessLog log = new JAccessLog();
		try {
			log.setDomainId(Long.parseLong(domainId));
		} catch(Exception e) {
			log.setDomainId(0L);
		}
		log.setRequestUri(request.getRequestURI());
		log.setClientType(clientType.name());
		log.setElapseTime(Double.toString(elapseTime));
		if(einfo!=null) {
			log.setResult(JResultModel.ERROR);
			log.setMessage(einfo.toString());
		} else {
			log.setResult(JResultModel.SUCCESS);
		}
		log.setReferer(request.getHeader("Referer"));
		log.setUserAgent("User-Agent");
		JAccessLogService jAccessLogService = 
					(JAccessLogService)ApplicationContextHolder.getBean(JAccessLogService.ID);
		try {
			jAccessLogService.create(log);
		} catch (JServiceException e) {
			logger.error("Fail to write JAccessLog - "+e.getMessage(),e);
		}
	}

	private String handleDomainId(JContext jContext, HttpServletRequest request, ClientType clientType) throws ServletException {
		String domainId = request.getHeader(JHeader.DID.getKey());
		if(domainId == null && !clientType.equals(ClientType.Web)) {
			throw new ServletException("Cannot find '"+JHeader.DID+"' at http header.");
		}
		
		// web client
		if(domainId == null) {
			logger.warn("Cannot find '"+JHeader.DID.getKey()+"' at http header.");
			// if client is signed in.
			HttpSession session = request.getSession();
			JDomain domain = (JDomain)session.getAttribute(GlobalKeys.DOMAIN_KEY);
			if(domain!=null) {
				domainId = domain.getObjectId().toString();
			} else {	// 	if client is not signed in.
				domainId = request.getParameter(JObject.A_DID);
				if(domainId==null || domainId.equals("")) {
					if(logger.isInfoEnabled()) {
						logger.info("Cannot find '"+JObject.A_DID+"' at http request.");
					}
					return null;
				} else {
					Long did = null;
					try {
						did = Long.parseLong(domainId);
						jContext.putDomainId(did);
					} catch(Exception e) {
						logger.warn("Invalid domainId. - "+domainId);
						return null;
					}
				}
			}
		}
		
		Long did = null;
		try {
			did = Long.parseLong(domainId);
			jContext.putDomainId(did);
			return domainId;
		} catch(Exception e) {
			throw new ServletException("Invalid domainId - "+domainId);
		}
	}
	
	private void doPostHandle(String domainId, ClientType clientType, HttpServletRequest request, HttpServletResponse response) 
	throws JException {
		switch(clientType) {
			case Android : 
			case iPhone  :
				break;
			case Web	 :
				break;
			default		 :
		}
	}
}
