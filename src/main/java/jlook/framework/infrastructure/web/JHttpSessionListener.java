package jlook.framework.infrastructure.web;


import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import jlook.framework.infrastructure.util.Log;

public class JHttpSessionListener implements HttpSessionBindingListener {
	private static Log logger = Log.getLog(JHttpSessionListener.class);
			
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		HttpSession session = event.getSession();
		if(logger.isInfoEnabled()) {
			logger.info("\t@--> bound ("+session.getId()+") - "+event.getSource());
		}
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		HttpSession session = event.getSession();
		if(logger.isInfoEnabled()) {
			logger.info("\t@--> unbound ("+session.getId()+") - "+event.getSource());
		}
	}
}
