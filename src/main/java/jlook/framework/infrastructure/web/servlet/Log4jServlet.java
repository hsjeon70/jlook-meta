package jlook.framework.infrastructure.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerRepository;

/**
 * Servlet implementation class Log4jServlet
 * author : hsjeon(hong.jeon@samsugn.com)
 */
public class Log4jServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@SuppressWarnings("unused")
	private static Log logger = LogFactory.getLog(Log4jServlet.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Log4jServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] category = request.getParameterValues("category");
		String[] level 	= request.getParameterValues("level");
		
		Logger root = Logger.getRootLogger();
		LoggerRepository repository = root.getLoggerRepository();
		
		if(category!=null) {
			for(int i=0;i<category.length;i++) {
				Logger log = repository.getLogger(category[i]);
				if(level[i].equals("N/A")) {
					log.setLevel(null);
					continue;
				}
				if(log!=null) {
					log.setLevel(Level.toLevel(level[i].toUpperCase()));
				}
			}
		}
		
		Enumeration<?> en = repository.getCurrentLoggers();
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println("<form name='frm' action='"+request.getContextPath()+"/admin?cmd=log4j' method='post'>");
		out.println("<table border='0' width='1000'>");
		out.println("<tr><td>");
		out.println("<b>Log4j Configuration</b>");
		out.println("</td>");
		out.println("<td align='right'>");
		out.println("<input type='submit' value='save'></input>");
		out.println("</td></tr>");
		out.println("</table>");
		out.println("<table border='1' width='1000'>");
		out.println("<tr><th>Level</th><th>Logger</th></tr>");
		while(en.hasMoreElements()) {
			Logger l = (Logger)en.nextElement();
			out.println("<tr>");
			out.println("<td align='center'><select name='level'>");
			out.println("<option value='N/A' "+((l.getLevel()==null)? "selected" : "")+"></option>");
			out.println("<option value='"+Level.FATAL+"' "+(Level.FATAL.equals(l.getLevel())? "selected" : "")+">"+Level.FATAL+"</option>");
			out.println("<option value='"+Level.ERROR+"' "+(Level.ERROR.equals(l.getLevel())? "selected" : "")+">"+Level.ERROR+"</option>");
			out.println("<option value='"+Level.WARN+"' "+(Level.WARN.equals(l.getLevel())? "selected" : "")+">"+Level.WARN+"</option>");
			out.println("<option value='"+Level.INFO+"' "+(Level.INFO.equals(l.getLevel())? "selected" : "")+">"+Level.INFO+"</option>");
			out.println("<option value='"+Level.DEBUG+"' "+(Level.DEBUG.equals(l.getLevel())? "selected" : "")+">"+Level.DEBUG+"</option>");
			out.println("<option value='"+Level.TRACE+"' "+(Level.TRACE.equals(l.getLevel())? "selected" : "")+">"+Level.TRACE+"</option>");
			out.println("</select></td>");
			out.println("<td><input type='hidden' name='category' value='"+l.getName()+"'></input>");
			out.println(l.getName());
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		out.println("</form>");
	}

}
