package jlook.framework.infrastructure.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ThreadDumpServlet
 */
public class ThreadDumpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadDumpServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		
		PrintWriter pw = response.getWriter();
		pw.println(dump(""));
	}
	
	@SuppressWarnings("deprecation")
	public void kill(String threadName) {     
        ThreadGroup it = Thread.currentThread().getThreadGroup();
        Thread[] threads = new Thread[it.activeCount()] ;
        it.enumerate(threads);
    	for (int i=0; i<threads.length; i++) {        
    		if (threads[i].getName().equals(threadName)) {
    			threads[i].interrupt();
    			threads[i].stop();
    			System.out.println("kill thread - " + threadName);
    			return;
    		}
    	}
    }

	public String dump(String filter) {
    	Hashtable<String,Thread> hash = new Hashtable<String,Thread>();
        StringBuffer sb = new StringBuffer();        
        ThreadGroup it = Thread.currentThread().getThreadGroup();

        //sb.append("[group] " + it.getName() + "<p><p><p>");

        Thread[] threads = new Thread[it.activeCount()] ;
        //sb.append("thread count : " + it.activeCount() + "<p>");
        it.enumerate(threads);
        sb.append("<table border='0' align='left'>");
        
    	String[] threadNames = new String[threads.length];
    	for (int i=0; i<threadNames.length; i++) {
    		threadNames[i] = threads[i].getName() + " - " + threads[i].getState();
    		hash.put(threadNames[i], threads[i]);
    	}
    	
    	Arrays.sort(threadNames);
        
    	int filterThreadCount = 0;
        for(int i=0;i<threadNames.length;i++) {    	
        	if (threadNames[i].toUpperCase().indexOf(filter.toUpperCase()) == -1) {
        		continue;
        	}
        	
        	Thread thread = (Thread) hash.get(threadNames[i]);
            if(thread != null) {
                sb.append("<tr>");
                //sb.append(getStackTrace(threads[i]));
                sb.append(getStackTrace(thread));
                sb.append("</tr>");
            }
            filterThreadCount++;
        }
        sb.append("</table>");
        
        StringBuffer sbb = new StringBuffer();
        sbb.append("<table><tr><td>thread count : " + filterThreadCount + " / " + it.activeCount() + "</td></tr>");
        sbb.append("<tr><td>");
        sbb.append(sb);
        sbb.append("</td></tr></table>");
    	return sbb.toString();
    }
    
    public String getStackTrace(Thread thread) {
        StringBuffer sb = new StringBuffer();
        sb.append("@ \"<b>" + thread.getName() + "</b>\" ");
        sb.append("prio=" + thread.getPriority() + " ");
        sb.append(thread.isDaemon() ? "daemon " : " ");
        
        StackTraceElement stack[] = thread.getStackTrace();

        if (stack==null || stack.length<1)
	        return sb.toString();

        if (stack[0].getMethodName().equals("wait"))
            sb.append("<b>wait</b> ");
        else if (stack[0].getMethodName().equals("sleep"))
            sb.append("<b>sleep</b> ");
        else
            sb.append("<b><font color='red'>running or blocked</font></b> ");
        
        //sb.append("  (<a href='/thread?kill=" + thread.getName() + "'>kill</a>)");
        sb.append("\n");

        for (int i=0; i<stack.length; i++) {
            String filename = stack[i].getFileName();
            if (filename == null) {
                // The source filename is not available
            }
            String className = stack[i].getClassName();
            String methodName = stack[i].getMethodName();
            boolean isNativeMethod = stack[i].isNativeMethod();
            int line = stack[i].getLineNumber();

            if (className.startsWith("java") 
            	|| className.startsWith("org.") 
            	|| className.startsWith("sun.") 
            	|| className.startsWith("com.sun.") 
            	|| className.startsWith("edu.emory."))
                sb.append("      <font color='gray'>at ");
            else
                sb.append("      at ");
            sb.append(className + "." + methodName);
            sb.append("(" + filename + ":" + (isNativeMethod ? "native" : String.valueOf(line)) + ")");
            if (className.startsWith("java") 
            	|| className.startsWith("org.") 
            	|| className.startsWith("sun.") 
            	|| className.startsWith("com.sun.") 
            	|| className.startsWith("edu.emory."))
                sb.append("</font>");
            sb.append("\n");
        }
        
        if (stack[0].getMethodName().equals("wait"))
            return "<td><pre>" + sb.toString() + "</pre></td>";
        else if (stack[0].getMethodName().equals("sleep"))
            return "<td bgcolor=''><pre>" + sb.toString() + "</pre></td>";
        else
            return "<td bgcolor=''><pre>" + sb.toString() + "</pre></td>";
    }	
}
