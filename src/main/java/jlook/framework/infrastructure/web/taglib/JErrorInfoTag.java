package jlook.framework.infrastructure.web.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import jlook.framework.domain.JErrorInfo;
import jlook.framework.infrastructure.util.Log;

public class JErrorInfoTag extends BodyTagSupport {
	private static Log logger = Log.getLog(JErrorInfoTag.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public int doStartTag() throws JspException {
		Exception ex = pageContext.getException();
		
		if(logger.isDebugEnabled()) {
			logger.debug("--> "+ex);
		}
		
		JErrorInfo info = new JErrorInfo(ex);
		pageContext.setAttribute(id, info);
	    return super.doStartTag();
	}
}
