package jlook.framework.infrastructure.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.ClientType;
import jlook.framework.GlobalKeys;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.util.Log;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	private static Log logger = Log.getLog(AuthenticationFailureHandler.class);
	
	@Override 
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) 
    throws IOException, ServletException { 
		JContextFactory jContextFactory = JContextFactory.newInstance();
		JContext context = jContextFactory.getJContext();
		
		String clientType = context.getClientType();;
		
		if(logger.isErrorEnabled()) {
			logger.error(clientType+"--> Fail to signIn - "+exception.getMessage(),exception);
		}
		
		request.setAttribute(GlobalKeys.ERROR_TEXT, "Fail to signIn - "+exception.getMessage()); 
		request.setAttribute(GlobalKeys.ERROR_CAUSE, exception);
		
        if(clientType==null || ClientType.Web.name().equals(clientType)) {
	        this.setDefaultFailureUrl(GlobalKeys.SIGNIN_WEBPAGE);
		} else {
			this.setDefaultFailureUrl(GlobalKeys.SIGNIN_MOBILEPAGE);
		}
        
        super.onAuthenticationFailure(request, response, exception);
    }  
}
