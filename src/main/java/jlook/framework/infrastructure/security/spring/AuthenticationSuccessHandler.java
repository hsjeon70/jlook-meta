package jlook.framework.infrastructure.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jlook.framework.ClientType;
import jlook.framework.GlobalKeys;
import jlook.framework.JHeader;
import jlook.framework.domain.JObject;
import jlook.framework.infrastructure.JException;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;


public class AuthenticationSuccessHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {
	private static Log logger = Log.getLog(AuthenticationSuccessHandler.class);
	
	protected JContextFactory jContextFactory = JContextFactory.newInstance();
	
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		JUserEntity user = (JUserEntity) authentication.getPrincipal();
		WebAuthenticationDetails authenticationDetails = (WebAuthenticationDetails) authentication.getDetails();
		
		Long domainId = null;
		try {
			domainId = getDomainId(request);
		} catch (JException e) {
			throw new ServletException(e);
		}
		
		HttpSession session = request.getSession();
		session.setAttribute(GlobalKeys.DID_KEY, domainId);
		if (user != null && authenticationDetails != null) {
			if(logger.isInfoEnabled()) {
				logger.info("Authentication is succeeded. -----> ("+domainId+") - "+user);
			}
		}

		super.onAuthenticationSuccess(request, response, authentication);
	}
	
	private Long getDomainId(HttpServletRequest request) throws JException {
		JContext jContext = this.jContextFactory.getJContext();
		String clientType = jContext.getClientType();
		String domainId = null;
		if(clientType!=null && clientType.equals(ClientType.Web.name())) {
			domainId = request.getParameter(JObject.A_DID);
		} else {
			domainId = request.getHeader(JHeader.DID.getKey());
		}
		
		Long did = null;
		try {
			 did = Long.parseLong(domainId);
		} catch(Exception e) {
			throw new JException("Invalid domainId - "+domainId);
		}
		
		return did;
	}
}
