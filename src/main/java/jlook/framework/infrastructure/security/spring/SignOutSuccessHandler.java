package jlook.framework.infrastructure.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jlook.framework.infrastructure.util.Log;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

public class SignOutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
	private static Log logger = Log.getLog(SignOutSuccessHandler.class);
	
	@Override
    public void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) 
    throws IOException, ServletException {
		if(logger.isDebugEnabled()) {
			logger.debug("@@ --> Logout Success Handler.");
		}
     	super.handle(request, response, authentication);
    }
}
