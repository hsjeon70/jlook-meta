package jlook.framework.infrastructure.security.spring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jlook.framework.domain.JObject;
import jlook.framework.domain.metadata.JAccessRule;
import jlook.framework.domain.metadata.JClass;
import jlook.framework.domain.security.JDataAccessRule;
import jlook.framework.infrastructure.loader.metadata.JDomainDef;
import jlook.framework.infrastructure.security.JClassPermission;
import jlook.framework.infrastructure.security.JUserEntity;
import jlook.framework.infrastructure.util.Log;
import jlook.framework.repository.JClassRepository;
import jlook.framework.repository.JDataAccessRuleRepository;
import jlook.framework.repository.JRepositoryException;
import jlook.framework.service.JSecurityException;
import jlook.framework.service.JSecurityService;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;


@Component(JPermissionEvaluator.ID)
public class JPermissionEvaluator implements PermissionEvaluator {
	private static Log logger = Log.getLog(JPermissionEvaluator.class);
	
	public static final String ID ="jPermissionEvaluator";
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Resource(name=JClassRepository.ID)
	private JClassRepository jClassRepository;
	
	@Resource(name=JDataAccessRuleRepository.ID)
	private JDataAccessRuleRepository jDataAccessRuleRepository;
	
	/**
	 * targetDomainObject : classId
	 * permission : read, write, update, delete, or new Object[]{read, write, update, delete}
	 */
	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		if(logger.isDebugEnabled()) {
			StringBuffer log = new StringBuffer();
			log.append("################# has Permission #################").append("\n");
			log.append("\t").append("Authentication=").append(authentication).append("\n");
			log.append("\t").append("targetDomainObject=").append(targetDomainObject).append("\n");
			log.append("\t").append("permission=").append(permission).append("\n");
			logger.debug(log.toString());
		}
		
		Long classId = null;
		if(targetDomainObject instanceof Long) {
			classId = (Long)targetDomainObject;
		} else if(targetDomainObject instanceof JObject) {
			JObject targetObj = (JObject)targetDomainObject;
			classId = targetObj.getClassId();
		} else {
			logger.error("Invalid target domain object(classId). - "+targetDomainObject);
			return false;
		}
		
		List<JClassPermission> actions = new ArrayList<JClassPermission>();
		if(permission instanceof String) {
			try {
				actions.add(JClassPermission.valueOf((String)permission));
			} catch(Exception e) {
				logger.error("Invalid permission value. - "+permission, e);
				return false;
			}
		} else if(permission instanceof List){
			List<?> perms = (List<?>)permission;
			for(Object obj : perms) {
				if(obj instanceof String) {
					try {
						actions.add(JClassPermission.valueOf((String)obj));
					} catch(Exception e) {
						logger.error("Invalid permission value. - "+permission, e);
						return false;
					}
				} else {
					logger.error("Invalid permission object. - "+permission);
					return false;
				}
			}
		}
		
		JUserEntity entity = (JUserEntity)authentication.getPrincipal();
		List<String> roleList = entity.getRoleList();
		Long signedDomainId = null;
		try {
			signedDomainId = security.getDomainId();
		} catch (JSecurityException e) {
			logger.error("Fail to get domainId from security context.",e);
			return false;
		}
		JClass targetJClass = null;
		try {
			targetJClass = this.jClassRepository.select(classId);
			if(targetJClass==null) {
				throw new RuntimeException("Cannot find JClass. classId="+classId+", permission="+permission+", targetDomainObject="+targetDomainObject);
			}
		} catch (JRepositoryException e) {
			logger.error("Fail to get JClass from metadata. - "+classId,e);
			return false;
		}
		
		// check permission : system or owner domain
		if(!targetJClass.getDomainId().equals(new Long(JDomainDef.SYSTEM_ID)) && 
				!signedDomainId.equals(targetJClass.getDomainId())) {
			logger.error("\t##NO PERMISSION : You cannot access to other domains."+entity
							+", signedDomainId="+signedDomainId+" targetClassId="+classId);
			return false;
		}
		
		List<JDataAccessRule> rules = null;
		try {
			rules = this.jDataAccessRuleRepository.select(targetJClass, roleList);
		} catch(JRepositoryException e) {
			logger.error("fail to get JDataAccessRules. classId="+classId+", "+entity, e);
			return false;
		}
		// if user don't have any rule, user can read and write it. and user update and delete his.
		
		JAccessRule jAccessRule = null;
		try {
			jAccessRule = JAccessRule.valueOf(targetJClass.getAccessRule());
		} catch(Exception e) {
			jAccessRule = JAccessRule.PUBLIC;
		}
		
		if(JAccessRule.PUBLIC.equals(jAccessRule) && (rules==null || rules.size()==0)) {
			return true;
		}
		
		if(JAccessRule.PROTECTED.equals(jAccessRule) && (rules==null || rules.size()==0)) {
			return false;
		}
		
		if(JAccessRule.PRIVATE.equals(jAccessRule)) {
			return true;
		}
		
		for(JClassPermission action : actions) {
			try {
				boolean hasPermission = false;
				switch(action) {
					case read 	: hasPermission = hasReadPermission(entity, targetJClass, rules);
						break;
					case create : hasPermission = hasCreatePermission(entity, targetJClass, rules);
						break;
					case update : hasPermission = hasUpdatePermission(entity, targetJClass, rules);
					 	break;
					case delete : hasPermission = hasDeletePermission(entity, targetJClass, rules);
						break;
				}
				
				if(!hasPermission) {
					return false;
				}
			} catch(JSecurityException e) {
				logger.error("\t##NO PERMISSION : You cannot access to target object."+entity
						+", targetClassId="+classId+", - "+e.getMessage(), e);
				return false;
			}
		}
		return true;
	}

	public boolean hasPermission(JUserEntity entity, Long classId, String permission) {
		if(logger.isDebugEnabled()) {
			StringBuffer log = new StringBuffer();
			log.append("################# has Permission #################").append("\n");
			log.append("\t").append("JUserEntity=").append(entity).append("\n");
			log.append("\t").append("classId=").append(classId).append("\n");
			log.append("\t").append("permission=").append(permission).append("\n");
			logger.debug(log.toString());
		}
		
		List<JClassPermission> actions = new ArrayList<JClassPermission>();
		try {
			actions.add(JClassPermission.valueOf(permission));
		} catch(Exception e) {
			logger.error("Invalid permission value. - "+permission, e);
			return false;
		}
		
		List<String> roleList = entity.getRoleList();
		Long signedDomainId = null;
		try {
			signedDomainId = security.getDomainId();
		} catch (JSecurityException e) {
			logger.error("Fail to get domainId from security context.",e);
			return false;
		}
		JClass targetJClass = null;
		try {
			targetJClass = this.jClassRepository.select(classId);
			if(targetJClass==null) {
				throw new RuntimeException("Cannot find JClass. classId="+classId+", permission="+permission+", classId="+classId);
			}
		} catch (JRepositoryException e) {
			logger.error("Fail to get JClass from metadata. - "+classId,e);
			return false;
		}
		
		// check permission : system or owner domain
		if(!targetJClass.getDomainId().equals(new Long(JDomainDef.SYSTEM_ID)) && 
				!signedDomainId.equals(targetJClass.getDomainId())) {
			logger.error("\t##NO PERMISSION : You cannot access to other domains."+entity
							+", signedDomainId="+signedDomainId+" targetClassId="+classId);
			return false;
		}
		
		List<JDataAccessRule> rules = null;
		try {
			rules = this.jDataAccessRuleRepository.select(targetJClass, roleList);
		} catch(JRepositoryException e) {
			logger.error("fail to get JDataAccessRules. classId="+classId+", "+entity, e);
			return false;
		}
		// if user don't have any rule, user can read and write it. and user update and delete his.
		
		JAccessRule jAccessRule = null;
		try {
			jAccessRule = JAccessRule.valueOf(targetJClass.getAccessRule());
		} catch(Exception e) {
			jAccessRule = JAccessRule.PUBLIC;
		}
		
		if(JAccessRule.PUBLIC.equals(jAccessRule) && (rules==null || rules.size()==0)) {
			return true;
		}
		
		if(JAccessRule.PROTECTED.equals(jAccessRule) && (rules==null || rules.size()==0)) {
			return false;
		}
		
		if(JAccessRule.PRIVATE.equals(jAccessRule)) {
			return true;
		}
		
		for(JClassPermission action : actions) {
			try {
				boolean hasPermission = false;
				switch(action) {
					case read 	: hasPermission = hasReadPermission(entity, targetJClass, rules);
						break;
					case create : hasPermission = hasCreatePermission(entity, targetJClass, rules);
						break;
					case update : hasPermission = hasUpdatePermission(entity, targetJClass, rules);
					 	break;
					case delete : hasPermission = hasDeletePermission(entity, targetJClass, rules);
						break;
				}
				
				if(!hasPermission) {
					return false;
				}
			} catch(JSecurityException e) {
				logger.error("\t##NO PERMISSION : You cannot access to target object."+entity
						+", targetClassId="+classId+", - "+e.getMessage(), e);
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
		if(logger.isDebugEnabled()) {
			StringBuffer log = new StringBuffer();
			log.append("################# has Permission #################").append("\n");
			log.append("\t").append("Authentication=").append(authentication).append("\n");
			log.append("\t").append("targetId=").append(targetId).append("\n");
			log.append("\t").append("targetType=").append(targetType).append("\n");
			log.append("\t").append("permission=").append(permission).append("\n");
			logger.debug(log.toString());
		}
		return true;
	}
	
	private boolean hasReadPermission(JUserEntity entity, JClass jClass, List<JDataAccessRule> rules) 
	throws JSecurityException {
		boolean hasPermission = false;
		for(JDataAccessRule rule : rules) {
			if(rule.isCanRead()) {
				hasPermission = true;
			}
		}
		
		if(!hasPermission) {
			logger.error("\t##NO PERMISSION : You don't have permission to read target entity. "+entity
					+", targetClassId="+jClass.getObjectId());
		}
		return hasPermission;
	}
	
	private boolean hasCreatePermission(JUserEntity entity, JClass jClass, List<JDataAccessRule> rules) 
			throws JSecurityException {
		boolean hasPermission = false;
		for(JDataAccessRule rule : rules) {
			if(rule.isCanCreate()) {
				hasPermission = true;
			}
		}
		
		if(!hasPermission) {
			logger.error("\t##NO PERMISSION : You don't have permission to create target entity. "+entity
					+", targetClassId="+jClass.getObjectId());
		}
		return hasPermission;
	}

	private boolean hasUpdatePermission(JUserEntity entity, JClass jClass, List<JDataAccessRule> rules) 
			throws JSecurityException {
		boolean hasPermission = false;
		for(JDataAccessRule rule : rules) {
			if(rule.isCanUpdate()) {
				hasPermission = true;
			}
		}
		
		if(!hasPermission) {
			logger.error("\t##NO PERMISSION : You don't have permission to update target entity. "+entity
					+", targetClassId="+jClass.getObjectId());
		}
		return hasPermission;
	}
	
	private boolean hasDeletePermission(JUserEntity entity, JClass jClass, List<JDataAccessRule> rules) 
			throws JSecurityException {
		boolean hasPermission = false;
		for(JDataAccessRule rule : rules) {
			if(rule.isCanDelete()) {
				hasPermission = true;
			}
		}
		
		if(!hasPermission) {
			logger.error("\t##NO PERMISSION : You don't have permission to delete target entity. "+entity
					+", targetClassId="+jClass.getObjectId());
		}
		return hasPermission;
	}
}
