package jlook.framework.infrastructure.security;

import org.springframework.security.core.authority.GrantedAuthorityImpl;

public class JGrantedAuthority extends GrantedAuthorityImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long roleId;
	
	public JGrantedAuthority(String role, Long roleId) {
		super(role);
		this.roleId = roleId;
	}

	public Long getRoleId() {
		return this.roleId;
	}
}
