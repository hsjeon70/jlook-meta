package jlook.framework.infrastructure.security;

public enum JClassPermission {
	read,
	create,
	update,
	delete;
	
	public static final String READ = "read";
	public static final String CREATE = "create";
	public static final String UPDATE = "update";
	public static final String DELETE = "delete";
	public static final String EXPORT = "export";
	public static final String IMPORT = "import";
}
