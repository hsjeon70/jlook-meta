package jlook.framework.infrastructure.context.impl;

import java.util.HashMap;
import java.util.Map;


public class JThreadLocal extends ThreadLocal<Map<String,Object>> {
	public Map<String, Object> initialValue() {
        return new HashMap<String,Object>();
    }
    
    public void clear() {
        ((Map<String,Object>) super.get()).clear();
    }
}
