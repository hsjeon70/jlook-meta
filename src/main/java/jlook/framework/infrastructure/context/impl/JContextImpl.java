package jlook.framework.infrastructure.context.impl;

import java.util.Map;

import jlook.framework.JHeader;
import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextException;
import jlook.framework.infrastructure.security.JBaseUser;
import jlook.framework.infrastructure.security.JUserEntity;


public class JContextImpl implements JContext {
	
	private static JThreadLocal threadlocal = new JThreadLocal();
	
	private Map<String,Object> getLocalMap() {
		Map<String,Object> map = threadlocal.get();
		if(map==null) {
			throw new RuntimeException("Cannot find threadlocal variable. - "+threadlocal);
		}
		return map;
	}
	
	@Override
	public void putUserEntity(JBaseUser entity) throws JContextException {
		getLocalMap().put(JBaseUser.ID, entity);
	}
	
	@Override
	public JBaseUser getUserEntity() throws JContextException {
		return (JBaseUser)getLocalMap().get(JUserEntity.ID);
	}

	@Override
	public String removeAll() {
		threadlocal.clear();
		return "";
	}

	@Override
	public Long getDomainId() {
		return (Long)getLocalMap().get(JHeader.DID.getKey());
	}

	@Override
	public void putDomainId(Long domainId) {
		getLocalMap().put(JHeader.DID.getKey(), domainId);
	}

	@Override
	public void putClientType(String clientType) {
		getLocalMap().put(JHeader.CLIENT.getKey(), clientType);
	}

	@Override
	public String getClientType() {
		return (String)getLocalMap().get(JHeader.CLIENT.getKey());
	}
}
