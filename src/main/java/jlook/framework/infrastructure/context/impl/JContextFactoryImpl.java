package jlook.framework.infrastructure.context.impl;

import jlook.framework.infrastructure.context.JContext;
import jlook.framework.infrastructure.context.JContextFactory;
import jlook.framework.infrastructure.util.Log;

import org.springframework.stereotype.Component;


@Component(JContextFactory.ID)
public class JContextFactoryImpl extends JContextFactory {
	@SuppressWarnings("unused")
	private static Log logger = Log.getLog(JContextFactoryImpl.class);
	
	public JContextFactoryImpl() {
		JContextFactory.instance = this;
	}
	
	@Override
	public JContext getJContext() {
		JContextImpl jContext = new JContextImpl();
		return jContext;
	}
}
