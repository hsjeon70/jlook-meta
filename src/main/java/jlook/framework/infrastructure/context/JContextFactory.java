package jlook.framework.infrastructure.context;

public abstract class JContextFactory {
	public static final String ID = "jContextFactory";
	protected static JContextFactory instance;
	
	public abstract JContext getJContext();
	
	public static JContextFactory newInstance() {
		return instance;
	}
}
