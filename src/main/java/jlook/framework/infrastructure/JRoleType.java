package jlook.framework.infrastructure;

public enum JRoleType {
	USER("User"),
	ADMINISTRATOR("Administrator"),
	ADMIN("Admin");
	
	JRoleType(String name) {
		this.name= name;
	}
	
	private String name;
	
	public String getName() {
		return this.name;
	}
}
