package jlook.framework;

public interface GlobalKeys {
	public static final String SIGNIN_URI = "/j_spring_security_check";
	public static final String SIGNOUT_URI = "/j_spring_security_logout";
	
	public static final String SIGNIN_WEBPAGE = "/service/security/signInFrm?signIn_error=true";
	public static final String SIGNIN_MOBILEPAGE = "/service/security/failure";
	
	// exception
	public static final String ERROR_TEXT = "errorText";
	public static final String ERROR_CAUSE = "errorCause";
	
	public static final String ERROR_INFO = "errorInfo";
	
	// --------------- HttpSession KEY
	// On signing in, temporary setting
	public static final String DID_KEY 		= "did";
	// Active domain key
	public static final String DOMAIN_KEY 	= "domain";
	// User domain map
	public static final String DOMAINS_KEY 	= "domains";
	public static final String DOMAIN_PREFERENCES_KEY  ="domainPreferences";
	
	public static final String CONFIG_PATH 	= "config";
	public static final String CLASSES_PATH = "classes";
	
	public static final String SERVICE_EXCEPTION = "jlook.framework.service.JServiceException";
}
