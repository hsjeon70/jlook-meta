<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/>
  	<xsl:param name="image_dir" select="'images'"/>
  	<xsl:template match="/OpenAPI-Service">
<article> 
    <header>
        <h1>
            <a href="#" title="Link to this post" rel="bookmark">Open API Specification</a>
        </h1>
	</header>
	<section>
		<table>
			<tr>
				<td class="title">Service Name </td>
				<td><xsl:value-of select="name"/></td>
			</tr>
			<tr>
				<td class="title">Service Category </td>
				<td><xsl:value-of select="category"/></td>
			</tr>
			<tr>
				<td class="title">Service Descripton </td>
				<td><xsl:value-of select="description"/></td>
			</tr>
		</table>
		<hr/>
		<table>
		<xsl:for-each select="OpenAPI">
			<tr>
				<td class="title">Name </td>
				<td colspan="3"><xsl:value-of select="name"/></td>
			</tr>
			<tr>
				<td class="title">Request URI </td>
				<td><xsl:value-of select="uri"/></td>
				<td class="title">HTTP Method </td>
				<td><xsl:value-of select="method"/></td>
			</tr>
			<tr>
				<td class="title">HTTP Response </td>
				<td colspan="3"><xsl:value-of select="response"/></td>
			</tr>
			<tr>
				<td class="title">Exception </td>
				<td colspan="3"><xsl:value-of select="exception"/></td>
			</tr>
			<tr>
				<td class="title">HTTP Header </td>
				<td colspan="3">
					<table>
					<xsl:for-each select="header">
						<tr>
							<td class="subtitle">Name </td>
							<td colspan="3" class="subarea"><xsl:value-of select="name"/></td>
						</tr>
						<tr>
							<td class="subtitle">Type</td>
							<td class="subarea"><xsl:value-of select="type"/></td>
							<td class="subtitle">Required</td>
							<td class="subarea"><xsl:value-of select="required"/></td>
						</tr>
						<tr>
							<td class="subtitle">Description</td>
							<td colspan="3"  class="subarea"><xsl:value-of select="description"/></td>
						</tr>
					</xsl:for-each>
					</table>
				</td>
			</tr>
			<tr>
				<td class="title">HTTP Parameter </td>
				<td colspan="3">
					<table>
					<xsl:for-each select="parameter">
						<tr>
							<td class="subtitle">Name </td>
							<td colspan="3" class="subarea"><xsl:value-of select="name"/></td>
						</tr>
						<tr>
							<td class="subtitle">Type</td>
							<td class="subarea"><xsl:value-of select="type"/></td>
							<td class="subtitle">Required</td>
							<td class="subarea"><xsl:value-of select="required"/></td>
						</tr>
						<tr>
							<td class="subtitle">Description</td>
							<td colspan="3"  class="subarea"><xsl:value-of select="description"/></td>
						</tr>
					</xsl:for-each>
					</table>
				</td>
			</tr>
		</xsl:for-each>
		</table>
	</section>
</article>
	</xsl:template>
</xsl:stylesheet>